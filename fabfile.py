#!/usr/bin/python
# -*- coding: utf-8 -*-
from fabric.api import cd, env, execute
from fabric.utils import abort, puts
from fabric.contrib.project import rsync_project
from fabric.operations import local, run
from StringIO import StringIO

env.user            = 'mybeestudent'
env.hosts           = ['127.0.0.1']
env.port            = 22
env.path            = '/home/mybeestudent/apps/student-app-webapp/'  # Don't forgot to add / at end
env.localpath       = './target/universal/stage/'
env.colorize_errors = True


def sync():
    '''
    Synchronize project with server
    '''
    currentBranch = local('git symbolic-ref --short -q HEAD', capture=True)
    if currentBranch == 'master':
        local('./activator stage')
        rsync_project(local_dir  = env.localpath,
                      remote_dir = env.path,
                      delete     = True,
                      exclude    = ['*.pyc', '*~', 'fabfile.py', '.gitignore',
                                    '.DS_Store', 'Thumbs.db', '.git',
                                    'conf/evolutions', 'conf/*.conf'])
    else:
        abort("You cannot synchronize production servers from non MASTER branch!")


def restart():
    '''
    Restart the MyBee Student services
    '''
    cd(env.path)
    run('supervisorctl restart student-app-webapp')


def status():
    '''
    Get the status of the MyBee Student services
    '''
    outputBuffer = StringIO()
    run('supervisorctl status', stdout=outputBuffer)
    outputBuffer.seek(0)
    for line in outputBuffer.readlines():
        line = line.split("]")[1].strip()
        if "student" in line:
            puts(line)


def deploy():
    '''
    Sync project with server and restart the MyBee Student services
    '''
    execute(sync)
    execute(restart)
    execute(status)
