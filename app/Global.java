/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import modules.GrayLogModule;
import org.graylog2.gelfclient.GelfMessage;
import org.graylog2.gelfclient.GelfMessageLevel;
import play.Application;
import play.GlobalSettings;
import play.Play;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;
import views.html.systemview.error400;
import views.html.systemview.error404;
import views.html.systemview.error500;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * This class allow overriding of some hooks on Play 2.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @see GlobalSettings
 * @since 15.10
 */
public class Global extends GlobalSettings {

    @Override
    public void onStart(Application app) {
        super.onStart(app);
        ObjectMapper mapper = new ObjectMapper()
                // enable features and customize the object mapper here ..
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        Json.setObjectMapper(mapper);
    }

    /**
     * Called when an action has been found, but the request parsing
     * has failed. The default behavior is to render the framework's
     * default 400 page. This is achieved by returning <code>null</code>,
     * so that the Scala engine handles <code>onBadRequest</code>. By
     * overriding this method one can provide an alternative 400 page.
     *
     * @param request The HTTP request
     * @param error   The HTTP error
     * @return A promise result
     * @since 15.10
     */
    @Override
    public Promise<Result> onBadRequest(RequestHeader request, String error) {
        if (error.contains("404")) {
            return Promise.<Result>pure(Results.badRequest(error404.render()));
        }
        return Promise.<Result>pure(Results.badRequest(error400.render()));
    }

    /**
     * Called when no action was found to serve a request. The default
     * behavior is to render the framework's default 404 page. This is
     * achieved by returning <code>null</code>, so that the Scala engine
     * handles <code>onHandlerNotFound</code>. By overriding this method
     * one can provide an alternative 404 page.
     *
     * @param request The HTTP request
     * @return A promise result
     * @since 15.10
     */
    @Override
    public Promise<Result> onHandlerNotFound(RequestHeader request) {
        if (play.Play.isDev()) {
            return super.onHandlerNotFound(request);
        }
        return Promise.<Result>pure(Results.notFound(error404.render()));
    }

    /**
     * Called when an exception occurred. The default is to send the
     * framework's default error page. This is achieved by returning
     * <code>null</code>, so that the Scala engine handles the exception
     * and shows an error page. By overriding this method one can provide
     * an alternative error page.
     *
     * @param request The current request
     * @param t       Is any throwable
     * @return A promise result
     */
    @Override
    public Promise<Result> onError(RequestHeader request, Throwable t) {
        if (play.Play.isDev()) {
            return super.onError(request, t);
        } else {
            final GrayLogModule glm = Play.application().injector().instanceOf(GrayLogModule.class);
            final GelfMessage message = glm.createMessage("Error", request);
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            message.setFullMessage(sw.toString());
            message.setLevel(GelfMessageLevel.ERROR);
            pw.close();
            try {
                sw.close();
            } catch (IOException ignore) {
            }
            glm.sendAsync(message);
        }
        return Promise.<Result>pure(Results.internalServerError(error500.render()));
    }
}
