package forms.common;

/**
 * @author Andrey Sokolov
 */
public class PaginationData {
    public final int page;
    public final int pageSize;
    public final long total;
    public final boolean show;

    public PaginationData(int page, int pageSize, long total, boolean show) {
        this.page = page;
        this.pageSize = pageSize;
        this.total = total;
        this.show = show;
    }

    public interface RouteTransformer {
        String transform(int page);
    }
}
