package forms.account;

import models.AccountModel;
import models.ActorProfileModel;
import org.apache.commons.lang3.text.WordUtils;
import play.data.validation.Constraints;

/**
 * AccountBaseInformationForm.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
public class AccountProfileForm {

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(35)
    public String firstname;

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(35)
    public String lastname;

    public String fbToken;

    /**
     * Default constructor
     *
     * @since 15.10
     */
    public AccountProfileForm() {
    }

    /**
     * Constructor to fill the form from a ActorProfileModel
     *
     * @param account The AccountModel
     * @see ActorProfileModel
     * @since 15.10
     */
    public AccountProfileForm(AccountModel account) {
        this.firstname = account.getFirstName();
        this.lastname = account.getLastName();
    }

    /**
     * Set the firstname, apply trim() and capitalize from the input data.
     *
     * @param firstname Firstname taken the request data
     * @since 15.10
     */
    public void setFirstname(final String firstname) {
        this.firstname = WordUtils.capitalizeFully(firstname.trim());
    }

    /**
     * Set the lastname, apply trim() and capitalize from the input data.
     *
     * @param lastname Lastname taken the request data
     * @since 15.10
     */
    public void setLastname(final String lastname) {
        this.lastname = WordUtils.capitalizeFully(lastname.trim());
    }

}
