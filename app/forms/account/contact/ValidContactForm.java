package forms.account.contact;

import play.data.validation.Constraints;

import java.util.Locale;

/**
 * ValidContactForm
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class ValidContactForm {
    @Constraints.Required
    @Constraints.MinLength(7)
    @Constraints.MaxLength(125)
    public String contact;

    @Constraints.Required
    @Constraints.MinLength(5)
    @Constraints.MaxLength(5)
    public String token;

    /**
     * Set the contact and apply trim() and lower to the input data.
     *
     * @param contact Contact taken from request data
     * @since 15.10
     */
    public void setContact(final String contact) {
        this.contact = contact.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Set the token and apply trim() and lower to the input data.
     *
     * @param token Token taken from request data
     * @since 15.10
     */
    public void setToken(final String token){
        this.token = token.trim().toUpperCase(Locale.ENGLISH);
    }
}
