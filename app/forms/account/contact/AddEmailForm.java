package forms.account.contact;

import models.AccountEmailModel;
import models.DomainBlacklistModel;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * AddEmailForm
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class AddEmailForm {

    @Constraints.Required
    @Constraints.Email
    @Constraints.MinLength(7)
    @Constraints.MaxLength(125)
    public String email;

    /**
     * Set the email and apply trim() to the input data.
     *
     * @param email Email taken from request data
     * @since 15.10
     */
    public void setEmail(final String email) {
        this.email = email.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate() {
        final List<ValidationError> errors = new ArrayList<ValidationError>();
        final String[] email_part = this.email.split("@");
        final String domain = email_part[email_part.length - 1];

        if (DomainBlacklistModel.find.where().raw("? LIKE CONCAT(domain, '%')", domain).findRowCount() > 0) {
            errors.add(new ValidationError("email", Messages.get("ERROR.FORM.EMAIL_BLACKLISTED")));
        }

        if (AccountEmailModel.find.where().like("email", this.email).findRowCount() > 0) {
            errors.add(new ValidationError("email", Messages.get("ERROR.FORM.EMAIL_ALREADY_EXIST")));
        }
        return errors.size() > 0 ? errors : null;
    }
}
