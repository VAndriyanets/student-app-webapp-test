package forms.account.contact;

import play.data.validation.Constraints;

/**
 * ManagePhoneForm
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class ManagePhoneForm {

    @Constraints.Required
    @Constraints.MinLength(7)
    @Constraints.MaxLength(50)
    public String phone;

    /**
     * Set the phone and apply trim() to the input data.
     *
     * @param phone Phone taken from request data
     * @since 15.10
     */
    public void setPhone(final String phone) {
        this.phone = phone.trim();
    }
}
