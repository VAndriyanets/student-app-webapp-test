package forms.account.contact;

import play.data.validation.Constraints;

import java.util.Locale;

/**
 * ManageEmailForm
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class ManageEmailForm {

    @Constraints.Required
    @Constraints.Email
    @Constraints.MinLength(7)
    @Constraints.MaxLength(125)
    public String email;

    /**
     * Set the email and apply trim() to the input data.
     *
     * @param email Email taken from request data
     * @since 15.10
     */
    public void setEmail(final String email) {
        this.email = email.trim().toLowerCase(Locale.ENGLISH);
    }
}
