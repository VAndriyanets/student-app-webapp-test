package forms.account.contact;

import models.AccountPhoneModel;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import toolbox.PhoneUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * AddPhoneForm
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class AddPhoneForm {

    @Constraints.Required
    @Constraints.MinLength(7)
    @Constraints.MaxLength(50)
    public String phone;

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(2)
    public String country;

    /**
     * Set the phone and apply trim() to the input data.
     *
     * @param phone Phone taken from request data
     * @since 15.10
     */
    public void setPhone(final String phone) {
        this.phone = phone.trim();
    }

    /**
     * Set the country and apply trim() and transform to UpperCase the input data
     *
     * @param country Country taken from request data
     * @since 15.10
     */
    public void setCountry(final String country){
        this.country = country.trim().toUpperCase(Locale.ENGLISH);
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate() {
        final List<ValidationError> errors = new ArrayList<ValidationError>();

        if(!PhoneUtils.isValidPhoneNumber(this.phone, this.country)){
            errors.add(new ValidationError("phone", Messages.get("ERROR.FORM.PHONE_INVALID")));
        }

        if (AccountPhoneModel.find.where().like("phone", this.phone).findRowCount() > 0) {
            errors.add(new ValidationError("phone", Messages.get("ERROR.FORM.PHONE_ALREADY_EXIST")));
        }
        return errors.size() > 0 ? errors : null;
    }
}
