package forms.account;

import play.data.validation.Constraints;

/**
 * AccountManageAddressForm.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class AccountManageAddressForm {
    @Constraints.Required
    @Constraints.Pattern(value = "[\\w]{8}-[\\w]{4}-[\\w]{4}-[\\w]{4}-[\\w]{12}", message = "ERROR.FORM.NOT.UUID")
    public String addressUid;

    /**
     * Default constructor.
     *
     * @since 15.10
     */
    public AccountManageAddressForm() {
    }

    /**
     * Set the addressUid and apply trim() to the input data.
     *
     * @param addressUid address uid taken from request data
     * @since 15.10
     */
    public void setAddressUid(final String addressUid){
        this.addressUid = addressUid.trim();
    }
}
