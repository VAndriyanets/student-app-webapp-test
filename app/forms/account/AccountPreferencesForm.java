/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.account;

import models.AccountModel;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import toolbox.form.validator.EnumValueString;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static play.Play.application;

/**
 * AccountPreferencesForm.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
public class AccountPreferencesForm {
    @Constraints.Required
    @EnumValueString(enumclass = TimezoneChoices.class)
    public String timezone;

    @Constraints.Required
    public String lang;

    /**
     * Default constructor.
     *
     * @since 15.10
     */
    public AccountPreferencesForm() {
    }

    /**
     * Constructor to fill the form from an AccountModel.
     *
     * @param account The AccountModel.
     * @see AccountModel
     * @since 15.10
     */
    public AccountPreferencesForm(AccountModel account) {
        TimezoneChoices tz = TimezoneChoices.getEnum(account.getTimezone());
        if (tz != null) {
            this.timezone = tz.toString();
        } else {
            this.timezone = "";
        }
        this.lang = account.getLang();
    }

    /**
     * Set the timezone and apply trim() to the input data.
     *
     * @param timezone timezone taken the request data
     * @since 15.10
     */
    public void setTimezone(final String timezone) {
        this.timezone = timezone.trim();
    }

    /**
     * Set the lang, apply trim() and toLowerCase() to the input data.
     *
     * @param lang Lang taken the request data
     * @since 15.10
     */
    public void setLang(final String lang) {
        this.lang = lang.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Get the timezone full name from the enum TimezoneChoices.
     *
     * @return The timezone full name.
     * @see forms.account.AccountPreferencesForm.TimezoneChoices
     * @since 15.10
     */
    public String getTimezoneName() {
        if (this.timezone == null) {
            return null;
        }
        return TimezoneChoices.valueOf(this.timezone).getTzName();
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate() {
        final List<ValidationError> errors = new ArrayList<ValidationError>();

        if (!application().configuration().getStringList("play.i18n.langs").contains(this.lang)) {
            errors.add(new ValidationError("lang", Messages.get("error.invalid")));
        }

        return errors.size() > 0 ? errors : null;
    }

    /**
     * TimezoneChoices.
     *
     * @author Pierre Adam
     * @version 15.10
     * @since 15.10
     */
    public enum TimezoneChoices {
        AMSTERDAM("Europe/Amsterdam"),
        ANDORRA("Europe/Andorra"),
        ATHENS("Europe/Athens"),
        BELFAST("Europe/Belfast"),
        BELGRADE("Europe/Belgrade"),
        BERLIN("Europe/Berlin"),
        BRATISLAVA("Europe/Bratislava"),
        BRUSSELS("Europe/Brussels"),
        BUCHAREST("Europe/Bucharest"),
        BUDAPEST("Europe/Budapest"),
        BUSINGEN("Europe/Busingen"),
        CHISINAU("Europe/Chisinau"),
        COPENHAGEN("Europe/Copenhagen"),
        DUBLIN("Europe/Dublin"),
        GIBRALTAR("Europe/Gibraltar"),
        GUERNSEY("Europe/Guernsey"),
        HELSINKI("Europe/Helsinki"),
        ISLE_OF_MAN("Europe/Isle_of_Man"),
        ISTANBUL("Europe/Istanbul"),
        JERSEY("Europe/Jersey"),
        KALININGRAD("Europe/Kaliningrad"),
        KIEV("Europe/Kiev"),
        LISBON("Europe/Lisbon"),
        LJUBLJANA("Europe/Ljubljana"),
        LONDON("Europe/London"),
        LUXEMBOURG("Europe/Luxembourg"),
        MADRID("Europe/Madrid"),
        MALTA("Europe/Malta"),
        MARIEHAMN("Europe/Mariehamn"),
        MINSK("Europe/Minsk"),
        MONACO("Europe/Monaco"),
        MOSCOW("Europe/Moscow"),
        NICOSIA("Europe/Nicosia"),
        OSLO("Europe/Oslo"),
        PARIS("Europe/Paris"),
        PODGORICA("Europe/Podgorica"),
        PRAGUE("Europe/Prague"),
        RIGA("Europe/Riga"),
        ROME("Europe/Rome"),
        SAMARA("Europe/Samara"),
        SAN_MARINO("Europe/San_Marino"),
        SARAJEVO("Europe/Sarajevo"),
        SIMFEROPOL("Europe/Simferopol"),
        SKOPJE("Europe/Skopje"),
        SOFIA("Europe/Sofia"),
        STOCKHOLM("Europe/Stockholm"),
        TALLINN("Europe/Tallinn"),
        TIRANE("Europe/Tirane"),
        TIRASPOL("Europe/Tiraspol"),
        UZHGOROD("Europe/Uzhgorod"),
        VADUZ("Europe/Vaduz"),
        VATICAN("Europe/Vatican"),
        VIENNA("Europe/Vienna"),
        VILNIUS("Europe/Vilnius"),
        VOLGOGRAD("Europe/Volgograd"),
        WARSAW("Europe/Warsaw"),
        ZAGREB("Europe/Zagreb"),
        ZAPOROZHYE("Europe/Zaporozhye"),
        ZURICH("Europe/Zurich");

        /**
         * Contains the timezone name.
         */
        private String tzName;

        /**
         * Constructor.
         *
         * @param tz_name The full name of the timezone.
         * @since 15.10
         */
        TimezoneChoices(final String tz_name) {
            this.tzName = tz_name;
        }

        /**
         * Get the enumeration object from it's human reading name.
         *
         * @param tz_name The name of the timezone
         * @return The TimezoneChoices if found. Otherwise null.
         * @since 15.10
         */
        public static TimezoneChoices getEnum(String tz_name) {
            for (TimezoneChoices tz : TimezoneChoices.values()) {
                if (tz_name.equals(tz.getTzName())) {
                    return tz;
                }
            }
            return null;
        }

        /**
         * Get the timezone name.
         *
         * @return The full name of the timezone
         * @since 15.10
         */
        public String getTzName() {
            return this.tzName;
        }
    }
}
