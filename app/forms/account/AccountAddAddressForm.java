package forms.account;

import play.data.validation.Constraints;
import toolbox.enumeration.CountryCode;
import toolbox.form.validator.EnumValueString;

/**
 * AccountAddressForm.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class AccountAddAddressForm {
    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(70)
    public String destName;

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(128)
    public String street1;

    @Constraints.MaxLength(128)
    public String street2;

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(16)
    public String zipcode;

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(64)
    public String city;

    @Constraints.Required
    @EnumValueString(enumclass = CountryCode.class)
    public String country;

    /**
     * Default constructor.
     *
     * @since 15.10
     */
    public AccountAddAddressForm(){
    }

    /**
     * Set the destination name and apply trim() to the input data.
     *
     * @param destName destination name taken from request data
     * @since 15.10
     */
    public void setDestName(final String destName){
        this.destName = destName.trim();
    }

    /**
     * Set the street1 and apply trim() to the input data.
     *
     * @param street1 street1 taken from request data
     * @since 15.10
     */
    public void setStreet1(final String street1){
        this.street1 = street1.trim();
    }

    /**
     * Set the street2 and apply trim() (if not empty) to the input data.
     *
     * @param street2 street2 taken from request data
     * @since 15.10
     */
    public void setStreet2(final String street2){
        this.street2 = street2 != null ? street2.trim() : street2;
    }

    /**
     * Set the zipcode and apply trim() to the input data.
     *
     * @param zipcode zipcode taken from request data
     * @since 15.10
     */
    public void setZipcode(final String zipcode){
        this.zipcode = zipcode.trim();
    }

    /**
     * Set the city and apply trim() to the input data.
     *
     * @param city city taken from request data
     * @since 15.10
     */
    public void setCity(final String city){
        this.city = city.trim();
    }

    /**
     * Set the country and apply trim() to the input data.
     *
     * @param country country taken from request data
     * @since 15.10
     */
    public void setCountry(final String country){
        this.country = country.trim();
    }
}
