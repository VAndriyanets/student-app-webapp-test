/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.system;

import com.fasterxml.jackson.databind.JsonNode;
import models.AccountModel;
import play.Play;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.libs.ws.WS;
import play.libs.ws.WSRequest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * AuthenticationForm.
 *
 * @author Thibault Meyer
 * @author Jean-Pierre Boudic
 * @version 15.10
 * @since 15.10
 */
public class AuthenticationForm {

    @Constraints.Required
    @Constraints.Email
    @Constraints.MinLength(7)
    @Constraints.MaxLength(125)
    public String email;

    @Constraints.Required
    @Constraints.MinLength(6)
    @Constraints.MaxLength(50)
    public String password;

    @Constraints.MinLength(50)
    public String captcha;

    @Constraints.MaxLength(255)
    public String redirect;

    /**
     * This variable will contain the account instance when
     * the form will be valid.
     */
    private AccountModel account;

    /**
     * Set the email and apply trim() to the input data.
     *
     * @param email Email taken the request data
     * @since 15.10
     */
    public void setEmail(final String email) {
        this.email = email.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Set the redirect URL, check internal URL before put to the input data.
     *
     * @param redirect URL to redirect post Authentication
     * @since 15.10
     */
    public void setRedirect(final String redirect) {
        this.redirect = "/";
        if(redirect.matches(("^/[a-z0-9.-/]+"))) {
            this.redirect = redirect.replace("//", "/");
        }
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate(final Boolean forceCaptcha) {
        final List<ValidationError> errors = new ArrayList<ValidationError>();
        this.account = AccountModel.find.where().like("emails.email", this.email).eq("is_validated", true).eq("is_primary", true).findUnique();
        if (this.account == null || !this.account.checkPassword(this.password)) {
            errors.add(new ValidationError("password", Messages.get("AUTHENTICATION.FORM.ERROR.CREDENTIAL")));
            errors.add(new ValidationError("email", ""));
        }
        if (this.captcha != null || forceCaptcha) {
            checkRecaptcha(errors);
        }
        return errors.size() > 0 ? errors : null;
    }

    /**
     * ReCAPTCHA validation.
     * Call Google API
     *
     * @param errors Error generated from validate()
     * @since 15.10
     */
    private void checkRecaptcha(List<ValidationError> errors) {
        WSRequest request = WS.url(Play.application().configuration().getString("google.recaptcha.url_verify"))
                .setQueryParameter("secret", play.Play.application().configuration().getString("google.recaptcha.secret"))
                .setQueryParameter("response", this.captcha);

        JsonNode json = request.post("").get(5000L).asJson();

        Boolean success = json.get("success").booleanValue();
        if (!success) {
            if (json.has("error-codes")) {
                Iterator<JsonNode> errs = json.get("error-codes").elements();
                while (errs.hasNext()) {
                    String errName = "RECAPTCHA.ERROR." + errs.next().asText().toUpperCase().replace('-', '_');
                    errors.add(new ValidationError("captcha", Messages.get(errName)));
                }
            }
        }
    }

    /**
     * Get the resolved account instance. This method will return
     * {@code null} if the form is not valid.
     *
     * @return An {@code AccountModel} instance
     * @since 15.10
     */
    public AccountModel getAccount() {
        return this.account;
    }
}
