/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.system;

import play.data.validation.Constraints;

import java.util.Locale;

/**
 * RealmSelectorEnrollTokenForm.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class RealmSelectorEnrollTokenForm {
    @Constraints.Required
    @Constraints.MinLength(8)
    @Constraints.MaxLength(8)
    public String enrollToken;

    /**
     * Set realm token and apply trim and upper to the input data.
     *
     * @param enrollToken enroll token taken from request data
     * @since 15.10
     */
    public void setEnrollToken(String enrollToken) {
        this.enrollToken = enrollToken.trim().toUpperCase(Locale.ENGLISH);
    }
}
