/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.system;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.List;

/**
 * ResetPasswordForm.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
public class ResetPasswordForm {

    @Constraints.Required
    @Constraints.MinLength(6)
    @Constraints.MaxLength(125)
    @Constraints.Pattern(value = "^(?=.*[a-zA-Z])(?=.*\\d)(?=.*(_|[^\\w])).+$", message = "ERROR.FORM.PASSWORD_WEAK")
    public String password;

    @Constraints.Required
    @Constraints.MinLength(6)
    @Constraints.MaxLength(125)
    public String password_confirm;

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate() {
        final List<ValidationError> errors = new ArrayList<ValidationError>();
        if (!this.password.equals(this.password_confirm)) {
            errors.add(new ValidationError("password_confirm", Messages.get("ERROR.FORM.PASSWORD_MISMATCH")));
        }
        return errors.size() > 0 ? errors : null;
    }
}
