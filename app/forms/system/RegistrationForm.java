/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.system;


import models.AccountEmailModel;
import models.DomainBlacklistModel;
import org.apache.commons.lang3.text.WordUtils;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * RegistrationForm.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
public class RegistrationForm {

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(35)
    public String firstname;

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(35)
    public String lastname;

    @Constraints.Required
    @Constraints.Email
    @Constraints.MinLength(7)
    @Constraints.MaxLength(125)
    public String email;

    @Constraints.Required
    @Constraints.Email
    @Constraints.MinLength(7)
    @Constraints.MaxLength(125)
    public String email_confirm;

    @Constraints.Required
    @Constraints.MinLength(6)
    @Constraints.MaxLength(125)
    @Constraints.Pattern(value = "^(?=.*[a-zA-Z])(?=.*\\d)(?=.*(_|[^\\w])).+$", message = "ERROR.FORM.PASSWORD_WEAK")
    public String password;

    @Constraints.Required
    @Constraints.MinLength(6)
    @Constraints.MaxLength(125)
    public String password_confirm;

    /**
     * Set the email and apply trim() to the input data.
     *
     * @param email Email taken the request data
     * @since 15.10
     */
    public void setEmail(final String email) {
        this.email = email.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Set the email confirmation and apply trim() to the input data.
     *
     * @param email Email taken the request data
     * @since 15.10
     */
    public void setEmail_confirm(final String email) {
        this.email_confirm = email.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Set the firstname, apply trim() and capitalize from the input data.
     *
     * @param firstname Firstname taken the request data
     * @since 15.10
     */
    public void setFirstname(final String firstname) {
        this.firstname = WordUtils.capitalizeFully(firstname.trim());
    }

    /**
     * Set the lastname, apply trim() and capitalize from the input data.
     *
     * @param lastname Lastname taken the request data
     * @since 15.10
     */
    public void setLastname(final String lastname) {
        this.lastname = WordUtils.capitalizeFully(lastname.trim());
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate() {
        final List<ValidationError> errors = new ArrayList<ValidationError>();
        final String[] email_part = this.email.split("@");
        final String domain = email_part[email_part.length - 1];

        if (this.password.compareTo(this.password_confirm) != 0) {
            errors.add(new ValidationError("password_confirm", Messages.get("ERROR.FORM.PASSWORD_MISMATCH")));
        }
        if (this.email.compareTo(this.email_confirm) != 0) {
            errors.add(new ValidationError("email_confirm", Messages.get("ERROR.FORM.EMAIL_MISMATCH")));
        }
        if (DomainBlacklistModel.find.where().raw("? LIKE CONCAT(domain, '%')", domain).findRowCount() > 0) {
            errors.add(new ValidationError("email", Messages.get("ERROR.FORM.EMAIL_BLACKLISTED")));
        }
        if (AccountEmailModel.find.where().like("email", this.email).findRowCount() > 0) {
            errors.add(new ValidationError("email", Messages.get("ERROR.FORM.EMAIL_ALREADY_EXIST")));
        }

        return errors.size() > 0 ? errors : null;
    }
}
