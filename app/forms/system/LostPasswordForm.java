/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.system;

import play.data.validation.Constraints;

import java.util.Locale;

/**
 * LostPasswordForm.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
public class LostPasswordForm {

    @Constraints.Required
    @Constraints.Email
    @Constraints.MinLength(7)
    @Constraints.MaxLength(125)
    public String email;

    /**
     * Set the email and apply trim() to the input data.
     *
     * @param email Email taken the request data
     * @since 15.10
     */
    public void setEmail(final String email) {
        this.email = email.trim().toLowerCase(Locale.ENGLISH);
    }
}
