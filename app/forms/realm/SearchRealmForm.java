/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm;

import models.SearchRequestType;
import play.data.validation.Constraints;
import toolbox.form.validator.EnumValueString;

import java.util.Locale;

/**
 * SearchRealmForm.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class SearchRealmForm {

    @Constraints.Required
    public String q;

    @EnumValueString(enumclass = SearchRequestType.class)
    public String type;

    @Constraints.Min(1)
    @Constraints.Max(200)
    public Integer page;

    /**
     * Trim and Set the query string. The query string is
     * arbitrary limited to 150 characters.
     *
     * @param q The query string to set
     */
    public void setQ(final String q) {
        this.q = q.trim();
        if (this.q.length() > 150) {
            this.q = this.q.substring(0, 150);
        }
    }

    /**
     * Convert the string value to the right enumeration. by default, this
     * method will return #ALL.
     *
     * @return The request type as enumeration
     * @see SearchRequestType
     */
    public SearchRequestType toTypeEnum() {
        if (this.type == null || this.type.isEmpty()) {
            return SearchRequestType.ALL;
        }
        return SearchRequestType.valueOf(this.type.toUpperCase(Locale.ENGLISH));
    }

}
