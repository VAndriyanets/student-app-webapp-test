/*
 * Copyright (C) 2014 - 2016 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.realm.group.GroupPageController;
import models.CommentModel;
import models.PostModel;
import models.RealmModel;
import models.S3FileModel;
import org.jetbrains.annotations.NotNull;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Http;
import toolbox.helper.ImageHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * GroupPostForm.
 *
 */
public class AddPostForm {

    /**
     * Size of picture.
     */
    private final static int[] PICTURE_SIZE = new int[]{512, 512};


    @Constraints.MaxLength(10000)
    public String postText;

    public Long picturesCount = 0L;

    public List<File> pictures = new ArrayList<>(20);

    public List<String> pictures_crop = new ArrayList<>(20);

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     */
    public List<ValidationError> validate() {
        final List<ValidationError> errors = new ArrayList<>();

        processPictures();

        if ((postText == null || postText.isEmpty()) && picturesCount < 1) {
            errors.add(new ValidationError("emptypost", Messages.get("REALM.GROUP.POSTS.FORM.EMPTY.POST.ERROR")));
        }

        return errors.size() > 0 ? errors : null;
    }

    /**
     * process File uploads from MultipartFormData with respect to client pictures removal
     *
     */
    private void processPictures() {
        final Http.MultipartFormData multipartFormData = Http.Context.current().request().body().asMultipartFormData();
        long notRemovedPicturesCount = 0;
        if (multipartFormData != null) {
            for (int i = 0; i < picturesCount; i++) {
                if (multipartFormData.getFile("picture" + i) != null) {
                    notRemovedPicturesCount++;
                    pictures.add( multipartFormData.getFile("picture" + i).getFile() );
                    pictures_crop.add(multipartFormData.asFormUrlEncoded().get("picture_crop" + i)[0]);
                }
            }
        }
        picturesCount = notRemovedPicturesCount;
    }

    @NotNull
    public PostModel getPostModel() {
        PostModel post = new PostModel();
        post.setText(postText);

        int pictureNum = 0;
        for (File picture : pictures) {
            final S3FileModel S3filePicture = ImageHelper.convertToS3File(
                    picture, pictures_crop.get(pictureNum), PICTURE_SIZE);

            post.addPicture(S3filePicture);
            pictureNum++;
        }
        return post;
    }

    @NotNull
    public CommentModel getCommentModel() {
        CommentModel comment = new CommentModel();
        comment.text = postText;

        int pictureNum = 0;
        for (File picture : pictures) {
            final String cropData = pictures_crop.get(pictureNum);
            final JsonNode selected = Json.parse(cropData);
            final S3FileModel S3filePicture = ImageHelper.convertToS3File(
                    picture, cropData, new int[]{selected.get("w").intValue(),selected.get("h").intValue()});

            comment.addPicture(S3filePicture);
            pictureNum++;
        }
        return comment;
    }
}
