/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package forms.realm.admin;

import models.RealmGroupModel;
import play.Play;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * ManageRealmGroupForm.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class ManageRealmGroupForm {

    @Constraints.Required
    @Constraints.MinLength(3)
    @Constraints.MaxLength(36)
    public String name;

    @Constraints.Required
    @Constraints.MinLength(3)
    @Constraints.MaxLength(36)
    @Constraints.Pattern(value = "^[a-z0-9._-]+$", message = "REALM.ADMIN.REALM.GROUP.FORM.SLUG.ERROR")
    public String slug;

    public File logo;

    public String logo_crop;

    public File banner;

    public String banner_crop;

    public List<GroupMember> members;

    public List<GroupMember> admins;

    public List<SubItemPermForm> adminPermissions;

    /**
     * Set the name and apply trim() to the input data.
     *
     * @param name Realm group name taken from request
     * @since 15.10
     */
    public void setName(String name) {
        this.name = name.trim();
    }

    /**
     * Set the slug and apply trim() to the input data
     *
     * @param slug Realm group slug (used in URL) taken from request
     * @since 15.10
     */
    public void setSlug(String slug) {
        this.slug = slug.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate(RealmGroupModel originalRealmGroup) {
        final List<ValidationError> errors = new ArrayList<>();
        final String forbiddenSlug = String.format(",%s,", Play.application().configuration().getString("realm-group.slug.forbidden"));
        if (forbiddenSlug.contains(String.format(",%s,", this.slug))) {
            errors.add(new ValidationError("slug", "REALM.ADMIN.REALM.GROUP.FORM.SLUG.EXIST"));
        }
        if (originalRealmGroup == null || !slug.equalsIgnoreCase(originalRealmGroup.getSlug())) {
            if (RealmGroupModel.find.where().ilike("slug", slug).findRowCount() > 0) {
                errors.add(new ValidationError("slug", "REALM.ADMIN.REALM.GROUP.FORM.SLUG.EXIST"));
            }
        }
        return errors.size() > 0 ? errors : null;
    }

    /**
     * Custom validation, fallback in case of {@code null} passed as param.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate() {
        return validate(null);
    }

    /**
     * Inner - GroupSubscriber
     *
     * @author Olivier Buiron
     * @version 15.10
     * @since 15.10
     */
    public static class GroupMember {
        public UUID id;

        public String fullname;

        public Boolean delete;

        /**
         * Build a GroupSubscriber object.
         *
         * @since 15.10
         */
        public GroupMember() {
            this.delete = false;
        }
    }


    /**
     * SubItemDomainForm.
     *
     * @author Jean-Pierre Boudic
     * @version 15.10
     * @since 15.10
     */
    public static class SubItemPermForm {

        @Constraints.Required
        public String name;
        @Constraints.Required
        public Boolean add;


        /**
         * Build a basic SubItemPermForm object.
         *
         * @since 15.10
         */
        public SubItemPermForm() {
            this.add = false;
        }
    }
}
