/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm.admin;

import com.google.common.base.Joiner;
import models.AccountModel;
import models.RealmModel;
import models.RoleWithAccountsModel;
import play.Logger;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;
import play.mvc.Controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * RoleUsersAdminRealmForm.
 *
 * @author Jean-Pierre Boudic
 * @version 15.10
 * @since 15.10
 */

public class RoleUsersAdminRealmForm {

    @Constraints.Required
    public List<SubItemUserForm> users;

    public void saveUsers(RealmModel currentRealm, RoleWithAccountsModel roleModel) {
        final List<AccountModel> currentAccounts = roleModel.getAccounts();
        final List<String> messages = new ArrayList<>();
        for (SubItemUserForm itmUser : users) {
            final AccountModel account = AccountModel.find.where().eq("uid", itmUser.uid).findUnique();
            if (account != null && account.getRealms().contains(currentRealm)) {
                if (itmUser.delete) {
                    if (currentAccounts.contains(account)) {
                        roleModel.removeAccount(account);
                        account.cleanPermissionsCache();
                        messages.add(Messages.get("REALM.ADMIN.ROLE.USER.DELETE.FLASH.SUCCESS", roleModel.getName(), account.getFullName()));
                    }
                } else {
                    if (!currentAccounts.contains(account)) {
                        roleModel.addAccount(account);
                        account.cleanPermissionsCache();
                        messages.add(Messages.get("REALM.ADMIN.ROLE.USER.ADD.FLASH.SUCCESS", roleModel.getName(), account.getFullName()));
                    }
                }
            }
            Controller.flash("success", Joiner.on(", ").join(messages));
        }
    }

    /**
     * SubItemDomainForm.
     *
     * @author Jean-Pierre Boudic
     * @version 15.10
     * @since 15.10
     */
    public static class SubItemUserForm {

        @Constraints.Required
        public UUID uid;
        @Constraints.Required
        public Boolean delete;

        /**
         * Build a basic SubItemPermForm object.
         *
         * @since 15.10
         */
        public SubItemUserForm() {
            this.delete = false;
        }
    }
}
