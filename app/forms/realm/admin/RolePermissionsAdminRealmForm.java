/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm.admin;

import models.PermissionModel;
import models.RoleModel;
import models.RoleRealmModel;
import play.Logger;
import play.data.validation.Constraints;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * RolePermissionsAdminRealmForm.
 *
 * @author Jean-Pierre Boudic
 * @version 15.10
 * @since 15.10
 */
public class RolePermissionsAdminRealmForm {


    @Valid
    public List<SubItemPermForm> permissions;

    public void init(RoleModel role, List<PermissionModel> permissionModels) {
        permissions = new ArrayList<>();
        for (final PermissionModel realmPermission : permissionModels) {
            final SubItemPermForm frmSubperm = new SubItemPermForm();
            frmSubperm.add = role.getPermissions().contains(realmPermission);
            frmSubperm.name = realmPermission.getScope();
            permissions.add(frmSubperm);
        }
    }

    public void save(RoleModel role, PermissionModel.Namespace namespace) {
        for (final SubItemPermForm itmPerm : permissions) {
            PermissionModel permission = PermissionModel.findByName(itmPerm.name, namespace);
            if (permission != null) {
                if (itmPerm.add) {
                    role.addPermission(permission);
                } else {
                    role.removePermission(permission);
                }
            }
        }
        Logger.info("role.getPermissions(): {}", role.getPermissions().size());
    }

    /**
     * SubItemDomainForm.
     *
     * @author Jean-Pierre Boudic
     * @version 15.10
     * @since 15.10
     */
    public static class SubItemPermForm {

        @Constraints.Required
        public String name;
        @Constraints.Required
        public Boolean add;


        /**
         * Build a basic SubItemPermForm object.
         *
         * @since 15.10
         */
        public SubItemPermForm() {
            this.add = false;
        }
    }
}
