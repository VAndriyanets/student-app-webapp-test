/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm.admin;

import models.RoleWithAccountsModel;
import play.Logger;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.mvc.Call;

import javax.validation.Valid;
import java.util.*;

/**
 * RolesAdminRealmForm.
 *
 * @author Jean-Pierre Boudic
 * @version 15.10
 * @since 15.10
 */
public class RolesAdminRealmForm {

    @Valid
    public List<SubItemRoleForm> roles;

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate() {
        final List<ValidationError> errors = new ArrayList<ValidationError>();

        final Iterator<SubItemRoleForm> it = this.roles.iterator();
        while (it.hasNext()) {
            final SubItemRoleForm role = it.next();
            if (role.name == null || role.name.isEmpty() || this.roles.stream().filter(f -> f.name != null && f.name.toLowerCase(Locale.ENGLISH).compareTo(role.name.toLowerCase(Locale.ENGLISH)) == 0 && role.uid == null).count() > 1) {
                it.remove();

                Logger.info("==== ValidationError ====");
                Logger.info("Delete : {}", role);
                Logger.info("Delete name: {}", role.name);
            }
        }

        return errors.size() > 0 ? errors : null;
    }

    /**
     * SubItemRoleForm.
     *
     * @author Jean-Pierre Boudic
     * @version 15.10
     * @since 15.10
     */
    public static class SubItemRoleForm {

        public UUID uid;

        @Constraints.MaxLength(25)
        public String name;
        public Boolean delete;
        public Boolean readonly;

        /**
         * Build a basic SubItemRoleForm object.
         *
         * @since 15.10
         */
        public SubItemRoleForm() {
            this.delete = false;
            this.readonly = false;
        }

        /**
         * Set the name and apply trim() to the input data.
         *
         * @param name Role Name taken the request data
         * @since 15.10
         */
        public void setName(String name) {
            this.name = name.trim();
        }
    }

    public static class RolesFormData<E extends RoleWithAccountsModel> {
        public final List<E> roles;
        public final RouteTransformer<E> routeTransformer;

        public RolesFormData(List<E> roles, RouteTransformer<E> routeTransformer) {
            this.roles = roles;
            this.routeTransformer = routeTransformer;
        }

        public Call route(E role) {
            return routeTransformer.route(role);
        }

        public E findRole(final String uid) {
            if (uid != null) {
                for (E role : roles) {
                    if (uid.equals(role.getUidAsString())){
                        return role;
                    }
                }
            }
            return null;
        }

        public Call findRoute(final String uid) {
            return routeTransformer.route(findRole(uid));
        }

        public int getAccountsSize(final String uid) {
            return findRole(uid).getAccounts().size();
        }
    }

    public interface RouteTransformer<E extends RoleWithAccountsModel> {
        Call route(E role);
    }
}