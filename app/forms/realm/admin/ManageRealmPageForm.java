/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm.admin;

import models.RealmPageModel;
import play.Play;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * ManageRealmPageForm.
 *
 * @author Olivier Buiron
 * @version 15.12
 * @since 15.12
 */
public class ManageRealmPageForm {
    @Constraints.Required
    @Constraints.MinLength(3)
    @Constraints.MaxLength(36)
    public String name;

    @Constraints.Required
    @Constraints.MinLength(3)
    @Constraints.MaxLength(36)
    @Constraints.Pattern(value = "^[a-z0-9._-]+$", message = "REALM.ADMIN.PAGE.FORM.SLUG.ERROR")
    public String slug;

    public File logo;

    public String logo_crop;

    public File banner;

    public String banner_crop;

    /**
     * Set the name and apply trim() to the input data.
     *
     * @param name Page name taken from the request
     * @since 15.12
     */
    public void setName(String name) {
        this.name = name.trim();
    }

    /**
     * Set the slug and apply trim() to the input data.
     *
     * @param slug Page slug taken form the request
     */
    public void setSlug(String slug) {
        this.slug = slug.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate(final RealmPageModel page) {
        final List<ValidationError> errors = new ArrayList<>();
        final String forbiddenSlug = String.format(",%s,", Play.application().configuration().getString("page.slug.forbidden"));
        if (forbiddenSlug.contains(String.format(",%s,", this.slug))) {
            errors.add(new ValidationError("slug", "REALM.ADMIN.PAGE.FORM.SLUG.EXIST"));
        }
        if (page == null || !slug.equalsIgnoreCase(page.getSlug())) {
            if (RealmPageModel.find.where().ilike("slug", slug).findRowCount() > 0) {
                errors.add(new ValidationError("slug", "REALM.ADMIN.PAGE.FORM.SLUG.EXIST"));
            }
        }
        return errors.size() > 0 ? errors : null;
    }
}
