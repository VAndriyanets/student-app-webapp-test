/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm.admin;

import java.io.File;

/**
 * ManageRealmUsersAdminRealmForm.
 *
 * @author Olivier Buiron
 * @version 15.12
 * @since 15.12
 */
public class ManageRealmUsersAdminRealmForm {

    /**
     * CSV file instance.
     */
    public File csv;

    /**
     * The original name of file send by user.
     */
    public String originalFileName;

    /**
     * Valid if CSV file have correct file type based on file suffix.
     *
     * @return {@code true} if file suffix is CSV, otherwise, {@code false}
     * @since 15.12
     */
    public boolean isValidCsvFileType() {
        return originalFileName != null && originalFileName.endsWith(".csv");
    }
}
