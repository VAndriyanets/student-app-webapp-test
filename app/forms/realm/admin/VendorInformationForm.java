/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm.admin;

import models.RealmPageModel;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * VendorInformationForm.
 *
 */
public class VendorInformationForm {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Constraints.Required
    @Constraints.MinLength(3)
    @Constraints.MaxLength(36)
    public String name;

    @Constraints.MinLength(2)
    @Constraints.MaxLength(10000)
    public String desc;

    public File logo;

    public String logo_crop;

    @Constraints.Pattern(value = EMAIL_PATTERN, message = "VENDOR.PAGE.ADMIN.INFO.EMAIL.ERROR")
    public String email;

    public String phone;

    @Constraints.MinLength(2)
    @Constraints.MaxLength(1000)
    public String address;

    @Constraints.MinLength(2)
    @Constraints.MaxLength(64)
    public String city;

    @Constraints.MinLength(2)
    @Constraints.MaxLength(16)
    public String zipcode;

    public String iban;

    public File ibanPDF;

    /**
     * Set the name and apply trim() to the input data.
     */
    public void setName(String name) {
        this.name = name.trim();
    }

    /**
     * Custom validation.
     */
    public List<ValidationError> validate(final RealmPageModel page) {
        // TODO
        final List<ValidationError> errors = new ArrayList<>();
        return errors.size() > 0 ? errors : null;
    }
}
