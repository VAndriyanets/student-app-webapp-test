/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm.admin;

import models.RealmModel;
import play.Play;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import javax.validation.Valid;
import java.io.File;
import java.util.*;

/**
 * NetworkAdminRealmForm.
 *
 * @author Jean-Pierre Boudic
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class NetworkAdminRealmForm {

    @Constraints.Required
    @Constraints.MaxLength(36)
    @Constraints.MinLength(3)
    public String name;

    @Constraints.Required
    @Constraints.MaxLength(36)
    @Constraints.MinLength(3)
    @Constraints.Pattern(value = "^[a-z0-9._-]+$", message = "REALM.ADMIN.NETWORK.FORM.ERROR.SLUG.INVALID")
    public String slug;

    public File logo;

    public String logo_crop;

    public File banner;

    public String banner_crop;

    @Valid
    public List<SubItemDomainForm> domains;

    /**
     * Set the name and apply trim() to the input data.
     *
     * @param name Realm Name taken the request data
     * @since 15.10
     */
    public void setName(String name) {
        this.name = name.trim();
    }

    /**
     * Set the slug and apply trim() to the input data.
     *
     * @param slug Realm Slug (used in URL) taken the request data
     * @since 15.10
     */
    public void setSlug(String slug) {
        this.slug = slug.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     * @since 15.10
     */
    public List<ValidationError> validate(RealmModel originalRealm) {
        final List<ValidationError> errors = new ArrayList<ValidationError>();
        final String forbiddenSlug = Play.application().configuration().getString("realm.slug.forbidden") + ",";
        if (forbiddenSlug.contains(this.slug + ",")) {
            errors.add(new ValidationError("slug", Messages.get("REALM.ADMIN.NETWORK.FORM.ERROR.SLUG.EXIST")));
        }
        if (!this.slug.equalsIgnoreCase(originalRealm.getSlug())) {
            if (RealmModel.find.where().ilike("slug", this.slug).findRowCount() > 0) {
                errors.add(new ValidationError("slug", Messages.get("REALM.ADMIN.NETWORK.FORM.ERROR.SLUG.EXIST")));
            }
        }
        final Iterator<SubItemDomainForm> it = this.domains.iterator();
        while (it.hasNext()) {
            final SubItemDomainForm sidf = it.next();
            if (sidf.domain == null || sidf.domain.isEmpty() || this.domains.stream().filter(f -> f.domain != null && f.domain.compareTo(sidf.domain) == 0 && sidf.id == null).count() > 1) {
                it.remove();
            }
        }
        return errors.size() > 0 ? errors : null;
    }

    /**
     * SubItemDomainForm.
     *
     * @author Thibault Meyer
     * @version 15.10
     * @since 15.10
     */
    public static class SubItemDomainForm {

        public UUID id;

        @Constraints.MaxLength(50)
        @Constraints.Pattern(value = "(^$|^[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$)", message = "REALM.ADMIN.NETWORK.EMAIL_DOMAINS_WHITHELIS.INVALID")
        public String domain;

        public Boolean delete;

        /**
         * Build a basic SubItemDomainForm object.
         *
         * @since 15.10
         */
        public SubItemDomainForm() {
            this.delete = false;
        }

        /**
         * Set the domain. This method will apply trim
         * and toLower to the input data.
         *
         * @param domain The domain to set
         * @since 15.10
         */
        public void setDomain(final String domain) {
            this.domain = domain.trim().toLowerCase(Locale.ENGLISH);
        }
    }
}
