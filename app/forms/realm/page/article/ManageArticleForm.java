/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package forms.realm.page.article;

import models.ArticleModel;
import models.ArticlePricingModel;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Form for create/edit article.
 *
 */
public class ManageArticleForm {
    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(36)
    public String name;

    @Constraints.Required
    @Constraints.MinLength(3)
    @Constraints.MaxLength(1000)
    public String description;

    @Constraints.Required
    public boolean saleOnWeb;

    @Constraints.Required
    public boolean saleOnMobile;

    @Constraints.Required
    public boolean ticketing;

    @Constraints.Required
    public String status;

    public File logo;

    public String logo_crop;

    public List<SubItemOptionForm> options;

    public Double price;

    public Integer maximumQuantity;

    public Integer limitPerUser;

    public List<SubItemPricingForm> rules;

    /**
     * Set the name and apply trim() to the input data.
     *
     * @param name Page name taken from the request
     */
    public void setName(String name) {
        this.name = name.trim();
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     */
    public List<ValidationError> validate(final ArticleModel article) {
        final List<ValidationError> errors = new ArrayList<>();
        final Iterator<SubItemOptionForm> it = this.options.iterator();
        while (it.hasNext()) {
            final SubItemOptionForm sidf = it.next();
            if (sidf.name == null || sidf.name.isEmpty()) {
                it.remove();
            }
        }
        final Iterator<SubItemPricingForm> rules = this.rules.iterator();
        int rulesNum = 0;
        while (rules.hasNext()) {
            final SubItemPricingForm sidf = rules.next();
            if (sidf.delete) {
                continue;
            }
            if (ArticlePricingModel.PricingType.OWN_AN_ARTICLE.name().equals(sidf.type) && sidf.articleId == null ) {
                errors.add(new ValidationError("rules["+rulesNum+"]", Messages.get("ARTICLE.MANAGE.PAGE.RULE.ARTICLE.ERROR")));
            }
            if (ArticlePricingModel.PricingType.MEMBER_OF_GROUP.name().equals(sidf.type )  && sidf.groupId == null ) {
                errors.add(new ValidationError("rules["+rulesNum+"]", Messages.get("ARTICLE.MANAGE.PAGE.RULE.GROUP.ERROR")));
            }
            rulesNum++;
        }
        return errors.size() > 0 ? errors : null;
    }

    public static class SubItemOptionForm {

        public Long id;

        @Constraints.MinLength(2)
        @Constraints.MaxLength(50)
        public String name;

        @Constraints.MaxLength(1000)
        public String argument;

        public Boolean mandatory;

        public Boolean delete;

        @Constraints.MaxLength(20)
        public String type;

        public SubItemOptionForm() {
            this.delete = false;
            this.mandatory = false;
        }

    }

    public static class SubItemPricingForm {

        public Long id;

        public Long articleId;

        public Long groupId;

        public Boolean delete;

        @Constraints.Required
        public Double price;

        @Constraints.MaxLength(20)
        public String type;

        public SubItemPricingForm() {
            this.delete = false;
        }

    }
}
