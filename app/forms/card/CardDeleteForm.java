package forms.card;

import play.data.validation.Constraints;

/**
 * CardDeleteForm.
 *
 * @author vitali.andriyanets
 */
public class CardDeleteForm {

    @Constraints.Required
    public Long cardId;

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }
}
