package forms.card;

import models.CardModel;
import models.CardTypeModel;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Constraints;
import play.data.validation.ValidationError;
import play.i18n.Messages;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * CardAddForm.
 *
 * @author vitali.andriyanets
 */
public class CardAddForm {

    @Constraints.Required
    @Constraints.MinLength(2)
    @Constraints.MaxLength(128)
    public String cardNumber;

    @Constraints.Required
    public CardTypeModel cardType;

    /**
     * Default constructor.
     */
    public CardAddForm() {
    }

    /**
     * Custom validation.
     *
     * @return A list of {@code ValidationError}
     * @see ValidationError
     */
    public List<ValidationError> validate() {
        final List<ValidationError> errors = new ArrayList<ValidationError>();

        if(CardModel.find.where().eq("card_number", this.cardNumber).findRowCount() > 0) {
            errors.add(new ValidationError("cardNumber", Messages.get("CARD.ERROR.FORM.CARD_NUMBER_EXISTS")));
        }

        if(this.cardType.getId() == null) {
            errors.add(new ValidationError("cardType", Messages.get("CARD.ERROR.FORM.INVALID_CARD_TYPE")));
            return errors;
        }

        CardTypeModel selectedCardType = CardTypeModel.find.byId(this.cardType.getId());
        if(selectedCardType == null) {
            errors.add(new ValidationError("cardType", Messages.get("CARD.ERROR.FORM.INVALID_CARD_TYPE")));
            return errors;
        }

        if(StringUtils.isNotBlank(selectedCardType.getValidationPattern()) && !validateCardNumber(selectedCardType)) {
            errors.add(new ValidationError("cardNumber", Messages.get("CARD.ERROR.FORM.INVALID_PATTERN")));
        }

        return errors.size() > 0 ? errors : null;
    }

    /**
     * Check card number by card type pattern
     * @param cardType - card type
     * @return true if card number matches the pattern
     */
    private boolean validateCardNumber(CardTypeModel cardType){
        Pattern p = Pattern.compile(cardType.getValidationPattern());
        Matcher m = p.matcher(this.cardNumber);
        return m.matches();
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public CardTypeModel getCardType() {
        return cardType;
    }

    public void setCardType(CardTypeModel cardType) {
        this.cardType = cardType;
    }
}
