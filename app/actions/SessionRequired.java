/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package actions;

import models.AccountModel;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

/**
 * SessionRequired.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class SessionRequired extends Security.Authenticator {

    /**
     * Get the username of the authenticated user.
     *
     * @param ctx The current request context
     * @return The username
     * @since 15.10
     */
    @Override
    public String getUsername(Http.Context ctx) {
        final AccountModel account = AccountModel.find.where().like("uid", ctx.session().get("uid")).findUnique();
        if (account == null) {
            return null;
        }
        ctx.args.put("account", account);
        return ctx.session().get("uid");
    }

    /**
     * This method is called if user is not authenticated.
     *
     * @param ctx The current request context
     * @return Redirection to authentication form
     * @since 15.10
     */
    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return redirect(controllers.system.routes.AuthenticationController.GET_Authentication().url() + "?n=" + ctx.request().path());
    }
}
