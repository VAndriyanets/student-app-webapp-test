/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package actions;

import models.AccountModel;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

/**
 * SessionOptional.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class SessionOptional extends Action.Simple {

    /**
     * Try to retrieve the current user authenticated.
     *
     * @param context The current request context
     * @return A promised result
     * @throws Throwable If something goes wrong
     * @since 15.10
     */
    @Override
    public F.Promise<Result> call(Http.Context context) throws Throwable {
        if (context.session().containsKey("uid")) {
            final AccountModel account = AccountModel.find.where().like("uid", context.session().get("uid")).findUnique();
            context.args.put("account", account);
        } else {
            context.args.put("account", null);
        }
        return delegate.call(context);
    }
}
