$ ->
    ### On Load ###
    $('#realm-add-domain').click ->
        $('#realm-list-domains').append(
            '<div class="uk-width-1-1">' +
                '<div class="uk-form-icon">' +
                    '<i class="uk-icon-at"></i>' +
                    '<input type="text" name="domains[' + (countDomainWhite++) + '].domain" value="" /> ' +
                '</div>' +
            '</div>'
        )


    $('.btn-delete').click ->
        $this = $(this)
        if(!$this.children('input[type="checkbox"]').prop('checked'))
            if($this.data('status')!='saved')
                $this.parents('.uk-width-1-1').remove()
            else
                $this.addClass('uk-button-danger')
                $this.children('input[type="checkbox"]').prop('checked', true)
                $this.children('i.uk-icon').removeClass('uk-icon-trash').addClass('uk-icon-check')
                $this.prev('input[type="text"]').addClass('uk-form-danger ')
        else
            $this.removeClass('uk-button-danger')
            $this.children('input[type="checkbox"]').prop('checked', false)
            $this.children('i.uk-icon').addClass('uk-icon-trash').removeClass('uk-icon-check')
            $this.prev('input[type="text"]').removeClass('uk-form-danger')