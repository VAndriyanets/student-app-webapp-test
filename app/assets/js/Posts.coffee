$(document).ready ->

  postId = (container) -> container.data("post-id")
  postLiked = (container) -> container.data("post-like")
  postDisliked = (container) -> container.data("post-dislike")



  initButtonsContainer = (container) ->
    likeButton = container.find(".like-button")
    likeButton.click( () ->
      if(postLiked(container))
        rollbackLike(container)
      else if(!postDisliked(container))
        sendLike(container)
    )

    dislikeButton = container.find(".dislike-button")
    dislikeButton.click( () ->
      if(postDisliked(container))
        rollbackDislike(container)
      else if(!postLiked(container))
        sendDislike(container)
    )

  $("div.post-like-controls").each((index,cont) ->
    container = $(cont)
    initButtonsContainer(container);
  )


  $("div.post-like-controls").each((index,cont) ->
    container = $(cont)

    deleteButton = container.find(".delete-button")
    deleteButton.click( () ->
        deletePost(container)
    )

  )

  deletePost = (container) ->
    appRoutes.controllers.realm.group.GroupPageController.POST_DeletePost(postId(container)).ajax({
      success: (data) ->
        location.reload()
        # container.closest( ".pit-wall-item" ).remove()
    })

  sendLike = (container) ->
    appRoutes.controllers.realm.VoteController.POST_voteUp(postId(container)).ajax({
      success: (data) ->
        sendCallback(container, "like")
    })

  sendDislike = (container) ->
    appRoutes.controllers.realm.VoteController.POST_voteDown(postId(container)).ajax({
      success: (data) ->
        sendCallback(container, "dislike")
    })



  sendCallback = (container, dataType) ->
    container.find("a." + dataType+"-button").addClass("voted");
    numberHolder = container.find("a." + dataType+"-button  span.number-holder")
    MBSFunctions.util.increment(numberHolder)
    container.data("post-"+dataType, true)


  rollbackCallback = (container, dataType) ->
    container.find("a." + dataType+"-button").removeClass("voted");
    numberHolder = container.find("a." + dataType+"-button span.number-holder")
    MBSFunctions.util.decrement(numberHolder)
    container.data("post-"+dataType, false)


  rollbackLike = (container) ->
    appRoutes.controllers.realm.VoteController.POST_voteUpRollback(postId(container)).ajax({
      success: (data) ->
        rollbackCallback(container, "like")
    })

  rollbackDislike = (container) ->
    appRoutes.controllers.realm.VoteController.POST_voteDownRollback(postId(container)).ajax({
      success: (data) ->
        rollbackCallback(container, "dislike")
    })

  $('.pit-wall-item-content').each ((index, element) ->
      urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig
      element.innerHTML = element.innerHTML.replace(urlRegex, (url) ->
        '<a href="' + url + '">' + url + '</a>'
    )
  )


