$(document).ready ->
    $('#form-user-search').focus ->
        this.value = ''
    $('#user-autocomplete').on 'selectitem.uk.autocomplete', (event, data, acobject) ->
        user = data.value
        if($('#user-'+user.uid).length==0)
            $('#grid-list-users').append(getUserRow(user))
        else
            if($('#user-'+user.uid).find('input[type="checkbox"]:first').prop('checked'))
                $('#user-'+user.uid).removeClass('hidden').find('input[type="checkbox"]:first').prop('checked', false)
            else
                error = MSG_USER_EXIST.replace('{0}', user.fullName).replace('{1}', CURRENT_ROLE)
                UIkit.notify({message: error, status:'warning', pos:'top-right'})

        user.toString = -> 
            return user.fullName

    getUserRow = (user) ->
        user.avatar = DEFAULT_AVATAR if(user.avatar=='')
        count = $('#grid-list-users .user-row').length;
        
        return '' +
        '<div class="uk-width-large-1-3 uk-width-medium-1-2 uk-width-small-1-1 user-row" id="user-' + user.uid + '">' +
            '<div class="uk-panel uk-panel-box uk-panel-box-secondary">' +
                '<div class="user-avatar">' +
                    '<img src="' + user.avatar + '"/>' +
                '</div>' +
                '<span class="uk-panel-badge uk-badge uk-badge-notification uk-badge-danger btn-user-delete" data-uid="' + user.uid + '">' +
                    '<i class="uk-icon-trash"></i>' +
                '</span>' +
                '<h3 class="uk-panel-title">' + user.fullName + '</h3>' +
                '<div class="uk-text-truncate"><b>Email: </b>' + user.primaryEmail + '</div>' +
                '<input type="hidden" name="users[' + count + '].uid" value="' + user.uid + '">' +
                '<input type="checkbox" name="users[' + count + '].delete" class="hidden">' +
            '</div>' +
        '</div>'

    $('#grid-list-users').on 'click', '.btn-user-delete', ->
        uid = $(this).data('uid')
        $('#user-'+uid).addClass('hidden').find('input[type="checkbox"]:first').prop('checked', true)