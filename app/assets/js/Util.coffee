MBSFunctions.util = {
  increment: (numberHolder) ->
    number = parseInt(numberHolder.text())
    numberHolder.text(number + 1)

  decrement: (numberHolder) ->
    number = parseInt(numberHolder.text())
    numberHolder.text(number - 1)
}