$ ->
    $('#form-member-search').focus ->
        this.value = ''

    $('#member-group-list').on 'click', '.btn-delete-member', ->
        uid = $(this).data('uid')
        $('#members-' + uid).addClass('hidden').find('input[type="checkbox"]:first').prop('checked', true)

    $('#member-autocomplete').on 'selectitem.uk.autocomplete', (event, data, acobject) ->
        member = data.value
        if($('#members-' + member.uid).length == 0)
            $('#member-group-list').append(getMemberRow(member,'#member-group-list .member','members'))
        else
            if($('#members-'+member.uid).find('input[type="checkbox"]:first').prop('checked'))
                $('#members-'+member.uid).removeClass('hidden').find('input[type="checkbox"]:first').prop('checked', false)
            else
                error = MSG_MEMBER_EXIST.replace('{0}', member.fullName)
                UIkit.notify({message: error, status:'warning', pos:'top-right'})

        member.toString = ->
            return member.fullName



    $('#form-admin-search').focus ->
        this.value = ''

    $('#admin-group-list').on 'click', '.btn-delete-member', ->
        uid = $(this).data('uid')
        $('#admins-' + uid).addClass('hidden').find('input[type="checkbox"]:first').prop('checked', true)

    $('#admin-autocomplete').on 'selectitem.uk.autocomplete', (event, data, acobject) ->
        member = data.value
        if($('#admins-' + member.uid).length == 0)
            $('#admin-group-list').append(getMemberRow(member,'#admin-group-list .member','admins'))
        else
            if($('#admins-'+member.uid).find('input[type="checkbox"]:first').prop('checked'))
                $('#admins-'+member.uid).removeClass('hidden').find('input[type="checkbox"]:first').prop('checked', false)
            else
                error = MSG_MEMBER_EXIST.replace('{0}', member.fullName)
                UIkit.notify({message: error, status:'warning', pos:'top-right'})

        member.toString = ->
            return member.fullName


    getMemberRow = (member, membersSelector, formField) ->
        member.avatar = DEFAULT_AVATAR if(member.avatar=='')
        count = $(membersSelector).length;

        return '' +
        '<div class="uk-form-controls uk-form-icon member" id="'+formField+'-' + member.uid + '">' +
            '<i class="uk-icon-user"></i>' +
            '<input type="hidden" name="'+formField+'[' + count + '].id" value="' + member.uid + '"/>' +
            '<input type="text" name="'+formField+'[' + count + '].fullName" value="' + member.fullName + '" disabled/>' +
            '<button type="button" class="uk-button btn-delete btn-delete-member" data-uid="' + member.uid + '">' +
                '<i class="uk-icon uk-icon-trash"></i>' +
                '<input class="hidden" type="checkbox" name="'+formField+'[' + count + '].delete"/>' +
            '</button>' +
        '</div>'