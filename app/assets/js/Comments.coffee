MBSFunctions.comments = {
  getCommentViewHtml : (comment, canDelete) ->
    html = '<li class="comment-item comment-' + comment.id + '">'
    html += '<div class="comment-item-header">'
    if(comment.authorAvatar)
      html += '<img src="' + comment.authorAvatar + '"/>'
    if(canDelete)
      html += '<button class="uk-button delete-comment-button" type="button" title="Delete"><i class="uk-icon uk-icon-trash"></i></button>'
    html += '<div class="comment-item-header-author"><h8><a href="#">'
    html += comment.authorName
    html += '</a></h8><p>'
    html += comment.created
    html += '</p></div></div>'
    html += '<div class="comment-item-content">'
    urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig
    processedText = comment.text.replace(urlRegex, (url) ->
      '<a href="' + url + '">' + url + '</a>')
    html += processedText
    html += '</div>'
    if(comment.pictures)
      comment.pictures.forEach((picture) ->
        html += '<div class="comment-item-media"><img src="'
        html += picture
        html += '"/></div>'
      )
    html += '</li>'


  appendCommentView : (comment, commentsList, postContainer) ->
    html = MBSFunctions.comments.getCommentViewHtml(comment, commentsList.data("comments-can-delete") || comment.self)
    commentsList.append(html)
    commentsList.find('li.comment-' + comment.id + ' .delete-comment-button').click(()->
      appRoutes.controllers.realm.CommentsController.DELETE_comment(comment.id).ajax({
        success: () ->
          commentsList.find('li.comment-' + comment.id).detach()
          numberHolder = postContainer.find("span.comment-count-holder")
          MBSFunctions.util.decrement(numberHolder)
        error: () ->
          UIkit.notify("Can't delete comment " + comment.id, {status: 'danger', pos: 'top-right'})
      })
    )

  addShowMoreButton : (commentsContainer,commentsList,postContainer) ->
    html = '<li><button class="show-more-button uk-button uk-button-link">'+MBSStrings.comments.showMore+'</button></li>'
    commentsList.append(html)
    commentsList.find('.show-more-button').click () ->
      appRoutes.controllers.realm.CommentsController.GET_allComments(MBSFunctions.comments.postId(postContainer)).ajax({
        success: (data) ->
          MBSFunctions.comments.commentsLoadCallback(data, commentsContainer, postContainer)
        error: (data) ->
          UIkit.notify("Can't load comments", {status:'danger', pos:'top-right'})
      })


  commentsLoadCallback : (data, commentsContainer, postContainer) ->
    if(data.total > 0)
      numberHolder = postContainer.find("span.comment-count-holder")
      numberHolder.text(data.total)
      commentsList = commentsContainer.find(".comments-wall")
      commentsList.empty()
      data.comments.forEach((comment) ->
        MBSFunctions.comments.appendCommentView(comment, commentsList, postContainer)
      )
      if(data.total > data.comments.length)
        MBSFunctions.comments.addShowMoreButton(commentsContainer, commentsList, postContainer)
    commentsContainer.find(".comment-form").css('display', 'block');
    commentsContainer.data("comments-show", true)

  addCommentCallback : (data, commentsContainer, postContainer) ->
    if(data.comment)
      commentsList = commentsContainer.find(".comments-wall")
      MBSFunctions.comments.appendCommentView(data.comment, commentsList, postContainer)
      numberHolder = postContainer.find("span.comment-count-holder")
      MBSFunctions.util.increment(numberHolder)
      commentsContainer.find(".pit-comment-publish-textarea").val('')
      commentsContainer.find("input.pictures-count").val(0)
      commentsContainer.find(".pictures-holder").empty()

  postId : (postContainer) -> postContainer.data("post-id")

  initialize : () ->
    $(".pit-comment-publish-textarea").elastic()
    $("div.pit-wall-item").each((index,cont) ->
      postContainer = $(cont)
      commentsContainer = postContainer.find("div.comments-container")
      commentsButton = postContainer.find("a.comments-button")
      commentsShown = (commentsContainer) -> commentsContainer.data("comments-show")
      commentsContainer.find(".add-comment-button").click( ()->
        appRoutes.controllers.realm.CommentsController.POST_comment(MBSFunctions.comments.postId(postContainer)).ajax({
          data: new FormData(commentsContainer.find(".comment-form form")[0]),
          contentType: false,
          processData: false,
          type: 'POST',
          success: (data) ->
            MBSFunctions.comments.addCommentCallback(data, commentsContainer, postContainer)
          error: (data) ->
            UIkit.notify("Can't post comment", {status:'danger', pos:'top-right'})
        })
      )
      commentsButton.click( () ->
        if(commentsShown(commentsContainer))
          commentsContainer.find(".comments-wall").empty()
          commentsContainer.find(".comment-form").css('display','none');
          commentsContainer.data("comments-show", false);
        else
          appRoutes.controllers.realm.CommentsController.GET_comments(MBSFunctions.comments.postId(postContainer)).ajax({
            success: (data) ->
              MBSFunctions.comments.commentsLoadCallback(data, commentsContainer, postContainer)
            error: (data) ->
              UIkit.notify("Can't load comments", {status:'danger', pos:'top-right'})
          })
        return true;
      )

      commentForm = commentsContainer.find(".comment-form")
      commentForm.find('.comment-add-picture').click ->
        picturesCount = commentForm.find('input.pictures-count').val()
        commentForm.find('.pictures-holder').append '<div class="post-picture'  + picturesCount + '-holder uk-form-controls hidden" data-picture-number="' + picturesCount + '">' +
            '<div class="button-group">'+
            '<button type="button" class="uk-button uk-button-success hidden file-crop post-picture-button-ok" name="post-picture-button-ok">' +
            '<i class="uk-icon-check"></i>' + MBSStrings.ok + '</button>' +
            '<button type="button" class="uk-button uk-button-danger hidden file-cancel post-picture-button-cancel" name="post-picture-button-cancel">' +
            '<i class="uk-icon-close "></i>' + MBSStrings.cancel + '</button></div><div class="post-picture-preview hidden preview"></div>' +
            '<input type="file" name="picture' + picturesCount + '" class="hidden post-picture-file" accept="image/x-png, image/gif, image/jpeg"/>' +
            '<input type="hidden" class="post-picture-crop" name="picture_crop' + picturesCount + '"/><div class="post-picture-thumbnail thumbnail">' +
            '<img width="0"/></div><button class="post-picture-remove uk-button" type="button" title="' + MBSStrings.pictures.delete + '">' +
            '<i class="uk-icon uk-icon-trash"></i></button></div>'

        pictureBlock = commentForm.find('.post-picture'  + picturesCount + '-holder')
        pictureBlock.find('.post-picture-remove').click ->
          commentForm.find('.post-picture' + picturesCount + '-holder').remove()
        pictureBlock.find('.post-picture-file').change ->
          reader = undefined
          reader = new FileReader
          if(@files[0])
            reader.readAsDataURL @files[0]
            reader.onload = (_file) ->
              img = undefined
              img = new Image
              img.onload = ->
                pictureBlock.find('.post-picture-preview').removeClass('hidden').html '<img src="' + @src + '" width="' + @width + '" height="' + @height + '"/>'
                pictureBlock.find('.post-picture-thumbnail').addClass 'hidden'
                pictureBlock.find('.post-picture-button-ok').removeClass 'hidden'
                pictureBlock.find('.post-picture-button-cancel').removeClass 'hidden'
                commentForm.find('.post-picture' + picturesCount + '-holder').removeClass 'hidden'
                pictureBlock.find('.post-picture-preview img').Jcrop
                  setSelect: [
                    0
                    0
                    @width / 2
                    @height / 2
                  ]
                  trueSize: [
                    @width
                    @height
                  ]
                  onChange: (c) ->
                    pictureBlock.find('.post-picture-crop').val JSON.stringify(c)
                  onSelect: (c) ->
                    pictureBlock.find('.post-picture-crop').val JSON.stringify(c)
              img.src = _file.target.result

        pictureBlock.find('button.file-cancel').click ->
          commentForm.find('.post-picture' + picturesCount + '-holder').remove()
        pictureBlock.find('button.file-crop').click ->
          coord = undefined
          img = undefined
          thumb = undefined
          coord = JSON.parse( pictureBlock.find('.post-picture-crop').val())
          thumb =
            w: pictureBlock.find('.post-picture-thumbnail').width()
            h: pictureBlock.find('.post-picture-thumbnail').height()
          img =
            w: parseInt(pictureBlock.find('.post-picture-preview img').attr('width'), 10)
            h: parseInt(pictureBlock.find('.post-picture-preview img').attr('height'), 10)
          pictureBlock.find('.post-picture-thumbnail img').attr('src', pictureBlock.find('.post-picture-preview img').attr('src')).css
            width: Math.ceil(img.w / coord.w * thumb.w)
            height: Math.ceil(img.h / coord.h * thumb.h)
            marginLeft: -1 * Math.ceil(coord.x / coord.w * thumb.w)
            marginTop: -1 * Math.ceil(coord.y / coord.h * thumb.h)
          pictureBlock.find('.post-picture-thumbnail').removeClass 'hidden'
          pictureBlock.find('.post-picture-preview').addClass('hidden').html ''
          pictureBlock.find('.post-picture-button-ok').addClass 'hidden'
          pictureBlock.find('.post-picture-button-cancel').addClass 'hidden'
        $target = pictureBlock.find('.post-picture-file')

        picturesCountInput = commentForm.find('input.pictures-count')
        number = parseInt(picturesCountInput.val())
        console.log("old number" + number)
        newNumber = number + 1
        picturesCountInput.val(newNumber);
        console.log("new number calc" + newNumber)
        console.log("new number set" + parseInt(picturesCountInput.val()))
        $target.trigger 'click'
    )
}

$(document).ready( () ->
  MBSFunctions.comments.initialize()
)





