$ ->
  $('.slug-input').keyup ->
    slugify()

  slugify = ->
    $('.slug-placeholder').html($('.slug-input').val().toLowerCase().replace(/[^a-z0-9-_.]/ig,''))

  slugify()