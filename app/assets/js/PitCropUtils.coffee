$ ->
    ### =============== CROP =============== ###
    $(window).resize ->
        if($('div.jcrop-holder').length>0)
            $jcrop =  $('div.jcrop-holder')
            $img =  $jcrop.prev('img')
            $preview = $jcrop.parent('.preview')
            w = parseInt($img.attr('width'), 10)
            h = parseInt($img.attr('height'), 10)
            $preview.html('<img src="'+$img.attr('src')+'" width="'+w+'" height="'+h+'"/>')
            name = $preview.attr('id').replace('-preview', '')
            select = $('#'+name+'-file').data('select').split(',')
            $('#'+name+'-preview img').Jcrop({
                aspectRatio: (select[0]/select[1]),
                setSelect: [0, 0, 60, 60*(select[0]/select[1])],
                trueSize: [w, h],
                onChange: (c) -> $('#'+name+'-crop').val(JSON.stringify(c)),
                onSelect: (c) -> $('#'+name+'-crop').val(JSON.stringify(c))
            })

    $('input[type="file"]').change ->
        name = $(this).attr('id').replace('-file', '')

        reader = new FileReader
        reader.readAsDataURL(this.files[0])
        reader.onload = (_file) ->
            img = new Image

            img.onload = () ->
                $('#'+name+'-preview')
                    .removeClass('hidden')
                    .html('<img src="'+this.src+'" width="'+this.width+'" height="'+this.height+'"/>')

                $('#'+name+'-thumbnail')
                    .addClass('hidden')

                $('#'+name+'-button-ok')
                    .removeClass('hidden')

                $('#'+name+'-button-cancel')
                    .removeClass('hidden')

                select = $('#'+name+'-file').data('select').split(',')

                $('#'+name+'-preview img')
                    .Jcrop({
                        aspectRatio: (select[0]/select[1]),
                        setSelect: [0, 0, 60, 60*(select[0]/select[1])],
                        trueSize: [this.width, this.height],
                        onChange: (c) -> $('#'+name+'-crop').val(JSON.stringify(c)),
                        onSelect: (c) -> $('#'+name+'-crop').val(JSON.stringify(c))
                    })


            img.src = _file.target.result


    $('button.file-upload').click ->
        name = $(this).attr('id').replace('-button-browse', '')
        $target = $('#'+name+'-file')
        $target.trigger("click")

    $('button.file-cancel').click ->
        name = $(this).attr('id').replace('-button-cancel', '')
        $('#'+name+'-file').val('')
        $('#'+name+'-crop').val('')
        $('#'+name+'-thumbnail').removeClass('hidden')
        $('#'+name+'-preview')
            .addClass('hidden')
            .html('')
        $('#'+name+'-button-ok')
            .addClass('hidden')
        $('#'+name+'-button-cancel')
            .addClass('hidden')

    $('button.file-crop').click ->
        name = $(this).attr('id').replace('-button-ok', '')

        coord = JSON.parse($('#'+name+'-crop').val());
        select = $('#'+name+'-file').data('select').split(',')
        thumb = {
            w: $('#'+name+'-thumbnail').width(),
            h: $('#'+name+'-thumbnail').height()
        }
        img = {
            w: parseInt($('#'+name+'-preview img').attr('width'), 10),
            h: parseInt($('#'+name+'-preview img').attr('height'), 10)
        }

        $('#'+name+'-thumbnail img')
            .attr('src', $('#'+name+'-preview img').attr('src'))
            .css({
            width:      Math.ceil(img.w/coord.w*thumb.w),
            height:     Math.ceil(img.h/coord.h*thumb.h),
            marginLeft: -1 * Math.ceil(coord.x/coord.w*thumb.w),
            marginTop:  -1 * Math.ceil(coord.y/coord.h*thumb.h)
        })

        $('#'+name+'-thumbnail').removeClass('hidden')

        $('#'+name+'-preview')
            .addClass('hidden')
            .html('')
        $('#'+name+'-button-ok')
            .addClass('hidden')
        $('#'+name+'-button-cancel')
            .addClass('hidden')