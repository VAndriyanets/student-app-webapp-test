$(document).ready ->
    $('#realm-add-role').click ->
        console.log('Salut !')
        count = $('#realm-list-roles .role-row').length
        $('#realm-list-roles').append(
            '<div class="uk-width-3-5 role-row">' +
                '<div class="uk-form-icon">' +
                    '<i class="uk-icon-unlock-alt"></i>' +
                    '<input type="hidden" name="roles[' + count + '].uid" value="" />' +
                    '<input type="text" name="roles[' + count + '].name" value="" />' +
                '</div>' +
            '</div>'
        )


    $('input[type="checkbox"].uk-like-button-delete').each ->
        $(this)
            .after(
                '<button type="button" class="uk-button button-checkbox">' +
                    '<i class="uk-icon uk-icon-trash"></i>' +
                '</button type="button">'
            )
            .hide()

    $('.button-checkbox').click ->
        $this = $(this)
        $input = $this.prev('input[type="checkbox"].uk-like-button-delete:first')

        if(!$input.prop('checked'))
            $input.prop('checked', true)
            $('#' + $input.data('for')).addClass('uk-form-danger')
            $this.addClass('uk-button-danger')
        else
            $input.prop('checked', false)
            $('#' + $input.data('for')).removeClass('uk-form-danger')
            $this.removeClass('uk-button-danger')

    $('.delete-warning').click ->
        $this = $(this)
        frm = $this.data('form')
        count = $('#' + frm + ' input[type="checkbox"]:checked').length
        if(count>0)
            confirm = $this.data('confirm').replace('{0}', count)
            UIkit.modal.confirm confirm, ->
                $('#' + frm).trigger("submit")
        else
            $('#' + frm).trigger("submit")