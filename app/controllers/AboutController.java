/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers;

import actions.SessionOptional;
import play.Play;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import views.html.AboutPoliciesView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * AboutController.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class AboutController extends Controller {

    /**
     * Try to load the specified document with the current request language.
     * If the document does not exist, this method will try to load it from
     * another language.
     *
     * @param fileName The filename to open
     * @return A {@code StringBuilder} instance, otherwise, {@code null}
     * @throws IOException If something goes wrong
     */
    private StringBuilder loadFile(final String fileName) throws IOException {
        InputStream is = play.api.Play.classloader(play.api.Play.current())
                .getResourceAsStream(String.format("terms-and-policies/%s_%s.html",
                        fileName,
                        Http.Context.current().lang().code()));
        if (is == null) {
            for (final String lng : Play.application().configuration().getStringList("play.i18n.langs")) {
                is = play.api.Play.classloader(play.api.Play.current())
                        .getResourceAsStream(String.format("terms-and-policies/%s_%s.html",
                                fileName,
                                lng));
                if (is != null) {
                    break;
                }
            }
            if (is == null) {
                return null;
            }
        }
        final InputStreamReader isr = new InputStreamReader(is, "UTF-8");
        final BufferedReader br = new BufferedReader(isr);
        final StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        isr.close();
        is.close();
        br.close();
        return sb;
    }

    /**
     * Show the policies page.
     *
     * @return The policies page
     * @since 15.10
     */
    @With(SessionOptional.class)
    public Result GET_Policies() throws IOException {
        final StringBuilder sbPoliciesTerms = this.loadFile("terms");
        final StringBuilder sbPoliciesData = this.loadFile("data");
        final StringBuilder sbPoliciesCommunity = this.loadFile("community");
        return ok(AboutPoliciesView.render(sbPoliciesTerms, sbPoliciesData, sbPoliciesCommunity));
    }
}
