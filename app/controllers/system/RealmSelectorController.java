/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.system;

import actions.SessionRequired;
import forms.system.RealmSelectorEnrollTokenForm;
import models.AccountEmailModel;
import models.AccountModel;
import models.RealmModel;
import modules.RedisModule;
import play.Play;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import redis.clients.jedis.Jedis;
import toolbox.helper.SessionHelper;
import toolbox.play.FlashScopeForwarder;
import views.html.RealmSelectorView;

import java.util.Set;

/**
 * RealmSelectorController.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class RealmSelectorController extends Controller {

    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_RealmSelectorInvite() {
        final AccountModel account = SessionHelper.getAccount();

        final Jedis jedis = Play.application().injector().instanceOf(RedisModule.class).getConnection();
        jedis.select(0);

        for (AccountEmailModel email : account.getEmails()) {
            if (jedis.exists(String.format("account.pre.realm.%s", email.getEmail()))) {
                if (!jedis.zrangeByScore(String.format("account.pre.realm.%s", email.getEmail()), System.currentTimeMillis() / 1000, Double.POSITIVE_INFINITY).isEmpty()) {
                    return ok(RealmSelectorView.render(true, null));
                }
            }
        }

        return redirect(controllers.routes.BaseApplicationController.GET_BaseApplication());
    }

    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_RealmEnrollByToken() {
        final FlashScopeForwarder flashScopeForwarder = new FlashScopeForwarder(flash());
        final AccountModel account = SessionHelper.getAccount();

        final Form<RealmSelectorEnrollTokenForm> form = Form.form(RealmSelectorEnrollTokenForm.class).bindFromRequest(request());

        if (form.hasErrors()) {
            return badRequest(RealmSelectorView.render(true, form));
        }

        final RealmSelectorEnrollTokenForm enrollTokenForm = form.get();

        final Jedis jedis = Play.application().injector().instanceOf(RedisModule.class).getConnection();
        jedis.select(0);

        for (AccountEmailModel email : account.getEmails()) {
            if (jedis.exists(String.format("account.pre.realm.%s", email.getEmail()))) {
                final Set<String> invites = jedis.zrangeByScore(String.format("account.pre.realm.%s", email.getEmail()), String.valueOf(System.currentTimeMillis() / 1000D), "+inf");
                for (String invite : invites) {
                    final String[] splits = invite.split("\\|");
                    if (enrollTokenForm.enrollToken.equals(splits[0])) {
                        final RealmModel realm = RealmModel.find.where().like("uid", splits[1]).findUnique();
                        if (realm != null) {
                            realm.addAccount(account);
                            realm.save();
                            jedis.zrem(String.format("account.pre.realm.%s", email.getEmail()), invite);
                            flashScopeForwarder.forwardFlash("success", Messages.get("REALM.SELECTOR.INVITE.SUCCESS", realm.getName()));
                        }
                        return redirect(controllers.routes.BaseApplicationController.GET_BaseApplication());
                    }
                }
            }
        }
        flash("danger", Messages.get("REALM.SELECTOR.INVITE.FAIL"));
        return redirect(routes.RealmSelectorController.GET_RealmSelectorInvite());
    }
}
