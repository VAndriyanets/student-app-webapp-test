/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.system;

import actors.SearchEngineActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import forms.system.RegistrationForm;
import models.AccountEmailModel;
import models.AccountModel;
import models.ActorModel;
import models.ActorProfileModel;
import modules.RedisModule;
import play.Play;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import redis.clients.jedis.Jedis;
import toolbox.SendMail;
import toolbox.enumeration.AccountStatus;
import toolbox.helper.ActorHelper;
import toolbox.play.UIDBinder;
import views.html.BaseApplicationView;
import views.html.mailview.RegistrationValidationMailView;

import java.util.UUID;

/**
 * RegistrationController.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
@Singleton
public class RegistrationController extends Controller {

    /**
     * Handle to SearchEngine actor reference.
     */
    private final ActorRef searchEngineActor;

    /**
     * Build a simple instance.
     *
     * @param actorSystem The actor system handle
     */
    @Inject
    public RegistrationController(ActorSystem actorSystem) {
        this.searchEngineActor = actorSystem.actorOf(SearchEngineActor.props);
    }

    /**
     * Show the registration page.
     *
     * @return The registration page
     * @since 15.10
     */
    @AddCSRFToken
    public Result GET_Registration() {
        return ok(BaseApplicationView.render(null, null));
    }

    /**
     * Process the submitted registration form.
     *
     * @return Redirection to home page in case of success, otherwise, bad request authentication page
     * @since 15.10
     */
    @RequireCSRFCheck
    public Result POST_Registration() {
        final Form<RegistrationForm> formRegistration = Form.form(RegistrationForm.class).bindFromRequest();

        if (!formRegistration.hasErrors()) {
            final RegistrationForm registration = formRegistration.get();
            final RedisModule redisModule = Play.application().injector().instanceOf(RedisModule.class);
            final Jedis jedis = redisModule.getConnection();
            final String validationKey = UUID.randomUUID().toString();

            final String currentLang = ctx().lang().code();
            final AccountModel account = new AccountModel();
            account.setPassword(registration.password);
            account.setLang(currentLang);
            account.setTimezone("Europe/Paris"); // TODO : Set the correct timezone for the client.
            final ActorModel actor = account.getActor();
            final ActorProfileModel profile = actor.getProfile();
            account.setFirstName(registration.firstname);
            account.setLastName(registration.lastname);
            profile.setName(account.getFullName());
            account.save();
            final AccountEmailModel accountEmail = new AccountEmailModel(account, registration.email);
            accountEmail.save();

            jedis.select(0);
            jedis.set(String.format("accountvalidation.%s", validationKey),
                    String.format("%d@%s", accountEmail.getId(), account.getUid().toString()));
            // Expire in 12h
            jedis.expire(String.format("accountvalidation.%s", validationKey), 43200);
            jedis.close();

            final SendGrid.Email email = SendMail.createEmail();
            email.addTo(accountEmail.getEmail());
            email.setSubject(Messages.get("REGISTRATION.EMAIL.SUBJECT"));
            email.setHtml(RegistrationValidationMailView.render(
                    controllers.system.routes.RegistrationController
                            .GET_RegistrationValidation(new UIDBinder(validationKey))
                            .absoluteURL(request()._underlyingHeader()))
                    .body());

            try {
                SendMail.sendEmail(email);
            } catch (SendGridException e) {
                e.printStackTrace();
            }
            flash("success", Messages.get("REGISTRATION.FLASH.SUCCESS"));
            return redirect(controllers.system.routes.AuthenticationController.GET_Authentication());
        }
        return badRequest(BaseApplicationView.render(null, formRegistration));
    }

    /**
     * Validate the account associated to the validation uid.
     *
     * @param uid Uid of the request
     * @return Redirection to authentication page
     * @since 15.10
     */
    public Result GET_RegistrationValidation(UIDBinder uid) {
        final RedisModule redisModule = Play.application().injector().instanceOf(RedisModule.class);
        final Jedis jedis = redisModule.getConnection();
        final String redisKey = String.format("accountvalidation.%s", uid.toString());
        String[] dataArray = null;

        jedis.select(0);
        final String redisData = jedis.get(redisKey);
        if (redisData != null) {
            dataArray = redisData.split("@");
        }

        if (dataArray == null || dataArray.length != 2) {
            jedis.close();
            flash("danger", Messages.get("REGISTRATION.VALIDATION.FLASH.INVALID"));
            return redirect(controllers.system.routes.AuthenticationController.GET_Authentication());
        }

        final Long accountEmailId = Long.parseLong(dataArray[0]);
        final String accountUid = dataArray[1];

        jedis.del(redisKey);
        jedis.close();

        final AccountModel account = AccountModel.find.where().like("uid", accountUid).findUnique();
        if (account != null) {
            account.setStatus(AccountStatus.OK);
            for (final AccountEmailModel email : account.getEmails()) {
                if (email.getId().compareTo(accountEmailId) == 0) {
                    email.setValidated(true);
                    email.save();
                    break;
                }
            }
            // getPrimaryEmail set the primary email if none exist.
            account.getPrimaryEmail();
            account.save();

            ActorHelper.indexModel(this.searchEngineActor, account);

            flash("success", Messages.get("REGISTRATION.VALIDATION.FLASH.SUCCESS"));
        }

        return redirect(controllers.system.routes.AuthenticationController.GET_Authentication());
    }
}
