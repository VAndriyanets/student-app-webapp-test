/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.system;

import controllers.routes;
import forms.system.AuthenticationForm;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import toolbox.form.ExtendedForm;
import views.html.AuthenticationView;

/**
 * AuthenticationController.
 *
 * @author Thibault Meyer
 * @author Jean-Pierre Boudic
 * @version 15.10
 * @since 15.10
 */
public class AuthenticationController extends Controller {

    /**
     * Max authentication fail
     */
    private final static int MAX_TRY_AUTHFAIL = 3;

    /**
     * Count authentication failed and trigger Captcha when
     * authentication failed count = maxAuthFail
     *
     * @param incError Is the error counter must be incremented?
     * @return Must be show Captcha
     * @see #MAX_TRY_AUTHFAIL
     * @since 15.10
     */
    private boolean showCaptcha(final boolean incError) {
        int auth_failed = Integer.parseInt(session().getOrDefault("auth_failed", "0"));
        if (incError) {
            auth_failed += 1;
            session().put("auth_failed", String.valueOf(auth_failed));
        }
        return auth_failed >= AuthenticationController.MAX_TRY_AUTHFAIL;
    }

    /**
     * Show the authentication page.
     *
     * @return The authentication page
     * @since 15.10
     */
    @AddCSRFToken
    public Result GET_Authentication() {
        return ok(AuthenticationView.render(null, this.showCaptcha(false)));
    }

    /**
     * Process the submitted authentication form.
     *
     * @return Redirection to home page in case of success, otherwise, bad request authentication page
     * @since 15.10
     */
    @RequireCSRFCheck
    public Result POST_Authentication() {
        final ExtendedForm<AuthenticationForm, Boolean> formAuthentication = ExtendedForm.form(AuthenticationForm.class, this.showCaptcha(false)).bindFromRequest();
        if (!formAuthentication.hasErrors()) {
            final AuthenticationForm authFormData = formAuthentication.get();
            switch (authFormData.getAccount().getStatus()) {
                case OK:
                    ctx().changeLang(authFormData.getAccount().getLang());
                    authFormData.getAccount().updateLastLogin(true);
                    session().put("uid", authFormData.getAccount().getUidAsString());
                    return redirect(authFormData.redirect);
                case NEED_VALIDATION:
                    flash("danger", Messages.get("AUTHENTICATION.FLASH.ACCOUNT_NOT_VALIDATED"));
                    break;
                case FROZEN:
                    flash("danger", Messages.get("AUTHENTICATION.FLASH.ACCOUNT_FROZEN"));
                    break;
            }
            return Results.redirect(controllers.system.routes.AuthenticationController.GET_Authentication());
        }
        return badRequest(AuthenticationView.render(formAuthentication, this.showCaptcha(true)));
    }

    /**
     * Disconnect the current authenticated user.
     *
     * @return Redirection to the authentication page
     * @since 15.10
     */
    public Result GET_Disconnect() {
        if (session().containsKey("uid")) {
            session().clear();
            flash("success", Messages.get("AUTHENTICATION.FLASH.DISCONNECTED"));
        }
        return redirect(routes.BaseApplicationController.GET_BaseApplication());
    }
}
