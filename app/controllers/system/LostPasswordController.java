/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.system;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import controllers.routes;
import forms.system.LostPasswordForm;
import forms.system.ResetPasswordForm;
import models.AccountEmailModel;
import models.AccountModel;
import modules.RedisModule;
import play.Play;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import redis.clients.jedis.Jedis;
import toolbox.SendMail;
import toolbox.play.UIDBinder;
import views.html.LostPasswordView;
import views.html.ResetPasswordView;
import views.html.mailview.LostPasswordMailView;

import java.util.UUID;

/**
 * LostPasswordController.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
public class LostPasswordController extends Controller {

    /**
     * Show the lost password page where user can
     * enter email address.
     *
     * @return The lost password form
     * @since 15.10
     */
    @AddCSRFToken
    public Result GET_LostPassword() {
        if (flash().containsKey("success-lost-password")) {
            flash().remove("success-lost-password");
            return ok(LostPasswordView.render(null, true));
        }
        return ok(LostPasswordView.render(null, false));
    }

    /**
     * Process the submitted lost password form.
     *
     * @return Redirection to authentication page
     * @since 15.10
     */
    @RequireCSRFCheck
    public Result POST_LostPassword() {
        final Form<LostPasswordForm> formLostPassword = Form.form(LostPasswordForm.class).bindFromRequest();
        if (!formLostPassword.hasErrors()) {
            final LostPasswordForm lostPasswordFormData = formLostPassword.get();
            final String resetKey = UUID.randomUUID().toString();
            final AccountEmailModel accountEmail = AccountEmailModel.find.where().like("email", lostPasswordFormData.email).findUnique();

            if (accountEmail != null) {
                AccountModel account = accountEmail.getAccount();
                final RedisModule redisModule = Play.application().injector().instanceOf(RedisModule.class);
                final Jedis jedis = redisModule.getConnection();
                jedis.select(0);
                jedis.set(String.format("lostpassword.%s", resetKey), account.getUid().toString());
                // Key expire in 1 day.
                jedis.expire(String.format("lostpassword.%s", resetKey), 86400);
                jedis.close();

                final SendGrid.Email email = SendMail.createEmail();
                email.addTo(accountEmail.getEmail());
                email.setSubject(Messages.get("LOSTPASSWORD.EMAIL.SUBJECT"));
                email.setHtml(LostPasswordMailView.render(
                        controllers.system.routes.LostPasswordController
                                .GET_ResetPassword(new UIDBinder(resetKey))
                                .absoluteURL(request()._underlyingHeader()))
                        .body());
                try {
                    SendMail.sendEmail(email);
                } catch (SendGridException e) {
                    e.printStackTrace();
                }
            }
            flash("success-lost-password", "ok");
            return redirect(controllers.system.routes.LostPasswordController.GET_LostPassword());
        }
        return badRequest(LostPasswordView.render(formLostPassword, false));
    }

    /**
     * Show the reset password page for a given reset request.
     *
     * @param uid Uid of the request
     * @return Redirection to authentication page
     * @since 15.10
     */
    @AddCSRFToken
    public Result GET_ResetPassword(UIDBinder uid) {
        final String resetKey = String.format("lostpassword.%s", uid.toString());
        final RedisModule redisModule = Play.application().injector().instanceOf(RedisModule.class);
        final Jedis jedis = redisModule.getConnection();

        jedis.select(0);
        String account = jedis.get(resetKey);
        jedis.close();

        if (account == null) {
            return notFound(views.html.systemview.error404.render());
        }
        return ok(ResetPasswordView.render(uid, null));
    }

    /**
     * Process the reset password for a given reset request.
     *
     * @param uid Uid of the request
     * @return Redirection to authentication page
     * @since 15.10
     */
    @RequireCSRFCheck
    public Result POST_SetNewPassword(UIDBinder uid) {
        final String resetKey = String.format("lostpassword.%s", uid.toString());
        final RedisModule redisModule = Play.application().injector().instanceOf(RedisModule.class);
        Jedis jedis = redisModule.getConnection();

        // Get the account UID in Redis.
        jedis.select(0);
        String accountUid = jedis.get(resetKey);
        jedis.close();

        if (accountUid == null) {
            return notFound(views.html.systemview.error404.render());
        }

        final Form<ResetPasswordForm> formResetPassword = Form.form(ResetPasswordForm.class).bindFromRequest();
        if (!formResetPassword.hasErrors()) {
            final ResetPasswordForm resetPassword = formResetPassword.get();
            final AccountModel hAccount = AccountModel.find.where().like("uid", accountUid).findUnique();
            if (hAccount == null) {
                // The account does not exist in database. Delete the entry in Redis.
                jedis = redisModule.getConnection();
                jedis.select(0);
                jedis.del(resetKey);
                jedis.close();
                return notFound(views.html.systemview.error404.render());
            }

            // Update the password
            hAccount.setPassword(resetPassword.password);
            hAccount.update();

            // Delete the entry in Redis.
            jedis = redisModule.getConnection();
            jedis.select(0);
            jedis.del(resetKey);
            jedis.close();

            flash("success", Messages.get("RESETPASSWORD.SUCCESS"));
            return redirect(routes.BaseApplicationController.GET_BaseApplication());
        }

        return ok(ResetPasswordView.render(uid, formResetPassword));
    }
}
