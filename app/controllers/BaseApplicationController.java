/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers;

import actions.SessionOptional;
import models.AccountEmailModel;
import models.AccountModel;
import modules.RedisModule;
import play.Play;
import play.Routes;
import play.filters.csrf.AddCSRFToken;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.With;
import redis.clients.jedis.Jedis;
import toolbox.helper.SessionHelper;
import toolbox.play.FlashScopeForwarder;
import views.html.BaseApplicationView;

/**
 * BaseApplicationController.
 *
 * @author Thibault Meyer
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class BaseApplicationController extends Controller {

    /**
     * The Base application show the registration and authentication form if
     * user is not already authenticated. Otherwise, user will be redirected
     * to the right realm or to the realm selector.
     *
     * @return The selected realm page or the realm selector page or the login page
     * @since 15.10
     */
    @AddCSRFToken
    @With(SessionOptional.class)
    public Result GET_BaseApplication() {
        final AccountModel authAccount = SessionHelper.getAccount();
        if (authAccount == null) {
            return ok(BaseApplicationView.render(null, null));
        } else {
            FlashScopeForwarder.flashForwarder(flash());
            if (authAccount.getRealmCount() > 0) {
                return redirect(controllers.realm.routes.HomeRealmController.GET_HomePage(authAccount.getRealms().get(0)));
            }
            if (accountHasRealmInvite(authAccount)) {
                return redirect(controllers.system.routes.RealmSelectorController.GET_RealmSelectorInvite());
            }
            return ok(views.html.RealmSelectorView.render(null, null));
        }
    }

    /**
     * Check if account has invitation in stand-by, invited previously as registration.
     *
     * @param account The account to check
     * @return {@code true} if has invite at less one invite, otherwise, {@code false}
     * @since 15.10
     */
    private boolean accountHasRealmInvite(final AccountModel account) {
        final Jedis jedis = Play.application().injector().instanceOf(RedisModule.class).getConnection();
        jedis.select(0);

        for (AccountEmailModel email : account.getEmails()) {
            if (jedis.exists(String.format("account.pre.realm.%s", email.getEmail()))) {
                if (!jedis.zrangeByScore(String.format("account.pre.realm.%s", email.getEmail()), System.currentTimeMillis() / 1000, Double.POSITIVE_INFINITY).isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    public Result jsRoutes() {
        response().setContentType("text/javascript");
        return ok(Routes.javascriptRouter("appRoutes",
                controllers.realm.routes.javascript.VoteController.POST_voteUp(),
                controllers.realm.routes.javascript.VoteController.POST_voteDown(),
                controllers.realm.routes.javascript.VoteController.POST_voteUpRollback(),
                controllers.realm.routes.javascript.VoteController.POST_voteDownRollback(),
                controllers.realm.routes.javascript.CommentsController.GET_comments(),
                controllers.realm.routes.javascript.CommentsController.GET_allComments(),
                controllers.realm.routes.javascript.CommentsController.POST_comment(),
                controllers.realm.routes.javascript.CommentsController.DELETE_comment(),
                controllers.realm.group.routes.javascript.GroupPageController.POST_DeletePost()));
    }
}
