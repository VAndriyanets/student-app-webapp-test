package controllers.card;

import actions.SessionRequired;
import forms.card.CardAddForm;
import forms.card.CardDeleteForm;
import models.AccountModel;
import models.CardModel;
import models.CardTypeModel;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.helper.SessionHelper;
import views.html.cardview.AccountCardView;

import java.util.List;

/**
 * Created by vitaly.andriyanets on 14-Jun-16.
 */
public class AccountCardController extends Controller {

    /**
     * Show the cards page.
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_AccountCard() {
        final AccountModel account = SessionHelper.getAccount();
        final List<CardTypeModel> cardTypes = CardTypeModel.find.all();
        return ok(AccountCardView.render(account.getCards(), cardTypes, null));
    }

    /**
     * Add new account card.
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result POST_AddCard() {
        final AccountModel account = SessionHelper.getAccount();
        final List<CardTypeModel> cardTypes = CardTypeModel.find.all();

        final Form<CardAddForm> addForm = Form.form(CardAddForm.class).bindFromRequest(request());
        if (addForm.hasErrors()) {
            return badRequest(AccountCardView.render(account.getCards(), cardTypes, addForm));
        }

        final CardAddForm cardForm = processForm(addForm.get());

        final CardModel cardModel = new CardModel(account);
        cardModel.setCardNumber(cardForm.getCardNumber());
        cardModel.setCardType(cardForm.getCardType());

        cardModel.save();

        return redirect(routes.AccountCardController.GET_AccountCard());
    }


    /**
     * Process delete card
     *
     * @return Redirection to the cards page
     */
    @Security.Authenticated(SessionRequired.class)
    @RequireCSRFCheck
    public Result POST_DeleteCard() {
        final Form<CardDeleteForm> cardDeleteForm = Form.form(CardDeleteForm.class).bindFromRequest(request());
        if (!cardDeleteForm.hasErrors()) {
            final CardDeleteForm cardDelete = cardDeleteForm.get();
            final AccountModel account = SessionHelper.getAccount();
            CardModel cardModel = CardModel.find.where().eq("account.id", account.getId()).eq("id", cardDelete.getCardId()).findUnique();
            if (cardModel != null) {
                cardModel.delete();
            }
        }
        return redirect(routes.AccountCardController.GET_AccountCard());
    }

    /**
     * Initialize all missed fields.
     *
     * @param cardAddForm The current form of new card
     * @return Form with filled CardType
     */
    private CardAddForm processForm(CardAddForm cardAddForm) {
        CardTypeModel cardType = CardTypeModel.find.byId(cardAddForm.getCardType().getId());
        cardAddForm.setCardType(cardType);
        return cardAddForm;
    }

}
