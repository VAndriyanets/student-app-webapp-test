package controllers.account;

import actions.SessionRequired;
import com.google.inject.Singleton;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import forms.account.contact.*;
import models.AccountEmailModel;
import models.AccountModel;
import models.AccountPhoneModel;
import modules.RedisModule;
import play.Play;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import redis.clients.jedis.Jedis;
import toolbox.SMSSender;
import toolbox.SendMail;
import toolbox.TokenGeneratorUtils;
import toolbox.helper.SessionHelper;
import views.html.accountview.AccountContactView;

/**
 * AccountContactController.
 *
 * @author Pierre Adam
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
@Singleton
public class AccountContactController extends Controller {

    /**
     * Show the contact page.
     *
     * @return The contact page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_AccountContact() {
        final AccountModel account = SessionHelper.getAccount();
        account.getPrimaryEmail();
        account.getPrimaryPhone();
        return ok(AccountContactView.render(account, null));
    }

    //==== EMAIL SECTION ====

    /**
     * Process add new email (submitted email)
     *
     * @return Redirection to the base contact page
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_AddEmailAccountContact() {
        final AccountModel account = SessionHelper.getAccount();
        final Form<AddEmailForm> form = Form.form(AddEmailForm.class).bindFromRequest(request());
        if (form.hasErrors()) {
            return badRequest(AccountContactView.render(account, form));
        }

        final AddEmailForm emailForm = form.get();

        final AccountEmailModel aem = new AccountEmailModel(account, emailForm.email);
        aem.save();

        sendTokenByEmail(account, aem);

        return redirect(routes.AccountContactController.GET_AccountContact());
    }

    /**
     * Validate an email already saved but not validated
     *
     * @return Redirection to the base contact page
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_ValidEmailAccountContact(){
        final Form<ManageEmailForm> form = Form.form(ManageEmailForm.class).bindFromRequest(request());
        if(!form.hasErrors()){
            final AccountModel account = SessionHelper.getAccount();
            final ManageEmailForm emailForm = form.get();
            final AccountEmailModel aem = AccountEmailModel.find.where()
                    .eq("email", emailForm.email)
                    .eq("account", account).findUnique();
            if(aem != null && !aem.isValidated()){
                sendTokenByEmail(account, aem);
            }
        }
        return redirect(routes.AccountContactController.GET_AccountContact());
    }

    /**
     * Generate a token add send this token by email.<br>
     * Used in {@link #POST_AddEmailAccountContact()} and {@link #POST_ValidEmailAccountContact()}
     *
     * @param account The user account
     * @param aem The email instance
     * @since 15.10
     */
    public void sendTokenByEmail(final AccountModel account, final AccountEmailModel aem){
        final String token = TokenGeneratorUtils.generateValidationToken();
        final String redisKey = String.format("valid-token.%s", aem.getEmail());

        final RedisModule redisModule = Play.application().injector().instanceOf(RedisModule.class);
        final Jedis jedis = redisModule.getConnection();
        if (jedis.isConnected()) {
            jedis.select(0);
            jedis.set(redisKey, token);
            jedis.expire(redisKey, 1800);
            jedis.close();
            flash("c-flash-valid-token", aem.getEmail());
        }

        final SendGrid.Email email = SendMail.createEmail();
        email.addTo(aem.getEmail(), account.getFullName());
        email.setSubject(Messages.get("ACCOUNT_SETTINGS.CONTACT.CONFIRM.MAIL.SUBJECT"));
        email.setHtml(views.html.mailview.ConfirmAccountContactMailView.render(token).body());
        try {
            SendMail.sendEmail(email);
        } catch (SendGridException e) {
            e.printStackTrace();
        }
    }

    /**
     * Process change primary email to submitted email
     *
     * @return Redirection to the base contact page
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_ChangePrimaryEmailAccountContact() {
        final Form<ManageEmailForm> form = Form.form(ManageEmailForm.class).bindFromRequest(request());
        if (!form.hasErrors()) {
            final ManageEmailForm emailForm = form.get();
            final AccountModel account = SessionHelper.getAccount();

            final AccountEmailModel email = AccountEmailModel.find.where()
                    .eq("email", emailForm.email)
                    .eq("account", account).findUnique();

            if (email != null && email.isValidated()) {
                for (AccountEmailModel aem : account.getEmails()) {
                    if (!aem.equals(email) && aem.isPrimary()) {
                        aem.setPrimary(false);
                        aem.update();
                    }
                }
                email.setPrimary(true);
                email.update();
            } else {
                flash("danger", Messages.get("ACCOUNT_SETTINGS.CONTACT.FLASH.MARK.FAVORITE.KO"));
            }
        }
        return redirect(routes.AccountContactController.GET_AccountContact());
    }

    /**
     * Process delete email submitted
     *
     * @return Redirection to the base contact page
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_DeleteEmailAccountContact() {
        final Form<ManageEmailForm> form = Form.form(ManageEmailForm.class).bindFromRequest(request());

        if (!form.hasErrors()) {
            final ManageEmailForm emailForm = form.get();
            final AccountModel account = SessionHelper.getAccount();

            if (account.getEmails().size() > 1) {
                final AccountEmailModel email = AccountEmailModel.find.where()
                        .eq("email", emailForm.email)
                        .eq("account", account).findUnique();
                if (email != null && !email.isPrimary()) {
                    email.delete();
                }
            }
        }
        return redirect(routes.AccountContactController.GET_AccountContact());
    }

    //==== PHONE SECTION ====

    /**
     * Process add new phone (submitted phone)
     *
     * @return Redirection to the base contact page
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_AddPhoneAccountContact() {
        final AccountModel account = SessionHelper.getAccount();
        final Form<AddPhoneForm> form = Form.form(AddPhoneForm.class).bindFromRequest(request());
        if (form.hasErrors()) {
            return badRequest(AccountContactView.render(account, form));
        }

        final AddPhoneForm phoneForm = form.get();

        final AccountPhoneModel apm = new AccountPhoneModel(account, phoneForm.phone, phoneForm.country);
        apm.save();

        sendTokenBySMS(account, apm);

        return redirect(routes.AccountContactController.GET_AccountContact());
    }

    /**
     * Validate a phone already saved but not validated
     *
     * @return Redirection to the base contact page
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_ValidPhoneAccountContact(){
        final Form<ManagePhoneForm> form = Form.form(ManagePhoneForm.class).bindFromRequest(request());
        if(!form.hasErrors()){
            final AccountModel account = SessionHelper.getAccount();
            final ManagePhoneForm phoneForm = form.get();
            final AccountPhoneModel apm = AccountPhoneModel.find.where()
                    .eq("phone", phoneForm.phone)
                    .eq("account", account).findUnique();
            if(apm != null && !apm.isValidated()){
                sendTokenBySMS(account, apm);
            }
        }
        return redirect(routes.AccountContactController.GET_AccountContact());
    }

    /**
     * Generate a token add send this token by sms.<br>
     * Used in {@link #POST_AddPhoneAccountContact()} and {@link #POST_ValidPhoneAccountContact()}
     *
     * @param account The user account
     * @param apm The phone instance
     * @since 15.10
     */
    public void sendTokenBySMS(final AccountModel account, final AccountPhoneModel apm){
        final String token = TokenGeneratorUtils.generateValidationToken();
        final String redisKey = String.format("valid-token.%s", apm.getPhone());

        final RedisModule redisModule = Play.application().injector().instanceOf(RedisModule.class);
        final Jedis jedis = redisModule.getConnection();
        if (jedis.isConnected()) {
            jedis.select(0);
            jedis.set(redisKey, token);
            jedis.expire(redisKey, 1800);
            jedis.close();
            flash("c-flash-valid-token", apm.getPhone());
        }

        final String smsMessage = Messages.get("ACCOUNT_SETTINGS.CONTACT.CONFIRM.SMS.MESSAGE", token);

        try {
            SMSSender.send(apm.getPhone(), smsMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Process change primary phone to submitted phone
     *
     * @return Redirection to the base contact page
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_ChangePrimaryPhoneAccountContact() {
        final Form<ManagePhoneForm> form = Form.form(ManagePhoneForm.class).bindFromRequest(request());
        if (!form.hasErrors()) {
            final ManagePhoneForm phoneForm = form.get();
            final AccountModel account = SessionHelper.getAccount();

            final AccountPhoneModel phone = AccountPhoneModel.find.where()
                    .eq("phone", phoneForm.phone)
                    .eq("account", account).findUnique();

            if (phone != null && phone.isValidated()) {
                for (AccountPhoneModel apm : account.getPhones()) {
                    if (!apm.equals(phone) && apm.isPrimary()) {
                        apm.setPrimary(false);
                        apm.update();
                    }
                }
                phone.setPrimary(true);
                phone.update();
            } else {
                flash("danger", Messages.get("ACCOUNT_SETTINGS.CONTACT.FLASH.MARK.FAVORITE.KO"));
            }
        }
        return redirect(routes.AccountContactController.GET_AccountContact());
    }

    /**
     * Process delete phone submitted
     *
     * @return Redirection to the base contact page
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_DeletePhoneAccountContact() {
        final Form<ManagePhoneForm> form = Form.form(ManagePhoneForm.class).bindFromRequest(request());

        if (!form.hasErrors()) {
            final ManagePhoneForm phoneForm = form.get();
            final AccountModel account = SessionHelper.getAccount();

            if (account.getPhones().size() > 1) {
                final AccountPhoneModel phone = AccountPhoneModel.find.where()
                        .eq("phone", phoneForm.phone)
                        .eq("account", account).findUnique();
                if (phone != null && !phone.isPrimary()) {
                    phone.delete();
                }
            }
        }
        return redirect(routes.AccountContactController.GET_AccountContact());
    }

    //==== TOKEN VALIDATION SECTION ====

    /**
     * Validate a contact submitted.
     *
     * @return Redirection to the base contact page
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_ValidContactTokenAccountContact() {
        final AccountModel account = SessionHelper.getAccount();
        final Form<ValidContactForm> form = Form.form(ValidContactForm.class).bindFromRequest(request());

        if (!form.hasErrors()) {
            final ValidContactForm contactForm = form.get();
            final String expectedRedisKey = String.format("valid-token.%s", contactForm.contact);

            final RedisModule redisModule = Play.application().injector().instanceOf(RedisModule.class);
            final Jedis jedis = redisModule.getConnection();

            if (jedis.isConnected()) {
                jedis.select(0);
                final String redisToken = jedis.exists(expectedRedisKey) ? jedis.get(expectedRedisKey) : null;
                jedis.close();
                if (contactForm.token.equals(redisToken)) {
                    if (contactForm.contact.contains("@")) {
                        final AccountEmailModel email = AccountEmailModel.find.where()
                                .eq("email", contactForm.contact)
                                .eq("account", account).findUnique();
                        if (email != null) {
                            email.setValidated(true);
                            email.update();
                        }
                    } else {
                        final AccountPhoneModel phone = AccountPhoneModel.find.where()
                                .eq("phone", contactForm.contact)
                                .eq("account", account).findUnique();
                        if (phone != null) {
                            phone.setValidated(true);
                            phone.update();
                        }
                    }
                    flash("success", Messages.get("ACCOUNT_SETTINGS.CONTACT.FLASH.VALIDATE.OK"));
                } else {
                    flash("danger", Messages.get("ACCOUNT_SETTINGS.CONTACT.FLASH.VALIDATE.KO"));
                }
            }
        }
        return redirect(routes.AccountContactController.GET_AccountContact());
    }
}
