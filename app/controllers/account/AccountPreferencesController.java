package controllers.account;

import actions.SessionRequired;
import forms.account.AccountPreferencesForm;
import models.AccountModel;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.helper.SessionHelper;
import views.html.accountview.AccountPreferencesView;

/**
 * AccountPreferencesController.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
public class AccountPreferencesController extends Controller {

    /**
     * Show the preferences page.
     *
     * @return The preferences page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_AccountPreferences() {
        final AccountModel account = SessionHelper.getAccount();
        final AccountPreferencesForm accountPreferencesForm = new AccountPreferencesForm(account);
        final Form<AccountPreferencesForm> formAccountPreferences = Form.form(AccountPreferencesForm.class).fill(accountPreferencesForm);


        return ok(AccountPreferencesView.render(formAccountPreferences));
    }

    /**
     * Process the submitted base preferences form.
     *
     * @return Redirection to profile page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result POST_AccountPreferences() {
        final AccountModel account = SessionHelper.getAccount();
        final Form<AccountPreferencesForm> formAccountPreferences = Form.form(AccountPreferencesForm.class).bindFromRequest(request());

        if (formAccountPreferences.hasErrors()) {
            return badRequest(AccountPreferencesView.render(formAccountPreferences));
        }
        AccountPreferencesForm accountPreferencesForm = formAccountPreferences.get();
        account.setLang(accountPreferencesForm.lang);
        account.setTimezone(accountPreferencesForm.getTimezoneName());
        account.save();
        ctx().changeLang(accountPreferencesForm.lang);

        return redirect(routes.AccountPreferencesController.GET_AccountPreferences());
    }
}
