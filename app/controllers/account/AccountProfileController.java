/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.account;

import actions.SessionRequired;
import actors.SearchEngineActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import forms.account.AccountProfileForm;
import models.AccountModel;
import models.FacebookDataModel;
import models.FacebookUserModel;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.Play;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.i18n.Messages;
import play.libs.F.Promise;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.helper.ActorHelper;
import toolbox.helper.SessionHelper;
import views.html.accountview.AccountProfileView;

import java.util.LinkedList;
import java.util.List;

/**
 * AccountProfileController.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
@Singleton
public class AccountProfileController extends Controller {

    @Inject WSClient ws;

    /**
     * Handle to SearchEngine actor reference.
     */
    private final ActorRef searchEngineActor;

    /**
     * Build a simple instance.
     *
     * @param actorSystem The actor system handle
     */
    @Inject
    public AccountProfileController(ActorSystem actorSystem) {
        this.searchEngineActor = actorSystem.actorOf(SearchEngineActor.props);
    }

    /**
     * Show the profile page.
     *
     * @return The profile page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_AccountProfile() {
        final AccountModel account = SessionHelper.getAccount();
        final AccountProfileForm accountProfileForm = new AccountProfileForm(account);
        final Form<AccountProfileForm> formAccountProfile = Form.form(AccountProfileForm.class).fill(accountProfileForm);

        return ok(AccountProfileView.render(formAccountProfile));
    }

    /**
     * Process the submitted base information form.
     *
     * @return Redirection to profile page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result POST_AccountProfile() {
        final AccountModel account = SessionHelper.getAccount();
        final Form<AccountProfileForm> formAccountProfile = Form.form(AccountProfileForm.class).bindFromRequest(request());

        AccountProfileForm accountProfileForm = formAccountProfile.get();

        if(accountProfileForm.fbToken.isEmpty()) {
            if (formAccountProfile.hasErrors()) {
                return badRequest(AccountProfileView.render(formAccountProfile));
            }
            account.setFirstName(accountProfileForm.firstname);
            account.setLastName(accountProfileForm.lastname);
            account.save();

            // Index account in the search engine
            ActorHelper.indexModel(this.searchEngineActor, account);

            return redirect(routes.AccountProfileController.GET_AccountProfile());
        } else { // facebook sync
            try {
                processFBSync(accountProfileForm, account);
            } catch (Exception e) {
                Logger.error("Facebook sync error", e);
                flash("danger", Messages.get("ACCOUNT_SETTINGS.PROFILE.FORM.FB.SYNC.FAIL.MESSAGE"));
            }

            // Index account in the search engine
            ActorHelper.indexModel(this.searchEngineActor, account);

            return ok(AccountProfileView.render(formAccountProfile));
        }

    }

    private void processFBSync(AccountProfileForm accountProfileForm, AccountModel account) {
        WSRequest request = ws.url(Play.application().configuration().getString("facebook.sync.url"));
        request.setQueryParameter("access_token", accountProfileForm.fbToken);
        Promise<JsonNode> responsePromise = request.get().map(response -> {
            return response.asJson();
        });

        long requestTimeout = Play.application().configuration().getLong("facebook.request.timeout", 2000l);
        JsonNode jsonResponse = responsePromise.get(requestTimeout);

        // id
        String fbId = jsonResponse.get("id").asText();

        // marital status
        JsonNode relationshipStatus = jsonResponse.get("relationship_status");
        String maritalStatus = relationshipStatus == null ? null : relationshipStatus.asText();

        // friends
        List<FacebookUserModel> friendsList = new LinkedList<>();
        JsonNode friends = jsonResponse.get("friends");
        if (friends != null) {
            boolean hasFriendsNextPage = true;
            do {
                JsonNode friendsData = friends.get("data");

                if (friendsData.isArray()) {
                    for (JsonNode node : friendsData) {
                        String id = node.path("id").asText();
                        String name = node.path("name").asText();
                        FacebookUserModel friend = new FacebookUserModel();
                        friend.setFbid(id);
                        friend.setName(name);
                        friendsList.add(friend);
                    }
                }
                JsonNode friendsPaging = friends.get("paging");
                JsonNode friendsPagingNext = friendsPaging == null? null : friendsPaging.get("next");
                if (friendsPagingNext != null) {
                    String nextPageURL = friendsPagingNext.asText();
                    Promise<JsonNode> promise = ws.url(nextPageURL).get().map(response -> {
                        return response.asJson();
                    });
                    friends = promise.get(requestTimeout);
                } else {
                    hasFriendsNextPage = false;
                }

            } while (hasFriendsNextPage);

        }

        // picture
        JsonNode pictureData = jsonResponse.get("picture").get("data");
        boolean isSilhouette = pictureData.get("is_silhouette").asBoolean();
        String pictureUrl = isSilhouette? null : pictureData.get("url").asText();

        FacebookDataModel facebookDataModel = account.getFacebookData();
        if (facebookDataModel == null) {
            facebookDataModel = new FacebookDataModel();
        }
        facebookDataModel.setAccount(account);
        facebookDataModel.setFbid(fbId);
        facebookDataModel.setMaritalStatus(maritalStatus);
        facebookDataModel.setPictureURL(pictureUrl);
        facebookDataModel.setFriends(friendsList);
        facebookDataModel.save();
    }
}
