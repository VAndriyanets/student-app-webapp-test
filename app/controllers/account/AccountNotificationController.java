package controllers.account;

import actions.SessionRequired;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.accountview.AccountNotificationView;

/**
 * AccountNotificationController.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
public class AccountNotificationController extends Controller {

    /**
     * Show the notification page.
     *
     * @return The notification page
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    public Result GET_AccountNotification() {

        return ok(AccountNotificationView.render());
    }
}
