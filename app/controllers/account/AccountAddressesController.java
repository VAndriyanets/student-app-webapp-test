package controllers.account;

import actions.SessionRequired;
import forms.account.AccountAddAddressForm;
import forms.account.AccountManageAddressForm;
import models.AccountModel;
import models.AccountPostalAddressModel;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.enumeration.CountryCode;
import toolbox.helper.SessionHelper;
import views.html.accountview.AccountAddressesView;

import java.util.UUID;

/**
 * AccountAddressesController.
 *
 * @author Pierre Adam
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class AccountAddressesController extends Controller {

    /**
     * Show the addresses page.
     *
     * @return The addresses page
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    @AddCSRFToken
    public Result GET_AccountAddresses() {
        final AccountModel account = SessionHelper.getAccount();
        account.getPrimaryAddress();
        return ok(AccountAddressesView.render(account.getAddresses(), null));
    }

    /**
     * Process new address submitted.
     *
     * @return Redirection to the addresses page if processed, otherwise, show form error
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    @RequireCSRFCheck
    public Result POST_AddAccountAddresses(){
        final AccountModel account = SessionHelper.getAccount();
        final Form<AccountAddAddressForm> addForm = Form.form(AccountAddAddressForm.class).bindFromRequest(request());
        if(addForm.hasErrors()){
            return badRequest(AccountAddressesView.render(account.getAddresses(), addForm));
        }

        final AccountAddAddressForm addressForm = addForm.get();

        final AccountPostalAddressModel accountPostalAddressModel = new AccountPostalAddressModel(account);

        accountPostalAddressModel.setDestName(addressForm.destName);
        accountPostalAddressModel.setStreet1(addressForm.street1);
        accountPostalAddressModel.setStreet2(addressForm.street2);
        accountPostalAddressModel.setZipCode(addressForm.zipcode);
        accountPostalAddressModel.setCity(addressForm.city);
        accountPostalAddressModel.setCountry(CountryCode.valueOf(addressForm.country));

        accountPostalAddressModel.save();

        return redirect(routes.AccountAddressesController.GET_AccountAddresses());
    }

    /**
     * Process change primary address to submitted address.
     *
     * @return Redirection to the addresses page
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    @RequireCSRFCheck
    public Result POST_ChangePrimaryAccountAddress(){
        final Form<AccountManageAddressForm> manageForm = Form.form(AccountManageAddressForm.class).bindFromRequest(request());
        if(!manageForm.hasErrors()){
            final AccountManageAddressForm addressForm = manageForm.get();
            final AccountModel account = SessionHelper.getAccount();
            for(AccountPostalAddressModel apam : account.getAddresses()){
                if(apam.getUidAsString().equals(addressForm.addressUid)){
                    apam.setPrimary(true);
                    apam.save();
                }else{
                    if(apam.isPrimary()){
                        apam.setPrimary(false);
                        apam.save();
                    }
                }
            }
        }
        return redirect(routes.AccountAddressesController.GET_AccountAddresses());
    }

    /**
     * Process delete address submitted address.
     *
     * @return Redirection to the addresses page
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    @RequireCSRFCheck
    public Result POST_DeleteAccountAddress(){
        final Form<AccountManageAddressForm> manageForm = Form.form(AccountManageAddressForm.class).bindFromRequest(request());
        if(!manageForm.hasErrors()){
            final AccountManageAddressForm addressForm = manageForm.get();
            final AccountModel account = SessionHelper.getAccount();

            if(account.getAddresses().size() > 1){
                AccountPostalAddressModel apam = AccountPostalAddressModel.find.where()
                        .eq("uid", UUID.fromString(addressForm.addressUid))
                        .eq("account", account)
                        .findUnique();
                if(apam != null && !apam.isPrimary()){
                    apam.delete();
                }
            }
        }
        return redirect(routes.AccountAddressesController.GET_AccountAddresses());
    }
}
