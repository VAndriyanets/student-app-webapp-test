package controllers.account;

import actions.SessionRequired;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.accountview.AccountBillingView;

/**
 * AccountBillingController.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
public class AccountBillingController extends Controller {

    /**
     * Show the billing page.
     *
     * @return The billing page
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    public Result GET_AccountBilling() {

        return ok(AccountBillingView.render());
    }
}
