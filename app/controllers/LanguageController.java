/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers;

import play.Play;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.Locale;

/**
 * LanguageController.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class LanguageController extends Controller {

    /**
     * Try to change the language of the site. If the langCode is
     * not found on the application.conf, the current language
     * will not be changed.
     *
     * @return Redirection to the previous page, otherwise, to home page
     * @since 15.10
     */
    public Result GET_ChangeLanguage(String langCode) {
        langCode = langCode.trim().toLowerCase(Locale.ENGLISH);
        if (Play.application().configuration().getStringList("play.i18n.langs").contains(langCode)) {
            ctx().changeLang(langCode);
        }
        final String redirectTo = request().getHeader("REFERER");
        if (redirectTo != null) {
            return temporaryRedirect(redirectTo);
        }
        return temporaryRedirect(controllers.routes.BaseApplicationController.GET_BaseApplication());
    }
}
