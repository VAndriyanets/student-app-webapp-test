/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.realm;

import actions.SessionRequired;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.Assets;
import forms.common.PaginationData;
import forms.realm.SearchRealmForm;
import models.RealmModel;
import models.S3FileModel;
import models.SearchRequestType;
import org.elasticsearch.action.search.SearchPhaseExecutionException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.cluster.block.ClusterBlockException;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.indices.IndexMissingException;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.jetbrains.annotations.NotNull;
import play.Play;
import play.data.Form;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.helper.ElasticSearchHelper;
import toolbox.helper.FormatHelper;
import toolbox.helper.SessionHelper;
import views.html.realmview.SearchRealmView;

import java.util.*;

/**
 * SearchRealmController.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class SearchRealmController extends Controller {


    public static final int SHORT_RESULT_PAGE_SIZE = 3;

    /**
     * Search object, persons or other objects on
     * the current realm.
     *
     * @return The search results page
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    public Result GET_Search(final RealmModel realm) {
        if (checkRealmAccess(realm))
            return redirect(controllers.routes.BaseApplicationController.GET_BaseApplication());
        final Form<SearchRealmForm> formSearch = Form.form(SearchRealmForm.class).bindFromRequest();
        if (!formSearch.hasErrors()) {
            final DefaultLogos logos = new DefaultLogos();
            final SearchRealmForm searchFormData = formSearch.get();
            final String searchQuery = getSearchQuery(realm, searchFormData.q);
            System.err.println(searchQuery);
            final Integer pageNumber;
            try (final Client client = ElasticSearchHelper.getClient()) {
                final List<SearchResultBlock> searchResults = new ArrayList<>();
                final SearchRequestType searchRequestType = searchFormData.toTypeEnum();
                final Integer pageSize;
                long total = 0;
                boolean showPagination = false;
                if (!SearchRequestType.ALL.equals(searchRequestType)) {
                    pageSize = Play.application().configuration().getInt("elasticsearch.resperpage", 25);
                    pageNumber = searchFormData.page != null ? searchFormData.page : 1;
                    showPagination = true;
                } else {
                    pageSize = SHORT_RESULT_PAGE_SIZE;
                    pageNumber = 1;
                }
                for (String searchType : searchRequestType.getTypes()) {
                    final List<Map<String, String>> searchResultList = new ArrayList<>();
                    final SearchResponse response = getSearchResponse(searchQuery, client, pageSize, pageNumber, searchType);
                    final SearchHits hits = response.getHits();
                    final long totalHits = total = hits.getTotalHits();
                    searchResults.add(new SearchResultBlock(totalHits > hits.getHits().length && !showPagination, searchType, searchResultList));
                    for (final SearchHit sh : hits.getHits()) {
                        searchResultList.add(processSearchHit(sh, logos, realm));
                    }
                }
                return ok(SearchRealmView.render(realm, formSearch, searchResults, new PaginationData(pageNumber, pageSize, total, showPagination)));
            } catch (NoNodeAvailableException | IndexMissingException ex) {
                ElasticSearchHelper.handleESError(ex);
                flash("danger", Messages.get("ERROR.SEARCH.OFFLINE"));
            } catch (SearchPhaseExecutionException ex) {
                ex.printStackTrace();
                flash("danger", Messages.get("ERROR.SEARCH.EXECUTION"));
            } catch (ClusterBlockException ignore) {
                // Don't care; cluster still loading data...
            }
            return ok(SearchRealmView.render(realm, formSearch, null, null));
        }
        return ok(SearchRealmView.render(realm, formSearch, null, null));
    }

    private boolean checkRealmAccess(final RealmModel realm) {
        if (!SessionHelper.getAccount().isEnrolledTo(realm)) {
            flash("danger", Messages.get("REALM.ERROR.NOT_ENROLLED"));
            return true;
        }
        return false;
    }

    private String getSearchQuery(final RealmModel realm, final String query) {
        String q = ElasticSearchHelper.formSearchQuery(query);
        return String.format("_realm:%d AND (%s)", realm.getId(), q);
    }

    private SearchResponse getSearchResponse(final String query,
                                             final Client client,
                                             final int pageSize,
                                             final int pageNumber,
                                             final String... types) {
        return ElasticSearchHelper.prepareSearch(client)
                .setTypes(types)
                .setQuery(QueryBuilders.queryStringQuery(query))
                .setFrom((pageNumber - 1) * pageSize)
                .setSize(pageSize)
                .setExplain(false)
                .execute()
                .actionGet();
    }


    /**
     * @param realm The current realm (binding by slug)
     * @return List of
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_AutocompleteSearch(final RealmModel realm) {
        if (checkRealmAccess(realm))
            return badRequest(Json.newObject().put("error_code", "403").put("error", "You have no access to this realm"));

        final Map<String, String[]> request = Http.Context.current().request().body().asFormUrlEncoded();

        String keyword = null;
        if (request != null && request.get("search").length == 1 && !request.get("search")[0].isEmpty()) {
            keyword = request.get("search")[0];
        }

        if (keyword == null) {
            return badRequest(Json.newObject().put("error_code", "400").put("error", "Bad request"));
        }

        final ArrayNode result = Json.newArray();
        final String searchQuery = getSearchQuery(realm, keyword);
        try (final Client client = ElasticSearchHelper.getClient()) {
            final Integer pageSize = Play.application().configuration().getInt("elasticsearch.resperpage", 25);
            final SearchResponse response = getSearchResponse(searchQuery, client, pageSize, 1, SearchRequestType.ALL.getTypes());
            final DefaultLogos logos = new DefaultLogos();
            for (final SearchHit sh : response.getHits().getHits()) {
                final Map<String, String> resItem = processSearchHit(sh, logos, realm);
                final ObjectNode resultItemJson = result.addObject();
                fillJson(resultItemJson, resItem);
            }
        } catch (NoNodeAvailableException | IndexMissingException ex) {
            ElasticSearchHelper.handleESError(ex);
            flash("danger", Messages.get("ERROR.SEARCH.OFFLINE"));
        } catch (SearchPhaseExecutionException ex) {
            ex.printStackTrace();
            flash("danger", Messages.get("ERROR.SEARCH.EXECUTION"));
        } catch (ClusterBlockException ignore) {
            // Don't care; cluster still loading data...
        }
        return ok(result);
    }

    @NotNull
    private Map<String, String> processSearchHit(final SearchHit sh, final DefaultLogos defaultLogos, RealmModel currentRealm) {
        final Map<String, String> resItem = new HashMap<>();
        resItem.put("_score", FormatHelper.formatNumber(sh.getScore(), Http.Context.current().lang().toLocale()));
        resItem.put("_type", sh.getType());
        final Map<String, Object> source = sh.getSource();
        switch (sh.getType()) {
            case SearchRequestType.Constants.ARTICLE_TYPE:
                resItem.put("label", (String) source.getOrDefault("name", "- article without name -"));
                resItem.put("logo", getLogoUrl(defaultLogos.defaultArticleLogo, (String) source.get("logo")));
                resItem.put("vendorLogo", getLogoUrl(defaultLogos.defaultVendorPageLogo, (String) source.get("vendorLogo")));
                initField(resItem, "description", source);
                initField(resItem, "price", source);
                initField(resItem, "vendorLabel", source);
                break;
            case SearchRequestType.Constants.GROUP_TYPE:
                initGroupOrPageResult(resItem, source, defaultLogos.defaultGroupLogo);
                break;
            case SearchRequestType.Constants.PAGE_TYPE:
                initGroupOrPageResult(resItem, source, defaultLogos.defaultVendorPageLogo);
                break;
            case SearchRequestType.Constants.USER_TYPE:
                resItem.put("label", (String) (source.getOrDefault("first_name", "") + " " + source.getOrDefault("last_name", "")).trim());
                resItem.put("description", (String) source.getOrDefault("email", null));
                resItem.put("logo", getLogoUrl(defaultLogos.defaultUserLogo, (String) source.get("logo")));
                break;
        }
        final Object slug = source.getOrDefault("slug", null);
        if (slug != null) {
            final String baseUrl = routes.HomeRealmController.GET_HomePage(currentRealm).absoluteURL(request());
            resItem.put("slug", baseUrl + "/" + sh.getType() + "/" + slug);
        }
        return resItem;
    }

    private void initField(Map<String, String> resItem, String fieldName, final Map<String, Object> source) {
        resItem.put(fieldName, (String) source.getOrDefault(fieldName, null));
    }

    private void fillJson(ObjectNode resultItemJson, Map<String, String> resItem) {
        for (Map.Entry<String, String> entry : resItem.entrySet()) {
            resultItemJson.put(entry.getKey(), entry.getValue());
        }
    }

    private void initGroupOrPageResult(final Map<String, String> resItem, Map<String, Object> source, final String defaultLogo) {
        resItem.put("label", (String) source.getOrDefault("name", ""));
        final String logoUid = (String) source.get("logo");
        String logoURL = getLogoUrl(defaultLogo, logoUid);
        resItem.put("logo", logoURL);
        initField(resItem, "description", source);
    }

    private String getLogoUrl(String defaultLogo, String logoUid) {
        String logoURL = defaultLogo;
        if (logoUid != null) {
            final S3FileModel s3FileModel = S3FileModel.find.byId(UUID.fromString(logoUid));
            if (s3FileModel != null) {
                logoURL = s3FileModel.getUrlAsString();
            }
        }
        return logoURL;
    }

    public static PaginationData.RouteTransformer pagedSearch(final RealmModel realm, final Form<SearchRealmForm> form) {
        final Http.Request request = Http.Context.current().request();
        final String q = request.getQueryString("q");
        final String type = request.getQueryString("type");
        return new PaginationData.RouteTransformer() {
            @Override
            public String transform(int i) {
                return routes.SearchRealmController.GET_Search(realm).absoluteURL(request) + "?q=" + q + "&type=" + type + "&page=" + i;
            }
        };
    }

    public static class SearchResultBlock {
        public final boolean hasMore;
        public final String searchType;
        public final List<Map<String, String>> searchResultList;

        public SearchResultBlock(final boolean hasMore,
                                 final String searchType,
                                 final List<Map<String, String>> searchResultList) {
            this.hasMore = hasMore;
            this.searchType = searchType;
            this.searchResultList = searchResultList;
        }
    }

    public static class DefaultLogos {
        public String defaultUserLogo = controllers.routes.Assets.versioned(new Assets.Asset("img/default-user.png")).absoluteURL(request());
        public String defaultGroupLogo = controllers.routes.Assets.versioned(new Assets.Asset("img/default-group.png")).absoluteURL(request());
        public String defaultVendorPageLogo = controllers.routes.Assets.versioned(new Assets.Asset("img/default-page.png")).absoluteURL(request());
        public String defaultArticleLogo = controllers.routes.Assets.versioned(new Assets.Asset("img/default-article.png")).absoluteURL(request());
    }
}
