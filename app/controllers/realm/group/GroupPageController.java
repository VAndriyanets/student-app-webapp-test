/*
 * Copyright (C) 2014 - 2016 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.realm.group;

import actions.SessionRequired;
import forms.realm.AddPostForm;
import models.*;
import play.Logger;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.form.ExtendedForm;
import toolbox.helper.SessionHelper;
import views.html.realmview.group.GroupHomeView;

/**
 * GroupPageController.
 *
 */
public class GroupPageController extends Controller {


    /**
     * Show the group main page, containing posts managing board.
     *
     * @return The group main page
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_PostsPage(final RealmModel realm, final RealmGroupModel realmGroup) {
        if (!SessionHelper.getAccount().isEnrolledTo(realm)) {
            flash("danger", Messages.get("REALM.ERROR.NOT_ENROLLED"));
            return redirect(controllers.routes.BaseApplicationController.GET_BaseApplication());
        }
        return ok(views.html.realmview.group.GroupHomeView.render(realm, realmGroup, null));
    }

    /**
     * Process group post publishing.
     *
     * @param realm The current realm
     * @return The selected realm form or GET of Realm validate
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_GroupPostPublish(final RealmModel realm, final RealmGroupModel realmGroup) {

        final ExtendedForm<AddPostForm, RealmModel> extendedForm = ExtendedForm.form(AddPostForm.class, realm).bindFromRequest();

        if (!extendedForm.hasErrors()) {
            final AddPostForm realFormData = extendedForm.get();
            final AccountModel author = SessionHelper.getAccount();
            final PostModel post = realFormData.getPostModel();
            post.group = realmGroup;
            post.setAuthor(author);
            post.save();
            realmGroup.getPosts().add(post);
            realmGroup.save();

            flash("success", Messages.get("REALM.GROUP.POSTS.FORM.SUCCESS"));
            return redirect(controllers.realm.group.routes.GroupPageController.GET_PostsPage(realm, realmGroup));
        }
        flash("danger", Messages.get("FORM.FLASH.ERRORS"));
        return badRequest(GroupHomeView.render(realm, realmGroup, extendedForm));
    }

    /**
     * Process post deletion.
     *
     * @param postId Post to be deleted
     * @return The selected realm form or GET of Realm validate
     */
    @Security.Authenticated(SessionRequired.class)
    public Result POST_DeletePost(final Long postId) {

        try {
            final PostModel post = PostModel.find.byId(postId);
            post.delete();

            return ok("OK");
        } catch (Exception e) {
            Logger.error("Could not delete Post " + postId, e);
            return badRequest("no post");
        }

    }

}
