/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.realm;

import actions.SessionRequired;
import com.avaje.ebean.PagedList;
import forms.realm.AddPostForm;
import models.AccountModel;
import models.ArticleModel;
import models.PostModel;
import models.RealmModel;
import org.jetbrains.annotations.NotNull;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.twirl.api.Content;
import toolbox.form.ExtendedForm;
import toolbox.helper.SessionHelper;

import java.util.List;

/**
 * HomeRealmController.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class HomeRealmController extends Controller {

    /**
     * Show the home page of the current realm.
     *
     * @return The realm home page
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    public Result GET_HomePage(final RealmModel realm) {
        final AccountModel account = SessionHelper.getAccount();
        if (!account.isEnrolledTo(realm)) {
            flash("danger", Messages.get("REALM.ERROR.NOT_ENROLLED"));
            return redirect(controllers.routes.BaseApplicationController.GET_BaseApplication());
        }
        return ok(getResult(realm, account, null));
    }

    @NotNull
    private Content getResult(RealmModel realm, AccountModel account, ExtendedForm<AddPostForm,RealmModel> addPostForm) {
        final List<PostModel> realmPosts = PostModel.findRealmPosts(realm, account.getRealmGroups(realm));
        final PagedList<ArticleModel> last = ArticleModel.findLastForRealm(realm, 0, 3);
        final PagedList<ArticleModel> bestSelling = ArticleModel.findBestSellingForRealm(realm, 0, 3);
        return views.html.realmview.HomeRealmView.render(realm, last.getList(), bestSelling.getList(), realmPosts, addPostForm);
    }


    /**
     * Show the home page of the current realm.
     *
     * @return The realm home page
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    public Result POST_HomePage(final RealmModel realm) {
        final AccountModel account = SessionHelper.getAccount();
        if (!account.isEnrolledTo(realm) || !account.isGrantedTo(realm)) {
            flash("danger", Messages.get("REALM.ERROR.NOT_ENROLLED"));
            return redirect(controllers.routes.BaseApplicationController.GET_BaseApplication());
        }

        final ExtendedForm<AddPostForm, RealmModel> extendedForm = ExtendedForm.form(AddPostForm.class, realm).bindFromRequest();

        if (!extendedForm.hasErrors()) {
            final AddPostForm realFormData = extendedForm.get();
            final AccountModel author = SessionHelper.getAccount();
            final PostModel post = realFormData.getPostModel();
            post.realm = realm;
            post.setAuthor(author);
            post.save();
            realm.posts.add(post);
            realm.save();

            flash("success", Messages.get("REALM.GROUP.POSTS.FORM.SUCCESS"));
            return redirect(routes.HomeRealmController.GET_HomePage(realm));
        }
        flash("danger", Messages.get("FORM.FLASH.ERRORS"));
        return badRequest(getResult(realm,account,extendedForm));
    }
}
