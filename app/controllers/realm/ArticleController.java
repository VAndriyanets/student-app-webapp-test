package controllers.realm;

import actions.SessionRequired;
import actors.SearchEngineActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.avaje.ebean.PagedList;
import com.avaje.ebean.annotation.Transactional;
import com.google.inject.Inject;
import forms.common.PaginationData;
import forms.realm.AddPostForm;
import forms.realm.page.article.ManageArticleForm;
import models.*;
import models.ArticlePricingModel;
import org.jetbrains.annotations.NotNull;
import play.Play;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.form.ExtendedForm;
import toolbox.helper.ActorHelper;
import toolbox.helper.ImageHelper;
import toolbox.helper.SessionHelper;
import views.html.realmview.article.ArticleCreateAndEditView;
import views.html.realmview.article.ArticleDetailView;
import views.html.systemview.error403;

import java.util.ArrayList;

/**
 * @author Andrey Sokolov
 */
public class ArticleController extends Controller {

    /**
     * Number of articles on articles page [for paging]
     */
    private static final int PAGE_ADMIN_ARTICLES_PAGE_SIZE = Play.application().configuration().getInt("vendor.page.admin.articles.per.page", 4);
    /**
     * Size of Logo
     */
    private final static int[] LOGO_SIZE = new int[]{512, 512};

    /**
     * Handle to SearchEngine actor reference.
     */
    private final ActorRef searchEngineActor;

    @Inject
    public ArticleController(ActorSystem actorSystem) {
        this.searchEngineActor = actorSystem.actorOf(SearchEngineActor.props);
    }


    @Security.Authenticated(SessionRequired.class)
    public Result GET_PageAdministrationArticles(final RealmModel realm, final RealmPageModel page, final Integer pageIndex) {

        final PagedList<ArticleModel> articleModels = ArticleModel.findLastForVendor(page, pageIndex - 1, PAGE_ADMIN_ARTICLES_PAGE_SIZE);
        final PaginationData pagination = new PaginationData(pageIndex, PAGE_ADMIN_ARTICLES_PAGE_SIZE, articleModels.getTotalRowCount(), articleModels.getTotalPageCount() > 1);

        return ok(views.html.realmview.vendorpage.VendorPageAdminArticles.render(realm, page, articleModels.getList(), pagination));
    }

    private Result redirectEditArticle(RealmModel realm, RealmPageModel page, ArticleModel article) {
        return redirect(routes.ArticleController.GET_EditArticle(realm, page, article));
    }

    @Security.Authenticated(SessionRequired.class)
    public Result GET_NewArticle(final RealmModel realm, final RealmPageModel page) {
        ArticleModel article = null;
        final ExtendedForm<ManageArticleForm, ArticleModel> eForm = ExtendedForm.form(ManageArticleForm.class, article);
        final ManageArticleForm maf = new ManageArticleForm();
        //maf.status = article.getStatus().name();
        maf.options = new ArrayList<>(0);
        return ok(ArticleCreateAndEditView.render(realm, page, null, eForm.fill(maf), 0, 0));
    }

    @Security.Authenticated(SessionRequired.class)
    public Result GET_EditArticle(final RealmModel realm, final RealmPageModel page, final ArticleModel article) {
        final ExtendedForm<ManageArticleForm, ArticleModel> eForm = ExtendedForm.form(ManageArticleForm.class, article);
        final ManageArticleForm maf = new ManageArticleForm();
        maf.name = article.getName();
        maf.description = article.getDescription();
        maf.saleOnWeb = article.isSaleOnWeb();
        maf.saleOnMobile = article.isSaleOnMobile();
        maf.ticketing = article.isTicketing();
        maf.status = article.getStatus().name();
        maf.price = article.getPrice();
        maf.maximumQuantity = article.getMaximumQuantity();
        maf.limitPerUser = article.getLimitPerUser();
        maf.options = new ArrayList<>(article.getOptions().size());
        for (ArticleOptionModel option : article.getOptions()) {
            ManageArticleForm.SubItemOptionForm optionForm = new ManageArticleForm.SubItemOptionForm();
            optionForm.id = option.getId();
            optionForm.name = option.getName();
            optionForm.type = option.getOptionType().name();
            optionForm.argument = option.getArgument();
            optionForm.mandatory = option.isMandatory();
            maf.options.add(optionForm);
        }
        maf.rules = new ArrayList<>(article.getPricingRules().size());
        for (ArticlePricingModel rule : article.getPricingRules()) {
            ManageArticleForm.SubItemPricingForm ruleForm = new ManageArticleForm.SubItemPricingForm();
            ruleForm.id = rule.getId();
            ruleForm.type = rule.getPricingType().name();
            ruleForm.price = rule.getPrice();
            if (ArticlePricingModel.PricingType.OWN_AN_ARTICLE ==  rule.getPricingType()) {
                ruleForm.articleId = rule.getOwnedArticle().getId();
            } else if (ArticlePricingModel.PricingType.MEMBER_OF_GROUP ==  rule.getPricingType()) {
                ruleForm.groupId = rule.getGroup().getId();
            }
            maf.rules.add(ruleForm);
        }
        return ok(ArticleCreateAndEditView.render(realm, page, article, eForm.fill(maf), article.getOptions().size(), article.getPricingRules().size()));
    }

    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_NewArticle(final RealmModel realm, final RealmPageModel page) {
        final ArticleModel article = new ArticleModel();

        return manageArticle(realm, page, article, "ARTICLE.MANAGE.PAGE.CREATE.SUCCESS");
    }

    @NotNull
    private Result manageArticle(RealmModel realm, RealmPageModel page, ArticleModel article, String message) {
        if (!page.hasVendor(SessionHelper.getAccount())) {
            return forbidden(error403.render());
        }

        final ExtendedForm<ManageArticleForm, ArticleModel> form = ExtendedForm.form(ManageArticleForm.class, article).bindFromRequest();

        if (form.hasErrors()) {
            flash("flash", Messages.get("FORM.FLASH.ERRORS"));
            return badRequest(ArticleCreateAndEditView.render(realm, page, article, form, article.getOptions().size(), article.getPricingRules().size()));
        }

        final ManageArticleForm pageForm = processForm(form.get());
        article.setName(pageForm.name);
        article.setDescription(pageForm.description);
        article.setSaleOnWeb(pageForm.saleOnWeb);
        article.setSaleOnMobile(pageForm.saleOnMobile);
        article.setTicketing(pageForm.ticketing);
        article.setPrice(pageForm.price);
        article.setMaximumQuantity(pageForm.maximumQuantity);
        article.setLimitPerUser(pageForm.limitPerUser);
        article.setStatus(ArticleModel.ArticleStatus.valueOf(pageForm.status));

        final S3FileModel logoS3File = ImageHelper.convertToS3File(pageForm.logo, pageForm.logo_crop, LOGO_SIZE);
        if (logoS3File != null) {
            article.setLogo(logoS3File);
        }
        article.setVendorPage(page);
        article.save();

        for (final ManageArticleForm.SubItemOptionForm sidf : pageForm.options) {
            if (sidf.id != null) {
                final ArticleOptionModel articleOption = ArticleOptionModel.find.byId(sidf.id);
                if (articleOption != null) {
                    if (sidf.delete ) {
                        articleOption.delete();
                    } else {
                        fillArticleOption(sidf, articleOption);
                        articleOption.save();
                    }
                }
            } else if (sidf.id == null) {
                final ArticleOptionModel articleOption = new ArticleOptionModel();
                articleOption.setArticle(article);
                fillArticleOption(sidf, articleOption);
                articleOption.save();
            }
        }

        for (final ManageArticleForm.SubItemPricingForm sidf : pageForm.rules) {
            if (sidf.id != null) {
                final ArticlePricingModel pricingRule = ArticlePricingModel.find.byId(sidf.id);
                if (pricingRule != null) {
                    if (sidf.delete ) {
                        pricingRule.delete();
                    } else {
                        fillPricingRule(sidf, pricingRule);
                        pricingRule.save();
                    }
                }
            } else if (sidf.id == null && sidf.price != null) {
                final ArticlePricingModel pricingRule = new ArticlePricingModel();
                pricingRule.setArticle(article);
                fillPricingRule(sidf, pricingRule);
                pricingRule.save();
            }
        }

        ActorHelper.indexModel(searchEngineActor, article);

        flash("success", Messages.get(message));

        return redirectEditArticle(realm, page, article);
    }

    private void fillPricingRule(ManageArticleForm.SubItemPricingForm form, ArticlePricingModel pricingRule) {
        pricingRule.setPricingType(ArticlePricingModel.PricingType.valueOf(form.type));
        pricingRule.setPrice(form.price);
        if (ArticlePricingModel.PricingType.MEMBER_OF_GROUP == pricingRule.getPricingType()) {
            pricingRule.setGroup(RealmGroupModel.find.byId(form.groupId));
        } else if (ArticlePricingModel.PricingType.OWN_AN_ARTICLE == pricingRule.getPricingType()) {
            pricingRule.setOwnedArticle(ArticleModel.find.byId(form.articleId));
        }
    }

    private void fillArticleOption(ManageArticleForm.SubItemOptionForm form, ArticleOptionModel articleOption) {
        articleOption.setName(form.name);
        articleOption.setOptionType(ArticleOptionModel.OptionType.valueOf(form.type));
        articleOption.setArgument(form.argument);
        articleOption.setMandatory(form.mandatory);
    }

    /**
     * Set form instance with File upload as MultipartFormData.
     *
     * @param pageFormData The current form of editing RealmPage
     * @return The FromData giving with File logo and banner informed
     * @since 15.10
     */
    private ManageArticleForm processForm(final ManageArticleForm pageFormData) {
        final Http.MultipartFormData multipartFormData = Http.Context.current().request().body().asMultipartFormData();
        if (multipartFormData != null) {
            if (multipartFormData.getFile("logo") != null) {
                pageFormData.logo = multipartFormData.getFile("logo").getFile();
            }
        }
        return pageFormData;
    }

    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_EditArticle(final RealmModel realm, final RealmPageModel page, final ArticleModel article) {
        return manageArticle(realm, page, article, "ARTICLE.MANAGE.PAGE.UPDATE.SUCCESS");
    }

    /**
     * Show Article Details page.
     *
     */
    @Security.Authenticated(SessionRequired.class)
    public Result GET_ArticleDetails(final RealmModel realm, final ArticleModel article) {
        return ok(views.html.realmview.article.ArticleDetailView.render(realm, article, null));
    }

    /**
     * Publish vendor Post.
     */
    @Security.Authenticated(SessionRequired.class)
    public Result POST_PublishPost(final RealmModel realm, final ArticleModel article) {

        final ExtendedForm<AddPostForm, RealmModel> extendedForm = ExtendedForm.form(AddPostForm.class, realm).bindFromRequest();

        if (!extendedForm.hasErrors()) {
            final AddPostForm realFormData = extendedForm.get();
            final AccountModel author = SessionHelper.getAccount();
            final PostModel post = realFormData.getPostModel();
            post.setAuthor(author);
            post.article = article;
            post.save();

            flash("success", Messages.get("REALM.GROUP.POSTS.FORM.SUCCESS"));
            return redirect(routes.ArticleController.GET_ArticleDetails(realm, article));
        }
        flash("danger", Messages.get("FORM.FLASH.ERRORS"));
        return badRequest(ArticleDetailView.render(realm, article, extendedForm));
    }
}

