package controllers.realm;

import actions.SessionRequired;
import models.AccountModel;
import models.PostModel;
import models.VoteModel;
import play.db.ebean.Transactional;
import play.filters.csrf.RequireCSRFCheck;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.helper.SessionHelper;

import javax.persistence.PersistenceException;

/**
 * @author Andrey Sokolov
 */
public class VoteController extends Controller {

    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_voteUp(final Long postId) {
        return vote(postId, true);
    }

    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_voteDown(final Long postId) {
        return vote(postId, false);
    }


    private Result vote(Long postId, boolean like) {
        final AccountModel account = SessionHelper.getAccount();
        final PostModel postModel = PostModel.find.byId(postId);
        if(postModel != null && postModel.canVote(account)){
            VoteModel newVote = new VoteModel();
            newVote.like = like;
            newVote.key = new VoteModel.VotePK(account.getId(), postId);
            try {
                newVote.save();
            } catch (PersistenceException e) {
                return badRequest("already voted");
            }
            VoteModel.cleanLikesCache(postId, like);
            SessionHelper.getAccount().cleanLikesCache(like);
            return ok("OK");
        } else {
            return badRequest("no post");
        }
    }


    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_voteUpRollback(Long postId) {

        return rollback(postId, true);
    }

    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_voteDownRollback(Long postId) {
        return rollback(postId, false);
    }

    private Result rollback(Long postId, boolean like) {
        final AccountModel account = SessionHelper.getAccount();
        VoteModel vote = VoteModel.findVote(postId, account.getId(), like);
        if(vote != null){
            try {
                vote.delete();
            } catch (PersistenceException e) {
                return badRequest("already voted");
            }
        } else {
            return badRequest("not voted");
        }
        VoteModel.cleanLikesCache(postId, like);
        SessionHelper.getAccount().cleanLikesCache(like);
        return ok("OK");
    }

}
