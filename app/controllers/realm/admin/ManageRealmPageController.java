/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.realm.admin;

import actions.SessionRequired;
import actors.SearchEngineActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.inject.Inject;
import forms.realm.admin.ManageRealmPageForm;
import forms.realm.admin.RolePermissionsAdminRealmForm;
import forms.realm.admin.RoleUsersAdminRealmForm;
import forms.realm.admin.RolesAdminRealmForm;
import models.*;
import play.data.Form;
import play.db.ebean.Transactional;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import play.twirl.api.Html;
import toolbox.form.ExtendedForm;
import toolbox.helper.ActorHelper;
import toolbox.helper.ImageHelper;
import toolbox.helper.SessionHelper;
import views.html.realmview.admin.page.ManageRealmPageView;
import views.html.realmview.admin.roles.RolePermissionsAdminRealmView;
import views.html.realmview.admin.roles.RoleUsersAdminRealmView;
import views.html.systemview.error403;

import java.util.ArrayList;
import java.util.List;

/**
 * ManageRealmPageController.
 *
 * @author Olivier Buiron
 * @version 15.12
 * @since 15.12
 */
public class ManageRealmPageController extends Controller {

    /**
     * Size of Logo
     */
    private final static int[] LOGO_SIZE = new int[]{512, 512};

    /**
     * Size of Banner
     */
    private final static int[] BANNER_SIZE = new int[]{1200, 250};


    private static final List<PermissionModel> PAGE_PERMISSIONS = PermissionModel.findByNamespace(PermissionModel.Namespace.PAGE);

    /**
     * Handle to SearchEngine actor reference.
     */
    private final ActorRef searchEngineActor;

    @Inject
    public ManageRealmPageController(ActorSystem actorSystem) {
        this.searchEngineActor = actorSystem.actorOf(SearchEngineActor.props);
    }

    /**
     * Show the 'manage page' page of the current realm.
     *
     * @param realm The current realm (binding by slug)
     * @return The selected realm page or the realm selector page
     * @since 15.12
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_ManageRealmPages(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.page")) {
            return forbidden(error403.render());
        }
        return ok(ManageRealmPageView.render(realm, null, null, null, null));
    }

    /**
     * Process page creation by the ManageRealmPageForm submitted.
     *
     * @param realm The current realm (binding by slug)
     * @return Redirect to base page 'manage realm pages'
     * @since 15.12
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_CreateRealmPage(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.page")) {
            return forbidden(error403.render());
        }

        final ExtendedForm<ManageRealmPageForm, RealmPageModel> form = ExtendedForm.form(ManageRealmPageForm.class, (RealmPageModel) null).bindFromRequest();

        if (form.hasErrors()) {
            flash("flash", Messages.get("FORM.FLASH.ERRORS"));
            return badRequest(ManageRealmPageView.render(realm, null, form, null, null));
        }

        final ManageRealmPageForm pageForm = processForm(form.get());
        final RealmPageModel page = new RealmPageModel();
        page.setName(pageForm.name);
        page.setSlug(pageForm.slug);

        final S3FileModel logoS3File = ImageHelper.convertToS3File(pageForm.logo, pageForm.logo_crop, ManageRealmPageController.LOGO_SIZE);
        if (logoS3File != null) {
            page.setLogo(logoS3File);
        }

        final S3FileModel bannerS3File = ImageHelper.convertToS3File(pageForm.banner, pageForm.banner_crop, ManageRealmPageController.BANNER_SIZE);
        if (bannerS3File != null) {
            page.setBanner(bannerS3File);
        }

        initPageRoles(page);
        page.getRealms().add(realm);
        page.save();
        realm.addPage(page);
        realm.save();
        ActorHelper.indexModel(searchEngineActor, page);

        flash("success", Messages.get("REALM.ADMIN.PAGE.FLASH.SUCCESS"));
        return redirect(routes.ManageRealmPageController.GET_ManageRealmPages(realm));
    }

    private void initPageRoles(RealmPageModel page) {
        final List<RoleRealmPageModel> roles = page.getRoles();
        final RoleRealmPageModel admin = new RoleRealmPageModel();
        admin.setPage(page);
        admin.setName(Messages.get("REALM.ADMIN.PAGE.ROLE.ADMIN.NAME"));
        admin.setPermissions(PermissionModel.findByNamespace(PermissionModel.Namespace.PAGE));
        admin.setReadonly(true);
        roles.add(admin);
        admin.save();
        final RoleRealmPageModel basicVendor = new RoleRealmPageModel();
        basicVendor.setPage(page);
        basicVendor.setName(Messages.get(RealmPageModel.VENDOR_PAGE_ROLE_VENDOR_KEY));
        basicVendor.setReadonly(false);
        roles.add(basicVendor);
        basicVendor.save();
    }

    /**
     * Show the 'manage page' page of the current realm with current selected page.
     *
     * @param realm The current realm (binding by slug)
     * @param page  The current page (binding by slug)
     * @return The selected realm page
     * @since 15.12
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_RealmPage(final RealmModel realm, final RealmPageModel page) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.page")) {
            return forbidden(error403.render());
        }

        final ExtendedForm<ManageRealmPageForm, RealmPageModel> form = ExtendedForm.form(ManageRealmPageForm.class, page);
        final ManageRealmPageForm pageForm = new ManageRealmPageForm();

        pageForm.name = page.getName();
        pageForm.slug = page.getSlug();

        final Form<RolesAdminRealmForm> frmRoles = getRolesAdminRealmFormForm(page);

        final RolesAdminRealmForm.RolesFormData<RoleRealmPageModel> data = new RolesAdminRealmForm.RolesFormData<>(page.getRoles(),
                role -> routes.ManageRealmPageController.GET_ManageRoleUsers(realm, page, role));
        return ok(ManageRealmPageView.render(realm, page, form.fill(pageForm), frmRoles, data));
    }

    private Form<RolesAdminRealmForm> getRolesAdminRealmFormForm(RealmPageModel page) {
        final RolesAdminRealmForm frmRolesData = new RolesAdminRealmForm();
        frmRolesData.roles = new ArrayList<>();
        for (final RoleRealmPageModel roleRealm : page.getRoles()) {
            final RolesAdminRealmForm.SubItemRoleForm subFrmRolesData = new RolesAdminRealmForm.SubItemRoleForm();
            subFrmRolesData.name = roleRealm.getName();
            subFrmRolesData.uid = roleRealm.getUid();
            subFrmRolesData.readonly = roleRealm.isReadonly();
            frmRolesData.roles.add(subFrmRolesData);
        }
        return Form.form(RolesAdminRealmForm.class).fill(frmRolesData);
    }

    /**
     * Process page edition the current group by the ManageRealmPageForm submitted.
     *
     * @param realm The current realm (binding by slug)
     * @param page  The current page (binding by slug)
     * @return Redirect to base page 'manage realm pages'
     * @since 15.12
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_EditRealmPage(final RealmModel realm, final RealmPageModel page) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.page")) {
            return forbidden(error403.render());
        }
        final ExtendedForm<ManageRealmPageForm, RealmPageModel> form = ExtendedForm.form(ManageRealmPageForm.class, (RealmPageModel) null).bindFromRequest();

        if (form.hasErrors()) {
            flash("flash", Messages.get("FORM.FLASH.ERRORS"));
            //roles form readonly for pages
            final Form<RolesAdminRealmForm> rolesForm = getRolesAdminRealmFormForm(page);
            final RolesAdminRealmForm.RolesFormData<RoleRealmPageModel> data = new RolesAdminRealmForm.RolesFormData<>(page.getRoles(), role -> routes.ManageRealmPageController.GET_ManageRoleUsers(realm, page, role));
            return badRequest(ManageRealmPageView.render(realm, null, form, rolesForm, data));
        }

        final ManageRealmPageForm pageForm = processForm(form.get());

        page.setName(pageForm.name);
        page.setSlug(pageForm.slug);

        final S3FileModel logoS3File = ImageHelper.convertToS3File(pageForm.logo, pageForm.logo_crop, ManageRealmPageController.LOGO_SIZE);
        if (logoS3File != null) {
            page.setLogo(logoS3File);
        }

        final S3FileModel bannerS3File = ImageHelper.convertToS3File(pageForm.banner, pageForm.banner_crop, ManageRealmPageController.BANNER_SIZE);
        if (bannerS3File != null) {
            page.setBanner(bannerS3File);
        }

        if (page.getRoles().isEmpty()) {
            initPageRoles(page);
        }

        page.save();
        ActorHelper.indexModel(searchEngineActor, page);

        flash("success", Messages.get("REALM.ADMIN.PAGE.FLASH.SUCCESS"));
        return redirect(routes.ManageRealmPageController.GET_ManageRealmPages(realm));
    }

    /**
     * Set form instance with File upload as MultipartFormData.
     *
     * @param pageFormData The current form of editing RealmPage
     * @return The FromData giving with File logo and banner informed
     * @since 15.10
     */
    private ManageRealmPageForm processForm(final ManageRealmPageForm pageFormData) {
        final Http.MultipartFormData multipartFormData = Http.Context.current().request().body().asMultipartFormData();
        if (multipartFormData != null) {
            if (multipartFormData.getFile("logo") != null) {
                pageFormData.logo = multipartFormData.getFile("logo").getFile();
            }
            if (multipartFormData.getFile("banner") != null) {
                pageFormData.banner = multipartFormData.getFile("banner").getFile();
            }
        }
        return pageFormData;
    }


    /**
     * Managing vendor page role view.
     *
     * @param currentRealm
     * @param currentPage
     * @param roleModel
     * @return
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_ManageRoleUsers(final RealmModel currentRealm, final RealmPageModel currentPage, final RoleRealmPageModel roleModel) {
        if (!SessionHelper.getAccount().hasPermission(currentRealm, "social.page") && !SessionHelper.getAccount().hasPermission(currentRealm, "page.grant.roles")) {
            return forbidden(views.html.systemview.error403.render());
        }
        return ok(getRenderUsers(currentRealm, currentPage, roleModel, null));
    }

    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_ManageRoleUsers(final RealmModel currentRealm, final RealmPageModel currentPage, final RoleRealmPageModel roleModel) {

        if (!SessionHelper.getAccount().hasPermission(currentRealm, "social.page") && !SessionHelper.getAccount().hasPermission(currentRealm, "page.grant.roles")) {
            return forbidden(views.html.systemview.error403.render());
        }

        final Form<RoleUsersAdminRealmForm> formUsersRole = Form.form(RoleUsersAdminRealmForm.class).bindFromRequest();
        if (!formUsersRole.hasErrors()) {
            final RoleUsersAdminRealmForm usersRoleFormData = formUsersRole.get();
            usersRoleFormData.saveUsers(currentRealm, roleModel);
            roleModel.save();
            return redirect(routes.ManageRealmPageController.GET_ManageRoleUsers(currentRealm, currentPage, roleModel));
        }

        return badRequest(getRenderUsers(currentRealm, currentPage, roleModel, formUsersRole));
    }

    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_ManageRolePerm(final RealmModel currentRealm, final RealmPageModel currentPage, final RoleRealmPageModel roleModel) {
        if (!SessionHelper.getAccount().hasPermission(currentRealm, "social.page")) {
            return forbidden(views.html.systemview.error403.render());
        }

        final Form<RolePermissionsAdminRealmForm> frmPerms = Form.form(RolePermissionsAdminRealmForm.class);
        final RolePermissionsAdminRealmForm perms = new RolePermissionsAdminRealmForm();
        perms.init(roleModel, PAGE_PERMISSIONS);
        return ok(getRenderPerms(currentRealm, currentPage, roleModel, frmPerms.fill(perms)));
    }

    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_ManageRolePerm(final RealmModel realm, final RealmPageModel page, final RoleRealmPageModel role) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.page") || role.isReadonly()) {
            return forbidden(views.html.systemview.error403.render());
        }

        final Form<RolePermissionsAdminRealmForm> frmPerms = Form.form(RolePermissionsAdminRealmForm.class).bindFromRequest();
        if (!frmPerms.hasErrors()) {
            final RolePermissionsAdminRealmForm permsFrmData = frmPerms.get();
            permsFrmData.save(role, PermissionModel.Namespace.PAGE);
            role.save();
            flash("success", Messages.get("REALM.ADMIN.ROLE.PERMISSIONS.FLASH.SUCCESS", role.getName()));
            return redirect(routes.ManageRealmPageController.GET_ManageRolePerm(realm, page, role));
        }
        return badRequest(getRenderPerms(realm, page, role, frmPerms));
    }

    private Html getRenderUsers(RealmModel currentRealm, RealmPageModel currentPage, RoleRealmPageModel roleModel, Form<RoleUsersAdminRealmForm> roleForm) {
        return RoleUsersAdminRealmView.render(currentRealm, roleModel, roleForm, true, "SOCIAL.PAGE",
                routes.ManageRealmPageController.GET_ManageRoleUsers(currentRealm, currentPage, roleModel),
                routes.ManageRealmPageController.GET_ManageRolePerm(currentRealm, currentPage, roleModel));
    }

    private Html getRenderPerms(RealmModel currentRealm, RealmPageModel currentPage, RoleRealmPageModel roleModel, Form<RolePermissionsAdminRealmForm> permsForm) {
        return RolePermissionsAdminRealmView.render(currentRealm, roleModel, permsForm, "SOCIAL.PAGE",
                routes.ManageRealmPageController.GET_ManageRoleUsers(currentRealm, currentPage, roleModel),
                routes.ManageRealmPageController.GET_ManageRolePerm(currentRealm, currentPage, roleModel));
    }
}
