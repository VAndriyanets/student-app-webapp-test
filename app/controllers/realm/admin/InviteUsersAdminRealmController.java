/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.realm.admin;

import actions.SessionRequired;
import actors.SearchEngineActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.google.inject.Inject;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import forms.realm.admin.ManageRealmUserAdminRealmForm;
import forms.realm.admin.ManageRealmUsersAdminRealmForm;
import models.AccountModel;
import models.RealmModel;
import modules.RedisModule;
import play.Play;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import redis.clients.jedis.Jedis;
import toolbox.SendMail;
import toolbox.TokenGeneratorUtils;
import toolbox.csv.CSVHelper;
import toolbox.csv.models.UserCsv;
import toolbox.helper.ActorHelper;
import toolbox.helper.SessionHelper;
import views.html.mailview.RealmWelcomeMailView;
import views.html.realmview.admin.InviteUsersAdminRealmView;
import views.html.systemview.error403;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * InviteUsersAdminRealmController.
 *
 * @author Olivier Buiron
 * @version 15.12
 * @since 15.10
 */
public class InviteUsersAdminRealmController extends Controller {

    /**
     * Handle to SearchEngine actor reference.
     */
    private final ActorRef searchEngineActor;

    @Inject
    public InviteUsersAdminRealmController(ActorSystem actorSystem) {
        this.searchEngineActor = actorSystem.actorOf(SearchEngineActor.props);
    }

    /**
     * Show the 'invite users' page of the current realm.
     *
     * @param realm The current realm (binding by slug)
     * @return The selected realm page or the realm selector page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_InviteUsers(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "user.invite")) {
            return forbidden(error403.render());
        }
        return ok(InviteUsersAdminRealmView.render(realm, null));
    }

    /**
     * Handle submit of 'invite user' form.
     *
     * @param realm The current realm (binding by slug)
     * @return Redirection to base 'invite users' admin realm page if nothing goes wrong.
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_InviteUser(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "user.invite")) {
            return forbidden(error403.render());
        }

        final Form<ManageRealmUserAdminRealmForm> form = Form.form(ManageRealmUserAdminRealmForm.class).bindFromRequest(request());

        if (form.hasErrors()) {
            return badRequest(InviteUsersAdminRealmView.render(realm, form));
        }

        final ManageRealmUserAdminRealmForm inviteUserForm = form.get();

        if (inviteUserForm.alreadyExist) {
            final AccountModel inviteAccount = AccountModel.find.fetch("emails").where()
                    .eq("email", inviteUserForm.email)
                    .findUnique();
            if (inviteAccount != null && !inviteAccount.isEnrolledTo(realm)) {
                realm.addAccount(inviteAccount);
                realm.save();
                if(inviteAccount.addRealm(realm)){
                    ActorHelper.indexModel(this.searchEngineActor, inviteAccount);
                }
                sendWelcomeEmail(realm, inviteAccount, null);
                flash("success", Messages.get("REALM.ADMIN.INVITE.USER.FLASH.ADDED", realm.getName()));
            } else {
                flash("warning", Messages.get("REALM.ADMIN.INVITE.USER.FLASH.ALREADY.ENROLLED", realm.getName()));
            }
        } else {
            if (!isUnknownUserAlreadyInvite(realm, inviteUserForm.email)) {
                sendWelcomeEmail(realm, null, inviteUserForm.email);
                flash("success", Messages.get("REALM.ADMIN.INVITE.USER.FLASH.UNKNW.INVITED"));
            } else {
                flash("info", Messages.get("REALM.ADMIN.INVITE.USER.FLASH.ALREADY.INVITED"));
            }
        }

        return redirect(routes.InviteUsersAdminRealmController.GET_InviteUsers(realm));
    }

    /**
     * Handle submit of 'invite users' form with csv file.
     *
     * @param realm The current realm (binding by slug)
     * @return Redirection to base 'invite users' admin realm page if nothing goes wrong.
     * @since 15.12
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_InviteUsers(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "user.invite")) {
            return forbidden(error403.render());
        }

        final Form<ManageRealmUsersAdminRealmForm> form = Form.form(ManageRealmUsersAdminRealmForm.class).bindFromRequest(request());
        final ManageRealmUsersAdminRealmForm inviteUsersForm = processForm(form.get());

        if (!inviteUsersForm.isValidCsvFileType()) {
            flash("danger", Messages.get("ERROR.FORM.INVALID.FILE.TYPE", "CSV", ".csv"));
            return redirect(routes.InviteUsersAdminRealmController.GET_InviteUsers(realm));
        }

        try {
            final Map<String, Integer> processResult = new HashMap<>();
            final List<UserCsv> users = CSVHelper.UserFromCSV(inviteUsersForm.csv);

            for (final UserCsv user : users) {
                final AccountModel account = AccountModel.find.fetch("emails").where().eq("email", user.getEmail()).findUnique();
                if (account != null) {
                    if (!account.isEnrolledTo(realm)) {
                        realm.addAccount(account);
                        realm.save();
                        sendWelcomeEmail(realm, account, null);
                        processResult.put("enrolled", processResult.get("enrolled") != null ? processResult.get("enrolled") + 1 : 1);
                    } else {
                        processResult.put("already-enrolled", processResult.get("already-enrolled") != null ? processResult.get("already-enrolled") + 1 : 1);
                    }
                } else {
                    if (!isUnknownUserAlreadyInvite(realm, user.getEmail())) {
                        sendWelcomeEmail(realm, null, user.getEmail());
                        processResult.put("invited", processResult.get("invited") != null ? processResult.get("invited") + 1 : 1);
                    } else {
                        processResult.put("already-invited", processResult.get("already-invited") != null ? processResult.get("already-invited") + 1 : 1);
                    }
                }
            }

            StringBuilder sb = new StringBuilder();
            if (processResult.get("enrolled") != null)
                sb.append(Messages.get("REALM.ADMIN.INVITE.USERS.FLASH.COUNT.ENROLLED", processResult.get("enrolled"))).append("<br>");
            if (processResult.get("already-enrolled") != null)
                sb.append(Messages.get("REALM.ADMIN.INVITE.USERS.FLASH.COUNT.ALREADY.ENROLLED", processResult.get("already-enrolled"))).append("<br>");
            if (processResult.get("invited") != null)
                sb.append(Messages.get("REALM.ADMIN.INVITE.USERS.FLASH.COUNT.INVITED", processResult.get("invited"))).append("<br>");
            if (processResult.get("already-invited") != null)
                sb.append(Messages.get("REALM.ADMIN.INVITE.USERS.FLASH.COUNT.ALREADY.INVITED", processResult.get("already-invited"))).append("<br>");
            flash("info", sb.toString());

        } catch (IOException e) {
            flash("danger", Messages.get("ERROR.FORM.FILE"));
        }

        return redirect(routes.InviteUsersAdminRealmController.GET_InviteUsers(realm));
    }

    /**
     * Check if unknown user have already a existing generated token for current realm.
     *
     * @param realm The current realm
     * @param email The unknown user email
     * @return {@code true} if had a valid invite, otherwise, {@code false}
     * @since 15.10
     */
    private boolean isUnknownUserAlreadyInvite(final RealmModel realm, final String email) {
        final Jedis jedis = Play.application().injector().instanceOf(RedisModule.class).getConnection();
        jedis.select(0);
        if (jedis.exists(String.format("account.pre.realm.%s", email.toLowerCase()))) {
            Set<String> invites = jedis.zrangeByScore(String.format("account.pre.realm.%s", email.toLowerCase()), String.valueOf(System.currentTimeMillis() / 1000D), "+inf");
            for (String invite : invites) {
                if (invite.contains(realm.getUidAsString())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Send a welcome mail, with a different content/context.
     *
     * @param realm        The current realm
     * @param account      The know account (can be null)
     * @param emailAddress The submitted email address (can be null)
     * @return {@code true} email send, otherwise, {@code false}
     * @since 15.10
     */
    private boolean sendWelcomeEmail(final RealmModel realm, final AccountModel account, final String emailAddress) {
        final SendGrid.Email email = SendMail.createEmail();
        email.setSubject(Messages.get("REALM.ADMIN.INVITE.WELCOME.MAIL.SUBJECT", realm.getName()));

        if (account != null) {
            email.addTo(emailAddress, account.getFullName());

            final String url = controllers.system.routes.RegistrationController.GET_Registration().absoluteURL(request()._underlyingRequest());
            email.setHtml(RealmWelcomeMailView.render(realm, account, null, url).body());
        } else {
            email.addTo(emailAddress);

            final String token = TokenGeneratorUtils.generateToken(8);

            final Jedis jedis = Play.application().injector().instanceOf(RedisModule.class).getConnection();
            jedis.select(0);
            jedis.zadd(String.format("account.pre.realm.%s", emailAddress), (System.currentTimeMillis() / 1000L) + 172800, String.format("%s|%s", token, realm.getUidAsString()));
            jedis.expire(String.format("account.pre.realm.%s", emailAddress), 172800);
            jedis.close();

            final String url = controllers.system.routes.RegistrationController.GET_Registration().absoluteURL(request()._underlyingRequest());
            email.setHtml(RealmWelcomeMailView.render(realm, null, token, url).body());
        }

        try {
            SendMail.sendEmail(email);
            return true;
        } catch (SendGridException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Set form instance with File upload from MultipartFormData.
     *
     * @param manageRealmUsers The current form of manage users Realm
     * @return The FormData giving with File CSV informed
     * @since 15.12
     */
    private ManageRealmUsersAdminRealmForm processForm(ManageRealmUsersAdminRealmForm manageRealmUsers) {
        final Http.MultipartFormData multipartFormData = Http.Context.current().request().body().asMultipartFormData();
        if (multipartFormData != null) {
            if (multipartFormData.getFile("csv") != null) {
                manageRealmUsers.csv = multipartFormData.getFile("csv").getFile();
                manageRealmUsers.originalFileName = multipartFormData.getFile("csv").getFilename();
            }
        }
        return manageRealmUsers;
    }
}
