/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.realm.admin;

import actions.SessionRequired;
import forms.realm.admin.NetworkAdminRealmForm;
import models.RealmDomainWhitelistModel;
import models.RealmModel;
import models.S3FileModel;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.form.ExtendedForm;
import toolbox.helper.ImageHelper;
import toolbox.helper.SessionHelper;
import views.html.realmview.admin.NetworkAdminRealmView;

import java.util.ArrayList;

/**
 * NetworkAdminRealmController.
 *
 * @author Thibault Meyer
 * @author Jean-Pierre Boudic
 * @version 15.10
 * @since 15.10
 */
public class NetworkAdminRealmController extends Controller {

    /**
     * Size of logo.
     */
    private final static int[] LOGO_SIZE = new int[]{512, 512};

    /**
     * Size of banner.
     */
    private final static int[] BANNER_SIZE = new int[]{1200, 250};

    /**
     * Show the home page of the current realm.
     *
     * @param realm The current realm (binding by slug)
     * @return The selected realm page or the realm selector page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_NetworkConfiguration(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "network.setting")) {
            return forbidden(views.html.systemview.error403.render());
        }

        final ExtendedForm<NetworkAdminRealmForm, RealmModel> formRealm = ExtendedForm.form(NetworkAdminRealmForm.class, realm);
        final NetworkAdminRealmForm narf = new NetworkAdminRealmForm();
        narf.name = realm.getName();
        narf.slug = realm.getSlug();
        narf.domains = new ArrayList<>();
        for (final RealmDomainWhitelistModel rdwm : realm.getWhitelistDomains()) {
            final NetworkAdminRealmForm.SubItemDomainForm sidf = new NetworkAdminRealmForm.SubItemDomainForm();
            sidf.id = rdwm.getUid();
            sidf.domain = rdwm.getDomain();
            narf.domains.add(sidf);
        }
        return ok(views.html.realmview.admin.NetworkAdminRealmView.render(realm, formRealm.fill(narf), realm.getWhitelistDomains().size()));
    }

    /**
     * Process the submitted realm form.
     *
     * @param realm The current realm (binding by slug)
     * @return The selected realm form or GET of Realm validate
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_NetworkConfiguration(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "network.setting")) {
            return forbidden(views.html.systemview.error403.render());
        }

        final ExtendedForm<NetworkAdminRealmForm, RealmModel> formRealm = ExtendedForm.form(NetworkAdminRealmForm.class, realm).bindFromRequest();
        if (!formRealm.hasErrors()) {
            final NetworkAdminRealmForm realFormData = this.processForm(formRealm.get());
            final S3FileModel S3fileLogo = ImageHelper.convertToS3File(realFormData.logo, realFormData.logo_crop, NetworkAdminRealmController.LOGO_SIZE);
            if (S3fileLogo != null) {
                realm.setLogo(S3fileLogo);
            }
            final S3FileModel S3fileBanner = ImageHelper.convertToS3File(realFormData.banner, realFormData.banner_crop, NetworkAdminRealmController.BANNER_SIZE);
            if (S3fileBanner != null) {
                realm.setBanner(S3fileBanner);
            }
            realm.setName(realFormData.name);
            realm.setSlug(realFormData.slug);

            for (final NetworkAdminRealmForm.SubItemDomainForm sidf : realFormData.domains) {
                if (sidf.id != null && sidf.domain != null) {
                    final RealmDomainWhitelistModel wlDomain = RealmDomainWhitelistModel.find.where().eq("realm", realm).like("uid", sidf.id.toString()).findUnique();
                    if (wlDomain != null) {
                        if (sidf.delete || sidf.domain.isEmpty()) {
                            wlDomain.delete();
                        } else {
                            wlDomain.setDomain(sidf.domain);
                            wlDomain.save();
                        }
                    }
                } else if (sidf.id == null && sidf.domain != null && !sidf.domain.isEmpty()) {
                    final RealmDomainWhitelistModel wlDomain = new RealmDomainWhitelistModel();
                    wlDomain.setDomain(sidf.domain);
                    wlDomain.setRealm(realm);
                    wlDomain.save();
                }
            }
            realm.save();

            flash("success", Messages.get("REALM.ADMIN.NETWORK.FLASH.SUCCESS"));
            return redirect(routes.NetworkAdminRealmController.GET_NetworkConfiguration(realm));
        }
        flash("danger", Messages.get("FORM.FLASH.ERRORS"));
        return badRequest(NetworkAdminRealmView.render(realm, formRealm, realm.getWhitelistDomains().size()));
    }

    /**
     * Set form instance with File upload from MultipartFormData
     *
     * @param realFormData The current form of editing Realm
     * @return The FormData giving with File logo and banner informed
     * @since 15.10
     */
    private NetworkAdminRealmForm processForm(NetworkAdminRealmForm realFormData) {
        final Http.MultipartFormData multipartFormData = Http.Context.current().request().body().asMultipartFormData();
        if (multipartFormData != null) {
            if (multipartFormData.getFile("logo") != null) {
                realFormData.logo = multipartFormData.getFile("logo").getFile();
            }
            if (multipartFormData.getFile("banner") != null) {
                realFormData.banner = multipartFormData.getFile("banner").getFile();
            }
        }
        return realFormData;
    }
}
