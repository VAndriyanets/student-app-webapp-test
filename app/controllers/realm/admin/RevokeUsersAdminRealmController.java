/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.realm.admin;

import actions.SessionRequired;
import forms.realm.admin.ManageRealmUserAdminRealmForm;
import forms.realm.admin.ManageRealmUsersAdminRealmForm;
import models.AccountModel;
import models.RealmModel;
import modules.RedisModule;
import play.Play;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import redis.clients.jedis.Jedis;
import toolbox.csv.CSVHelper;
import toolbox.csv.models.UserCsv;
import toolbox.helper.SessionHelper;
import views.html.realmview.admin.RevokeUsersAdminRealmView;
import views.html.systemview.error403;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * DeleteUsersAdminRealmController.
 *
 * @author Olivier Buiron
 * @version 15.12
 * @since 15.10
 */
public class RevokeUsersAdminRealmController extends Controller {

    /**
     * Show the 'revoke users' page of the current realm.
     *
     * @param realm The current realm (binding by slug)
     * @return The selected realm page or the realm selector page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_RevokeUsers(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "user.revoke")) {
            return forbidden(error403.render());
        }
        return ok(RevokeUsersAdminRealmView.render(realm, null));
    }

    /**
     * Handle sunmit of 'revoke user' form.
     *
     * @param realm The current realm (binding by slug)
     * @return Redirection to base 'revoke users' admin realm page if nothing goes wrong
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_RevokeUser(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "user.revoke")) {
            return forbidden(error403.render());
        }

        final Form<ManageRealmUserAdminRealmForm> form = Form.form(ManageRealmUserAdminRealmForm.class).bindFromRequest(request());

        if (form.hasErrors()) {
            return badRequest(RevokeUsersAdminRealmView.render(realm, form));
        }

        final ManageRealmUserAdminRealmForm revokeUsersForm = form.get();

        if (revokeUsersForm.alreadyExist) {
            final AccountModel revokeAccount = AccountModel.find.fetch("emails")
                    .where().eq("email", revokeUsersForm.email).findUnique();
            if (revokeAccount != null && revokeAccount.isEnrolledTo(realm)) {
                if (realm.removeAccount(revokeAccount)) {
                    realm.save();
                    flash("success", Messages.get("REALM.ADMIN.REVOKE.USER.FLASH.REVOKED", realm.getName()));
                } else {
                    flash("danger", Messages.get("ERROR.COMMON.SOMETHING.GOES.WRONG"));
                }
            } else {
                flash("danger", Messages.get("REALM.ADMIN.REVOKE.USER.FLASH.NOT.ENROLLED", realm.getName()));
            }
        } else {
            if (revokeAccountInvite(realm, revokeUsersForm.email)) {
                flash("success", Messages.get("REALM.ADMIN.REVOKE.USER.FLASH.REVOKED.INVITE"));
            } else {
                flash("warning", Messages.get("REALM.ADMIN.REVOKE.USER.FLASH.UNKNOWN.USER"));
            }
        }

        return redirect(routes.RevokeUsersAdminRealmController.GET_RevokeUsers(realm));
    }

    /**
     * Handle sunmit of 'revoke users' form with csv file.
     *
     * @param realm The current realm (binding by slug)
     * @return Redirection to base 'revoke users' admin realm page if nothing goes wrong
     * @since 15.12
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_RevokeUsers(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "user.revoke")) {
            return forbidden(error403.render());
        }

        final Form<ManageRealmUsersAdminRealmForm> form = Form.form(ManageRealmUsersAdminRealmForm.class).bindFromRequest(request());
        final ManageRealmUsersAdminRealmForm revokeForm = processForm(form.get());

        if (!revokeForm.isValidCsvFileType()) {
            flash("danger", Messages.get("ERROR.FORM.INVALID.FILE.TYPE", "CSV", ".csv"));
            return redirect(routes.RevokeUsersAdminRealmController.GET_RevokeUsers(realm));
        }

        try {
            final Map<String, Integer> result = new HashMap<>();
            final List<UserCsv> users = CSVHelper.UserFromCSV(revokeForm.csv);

            for (UserCsv user : users) {
                final AccountModel account = AccountModel.find.fetch("emails").where().eq("email", user.getEmail()).findUnique();
                if (account != null && account.isEnrolledTo(realm)) {
                    if (account.isEnrolledTo(realm)) {
                        if (realm.removeAccount(account)) {
                            realm.save();
                            result.put("revoked", result.get("revoked") != null ? result.get("revoked") + 1 : 1);
                        } else {
                            result.put("revoke-error", result.get("revoke-error") != null ? result.get("revoke-error") + 1 : 1);
                        }
                    } else {
                        result.put("not-enrolled", result.get("not-enrolled") != null ? result.get("not-enrolled") + 1 : 1);
                    }
                } else {
                    if (revokeAccountInvite(realm, user.getEmail())) {
                        result.put("revoked-invite", result.get("revoked-invite") != null ? result.get("revoked-invite") + 1 : 1);
                    } else {
                        result.put("unknown-user", result.get("unknown-user") != null ? result.get("unknown-user") + 1 : 1);
                    }
                }
            }

            StringBuilder sb = new StringBuilder();
            if (result.get("revoked") != null)
                sb.append(Messages.get("REALM.ADMIN.REVOKE.USERS.FLASH.COUNT.REVOKED", result.get("enrolled"))).append("<br>");
            if (result.get("revoked-invite") != null)
                sb.append(Messages.get("REALM.ADMIN.REVOKE.USERS.FLASH.COUNT.REVOKED.INVITE", result.get("revoked-invite"))).append("<br>");
            if (result.get("revoke-error") != null)
                sb.append(Messages.get("REALM.ADMIN.REVOKE.USERS.FLASH.COUNT.REVOKE.ERROR", result.get("revoke-error"))).append("<br>");

            int count = 0;
            count += result.get("not-enrolled") != null ? result.get("not-enrolled") : 0;
            count += result.get("unknown-user") != null ? result.get("unknown-user") : 0;
            if (count > 0)
                sb.append(Messages.get("REALM.ADMIN.REVOKE.USERS.FLASH.COUNT.REVOKE.NOT.INVOLVED", count)).append("<br>");

            flash("info", sb.toString());
        } catch (IOException e) {
            flash("danger", Messages.get("ERROR.FORM.FILE"));
        }

        return redirect(routes.RevokeUsersAdminRealmController.GET_RevokeUsers(realm));
    }

    /**
     * Revoke current invite for this user (email address)
     *
     * @param realm Realm context to revoke invitation
     * @param email Email to revoke invitation to realm
     * @return {@code true} if a least one invitation revoked, otherwise, {@code false}
     * @since 15.12
     */
    private boolean revokeAccountInvite(RealmModel realm, String email) {
        final boolean[] result = {false};
        final Jedis jedis = Play.application().injector().instanceOf(RedisModule.class).getConnection();
        jedis.select(0);
        if (jedis.exists(String.format("account.pre.realm.%s", email))) {
            final Set<String> invites = jedis.zrangeByScore(String.format("account.pre.realm.%s", email), String.valueOf(System.currentTimeMillis() / 1000D), "+inf");
            invites.stream()
                    .filter(invite -> invite.contains(realm.getUidAsString()))
                    .forEach(invite -> {
                        jedis.zrem(String.format("account.pre.realm.%s", email), invite);
                        result[0] = true;
                    });
        }
        return result[0];
    }

    /**
     * Set form instance with File upload from MultipartFormData
     *
     * @param manageRealmUsers The current form of manage users Realm
     * @return The FormData giving with File CSV informed
     * @since 15.12
     */
    private ManageRealmUsersAdminRealmForm processForm(ManageRealmUsersAdminRealmForm manageRealmUsers) {
        final Http.MultipartFormData multipartFormData = Http.Context.current().request().body().asMultipartFormData();
        if (multipartFormData != null) {
            if (multipartFormData.getFile("csv") != null) {
                manageRealmUsers.csv = multipartFormData.getFile("csv").getFile();
                manageRealmUsers.originalFileName = multipartFormData.getFile("csv").getFilename();
            }
        }
        return manageRealmUsers;
    }
}
