/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package controllers.realm.admin;

import actions.SessionRequired;
import actors.SearchEngineActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import forms.realm.admin.ManageRealmGroupForm;
import models.*;
import play.db.ebean.Transactional;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.form.ExtendedForm;
import toolbox.helper.ActorHelper;
import toolbox.helper.ImageHelper;
import toolbox.helper.SessionHelper;
import views.html.realmview.admin.ManageRealmGroupView;
import views.html.systemview.error403;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ManageRealmGroupController.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class ManageRealmGroupController extends Controller {

    private final static List<PermissionModel> PERMISSIONS = PermissionModel.findByNamespace(PermissionModel.Namespace.REALM_GROUP);

    /**
     * Maximum suggestions to autocomplete
     */
    private final static int AUTOCOMPLETE_MAX_ROWS = 10;

    /**
     * Size of logo
     */
    private final static int[] LOGO_SIZE = new int[]{512, 512};

    /**
     * Size of banner
     */
    private final static int[] BANNER_SIZE = new int[]{1200, 250};

    /**
     * Handle to SearchEngine actor reference.
     */
    private final ActorRef searchEngineActor;

    @Inject
    public ManageRealmGroupController(ActorSystem actorSystem) {
        this.searchEngineActor = actorSystem.actorOf(SearchEngineActor.props);
    }
    /**
     * Show the 'manage group' page of the current realm.
     *
     * @param realm The current realm (binding by slug)
     * @return The selected realm page or the realm selector page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_ManageRealmGroups(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.group")) {
            return forbidden(error403.render());
        }

        final ExtendedForm<ManageRealmGroupForm, RealmGroupModel> form = ExtendedForm.form(ManageRealmGroupForm.class, (RealmGroupModel)null);
        final ManageRealmGroupForm groupForm = new ManageRealmGroupForm();
        setupPermissions(groupForm,null);

        return ok(ManageRealmGroupView.render(realm, null,form.fill(groupForm), 0));
    }

    /**
     * Process the submitted Manage group form
     *
     * @param realm
     * @return
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_CreateRealmGroup(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.group")) {
            return forbidden(error403.render());
        }

        final ExtendedForm<ManageRealmGroupForm, RealmGroupModel> form = ExtendedForm.form(ManageRealmGroupForm.class, (RealmGroupModel) null).bindFromRequest();

        if (form.hasErrors()) {
            flash("danger", play.i18n.Messages.get("FORM.FLASH.ERRORS"));

            int count = 0;
            while (true) {
                try {
                    if (form.field("members[" + count + "].id").value() != null) {
                        count++;
                        continue;
                    }
                } catch (Exception ignore) {
                }
                break;
            }

            return badRequest(ManageRealmGroupView.render(realm, null, form, count));
        }

        final ManageRealmGroupForm groupForm = processForm(form.get());

        final RealmGroupModel realmGroup = new RealmGroupModel();
        realmGroup.setName(groupForm.name);
        realmGroup.setSlug(groupForm.slug);

        final S3FileModel logoS3File = ImageHelper.convertToS3File(groupForm.logo, groupForm.logo_crop, ManageRealmGroupController.LOGO_SIZE);
        if (logoS3File != null) {
            realmGroup.setLogo(logoS3File);
        }

        final S3FileModel bannerS3File = ImageHelper.convertToS3File(groupForm.banner, groupForm.banner_crop, ManageRealmGroupController.BANNER_SIZE);
        if (bannerS3File != null) {
            realmGroup.setBanner(bannerS3File);
        }

        processGroupMembersAndPermissions(realmGroup, groupForm, false);
        realmGroup.getRealms().add(realm);
        realmGroup.save();
        realm.addGroup(realmGroup);
        realm.save();
        ActorHelper.indexModel(searchEngineActor, realmGroup);

        flash("success", Messages.get("REALM.ADMIN.REALM.GROUP.FLASH.SUCCESS"));
        return redirect(routes.ManageRealmGroupController.GET_ManageRealmGroup(realm, realmGroup));
    }

    /**
     * Show the 'manage group' page of the current realm for a specific group.
     *
     * @param realm The current realm (binding by slug)
     * @return The selected realm page or the realm selector page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_ManageRealmGroup(final RealmModel realm, final RealmGroupModel realmGroup) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.group")) {
            return forbidden(error403.render());
        }

        final ExtendedForm<ManageRealmGroupForm, RealmGroupModel> form = ExtendedForm.form(ManageRealmGroupForm.class, realmGroup);

        final ManageRealmGroupForm groupForm = new ManageRealmGroupForm();

        groupForm.name = realmGroup.getName();
        groupForm.slug = realmGroup.getSlug();
        groupForm.members = new ArrayList<>();
        groupForm.admins = new ArrayList<>();

        realmGroup.getAccounts().stream().filter(m -> m != null).forEach(m -> {
            final ManageRealmGroupForm.GroupMember gm = new ManageRealmGroupForm.GroupMember();
            gm.id = m.getUid();
            gm.fullname = m.getFullName();
            groupForm.members.add(gm);
        });

        RoleRealmGroupModel adminRole = null;
        if (!realmGroup.getRoles().isEmpty()) {
            //group has only admin role
            adminRole = realmGroup.getRoles().get(0);
            adminRole.getAccounts().stream().filter(m -> m != null).forEach(m -> {
                final ManageRealmGroupForm.GroupMember gm = new ManageRealmGroupForm.GroupMember();
                gm.id = m.getUid();
                gm.fullname = m.getFullName();
                groupForm.admins.add(gm);
            });
        }

        setupPermissions(groupForm, adminRole);

        return ok(ManageRealmGroupView.render(realm, realmGroup, form.fill(groupForm), groupForm.members.size()));
    }

    private void setupPermissions(final ManageRealmGroupForm groupForm, final RoleRealmGroupModel adminRole) {
        groupForm.adminPermissions = new ArrayList<>();
        for (final PermissionModel realmPermission : PERMISSIONS) {
            final ManageRealmGroupForm.SubItemPermForm frmSubperm = new ManageRealmGroupForm.SubItemPermForm();
            frmSubperm.add = adminRole != null && adminRole.getPermissions().contains(realmPermission);
            frmSubperm.name = realmPermission.getScope();
            groupForm.adminPermissions.add(frmSubperm);
        }
    }

    /**
     * Process the submitted Manage group form
     *
     * @param realm
     * @return
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_EditRealmGroup(final RealmModel realm, final RealmGroupModel realmGroup) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.group")) {
            return forbidden(error403.render());
        }

        final ExtendedForm<ManageRealmGroupForm, RealmGroupModel> form = ExtendedForm.form(ManageRealmGroupForm.class, realmGroup).bindFromRequest();

        if (form.hasErrors()) {
            flash("danger", play.i18n.Messages.get("FORM.FLASH.ERRORS"));

            int count = 0;
            while (true) {
                try {
                    if (form.field("members[" + count + "].id").value() != null) {
                        count++;
                        continue;
                    }
                } catch (Exception ignore) {
                }
                break;
            }

            return badRequest(ManageRealmGroupView.render(realm, realmGroup, form, count));
        }

        final ManageRealmGroupForm groupForm = processForm(form.get());

        realmGroup.setName(groupForm.name);
        realmGroup.setSlug(groupForm.slug);

        final S3FileModel logoS3File = ImageHelper.convertToS3File(groupForm.logo, groupForm.logo_crop, ManageRealmGroupController.LOGO_SIZE);
        if (logoS3File != null) {
            realmGroup.setLogo(logoS3File);
        }

        final S3FileModel bannerS3File = ImageHelper.convertToS3File(groupForm.banner, groupForm.banner_crop, ManageRealmGroupController.BANNER_SIZE);
        if (bannerS3File != null) {
            realmGroup.setBanner(bannerS3File);
        }

        processGroupMembersAndPermissions(realmGroup, groupForm, true);

        realmGroup.save();
        ActorHelper.indexModel(searchEngineActor, realmGroup);

        flash("success", Messages.get("REALM.ADMIN.REALM.GROUP.FLASH.SUCCESS"));
        return redirect(routes.ManageRealmGroupController.GET_ManageRealmGroup(realm, realmGroup));
    }

    private void processGroupMembersAndPermissions(final RealmGroupModel realmGroup,
                                                   final ManageRealmGroupForm groupForm,
                                                   final boolean delete) {
        if (groupForm.members != null) {
            groupForm.members.stream().forEach(m -> {
                processGroupMember(realmGroup, m, delete);
            });
        }

        final RoleRealmGroupModel adminRole = getAdminRole(realmGroup);

        if (groupForm.admins != null) {
            groupForm.admins.stream().forEach(m -> {
                processGroupMember(adminRole, m, delete);
            });
        }
        if (groupForm.adminPermissions != null) {
            for (final ManageRealmGroupForm.SubItemPermForm itmPerm : groupForm.adminPermissions) {
                PermissionModel permission = PermissionModel.findByName(itmPerm.name, PermissionModel.Namespace.REALM_GROUP);
                if (permission != null) {
                    if (itmPerm.add) {
                        adminRole.addPermission(permission);
                    } else {
                        adminRole.removePermission(permission);
                    }
                }
            }
        }
        adminRole.save();
    }

    private RoleRealmGroupModel getAdminRole(final RealmGroupModel realmGroup) {
        final RoleRealmGroupModel adminRole;
        if (realmGroup.getRoles().isEmpty()) {
            realmGroup.save();
            adminRole = new RoleRealmGroupModel();
            adminRole.setName(RoleRealmGroupModel.ADMIN_ROLE_NAME_PREFIX + realmGroup.getName());
            adminRole.setGroup(realmGroup);
            adminRole.save();
            realmGroup.getRoles().add(adminRole);
        } else {
            adminRole = realmGroup.getRoles().get(0);
        }
        return adminRole;
    }

    private void processGroupMember(final HasAccountsList groupContainer, final ManageRealmGroupForm.GroupMember m, final boolean delete) {
        if (m != null) {
            final AccountModel a = AccountModel.findByUid(m.id);
            if (a != null) {
                if (m.delete) {
                    if (delete) {
                        groupContainer.removeAccount(a);
                    }
                } else {
                    groupContainer.addAccount(a);
                }
            }
        }
    }

    /**
     * List users on realm for a given keywork,
     * search in : emails, first name, last name
     *
     * @param realm The current realm (binding by slug)
     * @return List of Users (Json format)
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_AutocompleteFindRealmAccount(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.group")) {
            return forbidden(Json.newObject().put("error_code", "403").put("error", "Forbidden"));
        }

        final Map<String, String[]> request = Http.Context.current().request().body().asFormUrlEncoded();

        String keyword = null;
        if (request != null && request.get("search").length == 1 && !request.get("search")[0].isEmpty()) {
            keyword = request.get("search")[0];
        }

        if (keyword == null) {
            return badRequest(Json.newObject().put("error_code", "400").put("error", "Bad request"));
        }

        final ArrayNode result = convertRealmUserToJson(realm, keyword);

        if (result.size() == 0) {
            return notFound(Json.newObject().put("error_code", "404").put("error", "Not found"));
        }

        return ok(result);
    }

    /**
     * Convert user to a custom json representation.
     *
     * @param realm   The current realm, where users must been from
     * @param keyword The keyword to find am users occurrences
     * @return An Json array {@code ArrayNode} with size represent number of child, otherwise, 0 (empty)
     * @since 15.10
     */
    private ArrayNode convertRealmUserToJson(final RealmModel realm, final String keyword) {
        final ArrayNode result = Json.newArray();
        for (final AccountModel a : realm.getAccounts()) {
            boolean keywordInEmails = a.getEmails().stream().filter(e -> e.getEmail().contains(keyword)).findFirst().orElse(null) != null;
            if (keywordInEmails
                    || a.getFirstName().toLowerCase().contains(keyword.toLowerCase())
                    || a.getLastName().toLowerCase().contains(keyword.toLowerCase())) {
                final ObjectNode userNode = result.addObject();
                final ArrayNode mailsNode = userNode.putArray("emails");
                a.getEmails().stream().forEach(e -> mailsNode.add(e.getEmail()));
                userNode.put("uid", a.getUidAsString());
                userNode.put("avatar", "");
                userNode.put("firstName", a.getFirstName());
                userNode.put("lastName", a.getLastName());
                userNode.put("fullName", a.getFullName());
                userNode.put("primaryEmail", a.getPrimaryEmail());
                if (userNode.size() == AUTOCOMPLETE_MAX_ROWS) {
                    return result;
                }
            }
        }
        return result;
    }

    /**
     * Set form instance with File upload as MultipartFormDataact
     *
     * @param groupFormData The current form of editing RealmGroup
     * @return The FromData giving with File logo and banner informed
     * @since 15.10
     */
    private ManageRealmGroupForm processForm(final ManageRealmGroupForm groupFormData) {
        final Http.MultipartFormData multipartFormData = Http.Context.current().request().body().asMultipartFormData();
        if (multipartFormData != null) {
            if (multipartFormData.getFile("logo") != null) {
                groupFormData.logo = multipartFormData.getFile("logo").getFile();
            }
            if (multipartFormData.getFile("banner") != null) {
                groupFormData.banner = multipartFormData.getFile("banner").getFile();
            }
        }
        return groupFormData;
    }
}
