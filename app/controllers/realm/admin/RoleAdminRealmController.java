/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.realm.admin;

import actions.SessionRequired;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import forms.realm.admin.RolePermissionsAdminRealmForm;
import forms.realm.admin.RoleUsersAdminRealmForm;
import forms.realm.admin.RolesAdminRealmForm;
import models.*;
import play.Logger;
import play.data.Form;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.*;
import play.twirl.api.Html;
import toolbox.helper.SessionHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * RoleAdminRealmController.
 *
 * @author Jean-Pierre Boudic
 * @version 15.10
 * @since 15.10
 */
public class RoleAdminRealmController extends Controller {

    private final static List<PermissionModel> REALM_PERMISSION = PermissionModel.findByNamespace(PermissionModel.Namespace.REALM);

    /**
     * Show list of Roles in this realm
     *
     * @param realm The current realm (binding by slug)
     * @return The selected realm role list page
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_RolesAdministration(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "network.role")) {
            return forbidden(views.html.systemview.error403.render());
        }

        final Form<RolesAdminRealmForm> frmRoles = Form.form(RolesAdminRealmForm.class);
        final RolesAdminRealmForm frmRolesData = new RolesAdminRealmForm();

        frmRolesData.roles = new ArrayList<>();
        for (final RoleRealmModel roleRealm : realm.getRoles()) {
            final RolesAdminRealmForm.SubItemRoleForm subFrmRolesData = new RolesAdminRealmForm.SubItemRoleForm();
            subFrmRolesData.name = roleRealm.getName();
            subFrmRolesData.uid = roleRealm.getUid();
            subFrmRolesData.readonly = roleRealm.isReadonly();
            frmRolesData.roles.add(subFrmRolesData);
        }

        final RolesAdminRealmForm.RolesFormData<RoleRealmModel> data = new RolesAdminRealmForm.RolesFormData<>(realm.getRoles(), role -> routes.RoleAdminRealmController.GET_RoleUsersAdministration(realm,role));
        return ok(views.html.realmview.admin.roles.RolesAdminRealmView.render(realm, frmRoles.fill(frmRolesData), data));
    }

    /**
     * Show basic form for create a custom realm role valid and save
     *
     * @param realm The current realm (binding by slug)
     * @return A form for create a new role
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_RolesAdministration(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "network.role")) {
            return forbidden(views.html.systemview.error403.render());
        }

        final Form<RolesAdminRealmForm> formRole = Form.form(RolesAdminRealmForm.class).bindFromRequest();

        Logger.info("=============================================");
        Logger.info("Error?: {}", formRole.hasErrors());
        Logger.info("formRole: {}", formRole);

        if (!formRole.hasErrors()) {
            RolesAdminRealmForm frmRolesData = formRole.get();

            for (final RolesAdminRealmForm.SubItemRoleForm subFrmRoleData : frmRolesData.roles) {
                if (subFrmRoleData.uid != null && subFrmRoleData.name != null) {
                    final RoleRealmModel roleRealm = RoleRealmModel.find.where().like("uid", subFrmRoleData.uid.toString()).findUnique();
                    if (roleRealm != null) {
                        if (subFrmRoleData.delete || subFrmRoleData.name.isEmpty()) {
                            roleRealm.delete();
                        } else {
                            roleRealm.setName(subFrmRoleData.name);
                            roleRealm.save();
                        }
                    }
                } else if (subFrmRoleData.uid == null && subFrmRoleData.name != null && !subFrmRoleData.name.isEmpty()) {
                    final RoleRealmModel roleRealm = new RoleRealmModel();
                    roleRealm.setName(subFrmRoleData.name);
                    roleRealm.setRealm(realm);
                    roleRealm.save();
                }
            }

            flash("success", Messages.get("REALM.ADMIN.ROLE.ADD.FLASH.SUCCESS"));
            return redirect(routes.RoleAdminRealmController.GET_RolesAdministration(realm));
        }

        final RolesAdminRealmForm.RolesFormData<RoleRealmModel> data = new RolesAdminRealmForm.RolesFormData<>(realm.getRoles(), role -> routes.RoleAdminRealmController.GET_RoleUsersAdministration(realm,role));
        return badRequest(views.html.realmview.admin.roles.RolesAdminRealmView.render(realm, formRole, data));
    }

    /**
     * Show all users for a given Realm and Role, can add, delete users
     *
     * @param realm The current realm (binding by slug)
     * @param role  The current role (in realm) (binding by uid)
     * @return The list of user for this role
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_RoleUsersAdministration(final RealmModel realm, RoleRealmModel role) {
        if (!SessionHelper.getAccount().hasPermission(realm, "network.role")) {
            return forbidden(views.html.systemview.error403.render());
        }
        return ok(getRenderUsersView(realm, role, null));
    }

    /**
     * Save all users for a given Realm and Role (add, delete)
     *
     * @param realm The current realm (binding by slug)
     * @param role  The current role (in realm) (binding by uid)
     * @return List of users granted on realm
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_RoleUsersAdministration(final RealmModel realm, final RoleRealmModel role) {
        if (!SessionHelper.getAccount().hasPermission(realm, "network.role")) {
            return forbidden(views.html.systemview.error403.render());
        }

        final Form<RoleUsersAdminRealmForm> formUsersRole = Form.form(RoleUsersAdminRealmForm.class).bindFromRequest();
        if (!formUsersRole.hasErrors()) {
            final RoleUsersAdminRealmForm usersRoleFormData = formUsersRole.get();
            usersRoleFormData.saveUsers(realm,role);
            role.save();
            return redirect(routes.RoleAdminRealmController.GET_RoleUsersAdministration(realm, role));
        }

        return badRequest(getRenderUsersView(realm, role, formUsersRole));
    }

    private Html getRenderUsersView(RealmModel realm, RoleRealmModel role, Form<RoleUsersAdminRealmForm> formUsersRole) {
        return views.html.realmview.admin.roles.RoleUsersAdminRealmView.render(realm, role, formUsersRole, false, "NETWORK.ROLE",
                routes.RoleAdminRealmController.GET_RoleUsersAdministration(realm, role),
                routes.RoleAdminRealmController.GET_RolePermissionsAdministration(realm, role));
    }

    private Html getRenderPermsView(RealmModel realm, RoleRealmModel role, Form<RolePermissionsAdminRealmForm> formPermsRole) {
        return views.html.realmview.admin.roles.RolePermissionsAdminRealmView.render(realm, role, formPermsRole, "NETWORK.ROLE",
                routes.RoleAdminRealmController.GET_RoleUsersAdministration(realm, role),
                routes.RoleAdminRealmController.GET_RolePermissionsAdministration(realm, role));
    }

    /**
     * Show permissions for a given Role
     *
     * @param realm The current realm (binding by slug)
     * @param role  The current role (in realm) (binding by uid)
     * @return List of Permissions
     * @since 15.10
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_RolePermissionsAdministration(final RealmModel realm, RoleRealmModel role) {
        if (!SessionHelper.getAccount().hasPermission(realm, "network.role")) {
            return forbidden(views.html.systemview.error403.render());
        }

        final Form<RolePermissionsAdminRealmForm> frmPerms = Form.form(RolePermissionsAdminRealmForm.class);
        final RolePermissionsAdminRealmForm perms = new RolePermissionsAdminRealmForm();

        perms.init(role, REALM_PERMISSION);
        return ok(getRenderPermsView(realm,role,frmPerms.fill(perms)));
    }

    /**
     * Update ? Show permissions for a given Role
     *
     * @param realm The current realm (binding by slug)
     * @param role  The current role (in realm) (binding by uid)
     * @return List of Permissions
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_RolePermissionsAdministration(final RealmModel realm, RoleRealmModel role) {
        if (!SessionHelper.getAccount().hasPermission(realm, "network.role")) {
            return forbidden(views.html.systemview.error403.render());
        }

        if (role.isReadonly()) {
            return forbidden(views.html.systemview.error403.render());
        }

        final Form<RolePermissionsAdminRealmForm> frmPerms = Form.form(RolePermissionsAdminRealmForm.class).bindFromRequest();

        if (!frmPerms.hasErrors()) {
            final RolePermissionsAdminRealmForm permsFrmData = frmPerms.get();

            permsFrmData.save(role, PermissionModel.Namespace.REALM);
            role.save();

            flash("success", Messages.get("REALM.ADMIN.ROLE.PERMISSIONS.FLASH.SUCCESS", role.getName()));
            return redirect(routes.RoleAdminRealmController.GET_RolePermissionsAdministration(realm, role));
        }

        return badRequest(getRenderPermsView(realm,role,frmPerms));
    }

    /**
     * List users on realm for a given kewword,
     * search in emails, firstname, lastname
     *
     * @param realm The current realm (binding by slug)
     * @return List of Users (JSON Format)
     * @since 15.10
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    public Result POST_getUsers(final RealmModel realm) {
        if (!SessionHelper.getAccount().hasPermission(realm, "network.role")) {
            return forbidden(views.html.systemview.error403.render());
        }

        Map<String, String[]> request = Http.Context.current().request().body().asFormUrlEncoded();
        String kewword = "";
        if (request != null && request.get("search").length == 1 && !request.get("search")[0].isEmpty()) {
            kewword = request.get("search")[0];
        }
        return ok(getJSONUserByKeyword(realm, kewword));
    }

    /**
     * Make a JSON Object Array of 'Users'
     * User are Account, Actor, Profile informations merged
     *
     * @param realm The current realm (binding by slug)
     * @return List of Users (JSON Object)
     * @since 15.10
     */
    private JsonNode getJSONUserByKeyword(final RealmModel realm, final String keyword) {
        final ArrayNode result = Json.newArray();

        //TODO Use SearchEngine for fill accounts list
        for (final AccountModel account : realm.getAccounts()) {
            String tmp = "";
            for (final AccountEmailModel accountEmail : account.getEmails()) {
                tmp += accountEmail.getEmail() + ";";
            }

            final String allEmails = tmp;
            if (account.getFirstName().contains(keyword) ||
                    account.getLastName().contains(keyword) ||
                    allEmails.contains(keyword)) {

                /*** Make JSON Format ***/
                final ArrayNode mails = Json.newArray();
                for (final AccountEmailModel email : account.getEmails()) {
                    mails.add(email.getEmail());
                }
                result.addObject()
                        .put("uid", account.getUidAsString())
                        .put("avatar", "")
                        .put("firstName", account.getFirstName())
                        .put("lastName", account.getLastName())
                        .put("fullName", account.getFullName())
                        .put("primaryEmail", account.getPrimaryEmail())
                        .put("emails", mails);
            }
        }

        return Json.toJson(result);
    }

}
