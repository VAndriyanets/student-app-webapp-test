package controllers.realm;

import actions.SessionRequired;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import forms.realm.AddPostForm;
import models.*;
import play.db.ebean.Transactional;
import play.filters.csrf.RequireCSRFCheck;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.form.ExtendedForm;
import toolbox.helper.SessionHelper;

import java.util.List;

/**
 * @author Andrey Sokolov
 */
public class CommentsController extends Controller {

    public static final int INITIAL_PAGE_SIZE = 5;
    public static final int ALL_PAGES_SIZE = 40;

    @Security.Authenticated(SessionRequired.class)
    public Result GET_comments(final Long postId) {
        final PostModel postModel = PostModel.find.byId(postId);

        final PagedList<CommentModel> commentModelPagedList = CommentModel.find(postModel, 0, INITIAL_PAGE_SIZE);
        return ok(toJson(commentModelPagedList.getTotalRowCount(), commentModelPagedList.getPageIndex(), commentModelPagedList.getList()));
    }

    @Security.Authenticated(SessionRequired.class)
    public Result GET_allComments(final Long postId) {
        final PostModel postModel = PostModel.find.byId(postId);

        final List<CommentModel> commentModelPagedList = CommentModel.findAll(postModel);
        return ok(toJson(commentModelPagedList.size(), 0, commentModelPagedList));
    }


    @Security.Authenticated(SessionRequired.class)
    public Result GET_allCommentsPaged(final Long postId, int pageIndex) {
        final PostModel postModel = PostModel.find.byId(postId);

        final PagedList<CommentModel> commentModelPagedList = CommentModel.find(postModel, pageIndex, ALL_PAGES_SIZE);
        return ok(toJson(commentModelPagedList.getTotalRowCount(), commentModelPagedList.getPageIndex(), commentModelPagedList.getList()));
    }


    private JsonNode toJson(int totalRowCount, int pageIndex, List<CommentModel> commentModelList) {
        final AccountModel user = SessionHelper.getAccount();
        ObjectNode ret = Json.newObject();
        ret.put("total", totalRowCount);
        ret.put("page", pageIndex);
        final ArrayNode comments = ret.putArray("comments");
        final List<CommentModel> list = commentModelList;
        if (list != null && !list.isEmpty()) {
            for (CommentModel comment : list) {
                final ObjectNode node = comments.addObject();
                fillCommentJson(comment, node, user.equals(comment.author));
            }
        }
        return ret;
    }

    private void fillCommentJson(CommentModel comment, ObjectNode node, boolean self) {
        node.put("id", comment.id);
        node.put("text", comment.text);
        node.put("created", comment.getReadableElapsedTime());
        node.put("authorName", comment.author.getFullNameLight());
        node.put("authorAvatar", comment.author.getAvatarUrlAsString());
        if (self) {
            node.put("self", true);
        }
        if (comment.pictures != null && !comment.pictures.isEmpty()) {
            final ArrayNode pictures = node.putArray("pictures");
            for (S3FileModel picture : comment.pictures) {
                pictures.add(picture.getUrlAsString());
            }
        }
        final ActorModel actor = comment.author.getActor();
        if (actor != null) {
            node.put("authorSlug", actor.getSlug());
        }
    }

    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_comment(final Long postId) {
        final ExtendedForm<AddPostForm, RealmModel> extendedForm = ExtendedForm.form(AddPostForm.class, (RealmModel) null).bindFromRequest();
        if (!extendedForm.hasErrors()) {
            final PostModel postModel = PostModel.find.byId(postId);
            if (postModel != null) {
                final AccountModel account = SessionHelper.getAccount();
                if (postModel.canVote(account)) {
                    final AddPostForm realFormData = extendedForm.get();
                    final AccountModel author = SessionHelper.getAccount();
                    final CommentModel commentModel = realFormData.getCommentModel();
                    commentModel.author = author;
                    commentModel.post = postModel;
                    commentModel.save();
                    ObjectNode ret = Json.newObject();
                    final ObjectNode commentJson = ret.putObject("comment");
                    fillCommentJson(commentModel, commentJson, true);
                    return ok(ret);
                } else {
                    return forbidden();
                }
            } else {
                return notFound();
            }
        }
        return badRequest(extendedForm.errorsAsJson());
    }


    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result DELETE_comment(final Long commentId) {
        final CommentModel comment = CommentModel.find.byId(commentId);
        final AccountModel account = SessionHelper.getAccount();
        if (comment != null) {
            if (comment.post.canDeleteAllComments(account) || comment.author.equals(account)) {
                comment.delete();
                return ok("ok");
            } else {
                return forbidden();
            }
        } else {
            return notFound();
        }
    }
}
