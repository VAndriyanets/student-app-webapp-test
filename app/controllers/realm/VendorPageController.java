package controllers.realm;

import actions.SessionRequired;
import actors.SearchEngineActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.avaje.ebean.PagedList;
import com.google.inject.Inject;
import forms.common.PaginationData;
import forms.realm.admin.VendorInformationForm;
import models.*;
import play.Logger;
import play.db.ebean.Transactional;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.form.ExtendedForm;
import toolbox.helper.ActorHelper;
import toolbox.helper.ImageHelper;
import toolbox.helper.SessionHelper;
import views.html.systemview.error403;

import java.util.List;
import java.util.UUID;

/**
 * @author Andrey Sokolov
 */
public class VendorPageController extends Controller {

    /**
     * Size of Logo
     */
    final static int[] LOGO_SIZE = new int[]{512, 512};
    public static final int ARTICLES_NUMBER = 3;
    public static final int ARTICLES_PAGE_SIZE = 20;

    /**
     * Handle to SearchEngine actor reference.
     */
    private final ActorRef searchEngineActor;

    @Inject
    public VendorPageController(ActorSystem actorSystem) {
        this.searchEngineActor = actorSystem.actorOf(SearchEngineActor.props);
    }

    @Security.Authenticated(SessionRequired.class)
    public Result GET_vendorPage(final RealmModel realm, final RealmPageModel page) {
        final PagedList<ArticleModel> articleModels = ArticleModel.findLastForVendor(page, 0, ARTICLES_NUMBER);
        final List<RealmGroupModel> groups = SessionHelper.getAccount().getRealmGroups(realm);
        final List<PostModel> postModels = PostModel.findVendorPostsInGroups(groups);
        return ok(views.html.realmview.vendorpage.VendorPageView.render(realm, page, articleModels.getList(), postModels, articleModels.getTotalPageCount() > 1));
    }


    @Security.Authenticated(SessionRequired.class)
    public Result GET_vendorPageDetails(final RealmModel realm, final RealmPageModel page, final Integer pageIndex) {
        final PagedList<ArticleModel> articleModels = ArticleModel.findLastForVendor(page, pageIndex - 1, ARTICLES_PAGE_SIZE);
        final PaginationData pagination = new PaginationData(pageIndex, ARTICLES_PAGE_SIZE, articleModels.getTotalRowCount(), articleModels.getTotalPageCount() > 1);
        return ok(views.html.realmview.vendorpage.VendorPageDetailsView.render(realm, page, articleModels.getList(), pagination));
    }


    @Security.Authenticated(SessionRequired.class)
    public Result GET_PageAdministration(final RealmModel realm, final RealmPageModel page, String activeInfo) {

        return ok(views.html.realmview.vendorpage.VendorPageAdminView.render(realm, page, activeInfo));
    }

    @Security.Authenticated(SessionRequired.class)
    public Result GET_PageAdministrationArticleHistory(final RealmModel realm, final RealmPageModel page, final ArticleModel article) {
        // TODO
        return ok("TODO");
    }

    /**
     * Show the Vendor Information page of the current realm page.
     *
     * @param realm  The current realm
     * @param vendor The current vendor
     * @return The selected realm page Vendor Info page
     */
    @AddCSRFToken
    @Security.Authenticated(SessionRequired.class)
    public Result GET_PageAdministrationVendorInfo(final RealmModel realm, RealmPageModel vendor) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.page")) {
            return forbidden(error403.render());
        }
        return ok(views.html.realmview.vendorpage.admin.VendorInformationView.render(realm, vendor, null));
    }

    /**
     * Process vendor information edition.
     *
     * @param realm The current realm
     * @param page  The current vendor page
     * @return Redirect to vendor information
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_PageAdministrationVendorInfo(final RealmModel realm, final RealmPageModel page) {
        if (!SessionHelper.getAccount().hasPermission(realm, "social.page")) {
            return forbidden(error403.render());
        }
        final ExtendedForm<VendorInformationForm, RealmPageModel> form = ExtendedForm.form(VendorInformationForm.class, page).bindFromRequest();

        if (form.hasErrors()) {
            flash("flash", Messages.get("FORM.FLASH.ERRORS"));
            //roles form readonly for pages
            return badRequest(views.html.realmview.vendorpage.admin.VendorInformationView.render(realm, page, form));
        }

        final Http.MultipartFormData multipartFormData = Http.Context.current().request().body().asMultipartFormData();
        if (multipartFormData != null) {
            if (multipartFormData.getFile("logo") != null) {
                form.get().logo = multipartFormData.getFile("logo").getFile();
            }
            if (multipartFormData.getFile("ibanPdf") != null) {
                form.get().ibanPDF = multipartFormData.getFile("ibanPdf").getFile();
            }
        }

        page.setName(form.get().name);
        VendorProfileModel vendorProfile = page.getVendorProfile() != null ? page.getVendorProfile() : new VendorProfileModel();
        vendorProfile.setDescription(form.get().desc);

        final S3FileModel logoS3File = ImageHelper.convertToS3File(form.get().logo, form.get().logo_crop, LOGO_SIZE);
        if (logoS3File != null) {
            page.setLogo(logoS3File);
        }

        if (form.get().ibanPDF != null) {
            final S3FileModel f = new S3FileModel();
            f.setContentType("application/pdf");
            f.setSubDirectory("realm");
            f.setName(UUID.randomUUID().toString());
            f.setFile(form.get().ibanPDF);
            f.save();
            vendorProfile.setIbanPdf(f);
        }

        vendorProfile.setEmail(form.get().email);
        vendorProfile.setPhone(form.get().phone);
        vendorProfile.setAddress(form.get().address);
        vendorProfile.setCity(form.get().city);
        vendorProfile.setZipcode(form.get().zipcode);
        vendorProfile.setIban(form.get().iban);

        vendorProfile.setVendor(page);
        vendorProfile.save();

        page.save();
        ActorHelper.indexModel(searchEngineActor, page);

        flash("success", Messages.get("VENDOR.PAGE.ADMIN.INFO.UPDATED"));
        return redirect(routes.VendorPageController.GET_PageAdministrationVendorInfo(realm, page));

    }

    /**
     * Remove IBAN document from vendor information.
     *
     * @param realm The current realm
     * @param page  The current vendor page
     * @return Redirect to vendor information
     */
    @RequireCSRFCheck
    @Security.Authenticated(SessionRequired.class)
    @Transactional
    public Result POST_PageAdministrationRemoveIBAN(final RealmModel realm, final RealmPageModel page) {
        try {
            VendorProfileModel vendorProfile = page.getVendorProfile();
            vendorProfile.deleteIbanPdf();
            flash("success", Messages.get("VENDOR.PAGE.ADMIN.INFO.IBAN.DROPPED"));
        } catch (Exception e) {
            Logger.error("Could not remove IBAN/BIC Document", e);
            flash("danger", Messages.get("VENDOR.PAGE.ADMIN.INFO.IBAN.DROPERR"));
        }

        return redirect(routes.VendorPageController.GET_PageAdministrationVendorInfo(realm, page));
    }
}