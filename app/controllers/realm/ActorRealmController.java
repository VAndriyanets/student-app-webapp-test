/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package controllers.realm;

import actions.SessionRequired;
import models.ActorModel;
import models.RealmModel;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import toolbox.helper.SessionHelper;

/**
 * ActorRealmController.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class ActorRealmController extends Controller {

    /**
     * Show the social area of a member of the current realm. Member can
     * be a group of users, a page or a simple user.
     *
     * @return The actor's social area
     * @since 15.10
     */
    @Security.Authenticated(SessionRequired.class)
    public Result GET_Actor(final RealmModel realm, final ActorModel actor) {
        if (!SessionHelper.getAccount().isEnrolledTo(realm)) {
            flash("danger", Messages.get("REALM.ERROR.NOT_ENROLLED"));
            return redirect(controllers.routes.BaseApplicationController.GET_BaseApplication());
        }
        return ok(views.html.realmview.ActorRealmView.render(realm, actor));
    }
}
