package toolbox.helper;

import controllers.Assets;
import controllers.routes;
import models.ArticleModel;

/**
 * @author Andrey Sokolov
 */
public final class PicturesHelper {

    public static String getLogoUrl(final ArticleModel article) {
        if (article.getLogo() != null) {
            return article.getLogo().getUrlAsString();
        } else {
            return routes.Assets.versioned(new Assets.Asset("img/default-article.png")).toString();
        }
    }
}
