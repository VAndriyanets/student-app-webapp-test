package toolbox.helper;

import actors.SearchEngineActorProtocol;
import akka.actor.ActorRef;
import models.AccountModel;
import models.RealmGroupModel;
import models.RealmPageModel;
import models.SearchableModel;
import play.libs.Akka;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

/**
 * Utility class to invoke actors
 *
 * @author Andrey Sokolov
 */
public class ActorHelper {

    /**
     * Shedule model indexing
     *
     * @param searchEngineActor indexing actor
     * @param model           model to index
     */
    public static void indexModel(ActorRef searchEngineActor, SearchableModel model) {
        indexEntity(searchEngineActor, new SearchEngineActorProtocol.IndexModel(model));

    }

    private static void indexEntity(ActorRef searchEngineActor, Object entity) {
        // Index account in the search engine
        Akka.system().scheduler().scheduleOnce(
                FiniteDuration.apply(5, TimeUnit.SECONDS),
                searchEngineActor,
                entity,
                Akka.system().dispatcher(),
                null
        );
    }
}
