/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.helper;

import java.util.Currency;
import java.util.Locale;

/**
 * FormatHelper.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class FormatHelper {

    /**
     * Format the specified floating amount into the given locale.
     *
     * @param amount The amount to format
     * @param locale The local to use
     * @return A string containing the formatted number
     */
    public static String formatCurrency(final Double amount, final Locale locale) {
        final java.text.NumberFormat number = java.text.NumberFormat.getCurrencyInstance(locale);
        number.setCurrency(Currency.getInstance(Locale.FRANCE));
        return number.format(amount);
    }

    /**
     * Format the specified object amount into the given locale.
     *
     * @param amount The amount to format
     * @param locale The local to use
     * @return A string containing the formatted number
     */
    public static String formatCurrency(final Object amount, final Locale locale) {
        final java.text.NumberFormat number = java.text.NumberFormat.getCurrencyInstance(locale);
        number.setCurrency(Currency.getInstance(Locale.FRANCE));
        return number.format(amount);
    }

    /**
     * Format the specified floating number into the given locale.
     *
     * @param value  The value to format
     * @param locale The local to use
     * @return A string containing the formatted number
     */
    public static String formatNumber(final double value, final Locale locale) {
        return java.text.NumberFormat.getNumberInstance(locale).format(value);
    }

    /**
     * Format the specified object number into the given locale.
     *
     * @param value  The value to format
     * @param locale The local to use
     * @return A string containing the formatted number
     */
    public static String formatNumber(final Object value, final Locale locale) {
        return java.text.NumberFormat.getNumberInstance(locale).format(value);
    }

    /**
     * Format the specified number into the given locale.
     *
     * @param value  The value to format
     * @param locale The local to use
     * @return A string containing the formatted number
     */
    public static String formatNumber(final int value, final Locale locale) {
        return java.text.NumberFormat.getNumberInstance(locale).format(value);
    }

    /**
     * Format the specified number into the given locale.
     *
     * @param value  The value to format
     * @param locale The local to use
     * @return A string containing the formatted number
     */
    public static String formatNumber(final long value, final Locale locale) {
        return java.text.NumberFormat.getNumberInstance(locale).format(value);
    }
}
