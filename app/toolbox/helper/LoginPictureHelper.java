/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.helper;

import models.LoginPictureModel;

import java.util.concurrent.Callable;

/**
 * LoginPictureHelper.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class LoginPictureHelper {

    /**
     * Get the login picture of the day.
     *
     * @return The login picture of the day
     * @see LoginPictureModel
     * @since 15.10
     */
    public static LoginPictureModel getPictureOfTheDay() {
        final LoginPictureModel loginPicture = play.cache.Cache.getOrElse("loginpicture.object", new Callable<LoginPictureModel>() {
            @Override
            public LoginPictureModel call() throws Exception {
                return LoginPictureModel.find.where().raw("1=1 ORDER BY RAND((NOW())) LIMIT 1").findUnique();
            }
        }, 14400);
        return loginPicture;
    }
}
