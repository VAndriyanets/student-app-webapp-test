/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.helper;

import models.AccountModel;
import models.PostModel;
import play.mvc.Http;

/**
 * SessionHelper.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public final class SessionHelper {

    /**
     * Check if the user is authenticated.
     *
     * @return {@code true} if user is authenticated, otherwise, {@code false}
     * @since 15.10
     */
    public static boolean isAuthenticated() {
        return Http.Context.current().args.getOrDefault("account", null) != null;
    }

    /**
     * Get the authenticated account. If user is not authenticated, this
     * method will return {@code null}
     *
     * @return An {@code AccountModel} instance, otherwise, {@code null}
     * @since 15.10
     */
    public static AccountModel getAccount() {
        return (AccountModel) Http.Context.current().args.getOrDefault("account", null);
    }

    public static boolean isPostLiked(final PostModel post){
        return getAccount().isPostLiked(post);
    }

    public static boolean isPostDisliked(final PostModel post){
        return getAccount().isPostDisliked(post);
    }
}
