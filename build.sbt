name := "student-app-webapp"

version := "16.04-SNAPSHOT"

lazy val models = (project in file("modules/student-mod-core")).enablePlugins(PlayJava, PlayEbean)

lazy val restApi = (project in file("modules/rest-api")).enablePlugins(PlayJava).dependsOn(models)

lazy val root = (project in file(".")).enablePlugins(PlayJava).dependsOn(models).aggregate(models).dependsOn(restApi).aggregate(restApi)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  filters,
  javaWs
)

scriptClasspath := Seq("../conf/;%APP_LIB_DIR%/*")

playEbeanModels in Compile := Seq("models.*")

routesGenerator := InjectedRoutesGenerator

CoffeeScriptKeys.bare := true

includeFilter in (Assets, LessKeys.less) := "*.less"

excludeFilter in (Assets, LessKeys.less) := "_*.less"

LessKeys.compress in Assets := true

LessKeys.sourceMap in Assets := true

LessKeys.sourceMapFileInline in Assets := true

LessKeys.sourceMapLessInline in Assets := true

LessKeys.sourceMapRootpath in Assets := "../../"

sources in (Compile,doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false