STUDENT APPLICATION : WEBAPPLICATION
====================================

0x01 Environnement de développement
-----------------------------------
  Pour utiliser ce projet, vous avez besoin d'installer quelques outils
  indispensable a son bon fonctionnement.
     
  - PlayFramework 2.4.3 minimum
  - MySQL
  - Redis




0x02 Qualité du code
--------------------
  Afin de garantir une qualité de code minimal dans ce projet, il est
  important de suivre les recommandations suivantes :
  
  -	Vérifiez l’apparence de votre code :  
    1. Menu "Code" -> "Reformat Code"
    2. Menu "Code" -> "Rearrange Code"
    3. Menu "Code" -> "Optimize Imports"
  - Toutes méthodes, classes, énumérations, etc. doivent être commentées
    correctement (attention au copier /coller)
