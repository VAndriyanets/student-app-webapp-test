name := "rest-api"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  "javax.el" % "javax.el-api" % "2.2.4",
  "org.glassfish.web" % "javax.el" % "2.2.4"

)

routesGenerator := InjectedRoutesGenerator
