package rest.exception;

import com.google.common.collect.Lists;
import rest.dto.response.BaseResponse;
import rest.dto.response.ValidationErrorResponse;

import java.util.List;

/**
 * @author Andrey Sokolov
 */
public class ValidationException extends BaseException {

    private List<String> messages;

    public ValidationException(String message) {
        super(message);
        this.messages = Lists.newArrayList(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }

    @Override
    public BaseResponse toResponse() {
        final ValidationErrorResponse response = new ValidationErrorResponse();
        response.messages = messages;
        return response;
    }

    @Override
    public int status() {
        return BAD_REQUEST;
    }

    public ValidationException(List<String> messages) {
        this.messages = messages;
    }
}
