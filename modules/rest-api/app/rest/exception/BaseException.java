package rest.exception;

import rest.dto.response.BaseResponse;

import javax.validation.constraints.NotNull;

/**
 * @author Andrey Sokolov
 */
public abstract class BaseException extends RuntimeException {

    public static final int BAD_REQUEST = 400;

    public BaseException() {
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    /**
     * Returns content that should be used as response in case of this error
     * @return content, not null
     */
    public abstract BaseResponse toResponse();

    /**
     * Returns http code that should be used in case of this error
     * @return code
     */
    public abstract int status();
}
