package rest.actions;

import com.fasterxml.jackson.databind.JsonNode;
import play.Play;
import play.i18n.Messages;
import play.libs.F;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import rest.dto.request.PreProcessDTO;
import rest.exception.ValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Andrey Sokolov
 */
public class ValidatorAction extends Action<ParseWithValidation> {

    public static final String JSON_KEY = Play.application().configuration().getString("multipart.json.key");

    private final Validator validator;

    public ValidatorAction() {
        final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Override
    public F.Promise<Result> call(final Http.Context ctx) throws Throwable {
        final JsonNode jsonNode = getJsonNode(ctx);
        if (jsonNode == null) {
            throw new ValidationException(Messages.get("REQUEST.NOT.PARSED"));
        }
        final Object obj = Json.fromJson(jsonNode, configuration.value());
        if (obj instanceof PreProcessDTO) {
            ((PreProcessDTO) obj).postConstruct();
        }
        final Set<ConstraintViolation<Object>> violations = validator.validate(obj);

        final List<String> messages = buildMessages(violations);
        if (messages.size() > 0) {
            throw new ValidationException(messages);
        }
        ctx.args.put("SERIALIZED_REQUEST", obj);
        return delegate.call(ctx);
    }

    private List<String> buildMessages(final Set<ConstraintViolation<Object>> violations) {
        final List<String> messages = new ArrayList<>();
        if (violations.size() > 0) {
            for (ConstraintViolation<Object> cv : violations) {
                messages.add(cv.getPropertyPath() + ":" + cv.getMessage());
            }
        }
        return messages;
    }

    protected JsonNode getJsonNode(Http.Context ctx) {
        if (configuration.isMultipart()) {
            final String[] entries = ctx.request().body().asMultipartFormData().asFormUrlEncoded().get(JSON_KEY);
            if (entries != null && entries.length > 0) {
                return Json.parse(entries[0]);
            } else {
                return null;
            }
        } else {
            return ctx.request().body().asJson();
        }
    }

}
