/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package rest.actions;

import models.AccountAppToken;
import models.MobileAppModel;
import org.apache.commons.lang3.StringUtils;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import rest.helper.SessionHelper;

/**
 * SessionOptional for REST API.
 *
 */
public class SessionOptional extends Action.Simple {

    /**
     * Try to retrieve the current user authenticated.
     *
     * @param context The current request context
     * @return A promised result
     * @throws Throwable If something goes wrong
     */
    @Override
    public F.Promise<Result> call(Http.Context context) throws Throwable {
        final String token = SessionHelper.getTokenFromRequest(context);

        if (StringUtils.isNoneBlank(token)) {
            AccountAppToken tokenData = SessionHelper.getAccountAppToken(token);
            if (tokenData != null &&
                    MobileAppModel.Status.ACTIVE.equals(tokenData.mobileApp.status)) {
                context.args.put("account", tokenData.account);
            } else {
                context.args.put("account", null);
            }
        } else {
            context.args.put("account", null);
        }

        return delegate.call(context);
    }
}
