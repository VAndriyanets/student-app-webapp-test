/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package rest.actions;

import models.AccountAppToken;
import models.AccountModel;
import models.MobileAppModel;
import org.apache.commons.lang3.StringUtils;
import play.Logger;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import rest.helper.SessionHelper;

/**
 * SessionRequired for REST API.
 *
 */
public class SessionRequired extends Security.Authenticator {

    /**
     * Get the username of the authenticated user.
     *
     * @param ctx The current request context
     * @return The UID of application user
     */
    @Override
    public String getUsername(Http.Context ctx) {
        final String token = SessionHelper.getTokenFromRequest(ctx);
        if (StringUtils.isNoneBlank(token)) {
            AccountAppToken tokenData = SessionHelper.getAccountAppToken(token);
            if (tokenData != null) {
                if (!MobileAppModel.Status.ACTIVE.equals(tokenData.mobileApp.status)) {
                    Logger.info("Application {} inactive for token {}", tokenData.mobileApp.uid, token);
                    return null;
                }
                final AccountModel account = tokenData.account;
                if (account != null) {
                    ctx.args.put("account", account);
                    return account.getUidAsString();
                } else {
                    return null;
                }
            } else {
                Logger.info("Not registered token {}", token);
            }
        } else {
            Logger.info("Empty token {}", token);
        }
        return null;
    }

    /**
     * This method is called if user is not authenticated.
     *
     * @param ctx The current request context
     * @return unauthorized status with empty body
     */
    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return unauthorized();
    }
}
