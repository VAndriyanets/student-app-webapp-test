package rest.actions;

import play.libs.F;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import rest.dto.response.BaseResponse;
import rest.exception.BaseException;

/**
 * @author Andrey Sokolov
 */
public class ExceptionHandlerAction extends Action.Simple {

    @Override
    public F.Promise<Result> call(Http.Context ctx) throws Throwable {
        try {
            return delegate.call(ctx);
        } catch (BaseException e) {
            return handleException(ctx, e);
        }
    }

    protected F.Promise<Result> handleException(final Http.Context ctx, final BaseException t) {
        return F.Promise.promise((F.Function0<Result>) () -> {
            final BaseResponse response = t.toResponse();
            return status(t.status(), Json.toJson(response));
        });
    }


}
