/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package rest.helper;

import models.AccountAppToken;
import models.AccountModel;
import models.PostModel;
import play.Play;
import play.cache.Cache;
import play.mvc.Http;

/**
 * SessionHelper for REST API.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public final class SessionHelper {

    public static final String TOKEN_PARAMETER_NAME = Play.application().configuration().getString("access_token.key", "ACCESS_TOKEN");
    public static final int TOKEN_EXPIRATION_TIME = Play.application().configuration().getInt("access_token.expiration", 600);

    /**
     * Check if the user is authenticated.
     *
     * @return {@code true} if user is authenticated, otherwise, {@code false}
     * @since 15.10
     */
    public static boolean isAuthenticated() {
        return Http.Context.current().args.getOrDefault("account", null) != null;
    }

    /**
     * Get the authenticated account. If user is not authenticated, this
     * method will return {@code null}
     *
     * @return An {@code AccountModel} instance, otherwise, {@code null}
     * @since 15.10
     */
    public static AccountModel getAccount() {
        return (AccountModel) Http.Context.current().args.getOrDefault("account", null);
    }

    /**
     * Retrieve token string from incoming request.
     * @param ctx source of incoming request
     * @return token string
     */
    public static String getTokenFromRequest(Http.Context ctx) {
        final String token;
        final Http.Request request = ctx.request();
        if (request.hasHeader(TOKEN_PARAMETER_NAME)) {
            token = request.getHeader(TOKEN_PARAMETER_NAME);
        } else {
            token = request.getQueryString(TOKEN_PARAMETER_NAME);
        }
        return token;
    }

    /**
     * Retrieve registered token data.
     * @param token token String representation
     * @return token-related data as AccountAppToken instance
     */
    public static AccountAppToken getAccountAppToken(String token) {
        return Cache.getOrElse(String.format("account_token.%s", token), () -> {
            final AccountAppToken accountAppToken = AccountAppToken.findByToken(token);
            return accountAppToken;
        }, TOKEN_EXPIRATION_TIME);
    }

    /**
     * Put token to cache.
     * @param appToken token AccountAppToken instance
     */
    public static void cacheAccountAppToken(AccountAppToken appToken) {
        Cache.set(String.format("account_token.%s", appToken.accessToken.toString()), appToken, TOKEN_EXPIRATION_TIME);
    }

    /**
     * Remove token from cache.
     * @param appToken token AccountAppToken instance
     */
    public static void evictAccountAppToken(AccountAppToken appToken) {
        Cache.remove(String.format("account_token.%s", appToken.accessToken.toString()));
    }

}
