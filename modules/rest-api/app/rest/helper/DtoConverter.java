package rest.helper;

import com.google.common.collect.Lists;
import models.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import rest.dto.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class for DTO <> model converting
 */
public class DtoConverter {

    public static final String JSON_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public static PostDTO convertPost(PostModel post, AccountModel account) {
        final PostDTO dto = new PostDTO();

        dto.uid = post.getUidAsString();
        dto.text = post.getText();
        dto.authorName = post.getAuthor().getFullName();
        dto.created = post.getReadableElapsedTime();
        dto.pitureUrls = Lists.newArrayList();
        for (S3FileModel picture : post.getPictures()) {
            dto.pitureUrls.add(picture.getUrlAsString());
        }
        dto.likes = post.getLikes();
        dto.dislikes = post.getDislikes();
        dto.liked = account.isPostLiked(post);
        dto.disliked = account.isPostDisliked(post);
        dto.comments = Lists.newArrayList();
        for (CommentModel comment : CommentModel.findAll(post)) {
            dto.comments.add(convertComment(comment));
        }
        return dto;
    }

    public static List<PostDTO> convertPosts(List<PostModel> posts, AccountModel account) {
        List<PostDTO> resultPosts = new ArrayList<>(posts.size());
        for(PostModel post : posts) {
            PostDTO dto = DtoConverter.convertPost(post, account);
            resultPosts.add(dto);
        }
        return resultPosts;
    }

    public static CommentDTO convertComment(CommentModel comment) {
        final CommentDTO dto = new CommentDTO();
        dto.uid = comment.getUidAsString();
        dto.text = comment.text;
        dto.authorName = comment.author.getFullNameLight();
        dto.created = comment.getReadableElapsedTime();
        dto.pitureUrls = Lists.newArrayList();
        for (S3FileModel picture : comment.pictures) {
            dto.pitureUrls.add(picture.getUrlAsString());
        }
        return dto;
    }

    public static ArticleDTO convertArticle(ArticleModel article) {
        final ArticleDTO dto = new ArticleDTO();
        dto.uid = article.getUidAsString();
        DateTimeFormatter fmt = DateTimeFormat.forPattern(JSON_DATE_PATTERN);
        dto.createDate = fmt.print(article.getCreateDate());
        dto.name = article.getName();
        dto.description = article.getDescription();
        dto.logoUrl = article.getLogo() == null ? null : article.getLogo().getUrlAsString();
        dto.price = article.getPrice();
        dto.vendorUid = article.getVendorPage().getUidAsString();
        dto.maximumQuantity = article.getMaximumQuantity();
        dto.limitPerUser = article.getLimitPerUser();
        return dto;
    }

    public static List<ArticleDTO> convertArticles(List<ArticleModel> articles) {
        List<ArticleDTO> resultArticles = new ArrayList<>(articles.size());
        for(ArticleModel article : articles) {
            ArticleDTO dto = DtoConverter.convertArticle(article);
            resultArticles.add(dto);
        }
        return resultArticles;
    }

    public static VendorDTO convertVendor(RealmPageModel vendor) {
        final VendorDTO dto = new VendorDTO();
        dto.uid = vendor.getUidAsString();
        dto.slug = vendor.getSlug();
        dto.name = vendor.getName();
        dto.logoUrl = vendor.getLogo()==null ? null : vendor.getLogo().getUrlAsString();
        dto.bannerUrl = vendor.getBanner()==null ? null : vendor.getBanner().getUrlAsString();
        dto.description = vendor.getVendorProfile()==null ? null : vendor.getVendorProfile().getDescription();
        return dto;
    }

    public static List<VendorDTO> convertVendors(List<RealmPageModel> vendors) {
        List<VendorDTO> resultVendors = new ArrayList<>(vendors.size());
        for(RealmPageModel vendor : vendors) {
            VendorDTO dto = DtoConverter.convertVendor(vendor);
            resultVendors.add(dto);
        }
        return resultVendors;
    }

    public static VendorDetailsDTO convertVendorDetails(RealmPageModel vendor) {
        final VendorDetailsDTO dto = new VendorDetailsDTO();
        dto.mainInfo = convertVendor(vendor);
        DateTimeFormatter fmt = DateTimeFormat.forPattern(JSON_DATE_PATTERN);
        dto.createDate = fmt.print(vendor.getCreateDate());
        VendorProfileModel vendorProfile = vendor.getVendorProfile();
        if (vendorProfile != null) {
            dto.email = vendorProfile.getEmail();
            dto.phone = vendorProfile.getPhone();
            dto.address = vendorProfile.getAddress();
            dto.city = vendorProfile.getCity();
            dto.zipcode = vendorProfile.getZipcode();
            dto.iban = vendorProfile.getIban();
            dto.ibanPdfUrl = vendorProfile.getIbanPdf() == null? null : vendorProfile.getIbanPdf().getUrlAsString();
        }
        return dto;
    }

    public static AccountDTO convertAccount(AccountModel account) {
        final AccountDTO dto = new AccountDTO();

        dto.uid = account.getUidAsString();
        dto.firstName = account.getFirstName();
        dto.lastName = account.getLastName();
        dto.status = account.getStatus().name();
        dto.emails = new ArrayList<>(account.getEmails().size());
        dto.phones = new ArrayList<>(account.getPhones().size());
        dto.addresses = new ArrayList<>(account.getAddresses().size());
        dto.realms = new ArrayList<>(account.getRealms().size());
        for (AccountEmailModel email : account.getEmails()) {
            AccountDTO.AccountEmailDTO accountEmailDTO = new AccountDTO.AccountEmailDTO();
            accountEmailDTO.email = email.getEmail();
            accountEmailDTO.primary = email.isPrimary();
            accountEmailDTO.validated = email.isValidated();
            dto.emails.add(accountEmailDTO);
        }
        for (AccountPhoneModel phone : account.getPhones()) {
            AccountDTO.AccountPhoneDTO accountPhoneDTO = new AccountDTO.AccountPhoneDTO();
            accountPhoneDTO.phone = phone.getPhone();
            accountPhoneDTO.countryCode = phone.getCountryCode();
            accountPhoneDTO.primary = phone.isPrimary();
            accountPhoneDTO.validated = phone.isValidated();
            dto.phones.add(accountPhoneDTO);
        }
        for (AccountPostalAddressModel address : account.getAddresses()) {
            AccountDTO.AccountAddressDTO addressDTO = new AccountDTO.AccountAddressDTO();
            addressDTO.primary = address.isPrimary();
            addressDTO.destName = address.getDestName();
            addressDTO.street1 = address.getStreet1();
            addressDTO.street2 = address.getStreet2();
            addressDTO.zipCode = address.getZipCode();
            addressDTO.city = address.getCity();
            addressDTO.countryCode = address.getCountry().name();
            dto.addresses.add(addressDTO);
        }
        for (RealmModel realm : account.getRealms()) {
            AccountDTO.RealmDTO realmDTO = new AccountDTO.RealmDTO();
            realmDTO.uid = realm.getUidAsString();
            realmDTO.name = realm.getName();
            dto.realms.add(realmDTO);
        }
        dto.actorName = account.getActor().getProfile().getName();
        dto.actorSlug = account.getActor().getSlug();
        dto.lang = account.getLang();
        dto.timezone = account.getTimezone();
        DateTimeFormatter fmt = DateTimeFormat.forPattern(JSON_DATE_PATTERN);
        dto.lastLogin = fmt.print(account.getLastLogin());
        dto.createDate = fmt.print(account.getCreateDate());
        if (account.getFacebookData() != null) {
            dto.facebookData = new AccountDTO.FacebookDataDTO();
            dto.facebookData.fbid = account.getFacebookData().getFbid();
            dto.facebookData.maritalStatus = account.getFacebookData().getMaritalStatus();
            List<FacebookUserModel> friends = account.getFacebookData().getFriends();
            dto.facebookData.friends = new ArrayList<>(friends.size());
            for (FacebookUserModel friend : friends) {
                AccountDTO.FacebookUserDTO friendDTO = new AccountDTO.FacebookUserDTO();
                friendDTO.fbid = friend.getFbid();
                friendDTO.name = friend.getName();
                dto.facebookData.friends.add(friendDTO);
            }
        }
        return dto;
    }

    public static List<AccountBriefDTO> convertAccountsBrief(List<AccountModel> accounts) {
        List<AccountBriefDTO> accountDTOs = new ArrayList<>(accounts == null? 0 : accounts.size());
        if(accounts == null) return accountDTOs;
        for (AccountModel account : accounts) {
            final AccountBriefDTO dto = new AccountBriefDTO();
            dto.uid = account.getUidAsString();
            dto.firstName = account.getFirstName();
            dto.lastName = account.getLastName();
            dto.emails = new ArrayList<>(account.getEmails().size());
            for (AccountEmailModel email : account.getEmails()) {
                AccountBriefDTO.AccountEmailDTO emailDTO = new AccountBriefDTO.AccountEmailDTO();
                emailDTO.email = email.getEmail();
                emailDTO.primary = email.isPrimary();
                emailDTO.validated = email.isValidated();
                dto.emails.add(emailDTO);
            }
            dto.pictureUrl = account.getAvatarUrlAsString();
            accountDTOs.add(dto);
        }
        return accountDTOs;
    }
}
