package rest.controllers;

import models.AccountModel;
import models.ArticleModel;
import models.RealmPageModel;
import play.db.ebean.Transactional;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import rest.actions.ExceptionHandlerAction;
import rest.actions.SessionRequired;
import rest.dto.model.ArticleDTO;
import rest.helper.DtoConverter;
import rest.helper.SessionHelper;

import java.util.List;

/**
 * API - Vendor
 */
public class VendorController extends BaseController {


    /**
     * API - Vendor - Get vendor articles
     * @param vendorUid UUID of the vendor
     * @return list of all vendor's articles
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result getArticles(String vendorUid) {
        final RealmPageModel vendor = RealmPageModel.findByUid(vendorUid);
        if (vendor == null) {
            return notFound();
        }
        List<ArticleModel> articles = ArticleModel.findAllVendorArticles(vendor);
        List<ArticleDTO> resultArticles = DtoConverter.convertArticles(articles);
        return ok(Json.toJson(resultArticles));
    }

    /**
     * API - Vendor - Get vendors
     * @return details/information about an article
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result getVendors() {
        final AccountModel account = SessionHelper.getAccount();
        List<RealmPageModel> visibleVendors = RealmPageModel.findAllVisibleForUser(account);
        return ok(Json.toJson(DtoConverter.convertVendors(visibleVendors)));
    }

    /**
     * API - Vendor - Get vendor details
     * @param vendorUid UUID of the vendor
     * @return details/information about a vendor
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result getVendorDetails(String vendorUid) {
        final RealmPageModel vendor = RealmPageModel.findByUid(vendorUid);
        if (vendor == null) {
            return notFound();
        }
        return ok(Json.toJson(DtoConverter.convertVendorDetails(vendor)));
    }

}
