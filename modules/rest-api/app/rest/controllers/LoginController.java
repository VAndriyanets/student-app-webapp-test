package rest.controllers;

import models.AccountAppToken;
import models.AccountModel;
import models.MobileAppModel;
import play.Logger;
import play.db.ebean.Transactional;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import rest.actions.ExceptionHandlerAction;
import rest.actions.ParseWithValidation;
import rest.dto.model.LoginDTO;
import rest.dto.request.LoginRequest;
import rest.helper.SessionHelper;

import java.util.UUID;

/**
 * @author Andrey Sokolov
 */
public class LoginController extends BaseController {

    /**
     * API - Authentication - Login.
     * @return access token in json
     */
    @With(ExceptionHandlerAction.class)
    @ParseWithValidation(value = LoginRequest.class, isMultipart = false)
    @Transactional
    public Result login() {
        final LoginRequest request = getRequest(LoginRequest.class);
        MobileAppModel mobileApp = MobileAppModel.findByUid(UUID.fromString(request.applicationId));
        if (mobileApp == null) {
            return notFound();
        }
        if (MobileAppModel.Status.ACTIVE != mobileApp.status) {
            Logger.info("Application {} inactive", mobileApp.uid);
            return forbidden();
        }
        AccountModel account = AccountModel.find.where().like("emails.email", request.username).eq("is_validated", true)
                .eq("is_primary", true).findUnique();
        if (account == null || !account.checkPassword(request.password)) {
            Logger.info("Invalid credentials {}/{}", request.username, request.password);
            return forbidden();
        }
        AccountAppToken appToken = AccountAppToken.find.where().eq("mobileApp", mobileApp).eq("account", account).findUnique();
        if (appToken == null) {
            appToken = new AccountAppToken();
            appToken.account = account;
            appToken.mobileApp = mobileApp;
            appToken.insert();
        } else {
            appToken.accessToken = UUID.randomUUID();
            appToken.update();
        }

        SessionHelper.cacheAccountAppToken(appToken);
        return ok(Json.toJson(new LoginDTO(appToken.accessToken.toString())));
    }

    /**
     * API - Authentication - Logout.
     * @return Result with empty body and status ok(success) / forbidden(invalid token)
     */
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result logout() {
        String token = SessionHelper.getTokenFromRequest(Http.Context.current());
        AccountAppToken appToken = SessionHelper.getAccountAppToken(token);
        if (appToken == null) {
            Logger.info("Not registered token {}", token);
            return forbidden();
        }
        appToken.delete();

        SessionHelper.evictAccountAppToken(appToken);

        return ok();
    }
}
