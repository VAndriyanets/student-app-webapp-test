package rest.controllers;

import models.AccountModel;
import models.ArticleModel;
import models.ArticleOptionModel;
import models.ArticlePricingModel;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.db.ebean.Transactional;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import rest.actions.ExceptionHandlerAction;
import rest.actions.SessionRequired;
import rest.dto.model.ArticleDTO;
import rest.dto.model.ArticleDetailDTO;
import rest.dto.model.ArticleOptionDTO;
import rest.dto.model.ArticlePricingDTO;
import rest.helper.DtoConverter;
import rest.helper.SessionHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * API - Articles
 */
public class ArticlesController extends BaseController {


    /**
     * API - Articles - Get articles
     * @return list of all reacheable articles by user
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result getArticles() {
        final AccountModel account = SessionHelper.getAccount();
        List<ArticleModel> articles = ArticleModel.findAllReachableByUser(account);
        List<ArticleDTO> resultArticles = new ArrayList<>(articles.size());
        for(ArticleModel article : articles) {
            ArticleDTO dto = DtoConverter.convertArticle(article);
            resultArticles.add(dto);
        }
        return ok(Json.toJson(resultArticles));
    }

    /**
     * API - Articles - Get article details
     * @param articleUid UUID of the article
     * @return details/information about an article
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result getArticle(String articleUid) {
        final AccountModel account = SessionHelper.getAccount();
        ArticleModel article = ArticleModel.findByUid(articleUid);
        if (article == null) {
            return notFound();
        }
        ArticleDetailDTO dto = new ArticleDetailDTO();
        dto.mainInfo = DtoConverter.convertArticle(article);
        dto.options = new ArrayList<>(article.getOptions().size());
        for (ArticleOptionModel articleOption : article.getOptions()) {
            ArticleOptionDTO optionDTO = new ArticleOptionDTO();
            optionDTO.name = articleOption.getName();
            optionDTO.optionType = articleOption.getOptionType().name();
            optionDTO.argument = articleOption.getArgument();
            optionDTO.mandatory = articleOption.isMandatory();
            dto.options.add(optionDTO);
        }
        dto.pricing = new ArrayList<>(article.getPricingRules().size());
        for (ArticlePricingModel articlePricingModel : article.getPricingRules()) {
            ArticlePricingDTO pricingDTO = new ArticlePricingDTO();
            pricingDTO.pricingType = articlePricingModel.getPricingType().name();
            if (articlePricingModel.getPricingType() == ArticlePricingModel.PricingType.OWN_AN_ARTICLE) {
                pricingDTO.ownedArticleUID = articlePricingModel.getOwnedArticle().getUidAsString();
            } else {
                pricingDTO.groupUID = articlePricingModel.getGroup().getUidAsString();
            }
            pricingDTO.price = articlePricingModel.getPrice();
            dto.pricing.add(pricingDTO);
        }

        dto.posts = DtoConverter.convertPosts(article.posts, account);
        return ok(Json.toJson(dto));
    }

}
