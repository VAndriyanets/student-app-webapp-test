package rest.controllers;

import play.mvc.Controller;

/**
 * @author Andrey Sokolov
 */
public class BaseController extends Controller {

    protected static <T> T getRequest(final Class<T> clazz) {
        return clazz.cast(ctx().args.get("SERIALIZED_REQUEST"));
    }
}
