package rest.controllers;

import com.avaje.ebean.ExpressionList;
import com.google.common.collect.Lists;
import models.*;
import play.db.ebean.Transactional;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import rest.actions.ExceptionHandlerAction;
import rest.actions.ParseWithValidation;
import rest.actions.SessionRequired;
import rest.dto.model.CommentDTO;
import rest.dto.model.PostDTO;
import rest.dto.request.CreateCommentRequest;
import rest.dto.request.CreatePostRequest;
import rest.dto.request.GetPostsRequest;
import rest.helper.DtoConverter;
import rest.helper.SessionHelper;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Sokolov
 */
public class NewsController extends BaseController {

    /**
     * API - Social - Get single news.
     * @param postUid Post Uid
     * @return json representation of post
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result getPost(final String postUid) {
        final PostModel post = PostModel.findByUid(postUid);
        if (post != null) {
            final AccountModel account = SessionHelper.getAccount();
            PostDTO dto = DtoConverter.convertPost(post, account);
            return ok(Json.toJson(dto));
        }
        return notFound();
    }

    /**
     * API - Social - News feed
     * @see GetPostsRequest for request parameters
     * @return list of posts as json
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @ParseWithValidation(value = GetPostsRequest.class, isMultipart = false)
    @Transactional
    public Result getNews() {
        final GetPostsRequest request = getRequest(GetPostsRequest.class);
        ExpressionList<PostModel> postsQuery = PostModel.find.where();
        if (request.fromTime != null) {
            postsQuery = postsQuery.ge("createDate", request.fromTime);
        }
        List<PostModel>  posts = null;
        final AccountModel account = SessionHelper.getAccount();
        switch (request.entityType) {
            case GROUP:
                RealmGroupModel group = RealmGroupModel.findByUid(request.entityUid);
                postsQuery = postsQuery.eq("group", group);
                if(posts != null){
                    if(!account.getRealmGroups().contains(group)){
                        return forbidden();
                    }
                } else {
                    return notFound();
                }
                break;
            case ARTICLE:
                ArticleModel article = ArticleModel.findByUid(request.entityUid);
                postsQuery = postsQuery.eq("article", article);
                if(article == null){
                    return notFound();
                }
                break;
            case REALM:
                RealmModel realm = RealmModel.findByUid(request.entityUid);
                postsQuery = postsQuery.eq("realm", realm);
                if(realm != null){
                    if(!account.isGrantedTo(realm)){
                        return forbidden();
                    }
                } else {
                    return notFound();
                }
                break;
        }

        posts = postsQuery.orderBy().desc("createDate").findList();
        List<PostDTO> resultPosts = DtoConverter.convertPosts(posts, account);
        return ok(Json.toJson(resultPosts));
    }

    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @ParseWithValidation(value = CreatePostRequest.class, isMultipart = true)
    @Transactional
    public Result createNews() {
        final CreatePostRequest request = getRequest(CreatePostRequest.class);
        final PostModel post = request.getPostModel();
        final AccountModel account = SessionHelper.getAccount();
        post.setAuthor(account);
        switch (request.entityType) {
            case GROUP:
                post.group = RealmGroupModel.findByUid(request.entityUid);
                if(post.group != null){
                    if(!account.getRealmGroups().contains(post.group)){
                        return forbidden();
                    }
                } else {
                    return notFound();
                }
                break;
            case ARTICLE:
                post.article = ArticleModel.findByUid(request.entityUid);
                if(post.article != null){
                      if(!post.article.getVendorPage().hasVendor(SessionHelper.getAccount())){
                          return forbidden();
                      }
                } else {
                    return notFound();
                }
                break;
            case REALM:
                post.realm = RealmModel.findByUid(request.entityUid);
                if(post.realm != null){
                    if(!account.isGrantedTo(post.realm)){
                        return forbidden();
                    }
                } else {
                    return notFound();
                }
                break;
        }
        post.save();
        PostDTO dto = DtoConverter.convertPost(post, account);
        return ok(Json.toJson(dto));
    }


    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @ParseWithValidation(value = CreateCommentRequest.class, isMultipart = true)
    @Transactional
    public Result addComment(final String postUid) {
        final CreateCommentRequest request = getRequest(CreateCommentRequest.class);
        final CommentModel comment = request.getCommentModel();
        final PostModel post = PostModel.findByUid(postUid);
        if (post != null) {
            final AccountModel account = SessionHelper.getAccount();
            if (post.canVote(account)) {
                comment.post = post;
                comment.author = account;
                comment.save();
                CommentDTO dto = DtoConverter.convertComment(comment);
                return ok(Json.toJson(dto));
            }
        }
        return notFound();
    }

    /**
     * API - Social - Like a news
     * @param postUid UID of liked post
     * @return Result with empty body and status
     *      ok(success) / notFound(invalid postUid) / badRequest(already voted) / forbidden (permission denied)
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result likePost(final String postUid) {
        final PostModel post = PostModel.findByUid(postUid);
        if (post != null) {
            return vote(post, true);
        }
        return notFound();
    }

    /**
     * API - Social - Revoke post's Like by current user
     * @param postUid UID of 'like-canceled' post
     * @return Result with empty body and status
     *      ok(success) / notFound(invalid postUid) / badRequest(not liked yet)
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result likePostRollback(final String postUid) {
        final PostModel post = PostModel.findByUid(postUid);
        if (post != null) {
            return voteBack(post, true);
        }
        return notFound();
    }

    private Result vote(PostModel postModel, boolean like) {
        final AccountModel account = SessionHelper.getAccount();
        if(postModel.canVote(account)){
            VoteModel newVote = new VoteModel();
            newVote.like = like;
            newVote.key = new VoteModel.VotePK(account.getId(), postModel.getId());
            try {
                newVote.save();
            } catch (PersistenceException e) {
                return badRequest();
            }
            VoteModel.cleanLikesCache(postModel.getId(), like);
            SessionHelper.getAccount().cleanLikesCache(like);
            return ok();
        } else {
            return forbidden();
        }
    }

    private Result voteBack(PostModel postModel, boolean like) {
        final AccountModel account = SessionHelper.getAccount();
        VoteModel vote = VoteModel.findVote(postModel.getId(), account.getId(), like);
        if(vote != null){
            try {
                vote.delete();
            } catch (PersistenceException e) {
                return badRequest();
            }
        } else {
            return badRequest();
        }
        VoteModel.cleanLikesCache(postModel.getId(), like);
        SessionHelper.getAccount().cleanLikesCache(like);
        return ok();
    }

}
