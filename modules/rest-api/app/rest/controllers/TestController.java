package rest.controllers;

import play.Play;
import play.i18n.Messages;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import rest.actions.SessionRequired;
import rest.helper.SessionHelper;

/**
 * @author Andrey Sokolov
 */
public class TestController extends Controller {

    /**
     * test
     *
     * @return test string
     */
    @Security.Authenticated(SessionRequired.class)
    public Result testApi(){
        //TODO remove this method
        return ok("Test from REST API, test message: "+ Messages.get("TEST.REST.MESSAGE")+", test cfg:"+ Play.application().configuration().getString("some.rest.config") + ", user:"+ SessionHelper.isAuthenticated());
    }
}
