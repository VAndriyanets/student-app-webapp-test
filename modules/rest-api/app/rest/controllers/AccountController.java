package rest.controllers;

import com.avaje.ebean.ExpressionList;
import models.AccountModel;
import models.RealmModel;
import models.SearchRequestType;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.search.SearchPhaseExecutionException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.cluster.block.ClusterBlockException;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.indices.IndexMissingException;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.jetbrains.annotations.NotNull;
import play.db.ebean.Transactional;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import rest.actions.ExceptionHandlerAction;
import rest.actions.ParseWithValidation;
import rest.actions.SessionRequired;
import rest.dto.request.AccountsSearchElasticRequest;
import rest.dto.request.AccountsSearchRequest;
import rest.helper.DtoConverter;
import rest.helper.SessionHelper;
import toolbox.helper.ElasticSearchHelper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * API - Account
 */
public class AccountController extends BaseController {


    /**
     * API - Account - My account
     * @return account (of current user) information
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @Transactional
    public Result getMe() {
        final AccountModel account = SessionHelper.getAccount();
        return ok(Json.toJson(DtoConverter.convertAccount(account)));
    }

    /**
     * API - Account - Search user
     * @return list of accounts
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @ParseWithValidation(value = AccountsSearchRequest.class, isMultipart = false)
    @Transactional
    public Result findAccounts() {
        final AccountModel account = SessionHelper.getAccount();
        final AccountsSearchRequest request = getRequest(AccountsSearchRequest.class);
        ExpressionList<AccountModel> criteria = AccountModel.find.where()
                .in("realms", account.getRealms());
        if (request.email != null && !request.email.isEmpty()) {
            criteria = criteria.eq("emails.email", request.email);
        }
        if (request.firstName != null && !request.firstName.isEmpty()) {
            criteria = criteria.eq("firstName", request.firstName);
        }
        if (request.lastName != null && !request.lastName.isEmpty()) {
            criteria = criteria.eq("lastName", request.lastName);
        }
        final List<AccountModel> accounts = criteria.findList();
        return ok(Json.toJson(DtoConverter.convertAccountsBrief(accounts)));
    }

    /**
     * API - Account - Search user (text search)
     * @return list of accounts
     */
    @Security.Authenticated(SessionRequired.class)
    @With(ExceptionHandlerAction.class)
    @ParseWithValidation(value = AccountsSearchElasticRequest.class, isMultipart = false)
    @Transactional
    public Result findAccountsElastic() {
        final AccountsSearchElasticRequest request = getRequest(AccountsSearchElasticRequest.class);
        final AccountModel account = SessionHelper.getAccount();
        final List<AccountModel> accounts = new LinkedList<>();
        try (final Client client = ElasticSearchHelper.getClient()) {
            final SearchRequestType searchRequestType = SearchRequestType.USER;

            final String searchQuery = getSearchQueryForRealms(account.getRealms(), request.searchText);
            final SearchResponse response = getSearchResponse(searchQuery, client, searchRequestType.getType());
            final SearchHits hits = response.getHits();
            for (final SearchHit sh : hits.getHits()) {
                String accountUID = sh.getId();
                AccountModel foundAccount = AccountModel.find.where().like("uid", accountUID).findUnique();
                accounts.add(foundAccount);
            }

        } catch (ElasticsearchException ex) {
            ElasticSearchHelper.handleESError(ex);
            return internalServerError();
        }

        return ok(Json.toJson(DtoConverter.convertAccountsBrief(accounts)));
    }


    private SearchResponse getSearchResponse(final String query,
                                             final Client client,
                                             final String... types) {
        return ElasticSearchHelper.prepareSearch(client)
                .setTypes(types)
                .setQuery(QueryBuilders.queryStringQuery(query))
                .setExplain(false)
                .execute()
                .actionGet();
    }

    private String getSearchQueryForRealms(final List<RealmModel> realms, final String query) {
        String q = ElasticSearchHelper.formSearchQuery(query);

        if (realms != null && realms.size()>0) {
            String realmCriteria = getRealmsCriteria(realms);
            return String.format("%s AND (%s)", realmCriteria.toString(), q);
        } else {
            return q;
        }
    }

    private String getRealmsCriteria(List<RealmModel> realms) {
        StringBuilder realmCriteria = new StringBuilder("(");
        realmCriteria.append("_realm:"+realms.get(0).getId());

        for (int i = 1; i < realms.size(); i++) {
            realmCriteria.append(" OR _realm:"+realms.get(i).getId());
        }
        realmCriteria.append(")");
        return realmCriteria.toString();
    }

}
