package rest.dto.model;

import java.util.List;

/**
 * API - Account
 * Account transfer object
 */
public class AccountDTO {
    public String uid;
    public String firstName;
    public String lastName;
    public String status;
    public List<AccountEmailDTO> emails;
    public List<AccountPhoneDTO> phones;
    public List<AccountAddressDTO> addresses;
    public List<RealmDTO> realms;
    public String actorName;
    public String actorSlug;
    public String lang;
    public String timezone;
    public String lastLogin;
    public String createDate;
    public FacebookDataDTO facebookData;


    public static class AccountEmailDTO {
        public String email;
        public boolean primary;
        public boolean validated;
    }

    public static class AccountPhoneDTO {
        public String phone;
        public String countryCode;
        public boolean primary;
        public boolean validated;
    }

    public static class AccountAddressDTO {
        public boolean primary;
        public String destName;
        public String street1;
        public String street2;
        public String zipCode;
        public String city;
        public String countryCode;

    }

    public static class FacebookDataDTO {
        public String fbid;
        public String maritalStatus;
        public String pictureURL;
        public List<FacebookUserDTO> friends;
    }

    public static class FacebookUserDTO {
        public String fbid;
        public String name;
    }

    public static class RealmDTO {
        public String uid;
        public String name;
    }
}