package rest.dto.model;

/**
 * API - Vendor - Get vendor details
 * Vendor details transfer object
 */
public class VendorDetailsDTO {
    public VendorDTO mainInfo;
    public String createDate;
    public String email;
    public String phone;
    public String address;
    public String city;
    public String zipcode;
    public String iban;
    public String ibanPdfUrl;
}
