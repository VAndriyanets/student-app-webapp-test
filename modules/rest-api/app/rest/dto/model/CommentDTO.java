package rest.dto.model;

import java.util.List;

/**
 * @author Andrey Sokolov
 */
public class CommentDTO {
    public String uid;
    public String text;
    public String authorName;
    public String created;
    public List<String> pitureUrls;

}
