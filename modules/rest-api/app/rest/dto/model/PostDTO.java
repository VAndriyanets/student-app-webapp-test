package rest.dto.model;

import java.util.List;

/**
 * @author Andrey Sokolov
 */
public class PostDTO {
    public String uid;
    public String text;
    public String authorName;
    public String created;
    public List<String> pitureUrls;
    public int likes;
    public int dislikes;
    public boolean liked;
    public boolean disliked;
    public List<CommentDTO> comments;
}
