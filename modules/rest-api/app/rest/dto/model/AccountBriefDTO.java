package rest.dto.model;

import java.util.List;

/**
 * API - Account - Search user
 * Account transfer object
 */
public class AccountBriefDTO {
    public String uid;
    public String firstName;
    public String lastName;
    public List<AccountEmailDTO> emails;
    public String pictureUrl;

    public static class AccountEmailDTO {
        public String email;
        public boolean primary;
        public boolean validated;
    }

}