package rest.dto.model;

import models.ArticlePricingModel;

import java.util.List;

/**
 * API - Articles
 * Article transfer object
 */
public class ArticleDTO {
    public String uid;
    public String createDate;
    public String name;
    public String description;
    public String logoUrl;
    public Double price;
    public String vendorUid;
    public Integer maximumQuantity;
    public Integer limitPerUser;

}
