package rest.dto.model;

/**
 * API - Vendor
 * Vendor transfer object
 */
public class VendorDTO {
    public String uid;
    public String slug;
    public String name;
    public String logoUrl;
    public String bannerUrl;
    public String description;
}
