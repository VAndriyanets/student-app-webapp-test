package rest.dto.model;

import java.util.List;

/**
 * API - Articles - Get article details
 * Article details transfer object
 */
public class ArticleDetailDTO {
    public ArticleDTO mainInfo;
    public List<ArticleOptionDTO> options;
    public List<ArticlePricingDTO> pricing;
    public List<PostDTO> posts;
}
