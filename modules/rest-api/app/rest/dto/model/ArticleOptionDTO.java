/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package rest.dto.model;

/**
 * API - Articles
 * Article Option DTO.
 */
public class ArticleOptionDTO {

    public String name;
    public String argument;
    public boolean mandatory;
    public String optionType;

}
