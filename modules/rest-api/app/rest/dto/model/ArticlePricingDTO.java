/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package rest.dto.model;

/**
 * API - Articles
 * Article Pricing DTO.
 */
public class ArticlePricingDTO {
    public String pricingType;
    public String ownedArticleUID;
    public String groupUID;
    public Double price;
}
