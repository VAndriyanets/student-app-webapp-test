package rest.dto.model;

/**
 * API - Authentication - Login. Response.
 */
public class LoginDTO {
    public String accessToken;

    public LoginDTO(String accessToken) {
        this.accessToken = accessToken;
    }
}
