package rest.dto.response;

import java.util.List;

/**
 * @author Andrey Sokolov
 */
public class ValidationErrorResponse extends BaseResponse {
    public List<String> messages;
}
