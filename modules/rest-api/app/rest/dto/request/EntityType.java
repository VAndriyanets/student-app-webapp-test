package rest.dto.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public enum EntityType {
    GROUP,
    ARTICLE,
    REALM;

    private static Map<String, EntityType> namesMap = new HashMap<>(3);

    static {
        for(EntityType type: values()){
            namesMap.put(StringUtils.lowerCase(type.name()),type);
        }
    }

    @JsonCreator
    public static EntityType forValue(String value) {
        return namesMap.get(StringUtils.lowerCase(value));
    }

    @JsonValue
    public String toValue() {
        return name().toLowerCase();
    }

}