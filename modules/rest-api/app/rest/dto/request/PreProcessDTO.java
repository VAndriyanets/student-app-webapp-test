package rest.dto.request;

/**
 * @author Andrey Sokolov
 */
public interface PreProcessDTO {
    void postConstruct();
}
