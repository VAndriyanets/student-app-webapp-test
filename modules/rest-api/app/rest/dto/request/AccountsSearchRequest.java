package rest.dto.request;

import javax.validation.constraints.Pattern;

/**
 * API - Account - Search user
 */
public class AccountsSearchRequest {

    /**
     * user email.
     */
    @Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
    public String email;

    /**
     * user firstName.
     */
    public String firstName;

    /**
     * user lastName.
     */
    public String lastName;

}
