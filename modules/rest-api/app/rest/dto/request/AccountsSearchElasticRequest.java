package rest.dto.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * API - Account - Search user (text search)
 */
public class AccountsSearchElasticRequest {

    /**
     * search query string.
     */
    @NotNull
    @Size(min = 2)
    public String searchText;

}
