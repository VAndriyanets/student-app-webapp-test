package rest.dto.request;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


/**
 * API - Authentication - Login. Parameters for "login" Request
 */
public class LoginRequest {

    /**
     * some secret id.
     */
    @NotNull
    @Pattern(regexp = "[\\w]{8}-[\\w]{4}-[\\w]{4}-[\\w]{4}-[\\w]{12}")
    public String applicationId;

    /**
     * user login.
     */
    @NotNull
    public String username;

    /**
     * user password.
     */
    public String password;


}
