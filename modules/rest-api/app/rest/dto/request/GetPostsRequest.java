package rest.dto.request;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;


/**
 * API - Social - News feed. Parameters for "get news" Request
 */
public class GetPostsRequest extends AbstractCreatePostRequest {

    /**
     * Scope (context) type of news feed
     */
    @NotNull
    public EntityType entityType;

    /**
     * Scope (context) Uid of news feed
     */
    @NotNull
    @Pattern(regexp = "[\\w]{8}-[\\w]{4}-[\\w]{4}-[\\w]{4}-[\\w]{12}")
    public String entityUid;

    /**
     * Start date of news feed
     */
    public Date fromTime;


}
