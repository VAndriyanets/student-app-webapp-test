package rest.dto.request;

import models.CommentModel;
import models.S3FileModel;
import org.jetbrains.annotations.NotNull;
import play.mvc.Http;
import toolbox.helper.ImageHelper;

/**
 * @author Andrey Sokolov
 */
public class CreateCommentRequest extends AbstractCreatePostRequest {

    @NotNull
    public CommentModel getCommentModel() {
        CommentModel comment = new CommentModel();
        comment.text = text;
        for (Http.MultipartFormData.FilePart picture : pictures) {
            final S3FileModel S3filePicture = ImageHelper.convertToS3File(picture.getFile(), picture.getContentType());
            comment.addPicture(S3filePicture);
        }
        return comment;
    }
}
