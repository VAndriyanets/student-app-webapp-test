package rest.dto.request;

import com.google.common.collect.Lists;
import play.Play;
import play.i18n.Messages;
import play.mvc.Http;
import rest.exception.ValidationException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Sokolov
 */
public class AbstractCreatePostRequest implements PreProcessDTO {

    public static final List<String> ALLOWED_CONTENT_TYPES = Lists.newArrayList(Play.application().configuration().getString("picture.content_type.allowed").split(","));
    public String text;
    public List<Http.MultipartFormData.FilePart> pictures = new ArrayList<>();

    /**
     * process File uploads from MultipartFormData
     */
    public void postConstruct() {
        final Http.MultipartFormData multipartFormData = Http.Context.current().request().body().asMultipartFormData();
        if (multipartFormData != null) {
            final List<Http.MultipartFormData.FilePart> files = multipartFormData.getFiles();
            for (Http.MultipartFormData.FilePart filePart : files) {
                checkContentType(filePart);
                pictures.add(filePart);
            }
        }
    }

    private void checkContentType(Http.MultipartFormData.FilePart filePart) {
        final String content = filePart.getContentType().toLowerCase();
        if(!ALLOWED_CONTENT_TYPES.contains(content)){
            throw new ValidationException(Messages.get("PICTURE.CONTENT_TYPE.WRONG", filePart.getFilename(), content));
        }
    }
}
