package rest.dto.request;


import models.PostModel;
import models.S3FileModel;
import play.mvc.Http;
import toolbox.helper.ImageHelper;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


/**
 * @author Andrey Sokolov
 */
public class CreatePostRequest extends AbstractCreatePostRequest {


    @NotNull
    public EntityType entityType;

    @NotNull
    @Pattern(regexp = "[\\w]{8}-[\\w]{4}-[\\w]{4}-[\\w]{4}-[\\w]{12}")
    public String entityUid;

    @NotNull
    public PostModel getPostModel() {
        PostModel post = new PostModel();
        post.setText(text);
        for (Http.MultipartFormData.FilePart picture : pictures) {
            final S3FileModel S3filePicture = ImageHelper.convertToS3File(picture.getFile(), picture.getContentType());
            post.addPicture(S3filePicture);
        }
        return post;
    }

}
