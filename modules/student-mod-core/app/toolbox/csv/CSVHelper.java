/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.csv;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import toolbox.csv.models.UserCsv;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * CSVHelper.
 *
 * @author Olivier Buiron
 * @version 15.12
 * @since 15.12
 */
public class CSVHelper {

    /**
     * Parse CSV file to generate a {@code List<>} of UserCsv.
     *
     * @param csv The CSV file to parse
     * @return A List of parsed {@code UserCsv}
     * @throws IOException If an error occurred with CSV file
     * @since 15.12
     */
    public static List<UserCsv> UserFromCSV(final File csv) throws IOException {
        final ArrayList<UserCsv> result = new ArrayList<>();
        final CsvMapper mapper = new CsvMapper();
        final CsvSchema schema = mapper.schemaFor(UserCsv.class).withHeader();
        MappingIterator<UserCsv> iterator = mapper.readerFor(UserCsv.class).with(schema).readValues(csv);
        while (iterator.hasNext()){
            result.add(iterator.next());
        }
        return result;
    }
}
