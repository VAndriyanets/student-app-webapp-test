/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.csv.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ManageUsersCsvModel.
 *
 * @author Olivier Buiron
 * @version 15.12
 * @since 15.12
 */
public class UserCsv {
    /**
     * Email csv field.
     */
    @JsonProperty(value = "email", required = true)
    private String email;

    /**
     * FistName csv field.
     */
    @JsonProperty(value = "firstname")
    private String fisrtName;

    /**
     * LastName csv field.
     */
    @JsonProperty(value = "lastname")
    private String lastName;


    public String getEmail() {
        return email != null ? email.trim().toLowerCase() : email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return fisrtName != null ? fisrtName.trim().toLowerCase() : fisrtName;
    }

    public void setFirstName(String fisrtName) {
        this.fisrtName = fisrtName;
    }

    public String getLastName() {
        return lastName != null ? lastName.trim().toLowerCase() : lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
