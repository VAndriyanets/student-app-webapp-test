/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import play.Play;

/**
 * SendMail helper.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class SendMail {

    /**
     * Create a new email instance.
     *
     * @return A new email
     * @see SendGrid.Email
     * @since 15.10
     */
    public static SendGrid.Email createEmail() {
        final SendGrid.Email email = new SendGrid.Email();
        email.setFrom(Play.application().configuration().getString("email.default.from"));
        email.setFromName(Play.application().configuration().getString("email.default.fromname"));
        return email;
    }

    /**
     * Send the email. Use the method getCode() on the returned object
     * to know the result. If getCode() return 200, the mail was sent
     * with success.
     *
     * @param email The email message to send
     * @return The request response
     * @throws SendGridException If something goes wrong
     * @see SendGrid.Email
     * @see SendGrid.Response
     * @since 15.10
     */
    public static SendGrid.Response sendEmail(final SendGrid.Email email) throws SendGridException {
        email.setSubject(String.format("%s %s", Play.application().configuration().getString("email.subject.prefix"), email.getSubject()));
        final SendGrid sendGrid = new SendGrid(
                Play.application().configuration().getString("sendgrid.api.key"),
                Play.application().configuration().getString("sendgrid.api.secret")
        );
        return sendGrid.send(email);
    }
}
