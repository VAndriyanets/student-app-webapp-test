/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Joiner;
import play.Play;
import play.libs.ws.WS;
import play.libs.ws.WSRequest;

/**
 * The class {@code SMSSender} contains some methods for send
 * SMS via OVH API
 * See full documentation : http://guides.ovh.com/Http2Sms
 *
 * @author Jean-Pierre Boudic
 * @version 15.10
 * @since 15.10
 */

public final class SMSSender {

    /**
     * URL of OVH SMS API
     */
    private final static String api_url = "https://www.ovh.com/cgi-bin/sms/http2sms.cgi";

    /**
     * SMS account to use (format sms-nic-X)
     */
    private static String account;

    /**
     * SMS user to use on associate account
     */
    private static String login;

    /**
     * User password
     */
    private static String password;

    /**
     * SMS Sender (Show in sms message)
     */
    private static String sender;

    /**
     * SMS Left decount
     */
    private static int sms_left = 0;

    /**
     * Static constructor. It will initialize class with global settings.
     */
    static {
        SMSSender.account = Play.application().configuration().getString("ovh.httptosms.account");
        SMSSender.login = Play.application().configuration().getString("ovh.httptosms.login");
        SMSSender.password = Play.application().configuration().getString("ovh.httptosms.password");
        SMSSender.sender = Play.application().configuration().getString("ovh.httptosms.sender");
    }

    /**
     * Send a SMS using OVH API
     *
     * @return The left sms decount
     * @since 15.10
     */
    public static int getSMSLeft() {
        return sms_left;
    }

    /**
     * Send a SMS using OVH API
     *
     * @param from    Telephone number of sender in international format (00336X...) - Must be declared in your SMS account
     * @param to      List of telephone number of target in international format (00336X...)
     * @param message Message to send
     * @throws Exception if something goes wrong
     * @since 15.10
     */
    public static void send(String from, String[] to, String message) throws Exception {
        SMSSender.send(from, Joiner.on(',').join(to), message);
    }

    /**
     * Send a SMS using OVH API
     *
     * @param to      List of telephone number of target in international format (00336X...)
     * @param message Message to send
     * @throws Exception if something goes wrong
     * @since 15.10
     */
    public static void send(String[] to, String message) throws Exception {
        SMSSender.send(SMSSender.sender, Joiner.on(',').join(to), message);
    }

    /**
     * Send a SMS using OVH API
     *
     * @param to      List of telephone number of target in internation format (00336X...) - Number are separate by comma symbol (,)
     * @param message Message to send
     * @throws Exception if something goes wrong
     * @since 15.10
     */
    public static void send(String to, String message) throws Exception {
        SMSSender.send(SMSSender.sender, to, message);
    }

    /**
     * Send a SMS using OVH API
     *
     * @param from    Telephone number of sender in internation format (00336X...) - Must be declared in your SMS account
     * @param to      List of telephone number of target in internation format (00336X...) - Number are separate by comma symbol (,)
     * @param message Message to send
     * @throws Exception if something goes wrong
     * @since 15.10
     */
    public static void send(String from, String to, String message) throws Exception {
        WSRequest request = WS.url(api_url)
                .setQueryParameter("account", SMSSender.account)
                .setQueryParameter("login", SMSSender.login)
                .setQueryParameter("password", SMSSender.password)
                .setQueryParameter("from", from)
                .setQueryParameter("to", to)
                .setQueryParameter("message", message)
                .setQueryParameter("contentType", "application/json")
                .setQueryParameter("smscoding", "2")
                .setQueryParameter("noStop", "1");

        JsonNode json = request.get().get(5000L).asJson();

        int status = json.get("status").intValue();

        if (status != 100 && status != 101) {
            throw new Exception(json.get("message").textValue());
        } else {
            SMSSender.sms_left = json.get("creditLeft").intValue();
        }
    }
}
