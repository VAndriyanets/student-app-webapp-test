/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox;

/**
 * StringUtils.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public final class StringUtils {

    /**
     * Get a new string of the specified length with the specified centered text on it.
     *
     * @param s      The text to center
     * @param length The length of the container
     * @return A string containing the centered text.
     */
    public static String center(String s, int length) {
        if (s.length() > length) {
            return s.substring(0, length);
        } else if (s.length() == length) {
            return s;
        } else {
            int leftPadding = (length - s.length()) / 2;
            StringBuilder leftBuilder = new StringBuilder();
            for (int i = 0; i < leftPadding; i++) {
                leftBuilder.append(" ");
            }

            int rightPadding = length - s.length() - leftPadding;
            StringBuilder rightBuilder = new StringBuilder();
            for (int i = 0; i < rightPadding; i++)
                rightBuilder.append(" ");

            return leftBuilder.toString() + s + rightBuilder.toString();
        }
    }
}
