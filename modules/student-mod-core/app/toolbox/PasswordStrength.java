/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox;

/**
 * PasswordStrength.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class PasswordStrength {

    /**
     * Check if the given password is strong or not.
     *
     * @param password The password to test
     * @return {@code true} if the password is strong, otherwise, {@code false}
     * @since 15.10
     */
    public static boolean isStrong(final String password) {
        return password.length() >= 6 && password.matches("^(?=.*[a-zA-Z])(?=.*\\d)(?=.*(_|[^\\w])).+$");
    }
}
