/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import play.Logger;

/**
 * PhoneUtils class,
 * Provide a set of useful methods to use Phone Number
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public final class PhoneUtils {
    /**
     * Return {@code PhoneNumber} instance according to params
     *
     * @param phone   The phone number
     * @param country The phone number country
     * @return {@code PhoneNumber} instance
     * @throws NumberParseException
     * @since 15.10
     */
    public static PhoneNumber getPhoneNumberInstance(String phone, String country) throws NumberParseException {
        return PhoneNumberUtil.getInstance().parseAndKeepRawInput(phone, country);
    }

    public static boolean isValidPhoneNumber(String phone, String country) {
        try {
            return PhoneNumberUtil.getInstance().isValidNumber(getPhoneNumberInstance(phone, country));
        } catch (NumberParseException e) {
            Logger.debug("[isValidPhoneNumber] NumberParseException", e);
        }
        return false;
    }

    /**
     * Return converted phone number to according format
     *
     * @param phone   The phone number
     * @param country The country code (ex: "FR", "GB", "BE")
     * @param format  The desired output format
     * @return Phone number converted to expected format, otherwise, return original phone number
     * @since 15.10
     */
    public static String convertPhoneNumber(String phone, String country, PhoneNumberFormat format) {
        try {
            return PhoneNumberUtil.getInstance().format(getPhoneNumberInstance(phone, country), format);
        } catch (NumberParseException e) {
            Logger.debug("[comvertToInternationalFormat] NumberParseException", e);
        }
        return phone;
    }

    /**
     * Return converted phone number to international format :<br>
     * '+33602030405' -> '+33 6 02 03 04 05'
     *
     * @param phone   Phone number
     * @param country Country code (ex: "FR", "GB", "BE")
     * @return Phone number international format, otherwise, original phone number
     */
    public static String convertPhoneNumberInternational(String phone, String country) {
        return convertPhoneNumber(phone, country, PhoneNumberFormat.INTERNATIONAL);
    }

    /**
     * Return converted phone number to national format :<br>
     * '+33602030405' -> '06 02 03 04 05'
     *
     * @param phone   Phone number
     * @param country Country code (ex: "FR", "GB", "BE")
     * @return Phone number national format, otherwise, original phone number
     */
    public static String convertPhoneNumberNational(String phone, String country) {
        return convertPhoneNumber(phone, country, PhoneNumberFormat.NATIONAL);
    }
}
