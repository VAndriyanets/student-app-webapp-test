/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.eticket;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * QRCodeTicket.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
class QRCodeTicket {

    /**
     * Generate a QRCode.
     *
     * @param data The data to encode on the QRCode
     * @param size The size in pixels of the QRCode
     * @return The QRCode as stream
     * @see InputStream
     * @since 15.10
     */
    public static InputStream getQRCode(final String data, final int size) throws WriterException, IOException {
        final BitMatrix bitMatrix = new QRCodeWriter().encode(data, BarcodeFormat.QR_CODE, size, size);
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "png", os);
        final InputStream in = new ByteArrayInputStream(os.toByteArray());
        os.close();
        return in;
    }

    /**
     * Generate a QRCode.
     *
     * @param data The data to encode on the QRCode
     * @param size The size in pixels of the QRCode
     * @return The QRCode as buffered image
     * @see BufferedImage
     * @since 15.10
     */
    public static BufferedImage getQRCodeAsBufferedImage(final String data, final int size) throws WriterException, IOException {
        final InputStream in = QRCodeTicket.getQRCode(data, size);
        final BufferedImage bi = ImageIO.read(in);
        in.close();
        return bi;
    }
}
