/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.eticket;

import com.google.zxing.WriterException;
import models.AccountModel;
import models.RealmModel;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import play.i18n.Messages;
import toolbox.StringUtils;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * PDFTicket.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class PDFTicket {

    private AccountModel userAccount;
    private String ticketUid;
    private String eventName;
    private InputStream outputStream;

    /**
     * Build an empty PDF e-ticket.
     *
     * @since 15.10
     */
    public PDFTicket() {
        this(null, null, null);
    }

    /**
     * Build a PDF e-ticket.
     *
     * @param account   The account associated to this e-ticket
     * @param ticketUid The ticket UID to use on this e-ticket
     * @param eventName The event name to use on this e-ticket
     * @since 15.10
     */
    public PDFTicket(final AccountModel account, final String ticketUid, final String eventName) {
        this.setUserAccount(account);
        this.setTicketUid(ticketUid);
        this.setEventName(eventName);
    }

    /**
     * Set the event name.
     *
     * @param eventName The event name to use
     * @since 15.10
     */
    public void setEventName(final String eventName) {
        this.eventName = eventName;
        if (this.eventName != null && this.eventName.length() > 55) {
            this.eventName = this.eventName.substring(0, 55);
        }
    }

    /**
     * Set the ticket UID.
     *
     * @param ticketUid The UID to assign
     * @since 15.10
     */
    public void setTicketUid(final String ticketUid) {
        this.ticketUid = ticketUid;
    }

    /**
     * Set the owner of this e-ticket.
     *
     * @param userAccount The user account to assign
     * @see AccountModel
     * @since 15.10
     */
    public void setUserAccount(final AccountModel userAccount) {
        this.userAccount = userAccount;
    }

    /**
     * Generate the PDF e-ticket.
     *
     * @throws IOException         If something goes wrong
     * @throws COSVisitorException If the PDF can't be saved
     * @since 15.10
     */
    public void generateDocument() throws IOException, COSVisitorException {
        PDDocument document = null;
        try {
            document = PDDocument.load(play.api.Play.classloader(play.api.Play.current())
                    .getResourceAsStream("assets/eticket-template.pdf"));
            final PDPage page = (PDPage) document.getDocumentCatalog().getAllPages().get(0);
            final PDFont fontItalic = PDType1Font.HELVETICA_OBLIQUE;
            final PDFont fontBold = PDType1Font.HELVETICA_BOLD;
            final PDFont font = PDType1Font.HELVETICA;
            final PDPageContentStream contentStream = new PDPageContentStream(document, page, true, true);
            page.getContents().getStream();

            contentStream.setNonStrokingColor(new Color(15, 94, 149));
            contentStream.beginText();
            contentStream.setFont(fontBold, 15);
            contentStream.moveTextPositionByAmount(440, 565);
            contentStream.drawString(Messages.get("PDF.ETICKET.TITLE.REFERENCES"));
            contentStream.endText();

            if (this.userAccount != null) {
                contentStream.setNonStrokingColor(Color.black);
                contentStream.beginText();
                contentStream.setFont(font, 25);
                contentStream.moveTextPositionByAmount(440, 520);
                contentStream.drawString(this.userAccount.getFullName());
                contentStream.endText();

                int i = 0;
                for (final RealmModel rm : this.userAccount.getRealms()) {
                    contentStream.beginText();
                    contentStream.setFont(fontItalic, 12);
                    contentStream.moveTextPositionByAmount(440, 505 - (i * 13));
                    contentStream.drawString(rm.getName());
                    contentStream.endText();
                    i += 1;
                    if (i == 3) {
                        break;
                    }
                }
            }

            contentStream.beginText();
            contentStream.setFont(fontBold, 18);
            contentStream.moveTextPositionByAmount(433.5f, 450);
            contentStream.drawString(StringUtils.center(this.eventName, 55));
            contentStream.endText();

            try {
                final PDXObjectImage img = new PDPixelMap(document, QRCodeTicket.getQRCodeAsBufferedImage(this.ticketUid, 128));
                contentStream.drawImage(img, 568.5f, 299);
            } catch (WriterException ignore) {
                contentStream.setNonStrokingColor(Color.black);
                contentStream.beginText();
                contentStream.setFont(font, 15);
                contentStream.moveTextPositionByAmount(490, 320);
                contentStream.drawString(this.ticketUid);
                contentStream.endText();
            }

            contentStream.setNonStrokingColor(new Color(15, 94, 149));
            contentStream.beginText();
            contentStream.setFont(fontBold, 15);
            contentStream.moveTextPositionByAmount(440, 270);
            contentStream.drawString(Messages.get("PDF.ETICKET.TITLE.TERMS"));
            contentStream.endText();

            contentStream.setNonStrokingColor(Color.black);
            for (int i = 0; Messages.isDefined(String.format("PDF.ETICKET.TXT.TERMS.%d", i)); ++i) {
                final String line = Messages.get(String.format("PDF.ETICKET.TXT.TERMS.%d", i));
                float charSpacing = 0;
                if (line.length() > 80) {
                    float s = 8 * font.getStringWidth(line) / 1000;
                    float free = 380 - s;
                    if (free > 0) {
                        charSpacing = free / (line.length() - 1);
                    }
                }
                contentStream.beginText();
                contentStream.setFont(font, 8);
                contentStream.moveTextPositionByAmount(440, 245 - (i * 10));
                contentStream.appendRawCommands(String.format("%f Tc\n", charSpacing).replace(',', '.'));
                contentStream.drawString(line);
                contentStream.endText();
            }

            contentStream.close();
        } catch (IOException e) {
            if (document != null) {
                document.close();
            }
            throw e;
        }
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            document.save(os);
        } catch (COSVisitorException e) {
            document.close();
            throw e;
        }
        this.outputStream = new ByteArrayInputStream(os.toByteArray());
    }

    /**
     * Get the PDF e-ticket stream.
     *
     * @return The PDF e-ticket byte array stream
     * @see InputStream
     * @since 15.10
     */
    public InputStream getStream() {
        try {
            outputStream.reset();
        } catch (IOException ignore) {
        }
        return outputStream;
    }
}
