/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.i18n.Messages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * The class {@code UTCDateTime} contains some methods to help
 * with the joda's DateTime.
 *
 * @author Pierre Adam
 * @version 15.10
 * @see DateTime
 * @since 15.10
 */
public final class UTCDateTime {

    /**
     * The format used by the API
     */
    private static String defaultFormat;

    /**
     * UTC DateTimeZone.
     */
    private static DateTimeZone utcDateTimeZone;

    /**
     * Formatter using the API standard format.
     */
    private static DateTimeFormatter defaultDateTimeFormatter;

    /**
     * Formatter using the API standard format.
     */
    private static DateFormat defaultDateFormatter;

    /**
     * Static constructor.
     *
     * @since 15.10
     */
    static {
        UTCDateTime.defaultFormat = "yyyy-MM-dd'T'HH:mm:ss";
        UTCDateTime.utcDateTimeZone = DateTimeZone.forTimeZone(TimeZone.getTimeZone("UTC"));
        UTCDateTime.defaultDateTimeFormatter = DateTimeFormat.forPattern(UTCDateTime.defaultFormat);
        UTCDateTime.defaultDateFormatter = new SimpleDateFormat(UTCDateTime.defaultFormat);
    }

    /**
     * Convert the give dateTime to use an UTC Timezone.
     *
     * @param dateTime the original dateTime
     * @return the dateTime convert to UTC
     * @since 15.10
     */
    public static DateTime toDateTime(final DateTime dateTime) {
        return dateTime.toDateTime(UTCDateTime.utcDateTimeZone);
    }

    /**
     * Convert the give dateTime to use an UTC Timezone and convert
     * it to the API standard output format.
     *
     * @param dateTime the original dateTime
     * @return the dateTime convert to UTC and the API standard output format
     * @since 15.10
     */
    public static String format(DateTime dateTime) {
        return format(UTCDateTime.defaultDateTimeFormatter, dateTime);
    }

    /**
     * Convert the give dateTime to use an UTC Timezone and convert
     * it to the given output format.
     *
     * @param format   the output format
     * @param dateTime the original dateTime
     * @return the dateTime convert to UTC and the given format
     * @since 15.10
     */
    public static String format(String format, DateTime dateTime) {
        return format(DateTimeFormat.forPattern(format), dateTime);
    }

    /**
     * Convert the give dateTime to use an UTC Timezone and convert
     * it to the given output format.
     *
     * @param formatter the output format
     * @param dateTime  the original dateTime
     * @return the dateTime convert to UTC and the given format
     * @since 15.10
     */
    public static String format(DateTimeFormatter formatter, DateTime dateTime) {
        return dateTime.toDateTime(UTCDateTime.utcDateTimeZone).toString(formatter);
    }

    /**
     * Take the date from the DateTime and consider it to be in the precised TimeZone
     *
     * @param date the date to convert
     * @param tz   the TimeZone of the date
     * @return the new date as UTC
     * @since 15.10
     */
    public static DateTime localizeNaiveDateTime(DateTime date, TimeZone tz) {
        DateTimeZone timezone = DateTimeZone.forTimeZone(tz);

        return localizeNaiveDateTime(date.toString(UTCDateTime.defaultDateTimeFormatter), tz);
    }

    /**
     * Take the date from the DateTime and consider it to be in the precised TimeZone
     *
     * @param date the date to convert
     * @param tz   the TimeZone of the date
     * @return the new date as UTC
     * @since 15.10
     */
    public static DateTime localizeNaiveDateTime(Date date, TimeZone tz) {
        DateTimeZone timezone = DateTimeZone.forTimeZone(tz);
        DateTime dateTime = new DateTime(date);

        return localizeNaiveDateTime(UTCDateTime.defaultDateFormatter.format(date), tz);
    }

    /**
     * Take the date from the DateTime and consider it to be in the precised TimeZone
     *
     * @param date the date to convert
     * @param tz   the TimeZone of the date
     * @return the new date as UTC
     * @since 15.10
     */
    public static DateTime localizeNaiveDateTime(String date, TimeZone tz) {
        return localizeNaiveDateTime(UTCDateTime.defaultDateTimeFormatter, date, tz);
    }

    /**
     * Take the date from the DateTime and consider it to be in the precised TimeZone
     *
     * @param format the format of the date
     * @param date   the date to convert
     * @param tz     the TimeZone of the date
     * @return the new date as UTC
     * @since 15.10
     */
    public static DateTime localizeNaiveDateTime(String format, String date, TimeZone tz) {
        return localizeNaiveDateTime(DateTimeFormat.forPattern(format), date, tz);
    }

    /**
     * Take the date from the DateTime and consider it to be in the precised TimeZone
     *
     * @param formatter the formatter matching the date
     * @param date      the date to convert
     * @param tz        the TimeZone of the date
     * @return the new date as UTC
     * @since 15.10
     */
    public static DateTime localizeNaiveDateTime(DateTimeFormatter formatter, String date, TimeZone tz) {
        return formatter.withZone(DateTimeZone.forTimeZone(tz)).parseDateTime(date);
    }

    /**
     * Get the UTC timezone.
     *
     * @return The UTC timezone
     * @since 15.10
     */
    public static TimeZone getUTCTimeZone() {
        return TimeZone.getTimeZone("UTC");
    }


    /**
     * Forming human-readable representation of post creation elapsed time
     *
     * @param createDate entity create date
     * @return localized String like 'less than minute, xx minutes ago, xx hours ago, xx days ago'
     */
    public static String getReadableElapsedTime(DateTime createDate) {
        DateTime now = new DateTime();
        Duration duration = new Duration(createDate, now);
        long elapsedMinutes = duration.getStandardMinutes();
        long elapsedHours = duration.getStandardHours();
        long elapsedDays = duration.getStandardDays();
        if (elapsedDays >= 1) {
            if (elapsedDays == 1) {
                return Messages.get("REALM.GROUP.POSTS.BOARD.ELAPSED.DAY");
            } else {
                return Messages.get("REALM.GROUP.POSTS.BOARD.ELAPSED.DAYS", elapsedDays);
            }
        } else if (elapsedHours >= 1) {
            if (elapsedHours == 1) {
                return Messages.get("REALM.GROUP.POSTS.BOARD.ELAPSED.HOUR");
            } else {
                return Messages.get("REALM.GROUP.POSTS.BOARD.ELAPSED.HOURS", elapsedHours);
            }
        } else if (elapsedMinutes >= 1) {
            if (elapsedMinutes == 1) {
                return Messages.get("REALM.GROUP.POSTS.BOARD.ELAPSED.MINUTE");
            } else {
                return Messages.get("REALM.GROUP.POSTS.BOARD.ELAPSED.MINUTES", elapsedMinutes);
            }
        } else {
            return Messages.get("REALM.GROUP.POSTS.BOARD.ELAPSED.LESSMINUTE");
        }

    }
}
