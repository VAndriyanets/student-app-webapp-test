/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox;

import com.avaje.ebean.*;

import java.util.*;

/**
 * The {@code DBQueryFromArgs} helper create a SQL query from arguments
 * usually taken from URL.
 * <pre>
 * Query&lt;S3File&gt; query = DBQueryFromArgs.buildQuery(S3File.class, request().queryString());
 * List&lt;S3File&gt; results = query.findList();</pre>
 * <pre>
 * Query&lt;S3File&gt; query = DBQueryFromArgs.buildQuery(S3File.class, request().queryString(), "(name|contenttype)__(.*)");
 * List&lt;S3File&gt; results = query.findList();</pre>
 *
 * @author Thibault Meyer
 * @since 16.04
 */
public final class DBQueryFromArgs {

    /**
     * Check if the sentence can be processed or not.
     *
     * @param sentence The sentence to check
     * @param rules    An array of rules
     * @return {@code true} is the sentence can be processed, otherwise, {@code false}
     */
    private static boolean verifyRules(final String sentence, final String... rules) {
        for (final String rule : rules) {
            if (sentence.matches(rule)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Build a query for the given model class and arguments. All model
     * fields (id, FK, ...) are allowed.
     *
     * @param c    The model class that this method will create request for
     * @param args The arguments taken from request
     * @param <T>  Something that extends Model
     * @return The Query
     * @see Model
     * @see Query
     */
    @Deprecated
    public static <T extends Model> Query<T> buildQuery(final Class<T> c, final Map<String, String[]> args) {
        return DBQueryFromArgs.buildQuery(c, args, (String[]) null);
    }

    /**
     * Build a query for the given model class and arguments. The ending
     * varargs is used to specify rules to allow or deny queries on fields.
     *
     * @param c     The model class that this method will create request for
     * @param args  The arguments taken from request
     * @param rules Allowed rules
     * @param <T>   Something that extends Model
     * @return The Query
     * @see Model
     * @see Query
     */
    public static <T extends Model> Query<T> buildQuery(final Class<T> c, final Map<String, String[]> args, final String... rules) {
        final Query<T> query = Ebean.createQuery(c);
        final ExpressionList<T> predicats = query.where();
        for (Map.Entry<String, String[]> queryString : args.entrySet()) {
            if (!DBQueryFromArgs.verifyRules(queryString.getKey(), rules)) {
                continue;
            }
            final List<String> words = Arrays.asList(queryString.getKey().split("__"));
            final Iterator<String> wordIterator = words.iterator();
            String currentFK = "";
            String value = queryString.getValue()[0];
            if (value != null) {
                value = value.trim().replace("#", "").replace("--", "").replace("'", "".replace("`", ""));
            }
            while (wordIterator.hasNext()) {
                final String word = wordIterator.next();
                if (word.startsWith("_")) {
                    currentFK = String.format("%s.", word.substring(1));
                } else {
                    if (!wordIterator.hasNext()) {
                        predicats.eq(String.format("%s%s", currentFK, word), value);
                    } else {
                        final String functToApply = wordIterator.next().toLowerCase(Locale.ENGLISH);
                        final String functKey = String.format("%s%s", currentFK, word);
                        switch (functToApply) {
                            case "gt":
                                predicats.gt(functKey, value);
                                break;
                            case "gte":
                                predicats.ge(functKey, value);
                                break;
                            case "lt":
                                predicats.lt(functKey, value);
                                break;
                            case "lte":
                                predicats.le(functKey, value);
                                break;
                            case "like":
                                predicats.like(functKey, value);
                                break;
                            case "ilike":
                                predicats.ilike(functKey, value);
                                break;
                            case "contains":
                                predicats.contains(functKey, value);
                                break;
                            case "icontains":
                                predicats.icontains(functKey, value);
                                break;
                            case "null":
                                predicats.isNull(functKey);
                                break;
                            case "notnull":
                                predicats.isNotNull(functKey);
                                break;
                            case "startwith":
                                predicats.startsWith(functKey, value);
                                break;
                            case "endwith":
                                predicats.endsWith(functKey, value);
                                break;
                            case "istartwith":
                                predicats.istartsWith(functKey, value);
                                break;
                            case "iendwith":
                                predicats.iendsWith(functKey, value);
                                break;
                            case "eq":
                                predicats.eq(functKey, value);
                                break;
                            case "in":
                                if (value != null) {
                                    predicats.in(functKey, (Object[]) value.split(","));
                                }
                                break;
                            case "notin":
                                if (value != null) {
                                    predicats.not(Expr.in(functKey, value.split(",")));
                                }
                                break;
                            case "orderby":
                                if (value != null && (value.compareToIgnoreCase("asc") == 0 || value.compareToIgnoreCase("desc") == 0)) {
                                    predicats.orderBy(functKey + " " + value);
                                }
                                break;
                        }
                    }
                }
            }
        }
        return query;
    }
}
