/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.form.validator;

import play.data.validation.Constraints;

import javax.validation.ConstraintValidator;

import static play.libs.F.Tuple;

/**
 * Helper to validate Double maximum value.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class MaxDoubleValidator extends Constraints.Validator<Double> implements ConstraintValidator<MaxDouble, Double> {

    /**
     * The error message key.
     */
    final static public String message = "error.max";

    /**
     * Constraints given on the annotation.
     */
    private MaxDouble constraintAnnotation;

    /**
     * Default constructor.
     */
    public MaxDoubleValidator() {
    }

    /**
     * Check if the constrain is respected or not.
     *
     * @param object The object to check
     * @return {@code true} if passed object is valid, otherwise, {@code false}
     */
    public boolean isValid(final Double object) {
        return object == null || object <= this.constraintAnnotation.value();
    }

    /**
     * Get the error message key.
     *
     * @return The error message key
     */
    public Tuple<String, Object[]> getErrorMessageKey() {
        return Tuple(message, new Object[]{this.constraintAnnotation.value()});
    }

    /**
     * Initializes the validator in preparation for
     * {@link #isValid(Double object)} calls.
     * The constraint annotation for a given constraint declaration
     * is passed. This method is guaranteed to be called before any use
     * of this instance for validation.
     *
     * @param constraintAnnotation annotation instance for a given constraint declaration
     */
    @Override
    public void initialize(MaxDouble constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }
}
