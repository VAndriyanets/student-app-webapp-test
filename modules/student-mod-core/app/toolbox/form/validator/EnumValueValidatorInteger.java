/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.form.validator;

import play.data.validation.Constraints;
import play.libs.F;

import javax.validation.ConstraintValidator;

import static play.libs.F.Tuple;

/**
 * Helper to validate Enum value from Integer field.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class EnumValueValidatorInteger extends Constraints.Validator<Integer> implements ConstraintValidator<EnumValueInteger, Integer> {

    /**
     * The error message key.
     */
    final static public String message = "error.invalid";

    /**
     * Constraints given on the annotation.
     */
    private EnumValueInteger constraintAnnotation;

    /**
     * Default constructor.
     */
    public EnumValueValidatorInteger() {
    }

    /**
     * Check if the constrain is respected or not.
     *
     * @param object The object to check
     * @return {@code true} if passed object is valid, otherwise, {@code false}
     */
    public boolean isValid(final Integer object) {
        if (object == null) {
            return true;
        }
        for (final Enum ewi : constraintAnnotation.enumclass()[0].getEnumConstants()) {
            if (Integer.valueOf(ewi.toString()).compareTo(object) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the error message key.
     *
     * @return The error message key
     */
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return Tuple(message, new Object[]{});
    }

    /**
     * Initializes the validator in preparation for
     * {@link #isValid(Integer object)} calls.
     * The constraint annotation for a given constraint declaration
     * is passed. This method is guaranteed to be called before any use
     * of this instance for validation.
     *
     * @param constraintAnnotation annotation instance for a given constraint declaration
     */
    @Override
    public void initialize(EnumValueInteger constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }
}
