/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.form.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Helper to validate Double minimum value.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MinDoubleValidator.class)
@play.data.Form.Display(name = "constraint.min", attributes = {})
public @interface MinDouble {

    String message() default MinDoubleValidator.message;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    double value();
}
