/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.form.validator;

import play.data.validation.Constraints;
import play.libs.F;

import javax.validation.ConstraintValidator;

import static play.libs.F.Tuple;

/**
 * Helper to validate Enum value from String field.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class EnumValueValidatorString extends Constraints.Validator<String> implements ConstraintValidator<EnumValueString, String> {

    /**
     * The error message key.
     */
    final static public String message = "error.invalid";

    /**
     * Constraints given on the annotation.
     */
    private EnumValueString constraintAnnotation;

    /**
     * Default constructor.
     */
    public EnumValueValidatorString() {
    }

    /**
     * Check if the constrain is respected or not.
     *
     * @param object The object to check
     * @return {@code true} if passed object is valid, otherwise, {@code false}
     */
    public boolean isValid(final String object) {
        if (object == null) {
            return true;
        }
        for (final Enum ewi : constraintAnnotation.enumclass()[0].getEnumConstants()) {
            if (ewi.toString().compareToIgnoreCase(object) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the error message key.
     *
     * @return The error message key
     */
    public F.Tuple<String, Object[]> getErrorMessageKey() {
        return Tuple(message, new Object[]{});
    }

    /**
     * Initializes the validator in preparation for
     * {@link #isValid(String object)} calls.
     * The constraint annotation for a given constraint declaration
     * is passed. This method is guaranteed to be called before any use
     * of this instance for validation.
     *
     * @param constraintAnnotation annotation instance for a given constraint declaration
     */
    @Override
    public void initialize(EnumValueString constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
    }
}
