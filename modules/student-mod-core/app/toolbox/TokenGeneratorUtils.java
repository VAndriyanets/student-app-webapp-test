/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox;

import java.security.SecureRandom;
import java.util.Locale;

/**
 * TokenGeneratorUtils.
 * Provide a simple Token generator
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class TokenGeneratorUtils {
    /**
     * List in {@code String} all accepted characters in Token
     */
    private static final String ACCEPTED_CHARS = "abcdefghijklmnopqrstuvwxyz0123456789#";

    /**
     * Generate a random alphanumeric Token.
     *
     * @param size Expected token length.
     * @return The generated token
     * @since 15.10
     */
    public static String generateToken(int size) {
        final StringBuilder sb = new StringBuilder();
        final SecureRandom random = new SecureRandom();
        for (int i = 0; i < size; i++) {
            sb.append(ACCEPTED_CHARS.charAt(random.nextInt(ACCEPTED_CHARS.length())));
        }
        return sb.toString().toUpperCase(Locale.ENGLISH);
    }

    /**
     * Generate a random 5 char alphanumeric Token.
     *
     * @return The 5 char generate token
     */
    public static String generateValidationToken() {
        return generateToken(5);
    }
}
