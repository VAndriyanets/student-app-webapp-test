package toolbox.helper;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import modules.GrayLogModule;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.NoNodeAvailableException;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.graylog2.gelfclient.GelfMessage;
import org.graylog2.gelfclient.GelfMessageLevel;
import play.Logger;
import play.Play;
import play.libs.Json;
import play.mvc.Http;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Locale;

/**
 * ElasticSearchHelper.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public final class ElasticSearchHelper {

    public static final String INDEX_NAME = Play.application().configuration().getString("elasticsearch.index.name", "mybeestudent");

    /**
     * Index the given account to the search engine. If the account
     * is already indexed, entry will by updated.
     *
     * @param model The model to index
     * @since 15.10
     */
    public static void indexEntry(final SearchableModel model) {
        index(model.getUidAsString(), model.getSearchType(), model.getIndexSource());
    }

    /**
     * Properly log ES error.
     *
     * @param ex exception during working with ES
     */
    public static void handleESError(final Exception ex) {
        if (Play.isProd()) {
            final GrayLogModule glm = Play.application().injector().instanceOf(GrayLogModule.class);
            final GelfMessage message = glm.createMessage("ElasticSearch", Http.Context.current().request());
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            message.setFullMessage(sw.toString());
            message.setLevel(GelfMessageLevel.ERROR);
            pw.close();
            try {
                sw.close();
            } catch (IOException ignore) {
            }
            glm.sendAsync(message);
        } else {
            Logger.error("Can't connect to Elastic Search!", ex);
        }
    }


    /**
     * ES client factory method.
     *
     * @return client instance
     */
    public static Client getClient() {
        return new TransportClient().addTransportAddress(new InetSocketTransportAddress(
                Play.application().configuration().getString("elasticsearch.endpoint.host", "127.0.0.1"),
                Play.application().configuration().getInt("elasticsearch.endpoint.port", 9300)));
    }

    public static SearchRequestBuilder prepareSearch(Client client){
        return client.prepareSearch(INDEX_NAME);
    }

    /**
     * Form query string from search string
     * @param query search string
     * @return formed query string
     */
    public static String formSearchQuery(String query) {
        String q = query;
        if (!q.toUpperCase(Locale.ENGLISH).matches("(.*)(AND|OR|:|~|\\*)(.*)")) {
            if (q.length() > 3) {
                final String[] tokens = q.split(" ");
                for (int i = 0; i < tokens.length; ++i) {
                    if (tokens[i].length() > 3) {
                        tokens[i] += "~";
                    } else {
                        tokens[i] = "*" + tokens[i] + "*";
                    }
                }
                q = Arrays.toString(tokens).replace("[", "").replace("]", "").replace(",", "");
            } else {
                q = "*" + q + "*";
            }
        }
        return q;
    }

    private static void index(String uid, String type, ObjectNode source) {
        try (Client client = getClient()) {
            final String indexName = INDEX_NAME;
            client.prepareIndex(indexName, type, uid)
                    .setSource(source.toString())
                    .get();
        } catch (NoNodeAvailableException ex) {
            handleESError(ex);
        } catch (Throwable e) {
            Logger.error("Error indexing account entry", e);
        }
    }
}
