/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

package toolbox.helper;

import com.fasterxml.jackson.databind.JsonNode;
import models.S3FileModel;
import play.Logger;
import play.libs.Json;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * ImageHelper.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class ImageHelper {


    /**
     * S3FileModel Converter
     *
     * @param imageFile   The File image uploaded
     * @param contentType image content type
     * @return file model
     * @since 15.10
     */
    public static S3FileModel convertToS3File(File imageFile, String contentType) {
        if (imageFile != null) {
            final S3FileModel f = new S3FileModel();
            f.setContentType(contentType);
            f.setSubDirectory("realm");
            f.setName(UUID.randomUUID().toString());
            f.setFile(imageFile);
            f.save();
            return f;
        }
        return null;
    }


    /**
     * S3FileModel Converter
     * Convert a uploaded image file to crop into a S3File object (With S3File Save)
     *
     * @param imageFile The File image uploaded
     * @param cropData  A Json string contains coordonate for crop
     * @since 15.10
     */
    public static S3FileModel convertToS3File(File imageFile, String cropData, int[] size) {
        if (imageFile != null) {
            BufferedImage image = null;
            try {
                image = ImageIO.read(imageFile);
            } catch (IOException ex) {
                Logger.warn("Cant't read picture", ex);
            }
            if (!cropData.isEmpty()) {
                final JsonNode selected = Json.parse(cropData);
                final int[] coord = new int[]{0, 0, size[0], size[1]};
                if (selected.has("x") && selected.has("y")
                        && selected.has("w") && selected.has("h")) {
                    coord[0] = selected.get("x").intValue();
                    coord[1] = selected.get("y").intValue();
                    coord[2] = selected.get("w").intValue();
                    coord[3] = selected.get("h").intValue();
                }

                final Rectangle selection = new Rectangle(coord[0], coord[1], coord[2], coord[3]);
                final Rectangle maxSize = new Rectangle(size[0], size[1]);
                final BufferedImage newImg = cropImage(image, selection, maxSize);

                final S3FileModel f = new S3FileModel();
                f.setContentType("image/png");
                f.setSubDirectory("realm");
                f.setName(UUID.randomUUID().toString());

                try {
                    final File tmp = File.createTempFile("tempFile", ".tmp");
                    ImageIO.write(newImg, "png", tmp);
                    f.setFile(tmp);
                } catch (IOException ex) {
                    Logger.warn("Cant't create temporary file", ex);
                }
                f.save();

                return f;
            }
        }
        return null;
    }

    /**
     * Crop a image with size rectangle given
     *
     * @param srcImage  The image that will be cropped
     * @param selection The cropped area (top, left, width, height)
     * @param maxSize   The new area (width, height)
     * @return A cropped image
     * @since 15.10
     */
    private static BufferedImage cropImage(BufferedImage srcImage, Rectangle selection, Rectangle maxSize) {
        BufferedImage croppedImage;
        try {
            croppedImage = srcImage.getSubimage(selection.x, selection.y, selection.width, selection.height);
        } catch (Exception e) {
            croppedImage = srcImage;
        }

        final BufferedImage resizedImage = new BufferedImage(maxSize.width, maxSize.height, BufferedImage.TYPE_INT_ARGB);
        final Graphics2D g2d = resizedImage.createGraphics();
        g2d.drawImage(croppedImage, 0, 0, maxSize.width, maxSize.height, null);
        g2d.dispose();
        g2d.setComposite(AlphaComposite.Src);

        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        return resizedImage;
    }
}
