/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.play;

import play.Logger;
import play.mvc.Http;

import java.util.Map;

/**
 * The {@code FlashScopeForwarder} class allow to forward flash scope
 * over redirection hop.<br>
 * Note : The {@link #FORWARDER_PREFIX FORWARDER_PREFIX} : "{@value #FORWARDER_PREFIX}"
 * must be excluded of any flash scope render.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
public class FlashScopeForwarder {
    /**
     * The Forward prefix used must be ignored by any flash scope render
     */
    public static final String FORWARDER_PREFIX = "fwd-";

    /**
     * Flash scope, provide by controller
     */
    private Http.Flash mFlash;

    /**
     * Constructor with controller's flash scope.
     *
     * @param flash The flash scope provide by controller
     * @since 15.10
     */
    public FlashScopeForwarder(Http.Flash flash) {
        this.mFlash = flash;
    }

    /**
     * Add a "forward-able" message to flash scope, with 'n' hop forward.
     *
     * @param flash Controller flash scope
     * @param hop   Number of hop flash scope message survive
     * @param key   Flash scope key
     * @param value Flash scope value
     * @since 15.10
     */
    public static void forwardFlash(final Http.Flash flash, final int hop, final String key, final String value) {
        flash.put(keyFormatter(key, hop), value);
    }

    /**
     * Add a "forward-able" message to flash scope, will forwarded only one hop.
     *
     * @param flash Controller flash scope
     * @param key   Flash scope key
     * @param value Flash scope value
     * @since 15.10
     */
    public static void forwardFlash(final Http.Flash flash, String key, String value) {
        forwardFlash(flash, 1, key, value);
    }

    /**
     * Convert current "forward-able" flash scope message if no more hop expected.
     * Note must be call in each controller between (origin controller and destination controller)
     *
     * @since 15.10
     */
    public static void flashForwarder(final Http.Flash flash) {
        for (Map.Entry<String, String> e : flash.entrySet()) {
            if (e.getKey().startsWith(FORWARDER_PREFIX)) {
                flash.put(e.getKey().substring(FORWARDER_PREFIX.length(), e.getKey().length()), e.getValue());
                flash.remove(e.getKey());
            }
        }
    }

    /**
     * Utils to convert hop in prefix occurence in final key
     *
     * @param key Original flash scope key
     * @param hop Number of hop flash scope expect to be forward
     * @return Formatted key
     */
    private static String keyFormatter(String key, int hop) {
        final StringBuilder sbKey = new StringBuilder();
        for (int i = 0; i < hop; i++) {
            sbKey.append(FORWARDER_PREFIX);
        }
        sbKey.append(key);
        Logger.debug("Generated key : " + sbKey.toString());
        return sbKey.toString();
    }

    /**
     * Add a "forward-able" message to flash scope, will forwarded only one hop.
     *
     * @param key   Flash scope key
     * @param value Flash scope value
     * @return current instance of {@code FlashScopeForwarder}, call can be chained
     * @since 15.10
     */
    public FlashScopeForwarder forwardFlash(final String key, final String value) {
        forwardFlash(this.mFlash, key, value);
        return this;
    }

    /**
     * Add a "forward-able" message to flash scope, with 'n' hop forward.
     *
     * @param hop   Number of hop flash scope message survive
     * @param key   Flash scope key
     * @param value Flash scope value
     * @return current instance of {@code FlashScopeForwarder}, call can be chained
     * @since 15.10
     */
    public FlashScopeForwarder forwardFlash(final int hop, final String key, final String value) {
        forwardFlash(this.mFlash, hop, key, value);
        return this;
    }

    /**
     * Convert current "forward-able" flash scope message if no more hop expected.
     * Note must be call in each controller between (origin controller and destination controller)
     *
     * @return current instance of {@code FlashScopeForwarder}, call can be chained
     * @since 15.10
     */
    public FlashScopeForwarder flashForwarder() {
        flashForwarder(this.mFlash);
        return this;
    }
}
