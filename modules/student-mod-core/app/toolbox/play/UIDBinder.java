/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.play;

import play.mvc.PathBindable;

import java.util.UUID;

/**
 * The {@code UIDBinder} class allow to use UID directly on the
 * playframework's routes file.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public final class UIDBinder implements PathBindable<UIDBinder> {

    /**
     * The UUID bound variable.
     */
    private UUID uuid;

    /**
     * Default constructor.
     */
    public UIDBinder() {
        super();
    }

    /**
     * Create a {@code UIDBinder} instance with a specific UID.
     *
     * @param uid The UID to use
     */
    public UIDBinder(final String uid) {
        this();
        this.bind(null, uid);
    }

    /**
     * Create a {@code UIDBinder} instance with a specific UID.
     *
     * @param uid The UID to use
     * @since 15.10
     */
    public UIDBinder(final UUID uid) {
        this();
        this.bind(null, uid.toString());
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code UIDBinder} in case of success
     */
    @Override
    public UIDBinder bind(final String key, final String value) {
        try {
            this.uuid = UUID.fromString(value);
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException("404 " + ex.getMessage());
        }
        return this;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(final String key) {
        return this.uuid.toString();
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return this.uuid.toString();
    }

    /**
     * Get a String representation.
     *
     * @return A String
     */
    @Override
    public String toString() {
        return this.uuid.toString();
    }

    /**
     * Get the UUID.
     *
     * @return The UUID.
     * @see UUID
     * @since 15.10
     */
    public UUID getUuid() {
        return this.uuid;
    }
}
