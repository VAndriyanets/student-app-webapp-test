/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.enumeration;

import com.avaje.ebean.annotation.EnumValue;

/**
 * CountryCode.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @see <a href="http://en.wikipedia.org/wiki/ISO_3166-1">ISO 3166-1</a>
 * @since 15.10
 */
public enum CountryCode {

    /**
     * Austria.
     */
    @EnumValue("AUT")
    AUT("Austria", "Österreich"),

    /**
     * Belgium.
     */
    @EnumValue("BEL")
    BEL("Belgium", "België / Belgique  / Belgien "),

    /**
     * Bulgaria.
     */
    @EnumValue("BGR")
    BGR("Bulgaria", "България"),

    /**
     * Croatia.
     */
    @EnumValue("HRV")
    HRV("Croatia", "Hrvatska"),

    /**
     * Cyprus.
     */
    @EnumValue("CYP")
    CYP("Cyprus", "Κύπρος / Kıbrıs"),

    /**
     * Czech Republic.
     */
    @EnumValue("CZE")
    CZE("Czech Republic", "Česká republika"),

    /**
     * Denmark.
     */
    @EnumValue("DNK")
    DNK("Denmark", "Danmark"),

    /**
     * Estonia.
     */
    @EnumValue("EST")
    EST("Estonia", "Eesti"),

    /**
     * Finland.
     */
    @EnumValue("FIN")
    FIN("Finland", "Suomi"),

    /**
     * France.
     */
    @EnumValue("FRA")
    FRA("France", "France"),

    /**
     * Germany.
     */
    @EnumValue("DEU")
    DEU("Germany", "Deutschland"),

    /**
     * Greece.
     */
    @EnumValue("GRC")
    GRC("Greece", "Ελλάδα, Elláda"),

    /**
     * Hungary.
     */
    @EnumValue("HUN")
    HUN("Hungary", "Magyarország"),

    /**
     * Ireland.
     */
    @EnumValue("IRL")
    IRL("Ireland", "Éire"),

    /**
     * Italy.
     */
    @EnumValue("ITA")
    ITA("Italy", "Italia"),

    /**
     * Latvia.
     */
    @EnumValue("LVA")
    LVA("Latvia", "Latvija"),

    /**
     * Lithuania.
     */
    @EnumValue("LTU")
    LTU("Lithuania", "Lietuva"),

    /**
     * Luxembourg.
     */
    @EnumValue("LUX")
    LUX("Luxembourg", "Lëtzebuerg"),

    /**
     * Malta.
     */
    @EnumValue("MLT")
    MLT("Malta", "Malta"),

    /**
     * Netherlands.
     */
    @EnumValue("NLD")
    NLD("Netherlands", "Nederland"),

    /**
     * Poland.
     */
    @EnumValue("POL")
    POL("Poland", "Polska"),

    /**
     * Portugal.
     */
    @EnumValue("PRT")
    PRT("Portugal", "Portuguese Republic"),

    /**
     * Romania.
     */
    @EnumValue("ROU")
    ROU("Romania", "România"),

    /**
     * Slovakia.
     */
    @EnumValue("SVK")
    SVK("Slovakia", "Slovenská republika"),

    /**
     * Slovenia.
     */
    @EnumValue("SVN(")
    SVN("Slovenia", "Slovenija"),

    /**
     * Spain.
     */
    @EnumValue("ESP")
    ESP("Spain", "España"),

    /**
     * Sweden.
     */
    @EnumValue("SWE")
    SWE("Sweden", "Sverige"),

    /**
     * United Kingdom.
     */
    @EnumValue("GBR")
    GBR("United Kingdom", "United Kingdom");

    /**
     * The english country name.
     */
    private final String englishCountryName;

    /**
     * The translated country name.
     */
    private final String countryName;

    /**
     * Build a basic instance with specific name.
     *
     * @param englishCountryName The english name to assign
     * @param countryName        The name to assign
     */
    CountryCode(final String englishCountryName, final String countryName) {
        this.englishCountryName = englishCountryName;
        this.countryName = countryName;
    }

    /**
     * Get the country name.
     *
     * @return The country name.
     */
    public String getCountryName() {
        return this.countryName;
    }

    /**
     * Get the country name.
     *
     * @return The country name.
     */
    public String getEnglishCountryName() {
        return this.englishCountryName;
    }
}
