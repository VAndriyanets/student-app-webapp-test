/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package toolbox.enumeration;

import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Possible accounts' status values.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public enum AccountStatus {

    /**
     * Account need to be validated from a unique link
     * sent by email.
     */
    @EnumValue("0")
    NEED_VALIDATION(0),

    /**
     * Everything is good!
     */
    @EnumValue("1")
    OK(1),

    /**
     * Account is frozen. It cant logon and current
     * generated tokens are cursed.
     */
    @EnumValue("2")
    FROZEN(2);

    /**
     * This ID is internally used to have a numeric
     * representation of this enumeration.
     */
    private final int id;

    /**
     * Create a simple {@code AccountStatus} instance.
     *
     * @param id The ID to use
     */
    AccountStatus(final int id) {
        this.id = id;
    }

    /**
     * Jackson helper.
     *
     * @param value The value to resolve
     * @return A {@code CivilityEnum} object
     * @since 15.10
     */
    @JsonCreator
    public static AccountStatus create(String value) {
        if (value == null) {
            throw new IllegalArgumentException();
        }
        for (AccountStatus obj : values()) {
            if (value.equals(String.valueOf(obj.getId()))) {
                return obj;
            }
        }
        throw new IllegalArgumentException();
    }

    /**
     * Result enumeration value from ID.
     *
     * @param i The requested enumeration value ID
     * @return The found enumeration value or {@code null}
     * @since 15.10
     */
    public static AccountStatus valueOf(final Integer i) {
        for (final AccountStatus as : AccountStatus.values()) {
            if (as.getId() == i) {
                return as;
            }
        }
        return null;
    }

    /**
     * Get the ID of the current enumeration choice.
     *
     * @return The ID
     * @since 15.10
     */
    public int getId() {
        return this.id;
    }

    /**
     * Return the string representation of the current
     * enumeration choice.
     *
     * @return A simple string
     * @since 15.10
     */
    @JsonValue
    @Override
    public String toString() {
        return String.format("%d", this.id);
    }
}
