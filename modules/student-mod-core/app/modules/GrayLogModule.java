/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package modules;

import org.graylog2.gelfclient.*;
import org.graylog2.gelfclient.transport.GelfTransport;
import play.Configuration;
import play.inject.ApplicationLifecycle;
import play.libs.F;
import play.mvc.Http.Request;
import play.mvc.Http.RequestHeader;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.Map;

/**
 * GreyLogModule.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Singleton
public class GrayLogModule {

    /**
     * Configuration keys.
     */
    private static final String GREYLOG_SERVER_HOST = "greylog.host";
    private static final String GREYLOG_SERVER_PORT = "greylog.port";
    private static final String GREYLOG_SERVER_APPSOURCE = "greylog.appsource";

    /**
     * GreyLog transport handle.
     */
    private GelfTransport transport;

    /**
     * Message builder.
     */
    private GelfMessageBuilder builder;

    /**
     * Create a simple instance of {@code GreyLogModule}.
     *
     * @param lifecycle     The application life cycle
     * @param configuration The application configuration
     * @since 15.10
     */
    @Inject
    public GrayLogModule(final ApplicationLifecycle lifecycle, final Configuration configuration) {
        final String greyLogHost = configuration.getString(GrayLogModule.GREYLOG_SERVER_HOST);
        final Integer greyLogPort = configuration.getInt(GrayLogModule.GREYLOG_SERVER_PORT);
        final String greyLogAppSource = configuration.getString(GrayLogModule.GREYLOG_SERVER_APPSOURCE);
        if ((greyLogHost != null) && (greyLogPort != null) && (greyLogAppSource != null)) {
            final GelfConfiguration config = new GelfConfiguration(new InetSocketAddress(greyLogHost, greyLogPort))
                    .transport(GelfTransports.TCP)
                    .queueSize(512)
                    .connectTimeout(5000)
                    .reconnectDelay(1000)
                    .tcpNoDelay(true)
                    .sendBufferSize(32768);
            this.transport = GelfTransports.create(config);
            this.builder = new GelfMessageBuilder("", greyLogAppSource)
                    .level(GelfMessageLevel.INFO);

            try {
                this.builder.additionalField("gl2_remote_hostname", InetAddress.getLocalHost().getHostName());
            } catch (UnknownHostException ignore) {
            }
        } else {
            throw new RuntimeException("GreyLogModule is not properly configured");
        }
        lifecycle.addStopHook(() -> {
            this.transport.stop();
            return F.Promise.pure(null);
        });
    }

    /**
     * Create a new GELF message.
     *
     * @param shortMessage  The short message to assign
     * @param requestHeader The current request header to use
     * @return A {@code GelfMessage} instance
     * @since 15.10
     */
    public GelfMessage createMessage(final String shortMessage, final RequestHeader requestHeader) {
        final StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String[]> e : requestHeader.headers().entrySet()) {
            if (sb.length() == 0) {
                sb.append(String.format("%s=%s", e.getKey(), e.getValue()[0]));
            } else {
                sb.append(String.format(", %s=%s", e.getKey(), e.getValue()[0]));
            }
        }
        return builder.message(shortMessage)
                .additionalField("remote", requestHeader.remoteAddress())
                .additionalField("method", requestHeader.method())
                .additionalField("uri", requestHeader.uri())
                .additionalField("headers", sb.toString())
                .build();
    }

    /**
     * Create a new GELF message.
     *
     * @param shortMessage The short message to assign
     * @param request      The current request header to use
     * @return A {@code GelfMessage} instance
     * @since 15.10
     */
    public GelfMessage createMessage(final String shortMessage, final Request request) {
        final StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String[]> e : request.headers().entrySet()) {
            if (sb.length() == 0) {
                sb.append(String.format("%s=%s", e.getKey(), e.getValue()[0]));
            } else {
                sb.append(String.format(", %s=%s", e.getKey(), e.getValue()[0]));
            }
        }
        return builder.message(shortMessage)
                .additionalField("remote", request.remoteAddress())
                .additionalField("method", request.method())
                .additionalField("uri", request.uri())
                .additionalField("headers", sb.toString())
                .additionalField("auth_user", request.username())
                .build();
    }

    /**
     * Try to send the GELF message to GreyLog server.
     *
     * @param gelfMessage The message to send asynchronously
     * @return {@code true} in case of success, otherwise, {@code false}
     * @since 15.10
     */
    public boolean sendAsync(final GelfMessage gelfMessage) {
        return this.transport.trySend(gelfMessage);
    }
}
