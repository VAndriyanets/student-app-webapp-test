/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package modules;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import play.Configuration;
import play.Logger;
import play.inject.ApplicationLifecycle;
import play.libs.F;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Amazon S3 compatible service module to store files. This module
 * is compatible with Amazon S3 and Eucalyptus Walrus.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Singleton
public class S3Module {

    /**
     * Keys used to retrieve configuration.
     */
    private static final String AWS_S3_BUCKET = "aws.s3.bucket";
    private static final String AWS_ENDPOINT = "aws.s3.endpoint";
    private static final String AWS_WITHPATHSTYLE = "aws.s3.pathstyle";
    private static final String AWS_ACCESS_KEY = "aws.access.key";
    private static final String AWS_SECRET_KEY = "aws.secret.key";

    /**
     * Instance of the S3 API client.
     */
    public final AmazonS3 amazonS3;

    /**
     * Name of the S3 bucket to use.
     */
    public final String s3Bucket;

    /**
     * Create a simple instance of {@code S3Module}.
     *
     * @param lifecycle     The application life cycle
     * @param configuration The application configuration
     * @since 15.10
     */
    @Inject
    public S3Module(final ApplicationLifecycle lifecycle, final Configuration configuration) {
        final String accessKey = configuration.getString(S3Module.AWS_ACCESS_KEY);
        final String secretKey = configuration.getString(S3Module.AWS_SECRET_KEY);
        this.s3Bucket = configuration.getString(S3Module.AWS_S3_BUCKET);
        if ((accessKey != null) && (secretKey != null) && (this.s3Bucket != null)) {
            final AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
            this.amazonS3 = new AmazonS3Client(awsCredentials);
            this.amazonS3.setEndpoint(configuration.getString(S3Module.AWS_ENDPOINT));
            if (configuration.getBoolean(S3Module.AWS_WITHPATHSTYLE, false)) {
                this.amazonS3.setS3ClientOptions(new S3ClientOptions().withPathStyleAccess(true));
            }
            try {
                this.amazonS3.createBucket(this.s3Bucket);
            } catch (AmazonS3Exception e) {
                if (e.getErrorCode().compareTo("BucketAlreadyOwnedByYou") != 0) {
                    throw e;
                }
            } finally {
                Logger.info("Using S3 Bucket: " + this.s3Bucket);
            }
        } else {
            throw new RuntimeException("S3Module is not properly configured");
        }
        lifecycle.addStopHook(() -> F.Promise.pure(null));
    }
}
