/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package modules;

import play.Configuration;
import play.inject.ApplicationLifecycle;
import play.libs.F;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * RedisModule.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Singleton
public class RedisModule {

    /**
     * Configuration keys.
     */
    private static final String REDISPOOL_SERVER_HOST = "redis.server.host";
    private static final String REDISPOOL_SERVER_PORT = "redis.server.port";
    private static final String REDISPOOL_SERVER_PASSWORD = "redis.server.password";
    private static final String REDISPOOL_SERVER_TIMEOUT = "redis.server.timeout";
    private static final String REDISPOOL_SERVER_MAXCONN = "redis.server.maxconn";

    /**
     * The Redis connections pool.
     */
    private JedisPool redisPool;


    /**
     * Create a simple instance of {@code S3Module}.
     *
     * @param lifecycle     The application life cycle
     * @param configuration The application configuration
     * @since 15.10
     */
    @Inject
    public RedisModule(final ApplicationLifecycle lifecycle, final Configuration configuration) {
        final String redisHost = configuration.getString(RedisModule.REDISPOOL_SERVER_HOST);
        final String redisPassword = configuration.getString(RedisModule.REDISPOOL_SERVER_PASSWORD);
        final Integer redisPort = configuration.getInt(RedisModule.REDISPOOL_SERVER_PORT, 6379);
        final Integer redisTimeout = configuration.getInt(RedisModule.REDISPOOL_SERVER_TIMEOUT, 0);
        final Integer redisMaxTotal = configuration.getInt(RedisModule.REDISPOOL_SERVER_MAXCONN, 8);
        if (redisHost != null) {
            final JedisPoolConfig poolConfig = new JedisPoolConfig();
            poolConfig.setMaxTotal(redisMaxTotal);
            if (redisPassword != null && !redisPassword.isEmpty()) {
                this.redisPool = new JedisPool(poolConfig, redisHost, redisPort, redisTimeout, redisPassword);
            } else {
                this.redisPool = new JedisPool(poolConfig, redisHost, redisPort, redisTimeout);
            }
        } else {
            throw new RuntimeException("RedisModule is not properly configured");
        }
        lifecycle.addStopHook(() -> {
            this.redisPool.close();
            return F.Promise.pure(null);
        });
    }

    /**
     * Get a Redis connection from the pool.
     *
     * @return A redis connection
     * @see Jedis
     * @since 15.10
     */
    public Jedis getConnection() {
        return this.redisPool.getResource();
    }
}
