/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package actors;

import models.SearchableModel;

/**
 * SearchEngineActorProtocol. Read more information on Akka actors at
 * <a href="https://www.playframework.com/documentation/2.4.x/JavaAkka">PlayFramework website</a>.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class SearchEngineActorProtocol {

    /**
     * IndexAccount.
     *
     * @author Thibault Meyer
     * @version 15.10
     * @since 15.10
     */
    public static class IndexModel {

        /**
         * The account model instance to index.
         */
        public final SearchableModel searchableModel;

        /**
         * Build a default instance.
         *
         * @param searchableModel The account model to index
         * @see SearchableModel
         * @since 15.10
         */
        public IndexModel(final SearchableModel searchableModel) {
            this.searchableModel = searchableModel;
        }
    }
}
