/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package actors;

import akka.actor.Props;
import akka.actor.UntypedActor;
import models.SearchableModel;
import toolbox.helper.ElasticSearchHelper;

/**
 * SearchEngineActor. Read more information on Akka actors at
 * <a href="https://www.playframework.com/documentation/2.4.x/JavaAkka">PlayFramework website</a>.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public class SearchEngineActor extends UntypedActor {

    public static Props props = Props.create(SearchEngineActor.class);

    /**
     * This defines the behavior of the UntypedActor.
     */
    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof SearchEngineActorProtocol.IndexModel) {
            final SearchableModel model = ((SearchEngineActorProtocol.IndexModel) message).searchableModel;
            ElasticSearchHelper.indexEntry(model);
        }
    }
}
