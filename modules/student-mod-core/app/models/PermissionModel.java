/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * PermissionModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "security_permission", uniqueConstraints = @UniqueConstraint(columnNames = {"namespace", "scope"}))
public class PermissionModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, PermissionModel> find = new Finder<>(PermissionModel.class);

    /**
     * Find permission by name
     *
     * @param name perm name
     * @return perm model
     */
    public static PermissionModel findByName(final String name, final PermissionModel.Namespace namespace) {
        return find.where().like("scope", name).like("namespace", namespace.toString()).findUnique();
    }


    /**
     * Find permissions by namespace
     *
     * @param namespace perm namespace
     * @return perm model list
     */
    public static List<PermissionModel> findByNamespace(final PermissionModel.Namespace namespace) {
        return find.where().like("namespace", namespace.toString()).findList();
    }

    /**
     * The unique ID of this entry.
     */
    @Id
    private Long id;

    /**
     * Namespace of the permission.
     */
    @Column(name = "namespace", nullable = false, unique = false)
    @Enumerated(EnumType.STRING)
    private Namespace namespace;

    /**
     * Scope of the permission.
     */
    @Size(max = 50)
    @Column(name = "scope", nullable = false, unique = false)
    private String scope;

    /**
     * Create an empty permission.
     *
     * @since 15.10
     */
    public PermissionModel() {
    }

    /**
     * Create a named permission.
     *
     * @param scope     The scope to assign
     * @param namespace The namespace to use
     * @since 15.10
     */
    public PermissionModel(final String scope, final Namespace namespace) {
        this.setScope(scope);
        this.setNamespace(namespace);
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the namespace.
     *
     * @return The namespace
     * @see models.PermissionModel.Namespace
     * @since 15.10
     */
    public Namespace getNamespace() {
        return this.namespace;
    }

    /**
     * Set the namespace.
     *
     * @param namespace The namespace to set
     * @see models.PermissionModel.Namespace
     * @since 15.10
     */
    public void setNamespace(Namespace namespace) {
        this.namespace = namespace;
    }

    /**
     * Get the namespace as string.
     *
     * @return The namespace as string
     * @see models.PermissionModel.Namespace
     * @since 15.10
     */
    public String getNamespaceAsString() {
        return this.namespace.toString();
    }

    /**
     * Get the scope name.
     *
     * @return The scope name
     * @since 15.10
     */
    public String getScope() {
        return this.scope;
    }

    /**
     * Set the scope string. It will be trimmed.
     *
     * @param scope The scope to set
     * @since 15.10
     */
    public void setScope(String scope) {
        this.scope = scope.trim();
    }

    /**
     * Enumeration of possible namespaces.
     *
     * @author Thibault Meyer
     * @author Olivier Buiron
     * @version 15.12
     * @since 15.10
     */
    public enum Namespace {

        /**
         * Permission is admin related.
         *
         * @since 15.10
         */
        @EnumValue("ADMIN")
        ADMIN,

        /**
         * Permission is realm related.
         *
         * @since 15.10
         */
        @EnumValue("REALM")
        REALM,

        /**
         * Permission is realm group related.
         *
         * @since 15.12
         */
        @EnumValue("REALM_GROUP")
        REALM_GROUP,

        /**
         * Permission is page related.
         *
         * @since 15.10
         */
        @EnumValue("PAGE")
        PAGE
    }
}
