package models;

import java.util.List;

/**
 * @author Andrey Sokolov
 */
public interface RoleWithAccountsModel {
    String getName();
    List<AccountModel> getAccounts();
    String getUidAsString();
    boolean isReadonly();
    void removeAccount(AccountModel account);
    void addAccount(AccountModel account);
}
