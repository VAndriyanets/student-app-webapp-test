package models;

import com.avaje.ebean.Model;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * @author vitaly.andriyanets
 */
@Entity
@Table(name = "cards")
public class CardModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, CardModel> find = new Finder<>(CardModel.class);

    /**
     * The unique ID of this card.
     */
    @Id
    public Long id;

    /**
     * The account associated to this card.
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Column(name = "account")
    private AccountModel account;

    /**
     * Number of the card.
     */
    @Size(max = 128)
    @Column(name = "card_number", nullable = false, unique = true)
    public String cardNumber;

    /**
     * Type of the card
     */
    @ManyToOne
    @JoinTable(name = "card_type")
    public CardTypeModel cardType;

    /**
     * Date of creation.
     */
    @Column(name = "create_date", nullable = false, unique = false)
    public DateTime createDate;

    /**
     * Build a basic card.
     */
    public CardModel(final AccountModel account) {
        this.createDate = DateTime.now();
        this.account = account;
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public CardTypeModel getCardType() {
        return cardType;
    }

    public void setCardType(CardTypeModel cardType) {
        this.cardType = cardType;
    }

    public DateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(DateTime createDate) {
        this.createDate = createDate;
    }
}
