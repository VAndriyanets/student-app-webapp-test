/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import play.mvc.PathBindable;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * RoleRealmModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "security_role_realm")
public class RoleRealmModel extends RoleModel implements PathBindable<RoleRealmModel>, RoleWithAccountsModel {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, RoleRealmModel> find = new Finder<Long, RoleRealmModel>(RoleRealmModel.class);

    /**
     * Realm associated.
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REFRESH)
    @Column(name = "realm", nullable = false, unique = false)
    private RealmModel realm;

    /**
     * List of the permissions associated to the role.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "security_role_realm_has_permission")
    private List<PermissionModel> permissions;

    /**
     * List of the accounts with realm role.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "security_role_realm_has_account")
    private List<AccountModel> accounts;
    /**
     * Name of the role.
     */
    @Size(max = 25)
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    /**
     * Build a basic realm role.
     */
    public RoleRealmModel() {
        super();
    }

    /**
     * Build a realm role.
     *
     * @param realm The realm to associate
     */
    public RoleRealmModel(final RealmModel realm) {
        super();
        this.realm = realm;
    }

    /**
     * Get the realm associated to this role.
     *
     * @return The realm
     * @since 15.10
     */
    public RealmModel getRealm() {
        return this.realm;
    }

    /**
     * Set the realm associated to this role.
     *
     * @param realm The realm to associate
     * @since 15.10
     */
    public void setRealm(final RealmModel realm) {
        this.realm = realm;
    }

    /**
     * Get all accounts attached to this role.
     *
     * @return A list of accounts reference
     * @see AccountModel
     * @since 15.10
     */
    @Override
    public List<AccountModel> getAccounts() {
        return this.accounts;
    }

    /**
     * Set the accounts list.
     *
     * @param accounts The accounts to set
     * @since 15.10
     */
    @Override
    public void setAccounts(final List<AccountModel> accounts) {
        this.accounts = accounts;
    }

    /**
     * Add new account to the existing list.
     *
     * @param accounts Accounts to add
     * @since 15.10
     */
    @Override
    public void addAccounts(final List<AccountModel> accounts) {
        this.accounts.addAll(accounts);
    }

    /**
     * Add new account to the existing list.
     *
     * @param account Account to add
     * @since 15.10
     */
    @Override
    public void addAccount(final AccountModel account) {
        this.accounts.add(account);
    }

    /**
     * Remove account from the existing list.
     *
     * @param account Account to remove
     * @since 15.10
     */
    @Override
    public void removeAccount(final AccountModel account) {
        this.accounts.remove(account);
    }

    /**
     * Remove accounts from the existing list.
     *
     * @param accounts Accounts to remove
     * @since 15.10
     */
    @Override
    public void removeAccounts(final List<AccountModel> accounts) {
        for (final AccountModel a : accounts) {
            this.accounts.remove(a);
        }
    }

    /**
     * Get all permissions attached to this role.
     *
     * @return A list of permissions reference
     * @see PermissionModel
     * @since 15.10
     */
    @Override
    public List<PermissionModel> getPermissions() {
        return this.permissions;
    }

    /**
     * Set the permissions list.
     *
     * @param permissions The permissions to set
     * @since 15.10
     */
    @Override
    public void setPermissions(final List<PermissionModel> permissions) {
        this.permissions = permissions;
    }

    /**
     * Add new permissions to the existing list.
     *
     * @param permissions Permissions to add
     * @since 15.10
     */
    @Override
    public void addPermissions(final List<PermissionModel> permissions) {
        this.permissions.addAll(permissions);
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code RoleRealmModel} in case of success
     */
    @Override
    public RoleRealmModel bind(String key, String value) {
        final RoleRealmModel role = RoleRealmModel.find.where().like("uid", value).findUnique();
        if (role == null) {
            throw new IllegalArgumentException("404");
        }
        return role;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(String key) {
        return this.getUidAsString();
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return "function(k,v) {\n" +
                "    return v.getUidAsString();" +
                "}";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(final String name) {
        this.name = name.trim();
    }
}
