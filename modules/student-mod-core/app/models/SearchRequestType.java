package models;

/**
 * SearchRequestType.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
public enum SearchRequestType {

    /**
     * Looking for everything.
     */
    ALL(Constants.ARTICLE_TYPE, Constants.PAGE_TYPE, Constants.GROUP_TYPE, Constants.USER_TYPE),

    /**
     * Looking for articles only.
     */
    ARTICLE(Constants.ARTICLE_TYPE),

    /**
     * Looking for pages only.
     */
    PAGE(Constants.PAGE_TYPE),

    /**
     * Looking for groups only.
     */
    GROUP(Constants.GROUP_TYPE),

    /**
     * Looking for users only.
     */
    USER(Constants.USER_TYPE);

    /**
     * Contains the request types (Elastic).
     */
    private String[] types;

    SearchRequestType(final String... types) {
        this.types = types;
    }

    /**
     * Get the Elastic request types associated to this
     * current enumeration.
     *
     * @return Array of string
     * @since 15.10
     */
    public String[] getTypes() {
        return this.types;
    }

    public String getType(){
        return types[0];
    }

    public static class Constants {
        public static final String ARTICLE_TYPE = "article";
        public static final String PAGE_TYPE = "page";
        public static final String GROUP_TYPE = "group";
        public static final String USER_TYPE = "user";
    }
}
