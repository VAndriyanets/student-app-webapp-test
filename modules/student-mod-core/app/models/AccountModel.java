/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.DateTime;
import play.cache.Cache;
import play.libs.Json;
import toolbox.UTCDateTime;
import toolbox.enumeration.AccountStatus;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.*;
import java.util.stream.Collectors;

/**
 * AccountModel.
 *
 * @author Thibault Meyer
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "account")
public class AccountModel extends Model implements SearchableModel {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, AccountModel> find = new Finder<>(AccountModel.class);
    private static final int LIKES_CACHE_EXPIRATION = 120;
    public static final String USER_POSTS_LIKED_CACHE_FORMAT = "user.%d.posts.liked";
    public static final String USER_POSTS_DISLIKED_CACHE_FORMAT = "user.%d.posts.disliked";

    public static AccountModel findByUid(final UUID uid){
        return find.where().eq("uid", uid).findUnique();
    }

    /**
     * The unique ID of the account.
     */
    @Id
    private Long id;

    /**
     * Unique uid of the account.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;

    /**
     * Email addresses registered to this account.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = CascadeType.ALL)
    private List<AccountEmailModel> emails;

    /**
     * Phone number registered to this account.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = CascadeType.ALL)
    private List<AccountPhoneModel> phones;

    /**
     * Postal addresses registered to this account.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = CascadeType.ALL)
    private List<AccountPostalAddressModel> addresses;

    /**
     * ID Cards registered to this account.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = CascadeType.ALL)
    private List<CardModel> cards;

    /**
     * Password of the account.
     */
    @Size(max = 125)
    @Column(name = "password", nullable = false, unique = false)
    private String password;

    /**
     * First name.
     */
    @Size(max = 35)
    @Column(name = "first_name", nullable = true, unique = false)
    private String firstName;

    /**
     * Last name.
     */
    @Size(max = 35)
    @Column(name = "last_name", nullable = true, unique = false)
    private String lastName;

    /**
     * Status of this account.
     */
    @Column(name = "status", nullable = false, unique = false, columnDefinition = "INTEGER DEFAULT 0")
    private AccountStatus status;

    /**
     * List of realms where this account can access.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "realm_has_account")
    private List<RealmModel> realms;

    /**
     * List of realms groups where this account can access
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "realm_group_has_account")
    private List<RealmGroupModel> realmGroups;

    /**
     * Link to the actor object. Actor is used on
     * all "social" interactions.
     */
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Column(name = "actor", nullable = false, unique = false)
    private ActorModel actor;

    /**
     * Preferred language. This field is used to set the site
     * language when user is connected.
     */
    @Size(max = 2)
    @Column(name = "lang", nullable = false, unique = false)
    private String lang;

    /**
     * The timezone. This field is used to convert date.
     */
    @Column(name = "timezone", nullable = false, unique = false)
    @Size(max = 32)
    private String timezone;

    /**
     * Date of last login.
     */
    @Column(name = "last_login", nullable = true, unique = false)
    private DateTime lastLogin;

    /**
     * Date of creation.
     */
    @Column(name = "create_date", nullable = false, unique = false)
    private DateTime createDate;

    /**
     * Link to the Facebook Data object.
     */
    @OneToOne(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private FacebookDataModel facebookData;

    /**
     * Constructor for a new {@code AccountModel}.
     *
     * @since 15.10
     */
    public AccountModel() {
        this.uid = UUID.randomUUID();
        this.createDate = DateTime.now();
        this.status = AccountStatus.NEED_VALIDATION;
        this.realms = new ArrayList<>();
        this.actor = new ActorModel();
    }

    /**
     * Save the current entry on database.
     *
     * @since 15.10
     */
    @Override
    public void save() {
        if (this.actor.getId() == 0) {
            this.actor.save();
        } else {
            this.actor.update();
        }
        super.save();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.10
     */
    public UUID getUid() {
        return this.uid;
    }

    @Override
    public ObjectNode getIndexSource() {
        final ObjectNode esUserInfo = Json.newObject();
        esUserInfo.put("first_name", getFirstName());
        esUserInfo.put("last_name", getLastName());
        esUserInfo.put("email", getPrimaryEmail());
        esUserInfo.put("slug", getActor().getSlug());
        final ArrayNode esUserInfoRealm = esUserInfo.putArray("_realm");
        for (final RealmModel rm : getRealms()) {
            esUserInfoRealm.add(rm.getId());
        }
        return esUserInfo;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.10
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    @Override
    public String getSearchType() {
        return SearchRequestType.USER.getTypes()[0];
    }

    /**
     * Check the given password.
     *
     * @param chk_password The password to check
     * @return True if the password is correct
     * @since 15.10
     */
    public boolean checkPassword(final String chk_password) {
        final String[] password_x = this.password.split("\\$");
        try {
            final KeySpec spec = new PBEKeySpec(chk_password.toCharArray(), password_x[2].getBytes(), Integer.parseInt(password_x[1]), 160);
            final SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            final byte[] digest = f.generateSecret(spec).getEncoded();
            final StringBuilder sb_password = new StringBuilder();
            for (final byte aDigest : digest) {
                sb_password.append(Integer.toString((aDigest & 0xFF) + 0x100, 16).substring(1));
            }
            return password_x[3].compareTo(sb_password.toString()) == 0;
        } catch (NoSuchAlgorithmException e) {
            return false;
        } catch (InvalidKeySpecException e) {
            return false;
        }
    }

    /**
     * Set the password of the account.
     *
     * @param new_password The new password
     * @return True if successful
     * @since 15.10
     */
    public boolean setPassword(final String new_password) {
        try {
            // Generate random salt
            final SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            final byte[] salt = new byte[8];
            random.nextBytes(salt);
            final StringBuilder sb_salt = new StringBuilder();
            for (final byte aSalt : salt) {
                sb_salt.append(Integer.toString((aSalt & 0xFF) + 0x100, 16).substring(1));
            }

            // Encrypt password
            final KeySpec spec = new PBEKeySpec(new_password.toCharArray(), sb_salt.toString().getBytes(), 10000, 160);
            final SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            final byte[] digest = f.generateSecret(spec).getEncoded();
            final StringBuilder sb_password = new StringBuilder();

            // Assign password
            for (final byte aDigest : digest) {
                sb_password.append(Integer.toString((aDigest & 0xFF) + 0x100, 16).substring(1));
            }
            this.password = String.format("pbkdf2_hmacsha256$%d$%s$%s", 10000, sb_salt.toString(), sb_password.toString());
        } catch (NoSuchAlgorithmException e) {
            return false;
        } catch (InvalidKeySpecException e) {
            return false;
        }
        return true;
    }

    /**
     * Get the first name field value.
     *
     * @return The first name
     * @since 15.10
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Set the first name field.
     *
     * @param firstName The first name to use
     * @since 15.10
     */
    public void setFirstName(final String firstName) {
        this.firstName = WordUtils.capitalizeFully(firstName.trim());
    }

    /**
     * Get the last name field value.
     *
     * @return The last name
     * @since 15.10
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Set the last name field.
     *
     * @param lastName The last name to use
     * @since 15.10
     */
    public void setLastName(final String lastName) {
        this.lastName = WordUtils.capitalizeFully(lastName.trim());
    }

    /**
     * Get the fullname as FirstName and LastName.
     *
     * @return The fullname
     * @since 15.10
     */
    public String getFullName() {
        if (this.lastName.length() > 0) {
            return this.firstName + " " + this.lastName;
        }
        return this.firstName;
    }

    /**
     * Get the fullname as FirstName and first letter of the LastName.
     *
     * @return The fullname
     * @since 15.10
     */
    public String getFullNameLight() {
        if (this.lastName.length() > 0) {
            return this.firstName + " " + this.lastName.toCharArray()[0] + ".";
        }
        return this.firstName;
    }

    /**
     * Get the current status.
     *
     * @return The current status
     * @see AccountStatus
     * @since 15.10
     */
    public AccountStatus getStatus() {
        return this.status;
    }

    /**
     * Set the current status of this account.
     *
     * @param status The status to set
     * @see AccountStatus
     * @since 15.10
     */
    public void setStatus(AccountStatus status) {
        this.status = status;
    }

    /**
     * Get the number of realms where this account can access.
     *
     * @return The number of realms
     * @since 15.10
     */
    public int getRealmCount() {
        return this.realms.size();
    }

    /**
     * Get the list of realms where this account can access.
     *
     * @return A list of realm
     * @see RealmModel
     * @since 15.10
     */
    public List<RealmModel> getRealms() {
        return this.realms;
    }

    /**
     * Get the list of realms where this account can access (direct and through the realm groups).
     *
     * @return A set of realms
     * @see RealmModel
     */
    public Set<RealmModel> getRealmsAccessible() {
        Set<RealmModel> accessibleRealms = new HashSet<>(this.realms);
        for (RealmGroupModel realmGroup : getRealmGroups()) {
            accessibleRealms.addAll(realmGroup.getRealms());
        }
        return accessibleRealms;
    }

    /**
     * Get the number of realm groups where this account can access
     *
     * @return the number of realm groups
     * @since 15.10
     */
    public int getRealmGroupCount() {
        return this.realmGroups.size();
    }

    /**
     * Get the list of realm group where this account can access
     *
     * @return A list of realm group
     * @since 15.10
     */
    public List<RealmGroupModel> getRealmGroups() {
        return this.realmGroups;
    }

    /**
     * Get the actor associated to this account.
     *
     * @return The actor
     * @see ActorModel
     * @since 15.10
     */
    public ActorModel getActor() {
        return this.actor;
    }

    /**
     * Get the primary email adress of this account.
     *
     * @return The primary email address
     * @since 15.10
     */
    public String getPrimaryEmail() {
        for (final AccountEmailModel aem : this.emails) {
            if (aem.isPrimary()) {
                return aem.getEmail();
            }
        }
        if (this.emails.size() > 0) {
            for (final AccountEmailModel email : this.emails) {
                if (email.isValidated()) {
                    email.setPrimary(true);
                    email.update();
                    return email.getEmail();
                }
            }
            return this.emails.get(0).getEmail();
        } else {
            return null;
        }
    }

    /**
     * Get the list of AccountEmailModel associated with this account.
     *
     * @return The list of AccountEmailModel
     * @since 15.10
     */
    public List<AccountEmailModel> getEmails() {
        return this.emails;
    }

    /**
     * Get the primary email adress of this account.
     *
     * @return The primary email address
     * @since 15.10
     */
    public String getPrimaryPhone() {
        for (final AccountPhoneModel apm : this.phones) {
            if (apm.isPrimary()) {
                return apm.getPhone();
            }
        }
        if (this.phones.size() > 0) {
            for (final AccountPhoneModel apm : this.phones) {
                if (apm.isValidated()) {
                    apm.setPrimary(true);
                    apm.update();
                    return apm.getPhone();
                }
            }
            return this.phones.get(0).getPhone();
        } else {
            return null;
        }
    }

    /**
     * Get the list of AccountEmailModel associated with this account.
     *
     * @return The list of AccountEmailModel
     * @since 15.10
     */
    public List<AccountPhoneModel> getPhones() {
        return this.phones;
    }

    /**
     * Get primary postal address of this account, if none already primary first
     * postal address become primary.
     *
     * @return A {@code PostalAddressModel} which is primary, otherwise, {@code null}
     * @since 15.10
     */
    public AccountPostalAddressModel getPrimaryAddress() {
        for (AccountPostalAddressModel pam : this.addresses) {
            if (pam.isPrimary()) {
                return pam;
            }
        }
        if (this.addresses.size() > 0) {
            final AccountPostalAddressModel pam = this.addresses.get(0);
            pam.setPrimary(true);
            pam.update();
            return pam;
        }
        return null;
    }

    /**
     * Get the list of PostalAddressModel associated with this account
     *
     * @return The list of PostalAddressModel
     * @since 15.10
     */
    public List<AccountPostalAddressModel> getAddresses() {
        return this.addresses;
    }

    /**
     * Get the list of CardModel associated with this account
     *
     * @return The list of CardModel
     */
    public List<CardModel> getCards() {
        return cards;
    }

    /**
     * Get the datetime when this account has successfully loged in.
     *
     * @return The last login datetime
     * @since 15.10
     */
    public DateTime getLastLogin() {
        return this.lastLogin;
    }

    /**
     * Set the last login datetime. It must be superior to the
     * creation datetime.
     *
     * @param lastLogin The last login datetime to set
     * @since 15.10
     */
    public void setLastLogin(DateTime lastLogin) {
        if (this.createDate == null || this.lastLogin.isAfter(this.createDate)) {
            this.lastLogin = lastLogin;
        }
    }

    /**
     * Update the last_login field to the current datetime.
     *
     * @param save If {@code true}, the object will be saved immediately on database.
     * @since 15.10
     */
    public void updateLastLogin(final boolean save) {
        this.lastLogin = DateTime.now();
        if (save) {
            this.save();
        }
    }

    /**
     * Get the datetime when this account has successfully loged in as UTC string
     *
     * @return The last login datetime as UTC string
     * @since 15.10
     */
    public String getLastLoginAsUTCString() {
        return this.lastLogin == null ? null : UTCDateTime.format(this.lastLogin);
    }

    /**
     * The datetime when this account has been created.
     *
     * @return The creation datetime
     * @since 15.10
     */
    public DateTime getCreateDate() {
        return this.createDate;
    }

    /**
     * The datetime when this account has been created as UTC string
     *
     * @return The creation datetime as UTC string
     * @since 15.10
     */
    public String getCreateDateAsUTCString() {
        return UTCDateTime.format(this.createDate);
    }

    /**
     * The preferred language of this account
     *
     * @return The preferred language
     * @since 15.10
     */
    public String getLang() {
        return this.lang;
    }

    /**
     * Set the preferred language for this account
     *
     * @param lang The preferred language
     * @since 15.10
     */
    public void setLang(String lang) {
        this.lang = lang;
    }

    /**
     * The timezone for this account
     *
     * @return The preferred language
     * @since 15.10
     */
    public String getTimezone() {
        return this.timezone;
    }

    /**
     * Set the timezone for this account
     *
     * @param timezone The timezone
     * @since 15.10
     */
    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public FacebookDataModel getFacebookData() {
        return facebookData;
    }

    public void setFacebookData(FacebookDataModel facebookData) {
        this.facebookData = facebookData;
    }

    /**
     * Clean the admin permissions cache for this accounts.
     *
     * @since 15.10
     */
    public void cleanPermissionsCache() {
        play.cache.Cache.remove(String.format("perms.%d._admin", this.id));
    }

    /**
     * Clean the realm permissions cache for this accounts.
     *
     * @param realm The realm to use
     * @since 15.10
     */
    public void cleanPermissionsCache(final RealmModel realm) {
        play.cache.Cache.remove(String.format("perms.%d.realm%d", this.id, realm.getId()));
    }

    /**
     * Check if account can use given admin scope. If multiple scopes are
     * provided, account must have all of them.
     *
     * @param scopes Scopes to check
     * @return {@code true} if account has scopes, otherwise, {@code false}
     * @since 15.10
     */
    public boolean hasPermission(final String... scopes) {
        final List<String> perms = play.cache.Cache.getOrElse(String.format("perms.%d._admin", this.id), () -> {
            final String rawQuery = "SELECT security_permission.id, security_permission.namespace, security_permission.scope FROM security_permission \n" +
                    "JOIN security_role_admin_has_permission ON security_permission.id=security_role_admin_has_permission.security_permission_id\n" +
                    "JOIN security_role_admin_has_account ON security_role_admin_has_account.security_role_admin_id=security_role_admin_has_permission.security_role_admin_id\n" +
                    "GROUP BY security_permission.id";
            final RawSql rawSql = RawSqlBuilder.parse(rawQuery).create();
            return new ArrayList<>(Ebean.find(PermissionModel.class).setRawSql(rawSql).where().eq("security_role_admin_has_account.account_id", id).findSet().stream().map(PermissionModel::getScope).collect(Collectors.toList()));
        }, 120);
        return perms.containsAll(Arrays.asList(scopes));
    }

    /**
     * Check if account can use given realm scope. If multiple scopes are
     * provided, account must have all of them.
     *
     * @param realm  The realm to use
     * @param scopes Scopes to check
     * @return {@code true} if account has scopes, otherwise, {@code false}
     * @since 15.10
     */
    public boolean hasPermission(final RealmModel realm, final String... scopes) {
        final List<String> perms = play.cache.Cache.getOrElse(String.format("perms.%d.realm%d", this.id, realm.getId()), () -> {
            final String rawQuery = "SELECT security_permission.id, security_permission.namespace, security_permission.scope FROM security_permission \n" +
                    "JOIN security_role_realm_has_permission ON security_permission.id=security_role_realm_has_permission.security_permission_id\n" +
                    "JOIN security_role_realm_has_account ON security_role_realm_has_account.security_role_realm_id=security_role_realm_has_permission.security_role_realm_id\n" +
                    "JOIN security_role_realm ON security_role_realm.id = security_role_realm_has_account.security_role_realm_id\n" +
                    "GROUP BY security_permission.id";
            final RawSql rawSql = RawSqlBuilder.parse(rawQuery).create();
            return new ArrayList<>(Ebean.find(PermissionModel.class).setRawSql(rawSql).where().eq("security_role_realm_has_account.account_id", id).eq("security_role_realm.realm_id", realm.getId()).findSet().stream().map(PermissionModel::getScope).collect(Collectors.toList()));
        }, 120);
        return perms.containsAll(Arrays.asList(scopes));
    }

    /**
     * Check if account has, at least, one admin permission.
     *
     * @return {@code true} if account has at least one admin permission, otherwise, {@code false}
     * @since 15.10
     */
    public boolean isGrantedTo() {
        final List<String> perms = play.cache.Cache.getOrElse(String.format("perms.%d._admin", this.id), () -> {
            final String rawQuery = "SELECT security_permission.id, security_permission.namespace, security_permission.scope FROM security_permission \n" +
                    "JOIN security_role_admin_has_permission ON security_permission.id=security_role_admin_has_permission.security_permission_id\n" +
                    "JOIN security_role_admin_has_account ON security_role_admin_has_account.security_role_admin_id=security_role_admin_has_permission.security_role_admin_id\n" +
                    "GROUP BY security_permission.id";
            final RawSql rawSql = RawSqlBuilder.parse(rawQuery).create();
            return new ArrayList<>(Ebean.find(PermissionModel.class).setRawSql(rawSql).where().eq("security_role_admin_has_account.account_id", id).findSet().stream().map(PermissionModel::getScope).collect(Collectors.toList()));
        }, 600);
        return perms.size() > 0;
    }

    /**
     * Check if account has, at least, one realm permission.
     *
     * @param realm The realm to use
     * @return {@code true} if account has at least one realm permission, otherwise, {@code false}
     * @since 15.10
     */
    public boolean isGrantedTo(final RealmModel realm) {
        final List<String> perms = play.cache.Cache.getOrElse(String.format("perms.%d.realm%d", this.id, realm.getId()), () -> {
            final String rawQuery = "SELECT security_permission.id, security_permission.namespace, security_permission.scope FROM security_permission \n" +
                    "JOIN security_role_realm_has_permission ON security_permission.id=security_role_realm_has_permission.security_permission_id\n" +
                    "JOIN security_role_realm_has_account ON security_role_realm_has_account.security_role_realm_id=security_role_realm_has_permission.security_role_realm_id\n" +
                    "JOIN security_role_realm ON security_role_realm.id = security_role_realm_has_account.security_role_realm_id\n" +
                    "GROUP BY security_permission.id";
            final RawSql rawSql = RawSqlBuilder.parse(rawQuery).create();
            return new ArrayList<>(Ebean.find(PermissionModel.class).setRawSql(rawSql).where().eq("security_role_realm_has_account.account_id", id).eq("security_role_realm.realm_id", realm.getId()).findSet().stream().map(PermissionModel::getScope).collect(Collectors.toList()));
        }, 600);
        return perms.size() > 0;
    }

    /**
     * Check if account is enrolled on the realm.
     *
     * @param realm The realm to check
     * @return {@code true} if account is enrolled on the realm, otherwise, {@code false}
     * @since 15.10
     */
    public boolean isEnrolledTo(final RealmModel realm) {
        return this.realms.contains(realm);
    }

    /**
     * Check if account is member of realm group
     *
     * @param realmGroup The realm group to check
     * @return {@code true} if account is member of the realm group, otherwise, {@code false}
     */
    public boolean isMemberOf(final RealmGroupModel realmGroup) {
        return this.realmGroups.contains(realmGroup);
    }

    public boolean addRealm(final RealmModel realm) {
        if(!realms.contains(realm)){
            realms.add(realm);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get account picture URL.
     * @return picture URL as String
     */
    public String getAvatarUrlAsString() {
        return getFacebookData() == null? null : getFacebookData().getPictureURL();
    }


    public boolean isPostLiked(final PostModel post){
        return isPostVoted(post, USER_POSTS_LIKED_CACHE_FORMAT, true);
    }

    public boolean isPostDisliked(final PostModel post){
        return isPostVoted(post, USER_POSTS_DISLIKED_CACHE_FORMAT, false);
    }

    public void cleanLikesCache(final boolean like) {
        if(like){
            play.cache.Cache.remove(String.format(USER_POSTS_LIKED_CACHE_FORMAT, this.id));
        } else {
            play.cache.Cache.remove(String.format(USER_POSTS_DISLIKED_CACHE_FORMAT, this.id));
        }
    }

    private boolean isPostVoted(PostModel post, String format, boolean like) {
        final Set<Long> postIds = Cache.getOrElse(String.format(format, this.id),
                () -> VoteModel.getUserLikedPosts(this, like), LIKES_CACHE_EXPIRATION);
        return postIds.contains(post.getId());
    }

    public List<RealmGroupModel> getRealmGroups(final RealmModel realm) {
        return getRealmGroups().stream().filter(groupModel -> groupModel.getRealms().contains(realm))
                .collect(Collectors.toList());
    }
}
