/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Locale;
import java.util.UUID;

/**
 * RealmDomainWhitelistModel.
 *
 * @author Jean-Pierre Boudic
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "security_realmdomain_whitelist")
public class RealmDomainWhitelistModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<Long, RealmDomainWhitelistModel> find = new Model.Finder<>(RealmDomainWhitelistModel.class);

    /**
     * The unique ID of whitelisted domain.
     */
    @Id
    private Long id;

    /**
     * Unique uid of whitelisted domain.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;

    /**
     * Domain (For Emails)
     */
    @Size(max = 50)
    @Column(name = "domain", nullable = false, unique = false)
    private String domain;

    /**
     * The realm.
     */
    @ManyToOne(targetEntity = RealmModel.class)
    @JoinColumn(name = "realm", referencedColumnName = "id")
    private RealmModel realm;

    /**
     * Build a basic realm domain whitelist entry.
     *
     * @since 15.10
     */
    public RealmDomainWhitelistModel() {
        this.uid = UUID.randomUUID();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.10
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.10
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    /**
     * Get the Email field value.
     *
     * @return Domain Email WhiteListed
     */
    public String getDomain() {
        return this.domain;
    }

    /**
     * Set the Email Domain field.
     *
     * @param domain The Email that must be Whitelisted
     * @since 15.10
     */
    public void setDomain(final String domain) {
        this.domain = domain.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Get the owner of this whitelist domain.
     *
     * @return A {@code RealmModel} instance, or {@code null}
     * @since 15.10
     */
    public RealmModel getRealm() {
        return this.realm;
    }

    /**
     * Set the owner of this whitelist domain.
     *
     * @param realm The realm to use
     * @since 15.10
     */
    public void setRealm(final RealmModel realm) {
        this.realm = realm;
    }
}
