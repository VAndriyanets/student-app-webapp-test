/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;
import com.avaje.ebean.annotation.EnumValue;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.DateTime;
import play.libs.Json;
import play.mvc.PathBindable;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * ArticleModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "store_article")
public class ArticleModel extends Model implements PathBindable<ArticleModel>, SearchableModel {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<Long, ArticleModel> find = new Model.Finder<>(ArticleModel.class);

    /**
     * Get list of all reacheable articles by user.
     * @param account user
     * @return list of ArticleModel
     */
    public static List<ArticleModel> findAllReachableByUser(AccountModel account) {
        List<RealmPageModel> reachableVandors = RealmPageModel.findAllVisibleForUser(account);
        return find.where().in("vendorPage", reachableVandors).eq("status", ArticleStatus.SALES_OPEN)
                .eq("saleOnMobile", true).orderBy().desc("createDate").findList();
    }

    public static List<ArticleModel> findAllVendorArticles(final RealmPageModel vendor) {
        return find.where().eq("vendorPage", vendor).orderBy().desc("createDate").findList();
    }

    public static PagedList<ArticleModel> findLastForVendor(final RealmPageModel vendor, final int pageIndex, final int pageSize) {
        return find.where().eq("vendorPage", vendor).orderBy().desc("createDate").findPagedList(pageIndex, pageSize);
    }

    public static PagedList<ArticleModel> findLastForRealm(final RealmModel realmModel, final int pageIndex, final int pageSize) {
        return find.where().in("vendorPage", realmModel.getPages()).orderBy().desc("createDate").findPagedList(pageIndex, pageSize);
    }

    public static PagedList<ArticleModel> findBestSellingForRealm(final RealmModel realmModel, final int pageIndex, final int pageSize) {
        //TODO make it after implementing buying articles
        return findLastForRealm(realmModel,pageIndex,pageSize);
    }

    /**
     * The unique ID of the actor.
     */
    @Id
    private Long id;

    /**
     * Unique uid of the article.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;


    /**
     * Date of creation.
     */
    @Column(name = "create_date", nullable = false, unique = false)
    private DateTime createDate;

    /**
     * Name of the article.
     */
    @Size(max = 36)
    @Column(name = "name", nullable = false, unique = false)
    private String name;

    /**
     * Description of the article.
     */
    @Size(max = 1000)
    @Column(name = "description", nullable = false, unique = false)
    private String description;

    /**
     * Article logo hosted by S3.
     */
    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REMOVE)
    @Column(name = "logo", nullable = true, unique = false)
    private S3FileModel logo;

    @Column(name = "price")
    private Double price;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Column(name = "page_id")
    private RealmPageModel vendorPage;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "article", cascade = CascadeType.REMOVE)
    private List<ArticleOptionModel> options;

    /**
     * Pricing rules
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "article", cascade = CascadeType.REMOVE)
    private List<ArticlePricingModel> pricingRules;

    /**
     * Posts to this article.
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "article")
    @OrderBy("createDate desc")
    public List<PostModel> posts;

    /**
     * Status: sales open / sales closed / article disable
     */
    @Column(name = "status", nullable = false, unique = false)
    @Enumerated(EnumType.STRING)
    private ArticleStatus status;

    /**
     * Enumeration of article statuses.
     *
     */
    public enum ArticleStatus {

        /**
         * sales open.
         */
        @EnumValue("SALES_OPEN")
        SALES_OPEN,

        /**
         * sales closed.
         */
        @EnumValue("SALES_CLOSED")
        SALES_CLOSED,

        /**
         * article disable.
         */
        @EnumValue("DISABLED")
        DISABLED
    }

    /**
     * Maximum quantity to buy
     */
    @Column(name = "max_quantity", nullable = true, unique = false)
    private Integer maximumQuantity;

    /**
     * Purchase limit per user.
     */
    @Column(name = "limit_per_user", nullable = true, unique = false)
    private Integer limitPerUser;

    /**
     * Is article for sale on web platform
     */
    @Column(name = "sale_on_web", nullable = false, unique = false)
    private boolean saleOnWeb;

    /**
     * Is article for sale on mobile platform
     */
    @Column(name = "sale_on_mobile", nullable = false, unique = false)
    private boolean saleOnMobile;

    /**
     * Is article for ticketing sale
     */
    @Column(name = "ticketing", nullable = false, unique = false)
    private boolean ticketing;

    /**
     * Build a basic article.
     *
     * @since 15.10
     */
    public ArticleModel() {
        this.uid = UUID.randomUUID();
        this.status = ArticleStatus.DISABLED;
        this.createDate = new DateTime();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id == null ? 0 : this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.10
     */
    public UUID getUid() {
        return this.uid;
    }

    @Override
    public ObjectNode getIndexSource() {
        final ObjectNode esInfo = Json.newObject();
        esInfo.put("name", getName());
        esInfo.put("description", getDescription());
        final ArrayNode esUserInfoRealm = esInfo.putArray("_realm");
        for (final RealmModel rm : vendorPage.getRealms()) {
            esUserInfoRealm.add(rm.getId());
        }
        return esInfo;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.10
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    @Override
    public String getSearchType() {
        return SearchRequestType.ARTICLE.getTypes()[0];
    }

    /**
     * Get the name of this article.
     *
     * @return The article's name
     * @since 15.10
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name of this article.
     *
     * @param name The name to use
     * @since 15.10
     */
    public void setName(final String name) {
        this.name = WordUtils.capitalizeFully(name.trim());
    }

    /**
     * Get the description of this article.
     *
     * @return The article's description
     * @since 15.10
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Set the description of this article.
     *
     * @param description The description to use
     * @since 15.10
     */
    public void setDescription(final String description) {
        this.description = description.trim();
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code ArticleModel} in case of success
     */
    @Override
    public ArticleModel bind(final String key, final String value) {
        final ArticleModel article = ArticleModel.find.where().like("uid", value).findUnique();
        if (article == null) {
            throw new IllegalArgumentException("404");
        }
        return article;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(final String key) {
        return this.uid.toString();
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return "function(k,v) {\n" +
                "    return v.uid;" +
                "}";
    }


    public RealmPageModel getVendorPage() {
        return vendorPage;
    }

    public void setVendorPage(RealmPageModel vendorPage) {
        this.vendorPage = vendorPage;
    }

    public S3FileModel getLogo() {
        return logo;
    }

    public void setLogo(S3FileModel logo) {
        this.logo = logo;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public DateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(DateTime createDate) {
        this.createDate = createDate;
    }

    public List<ArticleOptionModel> getOptions() {
        return options;
    }

    public void setOptions(List<ArticleOptionModel> options) {
        this.options = options;
    }

    public ArticleStatus getStatus() {
        return status;
    }

    public void setStatus(ArticleStatus status) {
        this.status = status;
    }

    public List<ArticlePricingModel> getPricingRules() {
        return pricingRules;
    }

    public void setPricingRules(List<ArticlePricingModel> pricingRules) {
        this.pricingRules = pricingRules;
    }

    public boolean isOnSale() {
        return status == ArticleStatus.SALES_OPEN;
    }

    public boolean isSaleOnWeb() {
        return saleOnWeb;
    }

    public void setSaleOnWeb(boolean saleOnWeb) {
        this.saleOnWeb = saleOnWeb;
    }

    public boolean isSaleOnMobile() {
        return saleOnMobile;
    }

    public void setSaleOnMobile(boolean saleOnMobile) {
        this.saleOnMobile = saleOnMobile;
    }

    public boolean isTicketing() {
        return ticketing;
    }

    public void setTicketing(boolean ticketing) {
        this.ticketing = ticketing;
    }

    public Integer getMaximumQuantity() {
        return maximumQuantity;
    }

    public void setMaximumQuantity(Integer maximumQuantity) {
        this.maximumQuantity = maximumQuantity;
    }

    public Integer getLimitPerUser() {
        return limitPerUser;
    }

    public void setLimitPerUser(Integer limitPerUser) {
        this.limitPerUser = limitPerUser;
    }

    /**
     * get Quantity of sold aticles
     * @return number of sold items
     */
    public int getQuantitySold() {
        return (int)(Math.random()* 5); // todo implement after buy-article task
    }

    /**
     * get calculated price in respect to dynamic article pricing
     * @return dynamically calculated price
     */
    public Double getPriceCalculated() {
        return price; // todo implement after create-article task
    }

    /**
     * get facebook friends bought this Article.
     * @return list of friends as List<AccountModel>
     */
    public List<AccountModel> getFacebookFriendsBoughtArticle() {
        // todo should be filter of friends-accounts, bought article
        List<FacebookDataModel> users = FacebookDataModel.find.all();
        List<AccountModel> accounts = new ArrayList<>(users.size());
        for(FacebookDataModel user : users) {
            accounts.add(user.getAccount());
        }

        return accounts;
    }

    public static ArticleModel findByUid(String uid) {
        return find.where().like("uid", uid).findUnique();
    }
}
