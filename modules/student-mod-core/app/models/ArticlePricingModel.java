/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;

import javax.persistence.*;

/**
 * Article Pricing Rule.
 *
 */
@Entity
@Table(name = "article_pricing")
public class ArticlePricingModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, ArticlePricingModel> find = new Finder<>(ArticlePricingModel.class);

    /**
     * The unique ID .
     */
    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private ArticleModel article;

    /**
     * Pricing Type
     */
    @Column(name = "pricing_type", nullable = false, unique = false)
    @Enumerated(EnumType.STRING)
    private PricingType pricingType;

    /**
     * Article should be owned to apply this rule on pricing
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owned_article")
    private ArticleModel ownedArticle;

    /**
     * Article should be owned to apply this rule on pricing
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_of_group")
    private RealmGroupModel group;

    @Column(name = "price")
    private Double price;

    /**
     * Get the ID of the current entry.
     *
     */
    public Long getId() {
        return this.id;
    }

    public ArticleModel getArticle() {
        return article;
    }

    public void setArticle(ArticleModel article) {
        this.article = article;
    }

    public PricingType getPricingType() {
        return pricingType;
    }

    public void setPricingType(PricingType pricingType) {
        this.pricingType = pricingType;
    }

    public ArticleModel getOwnedArticle() {
        return ownedArticle;
    }

    public void setOwnedArticle(ArticleModel ownedArticle) {
        this.ownedArticle = ownedArticle;
    }

    public RealmGroupModel getGroup() {
        return group;
    }

    public void setGroup(RealmGroupModel group) {
        this.group = group;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * Enumeration of Pricing types.
     *
     */
    public enum PricingType {

        /**
         * own an article.
         */
        @EnumValue("OWN_AN_ARTICLE")
        OWN_AN_ARTICLE,

        /**
         * member of group.
         */
        @EnumValue("MEMBER_OF_GROUP")
        MEMBER_OF_GROUP
    }
}
