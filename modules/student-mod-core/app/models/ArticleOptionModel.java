/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.EnumValue;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Article Option.
 *
 */
@Entity
@Table(name = "article_options")
public class ArticleOptionModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, ArticleOptionModel> find = new Finder<>(ArticleOptionModel.class);

    /**
     * The unique ID .
     */
    @Id
    private Long id;

    /**
     * Option name
     */
    @Column(name = "name", nullable = false, unique = false)
    private String name;

    /**
     * Option argument
     */
    @Size(max = 1000)
    @Column(name = "argument", nullable = true, unique = false)
    private String argument;

    /**
     * Option mandatory flag
     */
    @Column(name = "mandatory", nullable = false, unique = false)
    private boolean mandatory;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Column(name = "article")
    private ArticleModel article;

    /**
     * Option Type
     */
    @Column(name = "option_type", nullable = false, unique = false)
    @Enumerated(EnumType.STRING)
    private OptionType optionType;

    /**
     * Get the ID of the current entry.
     *
     */
    public Long getId() {
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArticleModel getArticle() {
        return article;
    }

    public void setArticle(ArticleModel article) {
        this.article = article;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String description) {
        this.argument = description;
    }

    public OptionType getOptionType() {
        return optionType;
    }

    public void setOptionType(OptionType optionType) {
        this.optionType = optionType;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    /**
     * Get list of selectable values.
     * @return list of selectable values as String array
     */
    public String[] getValues() {
        return argument == null? null : argument.split(",");
    }

    /**
     * Set list of selectable values.
     */
    public void setValues(String[] values) {
        this.argument = StringUtils.join(values, ",");
    }
    /**
     * Enumeration of Option types.
     *
     */
    public enum OptionType {

        /**
         * select on of value.
         */
        @EnumValue("SELECT")
        SELECT,

        /**
         * select many values.
         */
        @EnumValue("MULTISELECT")
        MULTISELECT,

        /**
         * text input.
         */
        @EnumValue("TEXT")
        TEXT,

        /**
         * logical switch.
         */
        @EnumValue("CHECKBOX")
        CHECKBOX
    }

}
