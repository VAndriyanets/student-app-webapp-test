package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * CardTypeModel.
 *
 * @author vitaly.andriyanets
 */
@Entity
@Table(name = "card_type")
public class CardTypeModel {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<Long, CardTypeModel> find = new Model.Finder<>(CardTypeModel.class);

    /**
     * The unique ID of Card types.
     */
    @Id
    private Long id;

    /**
     * Type name.
     */
    @Size(max = 35)
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    /**
     * Type validation pattern.
     */
    @Size(max = 128)
    @Column(name = "validation_pattern", nullable = true, unique = false)
    private String validationPattern;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValidationPattern() {
        return validationPattern;
    }

    public void setValidationPattern(String validationPattern) {
        this.validationPattern = validationPattern;
    }
}
