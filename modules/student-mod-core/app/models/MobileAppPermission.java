package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * @author Andrey Sokolov
 */
@Entity
@Table(name = "mobile_app_permission")
public class MobileAppPermission extends Model {

    /**
     * The unique ID of this entry.
     */
    @Id
    public Long id;

    /**
     * Scope of the permission.
     */
    @Size(max = 50)
    @Column(name = "scope", nullable = false, unique = false)
    public String scope;

}
