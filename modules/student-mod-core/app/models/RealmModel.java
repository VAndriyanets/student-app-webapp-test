/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import org.joda.time.DateTime;
import play.mvc.PathBindable;
import toolbox.UTCDateTime;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * RealmModel.
 *
 * @author Thibault Meyer
 * @author Jean-Pierre Boudic
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "realm")
public class RealmModel extends Model implements PathBindable<RealmModel> {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<Long, RealmModel> find = new Model.Finder<>(RealmModel.class);

    /**
     * The unique ID of this entry.
     */
    @Id
    private Long id;

    /**
     * Unique uid of the realm.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;

    /**
     * Unique slug of the realm.
     */
    @Size(max = 36)
    @Column(name = "slug", nullable = false, unique = true)
    private String slug;

    /**
     * Name of the realm.
     */
    @Size(max = 36)
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    /**
     * Logo of the realm hosted by S3.
     */
    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REMOVE)
    @Column(name = "logo", nullable = true, unique = false)
    private S3FileModel logo;

    /**
     * Banner of the realm hosted by S3.
     */
    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REMOVE)
    @Column(name = "banner", nullable = true, unique = false)
    private S3FileModel banner;

    /**
     * User account granted to this realm.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "realm_has_account")
    @OrderBy("id asc")
    private List<AccountModel> accounts;

    /**
     * Group associated to this realm
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "realm_has_group")
    private List<RealmGroupModel> groups;

    /**
     * Page associated to this realm
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "realm_has_page")
    private List<RealmPageModel> pages;

    /**
     * Email Domain whitelisted to this realm.
     */
    @OneToMany(mappedBy = "realm", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @OrderBy("domain asc")
    private List<RealmDomainWhitelistModel> domainsWhitelist;

    /**
     * Roles existing to this realm.
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "security_role_realm", joinColumns = {@JoinColumn(name = "realm_id")})
    private List<RoleRealmModel> roles;

    /**
     * Posts to this realm.
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "realm")
    @OrderBy("createDate desc")
    public List<PostModel> posts;

    /**
     * Date of creation.
     */
    @Column(name = "create_date", nullable = false, unique = false)
    private DateTime createDate;

    /**
     * Build a basic realm.
     *
     * @since 15.10
     */
    public RealmModel() {
        this.uid = UUID.randomUUID();
        this.createDate = DateTime.now();
        this.accounts = new ArrayList<>();
        this.groups = new ArrayList<>();
        this.pages = new ArrayList<>();
        this.domainsWhitelist = new ArrayList<>();
        this.roles = new ArrayList<>();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.10
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.10
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    /**
     * Get the slug of this realm. A slug is a little string used to access
     * the realm from the URL.
     *
     * @return The slug as String
     * @since 15.10
     */
    public String getSlug() {
        return this.slug;
    }

    /**
     * Set the slug of this realm. A slug is a little string used to access
     * the realm from the URL.
     *
     * @param slug The slug to use
     * @since 15.10
     */
    public void setSlug(final String slug) {
        this.slug = slug.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Get the name of this realm.
     *
     * @return The realm's name
     * @since 15.10
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name of this realm.
     *
     * @param name The name to use
     * @since 15.10
     */
    public void setName(final String name) {
        this.name = name.trim();
    }

    /**
     * Get the current set logo.
     *
     * @return The realm's logo as {@code S3FileModel}
     * @see S3FileModel
     * @since 15.10
     */
    public S3FileModel getLogo() {
        return this.logo;
    }

    /**
     * Set the logo. If a logo is already set, it will be deleted.
     *
     * @param logo The logo to set
     * @since 15.10
     */
    public void setLogo(S3FileModel logo) {
        if (this.logo != null) {
            this.deleteLogo();
        }
        this.logo = logo;
    }

    /**
     * Delete the logo. This method will call {@link RealmModel#update()}
     * automatically.
     *
     * @since 15.10
     */
    public void deleteLogo() {
        if (this.logo != null) {
            final S3FileModel handle = this.logo;
            this.logo = null;
            this.update();
            handle.delete();
        }
    }

    /**
     * Get the current set banner.
     *
     * @return The realm's banner as {@code S3FileModel}
     * @see S3FileModel
     * @since 15.10
     */
    public S3FileModel getBanner() {
        return this.banner;
    }

    /**
     * Set the banner. If a banner is already set, it will be deleted.
     *
     * @param banner The banner to set
     * @since 15.10
     */
    public void setBanner(S3FileModel banner) {
        if (this.banner != null) {
            this.deleteBanner();
        }
        this.banner = banner;
    }

    /**
     * Delete the logo. This method will call {@link RealmModel#update()}
     * automatically.
     *
     * @since 15.10
     */
    public void deleteBanner() {
        if (this.banner != null) {
            final S3FileModel handle = this.banner;
            this.banner = null;
            this.update();
            handle.delete();
        }
    }

    /**
     * Get the Domain Emails Whitelisted registered to this realm.
     *
     * @return A list of email domains
     * @since 15.10
     */
    public List<RealmDomainWhitelistModel> getWhitelistDomains() {
        return this.domainsWhitelist;
    }

    /**
     * Delete domain from the whitelist.
     *
     * @param domain The Domain to remove
     */
    public void deleteWhitelistDomains(RealmDomainWhitelistModel domain) {
        RealmDomainWhitelistModel delDomain = null;
        for (final RealmDomainWhitelistModel wlDomain : this.domainsWhitelist) {
            if (wlDomain.getDomain().equalsIgnoreCase(domain.getDomain())) {
                delDomain = wlDomain;
            }
        }
        if (delDomain != null) {
            this.domainsWhitelist.remove(delDomain);
        }
    }

    /**
     * Get the roles registered to this realm.
     *
     * @return A list of Roles (RealmRole)
     * @since 15.10
     */
    public List<RoleRealmModel> getRoles() {
        return this.roles;
    }

    /**
     * Get the accounts registered to this realm.
     *
     * @return A list of Accounts
     * @since 15.10
     */
    public List<AccountModel> getAccounts() {
        return this.accounts;
    }

    /**
     * Add account to the list of registered accounts.
     *
     * @param account The account to add
     */
    public void addAccount(final AccountModel account) {
        this.accounts.add(account);
    }

    /**
     * Remove account to list of registered accounts.
     *
     * @param account The account to remove
     * @return {@code true} if remove success, otherwise, {@code false}
     * @since 15.10
     */
    public boolean removeAccount(final AccountModel account) {
        return this.accounts.remove(account);
    }

    /**
     * Get groups associated to this realm
     *
     * @return A list of Groups
     * @since 15.10
     */
    public List<RealmGroupModel> getGroups() {
        return this.groups;
    }

    /**
     * Add group to the list of associated groups.
     *
     * @param group The group to add
     */
    public void addGroup(final RealmGroupModel group) {
        this.groups.add(group);
    }

    /**
     * Remove group to list of associated groups.
     *
     * @param group The group to remove
     * @return {@code true} if remove success, otherwise, {@code false}
     * @since 15.10
     */
    public boolean removeGroup(final RealmGroupModel group) {
        return this.groups.remove(group);
    }

    /**
     * Get pages associated to this realm
     *
     * @return A list of Pages
     * @since 15.12
     */
    public List<RealmPageModel> getPages() {
        return this.pages;
    }

    /**
     * Add group to the list of associated pages.
     *
     * @param page The group to add
     * @since 15.12
     */
    public void addPage(final RealmPageModel page) {
        this.pages.add(page);
    }

    /**
     * Remove group to list of associated pages.
     *
     * @param page The group to remove
     * @return {@code true} if remove success, otherwise, {@code false}
     * @since 15.12
     */
    public boolean removePage(final RealmPageModel page) {
        return this.pages.remove(page);
    }

    /**
     * The datetime when this realm has been created.
     *
     * @return The creation datetime
     * @since 15.10
     */
    public DateTime getCreateDate() {
        return this.createDate;
    }

    /**
     * The datetime when this realm has been created as UTC string
     *
     * @return The creation datetime as UTC string
     * @since 15.10
     */
    public String getCreateDateAsUTCString() {
        return UTCDateTime.format(this.createDate);
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code RealmModel} in case of success
     */
    @Override
    public RealmModel bind(String key, String value) {
        final RealmModel realm = RealmModel.find.where().like("slug", value).findUnique();
        if (realm == null) {
            throw new IllegalArgumentException("404");
        }
        return realm;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(String key) {
        return this.slug;
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return "function(k,v) {\n" +
                "    return v.get;" +
                "}";
    }

    public static RealmModel findByUid(String uid) {
        return find.where().like("uid", uid).findUnique();
    }
}
