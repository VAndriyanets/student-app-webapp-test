package models;

import java.util.List;

/**
 * @author Andrey Sokolov
 */
public interface HasAccountsList {
    List<AccountModel> getAccounts();

    void addAccount(AccountModel account);

    void removeAccount(AccountModel account);
}
