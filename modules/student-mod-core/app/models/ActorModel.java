/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Locale;
import java.util.UUID;

/**
 * ActorModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "actor")
public class ActorModel extends Model implements PathBindable<ActorModel> {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, ActorModel> find = new Finder<>(ActorModel.class);

    /**
     * The unique ID of the actor.
     */
    @Id
    private Long id;

    /**
     * Unique slug of the actor.
     */
    @Size(max = 36)
    @Column(name = "slug", nullable = false, unique = true)
    private String slug;

    /**
     * Link to the actor profile.
     */
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Column(name = "profile", nullable = false, unique = false)
    private ActorProfileModel profile;

    /**
     * Build a basic actor.
     *
     * @since 15.10
     */
    public ActorModel() {
        this.slug = UUID.randomUUID().toString().replace("-", "");
        this.profile = new ActorProfileModel();
    }

    /**
     * Save the current entry on database.
     */
    @Override
    public void save() {
        this.profile.save();
        super.save();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id == null ? 0 : this.id;
    }

    /**
     * Get the slug of the current entry.
     *
     * @return The slug
     * @since 15.10
     */
    public String getSlug() {
        return this.slug;
    }

    /**
     * Get the slug of the current entry. Only a-z 0-9 "dot" and
     * "dash" caracters are allowed.
     *
     * @param slug The slug to use
     * @since 15.10
     */
    public void setSlug(final String slug) {
        this.slug = slug.replaceAll("[^a-z0-9.-]", "").toLowerCase(Locale.ENGLISH);
        if (this.slug.length() > 36) {
            this.slug = this.slug.substring(0, 36);
        }
    }

    /**
     * Get the actor's profile.
     *
     * @return The actor's profile
     */
    public ActorProfileModel getProfile() {
        return this.profile;
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code ActorModel} in case of success
     */
    @Override
    public ActorModel bind(String key, String value) {
        final ActorModel actor = ActorModel.find.where().like("slug", value).findUnique();
        if (actor == null) {
            throw new IllegalArgumentException("404");
        }
        return actor;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(String s) {
        return this.slug;
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return "function(k,v) {\n" +
                "    return v.slug;" +
                "}";
    }
}
