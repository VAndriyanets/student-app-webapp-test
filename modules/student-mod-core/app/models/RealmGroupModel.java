/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.joda.time.DateTime;
import play.libs.Json;
import play.mvc.PathBindable;
import toolbox.UTCDateTime;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * GroupModel.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "realm_groups")
public class RealmGroupModel extends Model implements PathBindable<RealmGroupModel>, HasAccountsList, SearchableModel {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<Long, RealmGroupModel> find = new Model.Finder<>(RealmGroupModel.class);

    public static RealmGroupModel findByUid(String uid){
        return find.where().like("uid", uid).findUnique();
    }
    /**
     * The unique ID of this entry.
     */
    @Id
    private Long id;

    /**
     * Unique uid of the group.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;

    /**
     * Unique slug of the group.
     */
    @Size(max = 36)
    @Column(name = "slug", nullable = false, unique = true)
    private String slug;

    /**
     * Name of the group.
     */
    @Size(max = 36)
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    /**
     * Logo of the group hosted by S3
     */
    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REMOVE)
    @Column(name = "logo", nullable = true, unique = false)
    private S3FileModel logo;

    /**
     * Banner of the group hosted by S3
     */
    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REMOVE)
    @Column(name = "banner", nullable = true, unique = false)
    private S3FileModel banner;

    /**
     * List of realm where this group can be access.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "realm_has_group")
    private List<RealmModel> realms;

    /**
     * User account granted to this group
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "realm_group_has_account")
    @OrderBy("id asc")
    private List<AccountModel> accounts;

    /**
     * Role existing to this group.
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "group")
    private List<RoleRealmGroupModel> roles;

    /**
     * Posts to this group.
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "group")
    @OrderBy("createDate desc")
    private List<PostModel> posts;

    /**
     * Date of creation.
     */
    @Column(name = "create_date", nullable = false, unique = false)
    private DateTime createDate;

    /**
     * Build a basic group.
     *
     * @since 15.10
     */
    public RealmGroupModel() {
        this.uid = UUID.randomUUID();
        this.createDate = DateTime.now();
        this.realms = new ArrayList<>();
        this.accounts = new ArrayList<>();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.10
     */
    public UUID getUid() {
        return this.uid;
    }

    @Override
    public ObjectNode getIndexSource() {
        final ObjectNode esUserInfo = Json.newObject();
        esUserInfo.put("name", getName());
        esUserInfo.put("logo", getLogo() != null ? getLogo().getIdAsString() : null);
        esUserInfo.put("slug", getSlug());
        final ArrayNode esUserInfoRealm = esUserInfo.putArray("_realm");
        for (final RealmModel rm : getRealms()) {
            esUserInfoRealm.add(rm.getId());
        }
        return esUserInfo;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.10
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    @Override
    public String getSearchType() {
        return SearchRequestType.GROUP.getTypes()[0];
    }

    /**
     * Get the slug of this group. A slug is a little string used to access
     * the group from the URL.
     *
     * @return The slug as String
     * @since 15.10
     */
    public String getSlug() {
        return this.slug;
    }

    /**
     * Set the slug of this group. A slug is a little string used to access
     * the group from the URL.
     *
     * @param slug The slug to use
     * @since 15.10
     */
    public void setSlug(final String slug) {
        this.slug = slug.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Get the name of this group.
     *
     * @return The group's name
     * @since 15.10
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name of this group.
     *
     * @param name The name to use
     * @since 15.10
     */
    public void setName(final String name) {
        this.name = name.trim();
    }

    /**
     * Get the current set logo.
     *
     * @return The group's logo as {@code S3FileModel}
     * @see S3FileModel
     * @since 15.10
     */
    public S3FileModel getLogo() {
        return this.logo;
    }

    /**
     * Set the logo. If a logo is already set, it will be deleted.
     *
     * @param logo The logo to set
     * @since 15.10
     */
    public void setLogo(S3FileModel logo) {
        if (this.logo != null) {
            this.deleteLogo();
        }
        this.logo = logo;
    }

    /**
     * Delete the logo. This method will call {@link RealmGroupModel#update()}
     * automatically.
     *
     * @since 15.10
     */
    public void deleteLogo() {
        if (this.logo != null) {
            final S3FileModel handle = this.logo;
            this.logo = null;
            this.update();
            handle.delete();
        }
    }

    /**
     * Get the current set banner.
     *
     * @return The group's banner as {@code S3FileModel}
     * @see S3FileModel
     * @since 15.10
     */
    public S3FileModel getBanner() {
        return this.banner;
    }

    /**
     * Set the banner. If a banner is already set, it will be deleted.
     *
     * @param banner The banner to set
     * @since 15.10
     */
    public void setBanner(S3FileModel banner) {
        if (this.banner != null) {
            this.deleteBanner();
        }
        this.banner = banner;
    }

    /**
     * Delete the logo. This method will call {@link RealmGroupModel#update()}
     * automatically.
     *
     * @since 15.10
     */
    public void deleteBanner() {
        if (this.banner != null) {
            final S3FileModel handle = this.banner;
            this.banner = null;
            this.update();
            handle.delete();
        }
    }

    /**
     * Get the roles registered to this group.
     *
     * @return A list of Roles (GroupRole)
     * @since 15.10
     */
    public List<RoleRealmGroupModel> getRoles() {
        return this.roles;
    }

    /**
     * Get realms associated to this group.
     *
     * @return A list of Realms
     * @since 15.10
     */
    public List<RealmModel> getRealms() {
        return this.realms;
    }

    /**
     * Get the accounts registered to this group.
     *
     * @return A list of Accounts
     * @since 15.10
     */
    @Override
    public List<AccountModel> getAccounts() {
        return this.accounts;
    }

    /**
     * Add account to the list of registered accounts.
     *
     * @param account The account to add
     * @since 15.10
     */
    @Override
    public void addAccount(final AccountModel account) {
        if (!this.accounts.contains(account)) {
            this.accounts.add(account);
        }
    }

    /**
     * Remove account to list of registered accounts.
     *
     * @param account The account to remove
     * @return {@code true} if remove success, otherwise, {@code false}
     * @since 15.10
     */
    @Override
    public void removeAccount(final AccountModel account) {
        this.accounts.remove(account);
    }

    /**
     * The datetime when this group has been created.
     *
     * @return The creation datetime
     * @since 15.10
     */
    public DateTime getCreateDate() {
        return this.createDate;
    }

    /**
     * The datetime when this group has been created as UTC string
     *
     * @return The creation datetime as UTC string
     * @since 15.10
     */
    public String getCreateDateAsUTCString() {
        return UTCDateTime.format(this.createDate);
    }

    public List<PostModel> getPosts() {
        return posts;
    }

    /**
     * Checks for administrative account
     * @param account  - The account to check
     * @return true - if account is administrator of this group, false - otherwise
     */
    public boolean hasAdministrator(AccountModel account) {
        for (RoleRealmGroupModel role : getRoles()) {
            if (role.getName().startsWith(RoleRealmGroupModel.ADMIN_ROLE_NAME_PREFIX)) {
                List<AccountModel> roleAccounts = role.getAccounts();
                for(AccountModel roleAccount : roleAccounts) {
                    if (roleAccount.getId() == account.getId()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code RealmModel} in case of success
     */
    @Override
    public RealmGroupModel bind(String key, String value) {
        final RealmGroupModel group = RealmGroupModel.find.where().like("slug", value).findUnique();
        if (group == null) {
            throw new IllegalArgumentException("404");
        }
        return group;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(String key) {
        return this.slug;
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return "function(k,v) {\n" +
                "    return v.get;" +
                "}";
    }
}
