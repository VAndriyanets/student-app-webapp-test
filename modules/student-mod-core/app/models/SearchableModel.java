package models;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Andrey Sokolov
 */
public interface SearchableModel {
    ObjectNode getIndexSource();
    String getUidAsString();
    String getSearchType();
}
