/*
 * Copyright (C) 2014 - 2016 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import play.cache.Cache;
import play.i18n.Messages;
import toolbox.UTCDateTime;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * PostModel.
 */
@Entity
@Table(name = "posts")
public class PostModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, PostModel> find = new Finder<>(PostModel.class);

    public static final int LIKES_CACHE_EXPIRATION = 120;
    public static final int COMMENTS_CACHE_EXPIRATION = 120;
    public static final String COMMENTS_CACHE_FORMAT = "post.%d.comments";
    private static final String DELETE_COMMENTS_CACHE = "post.%d.commentsCanBeDelBy.%d";
    private static final int DELETE_COMMENTS_CACHE_EXPIRATION = 600;
    /**
     * The unique ID of this post.
     */
    @Id
    private Long id;
    /**
     * Unique uid of the post.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;
    /**
     * Text of post.
     */
    @Size(max = 10000)
    @Column(name = "post_text", nullable = true, unique = false)
    private String text;
    /**
     * post's pictures
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "post_has_picture")
    private List<S3FileModel> pictures;
    /**
     * post's author
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "account_id", nullable = false, updatable = false)
    private AccountModel author;
    /**
     * Date of creation.
     */
    @Column(name = "create_date", nullable = false, unique = false)
    private DateTime createDate;


    /**
     * Group posted to, if post related to group.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id", nullable = true, updatable = false)
    public RealmGroupModel group;

    /**
     * Realm posted to, if post related to realm.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "realm_id", nullable = true, updatable = false)
    public RealmModel realm;

    /**
     * Realm posted to, if post related to realm.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_id", nullable = true, updatable = false)
    public ArticleModel article;


    /**
     * Build a basic post.
     */
    public PostModel() {
        this.uid = UUID.randomUUID();
        this.createDate = DateTime.now();
    }

    public static List<PostModel> findRealmPosts(final RealmModel realm, List<RealmGroupModel> realmGroups) {
        if (realmGroups != null && !realmGroups.isEmpty()) {
            final String rawQuery = "SELECT p.id as id," +
                    "    p.uid as uid," +
                    "    p.post_text as text, " +
                    "    p.create_date as createDate " +
                    "FROM posts p " +
                    "inner join account a on a.id = p.account_id " +
                    "inner join security_role_page_has_account grha on grha.account_id = a.id " +
                    "inner join security_role_page gr on gr.id = grha.security_role_page_id " +
                    "where p.group_id IN (:groupIds) or p.realm_id = :realmId order by p.create_date desc";
            final RawSql rawSql = RawSqlBuilder.parse(rawQuery).create();
            final List<Long> groupIds = realmGroups.stream().map(RealmGroupModel::getId).collect(Collectors.toList());
            return find.setRawSql(rawSql).setParameter("groupIds", groupIds).setParameter("realmId", realm.getId()).findList();
        } else {
            return realm.posts;
        }
    }

    public static List<PostModel> findVendorPostsInGroups(List<RealmGroupModel> realmGroups) {
        if (realmGroups != null && !realmGroups.isEmpty()) {
            final String rawQuery = "SELECT p.id as id," +
                    "    p.uid as uid," +
                    "    p.post_text as text, " +
                    "    p.create_date as createDate " +
                    "FROM posts p " +
                    "inner join account a on a.id = p.account_id " +
                    "inner join security_role_page_has_account grha on grha.account_id = a.id " +
                    "inner join security_role_page gr on gr.id = grha.security_role_page_id " +
                    "where p.group_id IN (:groupIds) order by p.create_date desc";
            final RawSql rawSql = RawSqlBuilder.parse(rawQuery).create();
            final List<Long> groupIds = realmGroups.stream().map(RealmGroupModel::getId).collect(Collectors.toList());
            return find.setRawSql(rawSql).setParameter("groupIds", groupIds).findList();
        }
        return Collections.emptyList();
    }


    public static PostModel findByUid(String uid){
        return find.where().like("uid", uid).findUnique();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    public List<S3FileModel> getPictures() {
        return pictures;
    }

    public void addPicture(S3FileModel picture) {
        if (this.pictures == null) {
            this.pictures = new ArrayList<>(20);
        }
        this.pictures.add(picture);
    }

    public DateTime getCreateDate() {
        return this.createDate;
    }

    @Override
    public void delete() {
        List<S3FileModel> oldPictures = getPictures();
        super.delete();

        for (S3FileModel picture : oldPictures) {
            picture.delete();
        }
    }

    /**
     * Forming human-readable representation of post creation elapsed time
     *
     * @return localized String like 'less than minute, xx minutes ago, xx hours ago, xx days ago'
     */
    public String getReadableElapsedTime() {
        return UTCDateTime.getReadableElapsedTime(createDate);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public AccountModel getAuthor() {
        return author;
    }

    public void setAuthor(AccountModel author) {
        this.author = author;
    }

    public int getLikes() {
        return getVotes("post.%d.likes", true);
    }

    public int getDislikes() {
        return getVotes("post.%d.dislikes", false);
    }

    private int getVotes(String cacheFormat, boolean like) {
        return play.cache.Cache.getOrElse(String.format(cacheFormat, this.id),
                () -> VoteModel.getLikedUsersNumber(this, like), LIKES_CACHE_EXPIRATION);
    }

    public boolean canDeleteAllComments(AccountModel accountModel){
        return play.cache.Cache.getOrElse(String.format(DELETE_COMMENTS_CACHE, this.id, accountModel.getId()),
                () -> this.group != null && this.group.hasAdministrator(accountModel), DELETE_COMMENTS_CACHE_EXPIRATION);
    }

    public boolean canVote(AccountModel account) {
        final RealmGroupModel group = this.group;
        if (group != null) {
            return group.getAccounts().contains(account);
        } else {
            final ArticleModel article = this.article;
            if(article != null){
                for(RealmModel realmModel : article.getVendorPage().getRealms()){
                    if(account.isEnrolledTo(realmModel)) {
                        return true;
                    }
                }
            } else {
                final RealmModel realm = this.realm;
                if(realm != null) {
                    return account.isEnrolledTo(realm);
                }
            }
        }
        return false;
    }

    private RealmModel findBoundRealm() {
        String rawQuery;
        RawSql rawSql;
        rawQuery = "SELECT r.id as id," +
                "    r.uid as uid," +
                "    r.name as name, " +
                "    r.create_date as createDate " +
                "FROM realm_has_posts rp " +
                "inner join realm r on r.id = rp.realm_id " +
                "where rp.post_id = :postId";
        rawSql = RawSqlBuilder.parse(rawQuery).create();
        return Ebean.find(RealmModel.class).setRawSql(rawSql).setParameter("postId", this.id).findUnique();
    }

    private ArticleModel findBoundArticle() {
        String rawQuery;
        RawSql rawSql;
        rawQuery = "SELECT a.id as id," +
                "    a.uid as uid," +
                "    a.name as name, " +
                "    a.create_date as createDate " +
                "FROM article_has_posts ap " +
                "inner join store_article a on a.id = ap.article_id " +
                "where ap.post_id = :postId";
        rawSql = RawSqlBuilder.parse(rawQuery).create();
        return Ebean.find(ArticleModel.class).setRawSql(rawSql).setParameter("postId", this.id).findUnique();
    }

    private RealmGroupModel findBoundRealmGroup() {
        String rawQuery = "SELECT gr.id as id," +
                "    gr.uid as uid," +
                "    gr.name as name, " +
                "    gr.create_date as createDate " +
                "FROM group_has_posts gp " +
                "inner join realm_groups gr on gr.id = gp.group_id " +
                "where gp.post_id = :postId";
        RawSql rawSql = RawSqlBuilder.parse(rawQuery).create();
        return Ebean.find(RealmGroupModel.class).setRawSql(rawSql).setParameter("postId", this.id).findUnique();
    }

    public int countComments(){
        return Cache.getOrElse(String.format(COMMENTS_CACHE_FORMAT, this.id), () -> CommentModel.count(this), COMMENTS_CACHE_EXPIRATION);
    }

    public void clearCommentsCache(){
        Cache.remove(String.format(COMMENTS_CACHE_FORMAT, this.id));
    }
}
