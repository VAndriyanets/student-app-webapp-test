/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import play.mvc.PathBindable;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * RoleGroupModel.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "security_role_realm_group")
public class RoleRealmGroupModel extends RoleModel implements PathBindable<RoleRealmGroupModel>, HasAccountsList {

    /**
     * Only group role for the moment.
     */
    public static final String ADMIN_ROLE_NAME_PREFIX = "ADMIN_";

    /**
     * Helper to request model.
     */
    public static final Find<Long, RoleRealmGroupModel> find = new Finder<>(RoleRealmGroupModel.class);

    /**
     * Group associated.
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REFRESH)
    @Column(name = "group", nullable = false, unique = false)
    private RealmGroupModel group;

    /**
     * List of the permissions associated to the role.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "security_role_realm_group_has_permission")
    private List<PermissionModel> permissions;

    /**
     * List of the accounts with group role.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "security_role_realm_group_has_account")
    private List<AccountModel> accounts;
    /**
     * Name of the role.
     */
    @Size(max = 25)
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    /**
     * Build a basic group role.
     *
     * @since 15.10
     */
    public RoleRealmGroupModel() {
        super();
    }

    /**
     * Build a group role.
     *
     * @param group The group to associate
     * @since 15.10
     */
    public RoleRealmGroupModel(final RealmGroupModel group) {
        super();
        this.group = group;
    }

    /**
     * Get the group associated to the role.
     *
     * @return The group
     * @since 15.10
     */
    public RealmGroupModel getGroup() {
        return this.group;
    }

    /**
     * Set the group associated to this role
     *
     * @param group The group to associate
     * @since 15.10
     */
    public void setGroup(final RealmGroupModel group) {
        this.group = group;
    }

    /**
     * Get all accounts attached to this role.
     *
     * @return A list of accounts reference
     * @see AccountModel
     * @since 15.10
     */
    @Override
    public List<AccountModel> getAccounts() {
        return this.accounts;
    }

    /**
     * Set the accounts list.
     *
     * @param accounts The accounts to set
     * @since 15.10
     */
    @Override
    public void setAccounts(List<AccountModel> accounts) {
        this.accounts = accounts;
    }

    /**
     * Add new account to the existing list.
     *
     * @param account Account to add
     * @since 15.10
     */
    @Override
    public void addAccount(AccountModel account) {
        if(!this.accounts.contains(account)){
            this.accounts.add(account);
        }
    }

    /**
     * Add new account to the existing list.
     *
     * @param accounts Accounts to add
     * @since 15.10
     */
    @Override
    public void addAccounts(List<AccountModel> accounts) {
        this.accounts.addAll(accounts);
    }

    /**
     * Remove account from the existing list.
     *
     * @param account Account to remove
     * @since 15.10
     */
    @Override
    public void removeAccount(AccountModel account) {
        this.accounts.remove(account);
    }

    /**
     * Remove accounts from the existing list.
     *
     * @param accounts Accounts to remove
     * @since 15.10
     */
    @Override
    public void removeAccounts(List<AccountModel> accounts) {
        this.accounts.removeAll(accounts);
    }

    /**
     * Get all permissions attached to this role.
     *
     * @return A list of permissions reference
     * @see PermissionModel
     * @since 15.10
     */
    @Override
    public List<PermissionModel> getPermissions() {
        return this.permissions;
    }

    /**
     * Set the permissions list.
     *
     * @param permissions The permissions to set
     * @since 15.10
     */
    @Override
    public void setPermissions(List<PermissionModel> permissions) {
        this.permissions = permissions;
    }

    /**
     * Add new permissions to the existing list.
     *
     * @param permissions Permissions to add
     * @since 15.10
     */
    @Override
    public void addPermissions(List<PermissionModel> permissions) {
        this.permissions.addAll(permissions);
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code RoleGroupModel} in cas of success
     */
    @Override
    public RoleRealmGroupModel bind(String key, String value) {
        final RoleRealmGroupModel role = RoleRealmGroupModel.find.where().like("uid", value).findUnique();
        if (role == null) {
            throw new IllegalArgumentException("404");
        }
        return role;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(String key) {
        return this.getUidAsString();
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return "function(k,v) {\n" +
                "    return v.getUidAsString();" +
                "}";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(final String name) {
        this.name = name.trim();
    }
}
