/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Locale;

/**
 * AccountPhoneModel.
 *
 * @author Olivier Buiron
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "account_phone")
public class AccountPhoneModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, AccountPhoneModel> find = new Finder<>(AccountPhoneModel.class);

    /**
     * The unique ID of this entry.
     */
    @Id
    private Long id;

    /**
     * The account associated to this email address.
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Column(name = "account")
    private AccountModel account;

    /**
     * The phone number.
     */
    @Size(min = 7, max = 50)
    @Column(name = "phone", nullable = false, unique = true)
    private String phone;

    /**
     * The country code ISO 2 of phone (ex: "FR", "GB")
     */
    @Size(min = 2, max = 2)
    @Column(name = "country_code", nullable = false, unique = false)
    private String countryCode;

    /**
     * Is this  phone number is used as primary phone?
     */
    @Column(name = "is_primary", nullable = false, columnDefinition = "BOOLEAN DEFAULT 0")
    private boolean primary;

    /**
     * Is this phone number is validated?
     */
    @Column(name = "is_validated", nullable = false, columnDefinition = "BOOLEAN DEFAULT 0")
    private boolean validated;

    /**
     * Constructor for a new {@code AccountPhoneModel}.
     *
     * @param account The account to assign email address to
     * @param phone   The phone number to use
     * @param countryCode The country code to set
     * @since 15.10
     */
    public AccountPhoneModel(final AccountModel account, final String phone, final String countryCode) {
        this.primary = false;
        this.validated = false;
        this.setAccount(account);
        this.setPhone(phone);
        this.setCountryCode(countryCode);
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Set the account assigned to this email address.
     *
     * @return An {@code AccountModel} object
     * @since 15.10
     */
    public AccountModel getAccount() {
        return this.account;
    }

    /**
     * Assign an account to this email address.
     *
     * @param account The account to assign
     * @since 15.10
     */
    public void setAccount(final AccountModel account) {
        this.account = account;
    }

    /**
     * Get the phone number.
     *
     * @return The phone number (international format)
     * @since 15.10
     */
    public String getPhone() {
        return this.phone;
    }

    /**
     * Set the phone number. It will be trimmed and lowered.
     *
     * @param phone The phone to set
     * @since 15.10
     */
    public void setPhone(final String phone) {
        this.phone = phone.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Get the phone country code (ISO_2).
     *
     * @return The country code of this phone
     * @since 15.10
     */
    public String getCountryCode(){
        return this.countryCode;
    }

    /**
     * Set the phone country code (ISO_2). It will be trimmed and upper.
     *
     * @param countryCode The country code to set
     * @since 15.10
     */
    public void setCountryCode(final String countryCode){
        this.countryCode = countryCode.trim().toUpperCase(Locale.ENGLISH);
    }

    /**
     * Check if this email address is set as primary.
     *
     * @return {@code true} if email address is primary, otherwise, {@code false}
     */
    public boolean isPrimary() {
        return this.primary;
    }

    /**
     * Set email address as primary.
     *
     * @param isPrimary {@code true} to set email address primary, otherwise, {@code false}
     */
    public void setPrimary(final boolean isPrimary) {
        this.primary = isPrimary;
    }

    /**
     * Check if this email address is validated.
     *
     * @return {@code true} if email address is validated, otherwise, {@code false}
     */
    public boolean isValidated() {
        return this.validated;
    }

    /**
     * Set email address validated.
     *
     * @param isValidated {@code true} to set email address validated, otherwise, {@code false}
     */
    public void setValidated(final boolean isValidated) {
        this.validated = isValidated;
    }
}
