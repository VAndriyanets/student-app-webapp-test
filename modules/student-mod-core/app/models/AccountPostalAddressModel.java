/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import org.apache.commons.lang3.text.WordUtils;
import play.mvc.PathBindable;
import toolbox.enumeration.CountryCode;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * PostalAddressModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "address")
public class AccountPostalAddressModel extends Model implements PathBindable<AccountPostalAddressModel> {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<Long, AccountPostalAddressModel> find = new Model.Finder<>(AccountPostalAddressModel.class);

    /**
     * The unique ID of the actor.
     */
    @Id
    private Long id;

    /**
     * The account associated to this email address.
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Column(name = "account")
    private AccountModel account;

    /**
     * Unique uid of the postal address.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;

    /**
     * Is this postal address is used as primary address?
     */
    @Column(name = "is_primary", nullable = false, columnDefinition = "BOOLEAN DEFAULT 0")
    private boolean primary;

    /**
     * Dest name.
     */
    @Size(max = 70)
    @Column(name = "dest_name", nullable = false, unique = false)
    private String destName;


    /**
     * The first line of the address.
     */
    @Size(max = 128)
    @Column(name = "street_1", nullable = false, unique = false)
    private String street1;

    /**
     * The second line of the address.
     */
    @Size(max = 128)
    @Column(name = "street_2", nullable = true, unique = false)
    private String street2;

    /**
     * The city name.
     */
    @Size(max = 16)
    @Column(name = "zip_code", nullable = false, unique = false)
    private String zipCode;

    /**
     * The city name.
     */
    @Size(max = 64)
    @Column(name = "city", nullable = false, unique = false)
    private String city;

    /**
     * The country code.
     */
    @Size(max = 3)
    @Column(name = "country", nullable = false, unique = false)
    private CountryCode country;

    /**
     * Build a basic address.
     *
     * @since 15.10
     */
    public AccountPostalAddressModel(final AccountModel account) {
        this.account = account;
        this.primary = false;
        this.uid = UUID.randomUUID();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id == null ? 0 : this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.10
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.10
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    /**
     * Check if this postal address is set as primary.
     *
     * @return {@code true} if postal address is primary, otherwise, {@code false}
     * @since 15.10
     */
    public boolean isPrimary() {
        return primary;
    }

    /**
     * Set postal address as primary.
     *
     * @param primary {@code true} to set postal address primary, otherwise, {@code false}
     * @since 15.10
     */
    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    /**
     * Get the destination name (ie: Robert Ledoux).
     *
     * @return The destination name
     */
    public String getDestName() {
        return this.destName;
    }

    /**
     * Set the destination name field.
     *
     * @param destName The destination name to use
     * @since 15.10
     */
    public void setDestName(final String destName) {
        this.destName = WordUtils.capitalizeFully(destName.trim());
    }

    /**
     * Get the first line of the address.
     *
     * @return The first line of the address
     * @since 15.10
     */
    public String getStreet1() {
        return this.street1;
    }

    /**
     * Set the first line of the address.
     *
     * @param street1 The information to use
     * @since 15.10
     */
    public void setStreet1(final String street1) {
        this.street1 = street1;
    }

    /**
     * Get the second line of the address.
     *
     * @return The second line of the address
     * @since 15.10
     */
    public String getStreet2() {
        return this.street2;
    }

    /**
     * Set the second line of the address.
     *
     * @param street2 The information to use
     * @since 15.10
     */
    public void setStreet2(final String street2) {
        this.street2 = street2;
    }

    /**
     * Get the postal zip code.
     *
     * @return The postal zip code
     * @since 15.10
     */
    public String getZipCode() {
        return this.zipCode;
    }

    /**
     * Set the postal zip code.
     *
     * @param zipCode The postal zip code to use
     * @since 15.10
     */
    public void setZipCode(final String zipCode) {
        this.zipCode = zipCode.trim();
    }

    /**
     * Get the city name.
     *
     * @return The city name
     * @since 15.10
     */
    public String getCity() {
        return this.city;
    }

    /**
     * Set the city name.
     *
     * @param city The city name to use
     * @since 15.10
     */
    public void setCity(final String city) {
        this.city = WordUtils.capitalizeFully(city.trim());
    }

    /**
     * Get the country.
     *
     * @return The country
     * @since 15.10
     */
    public CountryCode getCountry() {
        return this.country;
    }

    /**
     * Set the country.
     *
     * @param country The country to use
     * @since 15.10
     */
    public void setCountry(final CountryCode country) {
        this.country = country;
    }

    /**
     * Get a generated address label.
     *
     * @return An address label (pattern: {street_1} ,{city})
     * @since 15.10
     */
    public String getLabel(){
        return String.format("%s, %s", this.street1, this.city);
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code PostalAddressModel} in case of success
     */
    @Override
    public AccountPostalAddressModel bind(String key, String value) {
        final AccountPostalAddressModel address = AccountPostalAddressModel.find.where().like("uid", value).findUnique();
        if (address == null) {
            throw new IllegalArgumentException("404");
        }
        return address;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(String s) {
        return this.uid.toString();
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return "function(k,v) {\n" +
                "    return v.uid;" +
                "}";
    }
}
