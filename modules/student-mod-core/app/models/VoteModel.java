package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.google.common.base.Objects;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Andrey Sokolov
 */
@Entity
@Table(name = "post_votes")
public class VoteModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<VotePK, VoteModel> find = new Model.Finder<>(VoteModel.class);

    public static int getLikedUsersNumber(final PostModel post, final boolean like) {
        return Ebean.createSqlQuery("select count(user_id) as vote_number from post_votes where post_id = " + post.getId() + " and is_like = " + (like ? 1 : 0)).findList().get(0).getInteger("vote_number");
    }

    public static Set<Long> getUserLikedPosts(final AccountModel account, final boolean like) {
        return Ebean.createSqlQuery("select post_id from post_votes where user_id = " + account.getId() + " and is_like = " + (like ? 1 : 0))
                .findList().stream().map(row -> row.getLong(("post_id"))).collect(Collectors.toSet());
    }

    public static void cleanLikesCache(final Long id, final boolean like) {
        if (like) {
            play.cache.Cache.remove(String.format("post.%d.likes", id));
        } else {
            play.cache.Cache.remove(String.format("post.%d.dislikes", id));
        }
    }

    public static VoteModel findVote(Long postId, Long accountId, boolean like) {
        final VoteModel model = find.byId(new VotePK(accountId, postId));
        if (model != null && like == model.like) {
            return model;
        } else {
            return null;
        }
    }

    @EmbeddedId
    public VotePK key;

    @Column(name = "is_like", nullable = false, columnDefinition = "BOOLEAN DEFAULT 0")
    public boolean like;

    /**
     * Date of creation.
     */
    @Column(name = "create_date", nullable = false, unique = false)
    public DateTime createDate;

    public VoteModel() {
        createDate = DateTime.now();
    }

    @Embeddable
    public static class VotePK implements Serializable {

        @Column(name = "user_id")
        public Long userId;
        @Column(name = "post_id")
        public Long postId;

        public VotePK() {
        }

        public VotePK(Long userId, Long postId) {
            this.userId = userId;
            this.postId = postId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            VotePK votePK = (VotePK) o;
            return Objects.equal(userId, votePK.userId) &&
                    Objects.equal(postId, votePK.postId);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(userId, postId);
        }
    }
}
