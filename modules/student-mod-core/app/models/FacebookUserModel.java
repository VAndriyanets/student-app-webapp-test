/*
 * Copyright (C) 2014 - 2016 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * Facebook User Data Model.
 */
@Entity
@Table(name = "facebook_user")
public class FacebookUserModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, FacebookUserModel> find = new Finder<>(FacebookUserModel.class);

    /**
     * The unique ID of the user.
     */
    @Id
    private Long id;

    /**
     * Unique uid of the user.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;

    /**
     * Facebook user id.
     */
    @Size(max = 50)
    @Column(name = "fbid", nullable = false, unique = false)
    private String fbid;

    /**
     * Name of facebook user.
     */
    @Size(max = 500)
    @Column(name = "name", nullable = false, unique = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private FacebookDataModel account;
    /**
     * Build a Facebook User.
     *
     */
    public FacebookUserModel() {
        this.uid = UUID.randomUUID();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id == null ? 0 : this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.10
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.10
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    public String getFbid() {
        return fbid;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public FacebookDataModel getAccount() {
        return account;
    }

    public void setAccount(FacebookDataModel account) {
        this.account = account;
    }

}
