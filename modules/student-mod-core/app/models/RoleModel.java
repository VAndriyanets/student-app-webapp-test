/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

/**
 * RoleModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@MappedSuperclass
public abstract class RoleModel extends Model {

    /**
     * The unique ID of this entry.
     */
    @Id
    private Long id;

    /**
     * UID of the role.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;

    /**
     * ReadOny role ? (Can't modify by user)
     */
    @Column(name = "readonly", nullable = false)
    private boolean readonly = false;

    /**
     * Build a default role.
     *
     * @since 15.10
     */
    public RoleModel() {
        this.uid = UUID.randomUUID();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.10
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.10
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    /**
     * Get the name of this role.
     *
     * @return The name
     * @since 15.10
     */
    public abstract String getName();

    /**
     * Set the name. It will be trimmed.
     *
     * @param name The name to set
     * @since 15.10
     */
    public abstract void setName(String name);

    /**
     * Get the readonly? of this role.
     *
     * @return The readonly status
     * @since 15.10
     */
    public boolean isReadonly() {
        return this.readonly;
    }

    /**
     * Set the readonly status.
     *
     * @param readonly The status readonly to set
     * @since 15.10
     */
    public void setReadonly(final boolean readonly) {
        this.readonly = readonly;
    }

    /**
     * Get all accounts attached to this role.
     *
     * @return A list of accounts reference
     * @see AccountModel
     * @since 15.10
     */
    public abstract List<AccountModel> getAccounts();

    /**
     * Set the accounts list.
     *
     * @param accounts The accounts to set
     * @since 15.10
     */
    public abstract void setAccounts(final List<AccountModel> accounts);

    /**
     * Add new accounts to the existing list.
     *
     * @param accounts Accounts to add
     * @since 15.10
     */
    public abstract void addAccounts(final List<AccountModel> accounts);

    /**
     * Add new account to the existing list.
     *
     * @param account Account to add
     * @since 15.10
     */
    public abstract void addAccount(final AccountModel account);

    /**
     * Remove accounts from the existing list.
     *
     * @param accounts Accounts to remove
     * @since 15.10
     */
    public abstract void removeAccounts(final List<AccountModel> accounts);

    /**
     * Remove account from the existing list.
     *
     * @param account Account to remove
     * @since 15.10
     */
    public abstract void removeAccount(final AccountModel account);

    /**
     * Get all permissions attached to this role.
     *
     * @return A list of permissions reference
     * @see PermissionModel
     * @since 15.10
     */
    public abstract List<PermissionModel> getPermissions();

    /**
     * Set the permissions list.
     *
     * @param permissions The permissions to set
     * @since 15.10
     */
    public abstract void setPermissions(final List<PermissionModel> permissions);

    /**
     * Add new permissions to the existing list.
     *
     * @param permissions Permissions to add
     * @since 15.10
     */
    public abstract void addPermissions(final List<PermissionModel> permissions);

    /**
     * Add if not contains
     *
     * @param permission
     */
    public void addPermission(final PermissionModel permission){
        if (!getPermissions().contains(permission)) {
            getPermissions().add(permission);
        }
    }

    /**
     * Remove if contains
     *
     * @param permission
     */
    public void removePermission(final PermissionModel permission){
        if (getPermissions().contains(permission)) {
            getPermissions().remove(permission);
        }
    }
}
