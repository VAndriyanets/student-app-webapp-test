package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.PagedList;
import org.joda.time.DateTime;
import toolbox.UTCDateTime;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Andrey Sokolov
 */
@Entity
@Table(name = "comments")
public class CommentModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, CommentModel> find = new Finder<>(CommentModel.class);


    public static PagedList<CommentModel> find(final PostModel post, int pageIndex, int pageSize){
        return find.where().eq("post", post).findPagedList(pageIndex, pageSize);
    }

    public static List<CommentModel> findAll(final PostModel post){
        return find.where().eq("post", post).findList();
    }

    public static int count(final PostModel post){
        return find.where().eq("post", post).findRowCount();
    }

    /**
     * The unique ID of this post.
     */
    @Id
    public Long id;

    /**
     * Unique uid of the post.
     */
    @Column(name = "uid", nullable = false, unique = true)
    public UUID uid;

    /**
     * Name of the group.
     */
    @Size(max = 10000)
    @Column(name = "post_text", nullable = true, unique = false)
    public String text;

    /**
     * post's pictures
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "comment_has_picture")
    public List<S3FileModel> pictures;

    /**
     * post's author
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "account_id", nullable = false, updatable = false)
    public AccountModel author;

    /**
     * Group posted to, if post related to group.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id", nullable = false, updatable = false)
    public PostModel post;

    /**
     * Date of creation.
     */
    @Column(name = "create_date", nullable = false, unique = false)
    public DateTime createDate;

    /**
     * Build a basic post.
     */
    public CommentModel() {
        this.uid = UUID.randomUUID();
        this.createDate = DateTime.now();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    public void addPicture(S3FileModel picture) {
        if (this.pictures == null) {
            this.pictures = new ArrayList<>(20);
        }
        this.pictures.add(picture);
    }

    public DateTime getCreateDate() {
        return this.createDate;
    }

    /**
     * The datetime when this group has been created as UTC string
     *
     * @return The creation datetime as UTC string
     */
    public String getCreateDateAsUTCString() {
        return UTCDateTime.format(this.createDate);
    }


    /**
     * Forming human-readable representation of post creation elapsed time
     *
     * @return localized String like 'less than minute, xx minutes ago, xx hours ago, xx days ago'
     */
    public String getReadableElapsedTime() {
        return UTCDateTime.getReadableElapsedTime(createDate);
    }

    @Override
    public void delete() {
        if(pictures != null){
            List<S3FileModel> toDelete = new ArrayList<>(pictures);
            pictures.clear();
            save();
            for(S3FileModel picture:toDelete){
                picture.delete();
            }
        }
        post.clearCommentsCache();
        super.delete();
    }

    @Override
    public void save() {
        super.save();
        post.clearCommentsCache();
    }
}
