/*
 * Copyright (C) 2014 - 2016 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * VendorProfileModel - vendor-specific data.
 *
 */
@Entity
@Table(name = "vendor_profile")
public class VendorProfileModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, VendorProfileModel> find = new Finder<>(VendorProfileModel.class);

    /**
     * The unique ID .
     */
    @Id
    private Long id;

    /**
     * link to vendor [page].
     */
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vendor_id")
    private RealmPageModel vendor;

    /**
     * Vendor description.
     */
    @Size(max = 10000)
    @Column(name = "description", nullable = true, unique = false)
    private String description;

    /**
     * Vendor email.
     */
    @Size(max = 100)
    @Column(name = "email", nullable = true, unique = false)
    private String email;

    /**
     * Vendor phone.
     */
    @Size(max = 100)
    @Column(name = "phone", nullable = true, unique = false)
    private String phone;

    /**
     * Vendor address.
     */
    @Size(max = 1000)
    @Column(name = "address", nullable = true, unique = false)
    private String address;

    /**
     * Vendor city.
     */
    @Size(max = 100)
    @Column(name = "city", nullable = true, unique = false)
    private String city;

    /**
     * Vendor zipcode.
     */
    @Size(max = 100)
    @Column(name = "zipcode", nullable = true, unique = false)
    private String zipcode;

    /**
     * Vendor IBAN.
     */
    @Size(max = 100)
    @Column(name = "iban", nullable = true, unique = false)
    private String iban;

    /**
     * IBAN pdf-file.
     */
    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REMOVE)
    private S3FileModel ibanPdf;

    public Long getId() {
        return this.id;
    }

    public RealmPageModel getVendor() {
        return vendor;
    }

    public void setVendor(RealmPageModel vendor) {
        this.vendor = vendor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public S3FileModel getIbanPdf() {
        return this.ibanPdf;
    }

    public void setIbanPdf(S3FileModel pdf) {
        if (this.ibanPdf != null) {
            this.deleteIbanPdf();
        }
        this.ibanPdf = pdf;
    }

    /**
     * Delete the iban pdf. This method will drop S3-file and call {@link VendorProfileModel#update()}
     * automatically.
     *
     */
    public void deleteIbanPdf() {
        if (this.ibanPdf != null) {
            final S3FileModel handle = this.ibanPdf;
            this.ibanPdf = null;
            this.update();
            handle.delete();
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }
}
