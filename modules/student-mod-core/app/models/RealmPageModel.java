/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.joda.time.DateTime;
import play.i18n.Messages;
import play.libs.Json;
import play.mvc.PathBindable;
import toolbox.UTCDateTime;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * Realm vendor page model.
 *
 * @author Olivier Buiron
 * @version 15.12
 * @since 15.12
 */
@Entity
@Table(name = "realm_pages")
public class RealmPageModel extends Model implements PathBindable<RealmPageModel>, SearchableModel {

    public static final String VENDOR_PAGE_ROLE_VENDOR_KEY = "REALM.ADMIN.PAGE.ROLE.VENDOR.NAME";

    /**
     * Helper to request Model.
     */
    public static final Model.Finder<Long, RealmPageModel> find = new Model.Finder<>(RealmPageModel.class);

    public static RealmPageModel findByUid(String uid) {
        return find.where().eq("uid", uid).findUnique();
    }

    public static List<RealmPageModel> findAllVisibleForUser(AccountModel account) {
        Set<RealmModel> userRealms = account.getRealmsAccessible();

        List<RealmPageModel> vendors = RealmPageModel.find.where().in("realms", userRealms).findList();

        return vendors;
    }

    /**
     * The unique ID of this entry.
     */
    @Id
    private Long id;

    /**
     * Unique page uid.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;

    /**
     * Unique page slug.
     */
    @Size(max = 36)
    @Column(name = "slug", nullable = false, unique = true)
    private String slug;

    /**
     * Unique page name
     */
    @Size(max = 36)
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    /**
     * Page logo hosted by S3.
     */
    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REMOVE)
    @Column(name = "logo", nullable = true, unique = false)
    private S3FileModel logo;

    /**
     * Page banner hosted by S3.
     */
    @OneToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REMOVE)
    @Column(name = "banner", nullable = true, unique = false)
    private S3FileModel banner;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "realm_has_page")
    private List<RealmModel> realms;

    /**
     * Role existing to this page.
     */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "page")
    private List<RoleRealmPageModel> roles;

    /**
     * Date of creation.
     */
    @Column(name = "create_date", nullable = false, unique = false)
    private DateTime createDate;

    /**
     * Link to the Vendor Data object.
     */
    @OneToOne(mappedBy = "vendor", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private VendorProfileModel vendorProfile;

    /**
     * Build a basic page.
     *
     * @since 15.12
     */
    public RealmPageModel() {
        this.uid = UUID.randomUUID();
        this.createDate = DateTime.now();
        this.realms = new ArrayList<>();
        this.roles = new ArrayList<>();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.12
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.12
     */
    public UUID getUid() {
        return this.uid;
    }

    @Override
    public ObjectNode getIndexSource() {
        final ObjectNode esUserInfo = Json.newObject();
        esUserInfo.put("name", getName());
        esUserInfo.put("logo", getLogo() != null ? getLogo().getIdAsString() : null);
        esUserInfo.put("slug", getSlug());
        final ArrayNode esUserInfoRealm = esUserInfo.putArray("_realm");
        for (final RealmModel rm : getRealms()) {
            esUserInfoRealm.add(rm.getId());
        }
        return esUserInfo;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.12
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    @Override
    public String getSearchType() {
        return SearchRequestType.PAGE.getTypes()[0];
    }

    /**
     * Get the slug of this group. A slug is a little string used to access
     * the page from the URL.
     *
     * @return The slug as String
     * @since 15.12
     */
    public String getSlug() {
        return this.slug;
    }

    /**
     * Set the slug of this group. A slug is a little string used to access
     * the page from the URL.
     *
     * @param slug The slug to use
     * @since 15.12
     */
    public void setSlug(final String slug) {
        this.slug = slug.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Get the name of this page.
     *
     * @return The page's name
     * @since 15.12
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name of this page.
     *
     * @param name The name to use
     * @since 15.12
     */
    public void setName(final String name) {
        this.name = name.trim();
    }

    /**
     * Get the current set logo.
     *
     * @return The page's logo as {@code S3FileModel}
     * @see S3FileModel
     * @since 15.12
     */
    public S3FileModel getLogo() {
        return this.logo;
    }

    /**
     * Set the logo. If a logo is already set, it will be deleted.
     *
     * @param logo The logo to set
     * @since 15.12
     */
    public void setLogo(S3FileModel logo) {
        if (this.logo != null) {
            this.deleteLogo();
        }
        this.logo = logo;
    }

    /**
     * Delete the logo. This method will call {@link RealmPageModel#update()}
     * automatically.
     *
     * @since 15.12
     */
    public void deleteLogo() {
        if (this.logo != null) {
            final S3FileModel handle = this.logo;
            this.logo = null;
            this.update();
            handle.delete();
        }
    }

    /**
     * Get the current set banner.
     *
     * @return The page's banner as {@code S3FileModel}
     * @see S3FileModel
     * @since 15.12
     */
    public S3FileModel getBanner() {
        return this.banner;
    }

    /**
     * Set the banner. If a banner is already set, it will be deleted.
     *
     * @param banner The banner to set
     * @since 15.12
     */
    public void setBanner(S3FileModel banner) {
        if (this.banner != null) {
            this.deleteBanner();
        }
        this.banner = banner;
    }

    /**
     * Delete the logo. This method will call {@link RealmPageModel#update()}
     * automatically.
     *
     * @since 15.12
     */
    public void deleteBanner() {
        if (this.banner != null) {
            final S3FileModel handle = this.banner;
            this.banner = null;
            this.update();
            handle.delete();
        }
    }

    /**
     * Get the roles registered to this page.
     *
     * @return A list of Roles (RoleRealmPageModel)
     * @since 15.12
     */
    public List<RoleRealmPageModel> getRoles() {
        return this.roles;
    }

    /**
     * Get realms associated to this page.
     *
     * @return A list of Realms
     * @since 15.12
     */
    public List<RealmModel> getRealms() {
        return this.realms;
    }

    /**
     * The datetime when this group has been created.
     *
     * @return The creation datetime
     * @since 15.12
     */
    public DateTime getCreateDate() {
        return this.createDate;
    }

    /**
     * The datetime when this page has been created as UTC string
     *
     * @return The creation datetime as UTC string
     * @since 15.12
     */
    public String getCreateDateAsUTCString() {
        return UTCDateTime.format(this.createDate);
    }

    public VendorProfileModel getVendorProfile() {
        return vendorProfile;
    }

    public void setVendorProfile(VendorProfileModel vendorProfile) {
        this.vendorProfile = vendorProfile;
    }

    /**
     * Checks for account being in Vendor-role of this page
     * @param account  - The account to check
     * @return true - if account is in Vendor-role for this page, false - otherwise
     */
    public boolean hasVendor(AccountModel account) {
        for (RoleRealmPageModel role : getRoles()) {
            if (role.getName().equals(Messages.get(VENDOR_PAGE_ROLE_VENDOR_KEY))) {
                List<AccountModel> roleAccounts = role.getAccounts();
                for(AccountModel roleAccount : roleAccounts) {
                    if (roleAccount.getId() == account.getId()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code RealmModel} in case of success
     */
    @Override
    public RealmPageModel bind(String key, String value) {
        final RealmPageModel page = RealmPageModel.find.where().like("slug", value).findUnique();
        if (page == null) {
            throw new IllegalArgumentException("404");
        }
        return page;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(String key) {
        return this.slug;
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return "function(k,v) {\n" +
                "    return v.get;" +
                "}";
    }
}
