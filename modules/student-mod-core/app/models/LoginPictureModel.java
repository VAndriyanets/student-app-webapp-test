/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * LoginPictureModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "login_picture")
public class LoginPictureModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<Long, LoginPictureModel> find = new Model.Finder<>(LoginPictureModel.class);

    /**
     * The unique ID of the actor.
     */
    @Id
    private Long id;

    /**
     * Picture.
     */
    @OneToOne(fetch = FetchType.EAGER, optional = true, cascade = CascadeType.REMOVE)
    @Column(name = "picture", nullable = true, unique = false)
    private S3FileModel picture;

    /**
     * The title.
     */
    @Size(max = 50)
    @Column(name = "title", nullable = false, unique = false)
    private String title;

    /**
     * The content.
     */
    @Size(max = 300)
    @Column(name = "content", nullable = false, unique = false)
    private String content;

    /**
     * The link to read more about.
     */
    @Size(max = 125)
    @Column(name = "link", nullable = false, unique = false)
    private String link;

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the title of this picture.
     *
     * @return The title
     * @since 15.10
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Get the content describing this picture.
     *
     * @return The content
     * @since 15.10
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Get the URL link to read more about picture.
     *
     * @return The link
     * @since 15.10
     */
    public String getLink() {
        return this.link;
    }

    /**
     * Get the current picture.
     *
     * @return The picture as {@code S3FileModel}
     * @see S3FileModel
     * @since 15.10
     */
    public S3FileModel getPicture() {
        return this.picture;
    }
}
