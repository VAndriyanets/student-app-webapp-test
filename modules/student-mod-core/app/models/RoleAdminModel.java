/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * RoleAdminModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "security_role_admin")
public class RoleAdminModel extends RoleModel {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, RoleAdminModel> find = new Finder<Long, RoleAdminModel>(RoleAdminModel.class);

    /**
     * List of the permissions associated to the role.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "security_role_admin_has_permission")
    private List<PermissionModel> permissions;

    /**
     * List of the accounts with admin role.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "security_role_admin_has_account")
    private List<AccountModel> accounts;
    /**
     * Name of the role.
     */
    @Size(max = 25)
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    /**
     * Get all accounts attached to this role.
     *
     * @return A list of accounts reference
     * @see AccountModel
     * @since 15.10
     */
    @Override
    public List<AccountModel> getAccounts() {
        return this.accounts;
    }

    /**
     * Set the accounts list.
     *
     * @param accounts The accounts to set
     * @since 15.10
     */
    @Override
    public void setAccounts(final List<AccountModel> accounts) {
        this.accounts = accounts;
    }

    /**
     * Add new account to the existing list.
     *
     * @param accounts Accounts to add
     * @since 15.10
     */
    @Override
    public void addAccounts(final List<AccountModel> accounts) {
        this.accounts.addAll(accounts);
    }

    /**
     * Add new account to the existing list.
     *
     * @param account Account to add
     * @since 15.10
     */
    @Override
    public void addAccount(final AccountModel account) {
        this.accounts.add(account);
    }

    /**
     * Remove accounts from the existing list.
     *
     * @param accounts Accounts to remove
     * @since 15.10
     */
    @Override
    public void removeAccounts(final List<AccountModel> accounts) {
        for (final AccountModel a : accounts) {
            this.accounts.remove(a);
        }
    }

    /**
     * Remove account from the existing list.
     *
     * @param account Account to remove
     * @since 15.10
     */
    @Override
    public void removeAccount(final AccountModel account) {
        this.accounts.remove(account);
    }


    /**
     * Get all permissions attached to this role.
     *
     * @return A list of permissions reference
     * @see PermissionModel
     * @since 15.10
     */
    @Override
    public List<PermissionModel> getPermissions() {
        return this.permissions;
    }

    /**
     * Set the permissions list.
     *
     * @param permissions The permissions to set
     * @since 15.10
     */
    @Override
    public void setPermissions(final List<PermissionModel> permissions) {
        this.permissions = permissions;
    }

    /**
     * Add new permissions to the existing list.
     *
     * @param permissions Permissions to add
     * @since 15.10
     */
    @Override
    public void addPermissions(final List<PermissionModel> permissions) {
        this.permissions.addAll(permissions);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(final String name) {
        this.name = name.trim();
    }
}
