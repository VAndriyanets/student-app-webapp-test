/*
 * Copyright (C) 2014 - 2016 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

/**
 * Facebook Account's Data Model.
 */
@Entity
@Table(name = "facebook_data")
public class FacebookDataModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, FacebookDataModel> find = new Finder<>(FacebookDataModel.class);

    /**
     * The unique ID.
     */
    @Id
    private Long id;

    /**
     * Unique uid.
     */
    @Column(name = "uid", nullable = false, unique = true)
    private UUID uid;

    /**
     * Facebook user id.
     */
    @Size(max = 50)
    @Column(name = "fbid", nullable = false, unique = false)
    private String fbid;

    /**
     * Marital status of facebook user.
     */
    @Size(max = 50)
    @Column(name = "marital_status", nullable = true, unique = false)
    private String maritalStatus;

    /**
     * Marital status of facebook user.
     */
    @Size(max = 255)
    @Column(name = "picture_url", nullable = true, unique = false)
    private String pictureURL;

    /**
     * Comma separated list of friends of facebook user.
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account", cascade = CascadeType.ALL)
    private List<FacebookUserModel> friends;


    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "account_id")
    private AccountModel account;

    /**
     * Build a Facebook Data.
     *
     */
    public FacebookDataModel() {
        this.uid = UUID.randomUUID();
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id == null ? 0 : this.id;
    }

    /**
     * Get the unique public ID.
     *
     * @return The unique public ID
     * @see UUID
     * @since 15.10
     */
    public UUID getUid() {
        return this.uid;
    }

    /**
     * Get the unique public ID as string.
     *
     * @return The unique public ID as string
     * @since 15.10
     */
    public String getUidAsString() {
        return this.uid.toString();
    }

    public String getFbid() {
        return fbid;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public List<FacebookUserModel> getFriends() {
        return friends;
    }

    public void setFriends(List<FacebookUserModel> friends) {
        this.friends = friends;
    }

    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }
}
