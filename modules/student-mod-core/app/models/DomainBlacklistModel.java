/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;
import org.apache.commons.lang3.text.WordUtils;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * DomainBlacklistModel.
 *
 * @author Pierre Adam
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "security_domain_blacklist")
public class DomainBlacklistModel {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<Long, DomainBlacklistModel> find = new Model.Finder<>(DomainBlacklistModel.class);

    /**
     * The unique ID of blacklisted domain.
     */
    @Id
    private Long id;

    /**
     * Domain name.
     */
    @Size(max = 35)
    @Column(name = "domain", nullable = true, unique = false)
    private String domain;

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the domain field value.
     *
     * @return The first name
     */
    public String getDomain() {
        return this.domain;
    }

    /**
     * Set the domain field.
     *
     * @param domain The first name to use
     * @since 15.10
     */
    public void setDomain(final String domain) {
        this.domain = WordUtils.capitalizeFully(domain.trim());
    }
}
