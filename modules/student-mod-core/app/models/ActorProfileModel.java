/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * ActorProfileModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "actor_profile")
public class ActorProfileModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, ActorProfileModel> find = new Finder<>(ActorProfileModel.class);

    /**
     * The unique ID of the actor.
     */
    @Id
    private Long id;

    /**
     * Actor profile name.
     */
    @Size(max = 80)
    @Column(name = "name", nullable = true, unique = false)
    private String name;

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Get the name.
     *
     * @return The actor's name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name. A trim() will be applied.
     *
     * @param name The name to set
     */
    public void setName(final String name) {
        this.name = name.trim();
    }
}
