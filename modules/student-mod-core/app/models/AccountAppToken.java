package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.UUID;

/**
 * @author Andrey Sokolov
 */
@Entity
@Table(name = "account_token",
        uniqueConstraints =  @UniqueConstraint(columnNames={"account_id", "mobile_app_id"})
)
public class AccountAppToken extends Model {

    /**
     * Helpers to request model.
     */
    public static final Model.Finder<Long, AccountAppToken> find = new Model.Finder<>(AccountAppToken.class);

    @Id
    public Long id;
    @Column(name = "access_token", unique = true, nullable = false)
    public UUID accessToken;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    public AccountModel account;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    public MobileAppModel mobileApp;

    public AccountAppToken() {
        accessToken = UUID.randomUUID();
    }

    public static AccountAppToken findByToken(String token) {
        return find.where().eq("accessToken", token).findUnique();
    }

}
