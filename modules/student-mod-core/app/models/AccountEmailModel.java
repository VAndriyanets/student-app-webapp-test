/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Locale;

/**
 * EmailModel.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "account_email")
public class AccountEmailModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<Long, AccountEmailModel> find = new Finder<>(AccountEmailModel.class);

    /**
     * The unique ID of this entry.
     */
    @Id
    private Long id;

    /**
     * The account associated to this email address.
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Column(name = "account")
    private AccountModel account;

    /**
     * The email address.
     */
    @Size(min = 7, max = 125)
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    /**
     * Is this email address is used as primary address?
     */
    @Column(name = "is_primary", nullable = false, columnDefinition = "BOOLEAN DEFAULT 0")
    private boolean primary;

    /**
     * Is this email address is validated?
     */
    @Column(name = "is_validated", nullable = false, columnDefinition = "BOOLEAN DEFAULT 0")
    private boolean validated;

    /**
     * Constructor for a new {@code AccountEmailModel}.
     *
     * @param account The account to assign email address to
     * @param email   The email address to use
     * @since 15.10
     */
    public AccountEmailModel(final AccountModel account, final String email) {
        this.primary = false;
        this.validated = false;
        this.setAccount(account);
        this.setEmail(email);
    }

    /**
     * Get the ID of the current entry.
     *
     * @return The ID
     * @since 15.10
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Set the account assigned to this email address.
     *
     * @return An {@code AccountModel} object
     * @since 15.10
     */
    public AccountModel getAccount() {
        return this.account;
    }

    /**
     * Assign an account to this email address.
     *
     * @param account The account to assign
     * @since 15.10
     */
    public void setAccount(final AccountModel account) {
        this.account = account;
    }

    /**
     * Get the email address.
     *
     * @return The email address
     * @since 15.10
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Set the address email. It will be trimmed and lowered.
     *
     * @param email The email to set
     * @since 15.10
     */
    public void setEmail(final String email) {
        this.email = email.trim().toLowerCase(Locale.ENGLISH);
    }

    /**
     * Check if this email address is set as primary.
     *
     * @return {@code true} if email address is primary, otherwise, {@code false}
     */
    public boolean isPrimary() {
        return this.primary;
    }

    /**
     * Set email address as primary.
     *
     * @param isPrimary {@code true} to set email address primary, otherwise, {@code false}
     */
    public void setPrimary(final boolean isPrimary) {
        this.primary = isPrimary;
    }

    /**
     * Check if this email address is validated.
     *
     * @return {@code true} if email address is validated, otherwise, {@code false}
     */
    public boolean isValidated() {
        return this.validated;
    }

    /**
     * Set email address validated.
     *
     * @param isValidated {@code true} to set email address validated, otherwise, {@code false}
     */
    public void setValidated(final boolean isValidated) {
        this.validated = isValidated;
    }
}
