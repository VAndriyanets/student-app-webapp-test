package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

/**
 * @author Andrey Sokolov
 */
@Entity
@Table(name = "mobile_app")
public class MobileAppModel extends Model {

    public static final Finder<Long, MobileAppModel> find = new Finder<>(MobileAppModel.class);

    /**
     * The unique ID of this post.
     */
    @Id
    public Long id;
    /**
     * Unique uid of the post.
     */
    @Column(name = "application_id", nullable = false, unique = true)
    public UUID uid;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "mobile_app_has_permission")
    public List<MobileAppPermission> permissions;

    @Column(name = "app_status", nullable = false, unique = false)
    @Enumerated(EnumType.STRING)
    public Status status;

    public MobileAppModel() {
        uid = UUID.randomUUID();
    }

    public enum Status {
        ACTIVE,
        INACTIVE
    }

    public static MobileAppModel findByUid(final UUID uid){
        return find.where().eq("uid", uid).findUnique();
    }

}
