/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.avaje.ebean.Model;
import modules.S3Module;
import play.Logger;
import play.Play;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

/**
 * S3File.
 *
 * @author Thibault Meyer
 * @version 15.10
 * @since 15.10
 */
@Entity
@Table(name = "s3file")
public class S3FileModel extends Model {

    /**
     * Helpers to request model.
     */
    public static final Finder<UUID, S3FileModel> find = new Finder<>(S3FileModel.class);

    /**
     * The unique ID of the S3 file.
     */
    @Id
    @Column(name = "id")
    private UUID id;

    /**
     * The human readable name of the S3 file.
     */
    @Column(name = "name", nullable = false, columnDefinition = "VARCHAR(50)")
    private String name;

    /**
     * The content type of the S3 file.
     */
    @Column(name = "content_type", nullable = false, columnDefinition = "VARCHAR(20)")
    private String contentType;

    /**
     * The S3 File.
     */
    @Transient
    private File file;

    /**
     * Is this file private or not. Private file can only be accessed
     * by this application.
     */
    @Column(name = "is_private", nullable = false, columnDefinition = "BOOLEAN DEFAULT 1")
    private boolean isPrivate;

    /**
     * Subdirectory on the bucket where this file is located.
     */
    @Constraints.Max(25)
    @Column(name = "sub_directory", nullable = false, columnDefinition = "VARCHAR(25) DEFAULT ''")
    private String subDirectory;

    /**
     * Name of the bucket where the file is stored in.
     */
    @Column(name = "bucket")
    private String bucket;

    /**
     * The S3 module instance.
     *
     * @see S3Module
     */
    @Transient
    private S3Module s3module;

    /**
     * Get the ID of this {code S3File} entry.
     *
     * @return The ID
     * @see UUID
     * @since 15.10
     */
    public UUID getId() {
        return this.id;
    }

    /**
     * Get the ID of this {code S3File} entry as string.
     *
     * @return The ID as string
     * @since 15.10
     */
    public String getIdAsString() {
        return this.id.toString();
    }

    /**
     * Get the filename.
     *
     * @return The filename
     * @since 15.10
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the filename.
     *
     * @param name The filename to use
     * @since 15.10
     */
    public void setName(String name) {
        this.name = name.trim();
    }

    /**
     * Get the content type (ie: image/png)
     *
     * @return The content type
     * @since 15.10
     */
    public String getContentType() {
        return this.contentType;
    }

    /**
     * Set the content type (ie: image/png).
     *
     * @param contentType The content type of the file
     * @since 15.10
     */
    public void setContentType(String contentType) {
        this.contentType = contentType.trim();
    }

    /**
     * Is this file private?
     *
     * @return {@code true} if private, otherwise, {@code false}
     * @since 15.10
     */
    public boolean isPrivate() {
        return this.isPrivate;
    }

    /**
     * Get the subdirectory where is located the file.
     *
     * @return The subdirectory where the file is located
     * @since 15.10
     */
    public String getSubDirectory() {
        return this.subDirectory;
    }

    /**
     * Set the subdirectory where the file will be saved.
     *
     * @param subDirectory The subdirectory to use
     * @since 15.10
     */
    public void setSubDirectory(String subDirectory) {
        this.subDirectory = subDirectory.trim();
    }

    /**
     * Set the file instance.
     *
     * @param file The file to upload to S3
     * @since 15.10
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Set if this file private or not.
     *
     * @param isPrivate {@code true} if private, otherwise, {@code false}
     * @since 15.10
     */
    public void setIsPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    /**
     * Make this entry orphan. Orphan entry can be delete without any
     * issues. Use this method with cautions!
     *
     * @since 15.10
     */
    public void makeOrphan() {
        //if (this.account != null) {
        //this.account.avatar = null;
        //this.account.update();
        //}
    }

    /**
     * Get the base URL to access S3 uploaded file. This value can be
     * set on application.conf file.
     *
     * @return The public base URL
     * @since 15.10
     */
    private String getBasePublicUrl() {
        String basePublicUrl = Play.application().configuration().getString("aws.s3.puburl", "/");
        if (!basePublicUrl.endsWith("/")) {
            basePublicUrl += "/";
        }
        return basePublicUrl;
    }

    /**
     * Get the public URL of this S3 file.
     *
     * @return The public URL of this S3 file
     * @throws MalformedURLException If URL is malformed (check application.conf)
     * @since 15.10
     */
    public URL getUrl() throws MalformedURLException {
        return new URL(this.getBasePublicUrl() + this.bucket + "/" + this.getActualFileName());
    }

    /**
     * Get the public URL of this S3 file as string.
     *
     * @return The public URL of this S3 file, otherwise, null
     * @since 15.10
     */
    public String getUrlAsString() {
        if (id.toString().isEmpty()) {
            return null;
        }
        try {
            return new URL(this.getBasePublicUrl() + this.bucket + "/" + this.getActualFileName()).toString();
        } catch (MalformedURLException e) {
            return null;
        }
    }

    /**
     * Get the actual file name.
     *
     * @return The actual file name
     * @since 15.10
     */
    private String getActualFileName() {
        if (this.subDirectory == null || this.subDirectory.isEmpty()) {
            return String.format("%s", this.id);
        }
        return String.format("%s/%s", this.subDirectory, this.id);
    }

    /**
     * Save the current object. The file will be uploaded to S3 bucket.
     *
     * @since 15.10
     */
    @Override
    public void save() {
        if (this.s3module == null) {
            this.s3module = Play.application().injector().instanceOf(S3Module.class);
        }
        if (this.s3module == null || this.s3module.amazonS3 == null) {
            Logger.error("Could not save S3 file because amazonS3 variable is null");
            throw new RuntimeException("Could not save");
        } else {
            this.bucket = this.s3module.s3Bucket;
            if (this.subDirectory == null) {
                this.subDirectory = "";
            }
            this.subDirectory = this.subDirectory.trim();

            // Save object to get an ID
            super.save();

            // Set cache control and server side encryption
            ObjectMetadata objMetaData = new ObjectMetadata();
            objMetaData.setContentType(this.contentType);
            objMetaData.setCacheControl("max-age=315360000, public");
            objMetaData.setSSEAlgorithm(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);

            // Upload file to S3
            PutObjectRequest putObjectRequest = new PutObjectRequest(this.bucket, this.getActualFileName(), this.file);
            putObjectRequest.withCannedAcl(this.isPrivate ? CannedAccessControlList.Private : CannedAccessControlList.PublicRead);
            putObjectRequest.setMetadata(objMetaData);
            this.s3module.amazonS3.putObject(putObjectRequest);
        }
    }

    /**
     * Delete the remote file.
     *
     * @since 15.10
     */
    protected void deleteRemoteFile() {
        if (this.s3module == null) {
            this.s3module = Play.application().injector().instanceOf(S3Module.class);
        }
        if (this.s3module == null || this.s3module.amazonS3 == null) {
            Logger.error("Could not delete S3 file because amazonS3 variable is null");
            throw new RuntimeException("Could not delete");
        } else {
            this.s3module.amazonS3.deleteObject(bucket, getActualFileName());
        }
    }

    /**
     * Delete the current object. The file will be deleted from the S3 bucket.
     *
     * @since 15.10
     */
    @Override
    public void delete() {
        this.deleteRemoteFile();
        super.delete();
    }

    /**
     * Get the file content. In case of error (network error, file not
     * found, ...), this method will return null.
     *
     * @return The file content, otherwise, null
     * @see InputStream
     * @since 15.10
     */
    public InputStream getFileContent() {
        if (this.s3module == null) {
            this.s3module = Play.application().injector().instanceOf(S3Module.class);
        }
        final S3Object obj = this.s3module.amazonS3.getObject(bucket, getActualFileName());
        if (obj != null) {
            return obj.getObjectContent();
        }
        return null;
    }
}
