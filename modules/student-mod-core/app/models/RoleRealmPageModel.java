/*
 * Copyright (C) 2014 - 2015 PayinTech, SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package models;

import play.mvc.PathBindable;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * RoleRealmPageModel.
 *
 * @author Olivier Buiron
 * @version 15.12
 * @since 15.12
 */
@Entity
@Table(name = "security_role_page")
public class RoleRealmPageModel extends RoleModel implements PathBindable<RoleRealmPageModel>, RoleWithAccountsModel {

    /**
     * Helper to request model.
     */
    public static final Find<Long, RoleRealmPageModel> find = new Finder<>(RoleRealmPageModel.class);

    /**
     * Page associated
     */
    @ManyToOne(fetch = FetchType.LAZY, optional = true, cascade = CascadeType.REFRESH)
    @Column(name = "page", nullable = false, unique = false)
    private RealmPageModel page;

    /**
     * List of permissions associated to the role.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "security_role_page_has_permission")
    private List<PermissionModel> permissions;

    /**
     * List of account with page role.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "security_role_page_has_account")
    private List<AccountModel> accounts;
    /**
     * Name of the role.
     */
    @Size(max = 25)
    @Column(name = "name", nullable = false, unique = false)
    private String name;

    /**
     * Build a basic page role.
     *
     * @since 15.12
     */
    public RoleRealmPageModel() {
        super();
    }

    /**
     * Build a page role.
     *
     * @param page The page to associate
     * @since 15.12
     */
    public RoleRealmPageModel(final RealmPageModel page) {
        super();
        this.page = page;
    }

    /**
     * Get the page associated to the role.
     *
     * @return The page
     * @since 15.12
     */
    public RealmPageModel getPage() {
        return this.page;
    }

    /**
     * Set the page associated to this page.
     *
     * @param page The page to associate
     * @since 15.12
     */
    public void setPage(final RealmPageModel page) {
        this.page = page;
    }

    /**
     * Get all accounts attached to this role.
     *
     * @return A list of accounts reference
     * @see AccountModel
     * @since 15.12
     */
    @Override
    public List<AccountModel> getAccounts() {
        return this.accounts;
    }

    /**
     * Set the accounts list.
     *
     * @param accounts The accounts to set
     * @since 15.12
     */
    @Override
    public void setAccounts(List<AccountModel> accounts) {
        this.accounts = accounts;
    }

    /**
     * Add new account to the existing list.
     *
     * @param account Account to add
     * @since 15.12
     */
    @Override
    public void addAccount(AccountModel account) {
        this.accounts.add(account);
    }

    /**
     * Add new account to the existing list.
     *
     * @param accounts Accounts to add
     * @since 15.12
     */
    @Override
    public void addAccounts(List<AccountModel> accounts) {
        this.accounts.addAll(accounts);
    }

    /**
     * Remove account from the existing list.
     *
     * @param account Account to remove
     * @since 15.12
     */
    @Override
    public void removeAccount(AccountModel account) {
        this.accounts.remove(account);
    }

    /**
     * Remove accounts from the existing list.
     *
     * @param accounts Accounts to remove
     * @since 15.12
     */
    @Override
    public void removeAccounts(List<AccountModel> accounts) {
        this.accounts.removeAll(accounts);
    }

    /**
     * Get all permissions attached to this role.
     *
     * @return A list of permissions reference
     * @see PermissionModel
     * @since 15.10
     */
    @Override
    public List<PermissionModel> getPermissions() {
        return this.permissions;
    }

    /**
     * Set the permissions list.
     *
     * @param permissions The permissions to set
     * @since 15.10
     */
    @Override
    public void setPermissions(List<PermissionModel> permissions) {
        this.permissions = permissions;
    }

    /**
     * Add new permissions to the existing list.
     *
     * @param permissions Permissions to add
     * @since 15.10
     */
    @Override
    public void addPermissions(List<PermissionModel> permissions) {
        this.permissions.addAll(permissions);
    }

    /**
     * Bind an URL path parameter.
     *
     * @return A {@code RoleRealmPageModel} in case of success
     */
    @Override
    public RoleRealmPageModel bind(String key, String value) {
        final RoleRealmPageModel role = RoleRealmPageModel.find.where().like("uid", value).findUnique();
        if (role == null) {
            throw new IllegalArgumentException("404");
        }
        return role;
    }

    /**
     * Unbind a URL path parameter.
     *
     * @return A String
     */
    @Override
    public String unbind(String key) {
        return this.getUidAsString();
    }

    /**
     * Javascript function to unbind in the Javascript router.
     *
     * @return A String
     */
    @Override
    public String javascriptUnbind() {
        return "function(k,v) {\n" +
                "    return v.getUidAsString();" +
                "}";
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(final String name) {
        this.name = name.trim();
    }
}
