STUDENT MODULE : CORE
=================

0x01 Configuration
------------------
  Pour fonctionner, ce module a besoin d'être correctement configuré. Vous
  trouverez ci-dessous un exemple de configuration à intégrer dans le fichier
  _application.conf_ de votre projet Play (version 2.4.x et supérieur).
  
    # Database configuration
    # ~~~~~
    db.default.driver   = "org.h2.Driver"
    db.default.url      = "jdbc:h2:mem:play;DATABASE_TO_UPPER=false;MODE=MYSQL;"
    db.default.username = "sa"
    db.default.password = ""
    #db.default.driver   = "com.mysql.jdbc.Driver"
    #db.default.url      = "jdbc:mysql://localhost/student?useLegacyDatetimeCode=false&serverTimezone=UTC"
    #db.default.username = "root"
    #db.default.password = ""
    
    # Evolution
    # ~~~~~
    # play.evolutions.db.core.enabled=false
  
    # Amazon S3 Plugin
    # ~~~~~
    aws.s3.endpoint  = "http://0xbaadf00d.com:4567"
    aws.s3.bucket    = "com.payintech.student-develop"
    aws.s3.puburl    = "http://0xbaadf00d.com:4567"
    aws.s3.pathstyle = true
    aws.access.key   = "foo"
    aws.secret.key   = "bar"


0x02 A propos de la base de données
-----------------------------------
  Ce module fourni un ensemble de modèles qui utilisent la base de données
  "core". Si vous deviez effectuer des requêtes en dures (raw queries), faites
  attention à bien utiliser la bonne base de données.
  
    EbeanServer secondary = Ebean.getServer("core");
    secondary.find(models.Account.class).findList();

  Si vous utilisez le helper _find()_ fourni dans chaque modèle, vous n'avez
  pas besoin de vous occuper de la base de données à utiliser.
