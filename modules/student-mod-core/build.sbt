name := "student-mod-core"

version := "16.04-SNAPSHOT"

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  cache,
  evolutions,
  javaWs,
  "mysql" % "mysql-connector-java" % "5.1.37",
  "com.amazonaws" % "aws-java-sdk" % "1.10.27",
  "com.sendgrid" % "sendgrid-java" % "2.2.2",
  "redis.clients" % "jedis" % "2.7.3",
  "org.graylog2" % "gelfclient" % "1.3.0",
  "org.apache.pdfbox" % "pdfbox" % "1.8.10",
  "com.google.zxing" % "core" % "3.2.1",
  "com.google.zxing" % "javase" % "3.2.1",
  "org.elasticsearch" % "elasticsearch" % "1.7.2",
  "com.googlecode.libphonenumber" % "libphonenumber" % "7.1.1",
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-csv" % "2.6.4"

)

playEbeanModels in Compile := Seq("models.*")

sources in (Compile,doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false