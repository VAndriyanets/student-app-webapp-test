# --- !Ups

# ---- PERMISSIONS
INSERT INTO security_permission (id, namespace, scope) VALUES(1, "ADMIN", "account.add") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "account.add";
INSERT INTO security_permission (id, namespace, scope) VALUES(2, "ADMIN", "account.update") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "account.update";
INSERT INTO security_permission (id, namespace, scope) VALUES(3, "ADMIN", "account.delete") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "account.delete";
INSERT INTO security_permission (id, namespace, scope) VALUES(4, "ADMIN", "realm.add") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "realm.add";
INSERT INTO security_permission (id, namespace, scope) VALUES(5, "ADMIN", "realm.update") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "realm.update";
INSERT INTO security_permission (id, namespace, scope) VALUES(6, "ADMIN", "realm.delete") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "realm.delete";
INSERT INTO security_permission (id, namespace, scope) VALUES(7, "ADMIN", "page.add") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "page.add";
INSERT INTO security_permission (id, namespace, scope) VALUES(8, "ADMIN", "page.update") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "page.update";
INSERT INTO security_permission (id, namespace, scope) VALUES(9, "ADMIN", "page.delete") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "page.delete";
INSERT INTO security_permission (id, namespace, scope) VALUES(10, "ADMIN", "group.add") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "group.add";
INSERT INTO security_permission (id, namespace, scope) VALUES(11, "ADMIN", "group.update") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "group.update";
INSERT INTO security_permission (id, namespace, scope) VALUES(12, "ADMIN", "group.delete") ON DUPLICATE KEY UPDATE namespace="ADMIN", scope = "group.delete";
INSERT INTO security_permission (id, namespace, scope) VALUES(13, "REALM", "network.setting") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "network.setting";
INSERT INTO security_permission (id, namespace, scope) VALUES(14, "REALM", "network.role") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "network.role";
INSERT INTO security_permission (id, namespace, scope) VALUES(15, "REALM", "user.invite") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "user.invite";
INSERT INTO security_permission (id, namespace, scope) VALUES(16, "REALM", "user.revoke") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "user.revoke";
INSERT INTO security_permission (id, namespace, scope) VALUES(17, "REALM", "user.activity") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "user.activity";
INSERT INTO security_permission (id, namespace, scope) VALUES(18, "REALM", "user.block") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "user.block";
INSERT INTO security_permission (id, namespace, scope) VALUES(19, "REALM", "user.bulk") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "user.bulk";
INSERT INTO security_permission (id, namespace, scope) VALUES(20, "REALM", "social.page") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "social.page";
INSERT INTO security_permission (id, namespace, scope) VALUES(21, "REALM", "social.group") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "social.group";
INSERT INTO security_permission (id, namespace, scope) VALUES(22, "REALM", "content.monitor") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "content.monitor";
INSERT INTO security_permission (id, namespace, scope) VALUES(23, "REALM", "content.export") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "content.export";
INSERT INTO security_permission (id, namespace, scope) VALUES(24, "REALM", "analytics.dashboard") ON DUPLICATE KEY UPDATE namespace="REALM", scope = "analytics.dashboard";


# ---- Create Admin Role
TRUNCATE security_role_admin_has_account;
TRUNCATE security_role_admin_has_permission;
DELETE FROM security_role_admin;
INSERT INTO security_role_admin (id, uid, name) VALUES(1, "0505f4ff-08cc-4b3d-b9d4-22662cf11785", "Administrateur") ON DUPLICATE KEY UPDATE uid = "0505f4ff-08cc-4b3d-b9d4-22662cf11785", name = "Administrateur";
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 1) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 1;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 2) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 2;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 3) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 3;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 4) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 4;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 5) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 5;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 6) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 6;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 7) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 7;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 8) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 8;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 9) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 9;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 10) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 10;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 11) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 11;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 12) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 12;
INSERT INTO security_role_admin_has_permission (security_role_admin_id, security_permission_id) VALUES(1, 13) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, security_permission_id = 13;


# ---- REALM - PAYINTECH
INSERT INTO realm (id, uid, slug, name, logo_id, banner_id, create_date) VALUES(1, "9584f65f-bb1c-4521-8b43-e49c20de0e31", "payintech.com", "PayinTech", NULL, NULL, "2015-10-05 14:10:22") ON DUPLICATE KEY UPDATE uid = "9584f65f-bb1c-4521-8b43-e49c20de0e31", slug = "payintech.com", name = "PayinTech";


# ---- DOMAIN WHITELIST - PAYINTECH
INSERT INTO security_realmdomain_whitelist(id, uid, domain, realm) VALUES(1, "45f7d1f6-6cf5-4982-977e-0865bca95477", "payintech.com", 1) ON DUPLICATE KEY UPDATE uid = "45f7d1f6-6cf5-4982-977e-0865bca95477", domain = "payintech.com", realm = 1;
INSERT INTO security_realmdomain_whitelist(id, uid, domain, realm) VALUES(2, "2c97e828-823e-4d86-954e-8f226e24d5b6", "mybeestudent.com", 1) ON DUPLICATE KEY UPDATE uid = "2c97e828-823e-4d86-954e-8f226e24d5b6", domain = "mybeestudent.com", realm = 1;
INSERT INTO security_realmdomain_whitelist(id, uid, domain, realm) VALUES(3, "de8241fe-5b1a-413d-85bd-3495e3fc40d9", "mybee-events.com", 1) ON DUPLICATE KEY UPDATE uid = "de8241fe-5b1a-413d-85bd-3495e3fc40d9", domain = "mybee-events.com", realm = 1;
INSERT INTO security_realmdomain_whitelist(id, uid, domain, realm) VALUES(4, "c9b1a1b1-1c6a-46b6-8964-fac0380618fe", "my-bee.fr", 1) ON DUPLICATE KEY UPDATE uid = "c9b1a1b1-1c6a-46b6-8964-fac0380618fe", domain = "my-bee.fr", realm = 1;


# ---- REALM - STALAG 13
INSERT INTO realm (id, uid, slug, name, logo_id, banner_id, create_date) VALUES(2, "fb8cf7fa-77f0-479e-b654-3a8c6efd5907", "stalag13", "Stalag 13", NULL, NULL, "2015-10-05 14:10:22") ON DUPLICATE KEY UPDATE uid = "fb8cf7fa-77f0-479e-b654-3a8c6efd5907", slug = "stalag13", name = "Stalag 13";


# ---- Create Realm Role
TRUNCATE security_role_realm_has_account;
TRUNCATE security_role_realm_has_permission;
DELETE FROM security_role_realm;
INSERT INTO security_role_realm (id, uid, name, realm_id, readonly) VALUES( 1, "a6ed431d-5ec9-4518-8043-c56c5b68aff5", "Administrateur", 1, true) ON DUPLICATE KEY UPDATE uid = "a6ed431d-5ec9-4518-8043-c56c5b68aff5", name = "Administrateur", realm_id = 1;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 13) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 13;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 14) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 14;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 15) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 15;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 16) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 16;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 17) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 17;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 18) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 18;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 19) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 19;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 20) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 20;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 21) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 21;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 22) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 22;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 23) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 23;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(1, 24) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, security_permission_id = 24;
INSERT INTO security_role_realm (id, uid, name, realm_id, readonly) VALUES(2, "65a1e75d-9642-447e-ae05-23760ccddf13", "Account Manager", 1, true) ON DUPLICATE KEY UPDATE uid = "65a1e75d-9642-447e-ae05-23760ccddf13", name = "Account Manager", realm_id = 1;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(2, 15) ON DUPLICATE KEY UPDATE security_role_realm_id = 2, security_permission_id = 15;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(2, 16) ON DUPLICATE KEY UPDATE security_role_realm_id = 2, security_permission_id = 16;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(2, 17) ON DUPLICATE KEY UPDATE security_role_realm_id = 2, security_permission_id = 17;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(2, 18) ON DUPLICATE KEY UPDATE security_role_realm_id = 2, security_permission_id = 18;
INSERT INTO security_role_realm_has_permission (security_role_realm_id, security_permission_id) VALUES(2, 19) ON DUPLICATE KEY UPDATE security_role_realm_id = 2, security_permission_id = 19;


# ---- USER ACCOUNT 'Charlie root' (1) - DEFAULT PASSWORD : 123456789
INSERT INTO actor_profile (id, name) VALUES(1, "Charlie Root") ON DUPLICATE KEY UPDATE name = "Charlie Root";
INSERT INTO actor (id, slug, profile_id) VALUES(1, "charlie.root", 1) ON DUPLICATE KEY UPDATE slug = "charlie.root", profile_id = 1;
INSERT INTO account (id, uid, password, status, actor_id, last_login, create_date, lang, timezone, first_name, last_name) VALUES(1, "16ae46f5-7aab-4384-8d9e-f7ccef2fcb44", "pbkdf2_hmacsha256$10000$3904dac980cd60fd$8e466ffa2490e9c67f4685d84eff6dc9356cc3c0", 1, 1, NULL, "2015-10-05 14:10:22", "fr", "Europe/Paris", "Charlie", "Root") ON DUPLICATE KEY UPDATE uid = "16ae46f5-7aab-4384-8d9e-f7ccef2fcb44", password = "pbkdf2_hmacsha256$10000$3904dac980cd60fd$8e466ffa2490e9c67f4685d84eff6dc9356cc3c0", first_name = "Charlie", last_name = "Root";
INSERT INTO account_email (id, account_id, email, is_primary, is_validated) VALUES(1, 1, "root@payintech.com", 1, 1) ON DUPLICATE KEY UPDATE email = "root@payintech.com";


# ---- ADD ROOT ACCOUNT TO "Administrateur" GROUP ON ADMIN + REALM
INSERT INTO security_role_admin_has_account (security_role_admin_id, account_id) VALUES(1, 1) ON DUPLICATE KEY UPDATE security_role_admin_id = 1, account_id = 1;
INSERT INTO security_role_realm_has_account (security_role_realm_id, account_id) VALUES(1, 1) ON DUPLICATE KEY UPDATE security_role_realm_id = 1, account_id = 1;
INSERT INTO realm_has_account (realm_id, account_id) VALUES(1, 1) ON DUPLICATE KEY UPDATE account_id = 1;
INSERT INTO realm_has_account (realm_id, account_id) VALUES(2, 1) ON DUPLICATE KEY UPDATE account_id = 1;


# ---- REALM - Login Picture
INSERT INTO login_picture (id, picture_id, title, content, link) VALUES(1, null, "IÉSEG School of Management", "L’IÉSEG School of Management est une grande école et un établissement associatif d’enseignement supérieur enseignant le commerce et la gestion.", "https://fr.wikipedia.org/wiki/I%C3%89SEG_School_of_Management") ON DUPLICATE KEY UPDATE title = "IÉSEG School of Management", content = "L’IÉSEG School of Management est une grande école et un établissement associatif d’enseignement supérieur enseignant le commerce et la gestion.", link = "https://fr.wikipedia.org/wiki/I%C3%89SEG_School_of_Management";
INSERT INTO login_picture (id, picture_id, title, content, link) VALUES(2, null, "EPITECH", "L’École pour l’informatique et les nouvelles technologies (Epitech) aussi appelée European Institute of Information Technology, est un établissement d’enseignement privé français créé en 1999 par le groupe IONIS qui délivre un enseignement supérieur en informatique et nouvelles technologies.", "https://fr.wikipedia.org/wiki/%C3%89cole_pour_l%27informatique_et_les_nouvelles_technologies") ON DUPLICATE KEY UPDATE title = "EPITECH", content = "L’École pour l’informatique et les nouvelles technologies (Epitech) aussi appelée European Institute of Information Technology, est un établissement d’enseignement privé français créé en 1999 par le groupe IONIS qui délivre un enseignement supérieur en informatique et nouvelles technologies.", link = "https://fr.wikipedia.org/wiki/%C3%89cole_pour_l%27informatique_et_les_nouvelles_technologies";


# ---- DOMAIN BLACKLIST
INSERT INTO security_domain_blacklist (id, domain) VALUES(1, "yopmail") ON DUPLICATE KEY UPDATE `domain` = "yopmail";
INSERT INTO security_domain_blacklist (id, domain) VALUES(2, "hotmail.de") ON DUPLICATE KEY UPDATE `domain` = "hotmail.de";
INSERT INTO security_domain_blacklist (id, domain) VALUES(3, "facebook") ON DUPLICATE KEY UPDATE `domain` = "facebook";
INSERT INTO security_domain_blacklist (id, domain) VALUES(4, "gmail.fr") ON DUPLICATE KEY UPDATE `domain` = "gmail.fr";
INSERT INTO security_domain_blacklist (id, domain) VALUES(5, "ymail") ON DUPLICATE KEY UPDATE `domain` = "ymail";
INSERT INTO security_domain_blacklist (id, domain) VALUES(6, "laposte.net") ON DUPLICATE KEY UPDATE `domain` = "laposte.net";
INSERT INTO security_domain_blacklist (id, domain) VALUES(7, "jetable.fr.nf") ON DUPLICATE KEY UPDATE `domain` = "jetable.fr.nf";
INSERT INTO security_domain_blacklist (id, domain) VALUES(8, "yahoo") ON DUPLICATE KEY UPDATE `domain` = "yahoo";
INSERT INTO security_domain_blacklist (id, domain) VALUES(9, "gmai.") ON DUPLICATE KEY UPDATE `domain` = "gmai.";
INSERT INTO security_domain_blacklist (id, domain) VALUES(10, "mybee") ON DUPLICATE KEY UPDATE `domain` = "mybee";
INSERT INTO security_domain_blacklist (id, domain) VALUES(11, "edhec.con") ON DUPLICATE KEY UPDATE `domain` = "edhec.con";
INSERT INTO security_domain_blacklist (id, domain) VALUES(12, "orange.fe") ON DUPLICATE KEY UPDATE `domain` = "orange.fe";
INSERT INTO security_domain_blacklist (id, domain) VALUES(13, "hotmail.fe") ON DUPLICATE KEY UPDATE `domain` = "hotmail.fe";
INSERT INTO security_domain_blacklist (id, domain) VALUES(14, "esc-rennes.student.com") ON DUPLICATE KEY UPDATE `domain` = "esc-rennes.student.com";
INSERT INTO security_domain_blacklist (id, domain) VALUES(15, "student.esc-rennes.fr") ON DUPLICATE KEY UPDATE `domain` = "student.esc-rennes.fr";
INSERT INTO security_domain_blacklist (id, domain) VALUES(16, "remibarbe.de") ON DUPLICATE KEY UPDATE `domain` = "remibarbe.de";

INSERT INTO security_permission (id, namespace, scope) VALUES (25, 'PAGE', 'page.grant.roles');
INSERT INTO security_permission (id, namespace, scope) VALUES (26, 'PAGE', 'page.request.bank.transfert');

# ---- PAGE
INSERT INTO realm_pages (`id`, `uid`, `slug`, `name`, `create_date`) VALUES (1, "2f0b52f8-84c7-4f14-b00e-534aada709b1", "testpage", "Test page", "2015-10-06 14:10:22");
INSERT INTO realm_has_page (`realm_pages_id`, `realm_id`) VALUES (1, 1);
INSERT INTO security_role_page (`id`, `uid`, `readonly`, `page_id`, `name`) VALUES ('1', '8cb16e16-cbbf-4a1f-8390-b289a6a5305d', '1', '1', 'Administrator');
INSERT INTO security_role_page (`id`, `uid`, `readonly`, `page_id`, `name`) VALUES ('2', 'cfac97b4-c954-40cf-b74f-c0f86d126dcd', '0', '1', 'Vendor');

INSERT INTO security_role_page_has_permission (`security_role_page_id`, `security_permission_id`) VALUES (1, 25);
INSERT INTO security_role_page_has_permission (`security_role_page_id`, `security_permission_id`) VALUES (1, 26);

INSERT INTO security_role_page_has_account (`security_role_page_id`, `account_id`) VALUES (2, 1);

INSERT INTO store_article (`id`, `uid`, `create_date`, `name`, `description`, `logo_id`, `price`, `vendor_page_id`, `status`) VALUES (1, "3eb94a5b-c564-4ada-9ffb-7d76926d0e2d","2015-10-06 14:12:22", "Test article 1","Some test article 1 description", NULL, 123.5, 1, "DISABLED");
INSERT INTO store_article (`id`, `uid`, `create_date`, `name`, `description`, `logo_id`, `price`, `vendor_page_id`, `status`) VALUES (2, "c425cddc-1aef-46e1-a1ee-819dd7413271","2015-10-06 14:13:22", "Test article 2","Some test article 2 description", NULL, 321.5, 1, "SALES_OPEN");
INSERT INTO store_article (`id`, `uid`, `create_date`, `name`, `description`, `logo_id`, `price`, `vendor_page_id`, `status`) VALUES (3, "5a41c791-c0e1-4df9-b8eb-c2a79ca29502","2015-10-06 14:14:22", "Test article 3","Some test article 3 description", NULL, 123.4, 1, "SALES_CLOSED");
INSERT INTO store_article (`id`, `uid`, `create_date`, `name`, `description`, `logo_id`, `price`, `vendor_page_id`, `status`) VALUES (4, "2ac9aec7-47e2-4ee9-8ab3-0c20c1c7a0fe","2015-10-06 14:11:22", "Test article 4","Some test article 4 description", NULL, 1240, 1, "SALES_OPEN");
INSERT INTO store_article (`id`, `uid`, `create_date`, `name`, `description`, `logo_id`, `price`, `vendor_page_id`, `status`) VALUES (5, "b4681d93-1f28-4ff8-a4b0-e0dd0ed2ee17","2015-10-06 14:15:22", "Test article 5","Some test article 5 description", NULL, 123.5, 1, "DISABLED");

INSERT INTO `article_options` (`option_type`, `id`, `name`, `article_id`, `argument`) VALUES ('SELECT', '-2', 'burger', '5', 'b1,b2,b3');
INSERT INTO `article_options` (`option_type`, `id`, `name`, `article_id`) VALUES ('TEXT', '-3', 'N° de Téléphone', '5');
INSERT INTO `article_options` (`option_type`, `id`, `name`, `article_id`, `argument`) VALUES ('MULTISELECT', '-4', 'Choix boisson', '5', 'Coca,Coca lignt,Coca zéro,Sprite,Cristaline pétillante');
INSERT INTO `article_options` (`option_type`, `id`, `name`, `article_id`) VALUES ('CHECKBOX', '-5', '5 bières achetées 1 offerte : Reduc pour 5 bières achetées', '5');

INSERT INTO `facebook_data` (`id`, `uid`, `fbid`, `marital_status`, `picture_url`, `account_id`) VALUES ('-1', 'a074edc5-6c4b-464d-94f8-2e8233517456', '718893818211547', 'In a relationship', 'https://scontent.xx.fbcdn.net/hprofile-xfa1/v/t1.0-1/c65.65.814.814/s50x50/305536_298556930245240_1328913653_n.jpg?oh=ab285697a9f6c949d5f71eb780285614&oe=5790D8A3', '1');

INSERT INTO mobile_app (id, application_id, app_status) VALUES (1,'10a65878-a462-41c9-ab50-ea2c2a4398c6', 'ACTIVE');

# ---- DEFAULT CARD TYPES

INSERT INTO `card_type` (`id`, `name`, `validation_pattern`) VALUES (1, 'visa', '^4[0-9]{12}(?:[0-9]{3})?');
INSERT INTO `card_type` (`id`, `name`, `validation_pattern`) VALUES (2, 'mastercard', '^5[1-5][0-9]{14}');