# PlayFramework
error.required  = Ce champ est obligatoire
error.minLength = La longueur minimale est de {0}
error.maxLength = La longueur maximale est de {0}
error.email     = Addresse électronique valide requise
error.min       = Doit être supérieur ou égal à {0}
error.max       = Doit être inférieure ou égale à {0}
error.pattern   = Doit correspondre à {0}
error.invalid   = La valeur est invalide


# Common
MYBEE.STUDENT        = MyBee Student
FORM.BUTTON.SAVE     = Sauvegarder
FORM.BUTTON.CANCEL   = Annuler
FORM.BUTTON.OK       = OK
FORM.BUTTON.VALID    = Valider
FORM.BUTTON.ADD      = Ajouter
FORM.FLASH.ERRORS    = Veuillez vérifier les informations que vous avez saisies
COMMON.TITLE.NOTE    = Note
COMMON.TITLE.WARNING = Attention
COMMON.USERS         = Utilisateur(s)
COMMON.USER.MAIL     = Adresse électronique
COMMON.LABEL.EMAIL   = Email
COMMON.ARTICLE.DETAILS = Details

COMMON.VIEW.MORE = View more...

# General Errors
ERROR.COMMON.SOMETHING.GOES.WRONG   = Quelque chose ne s'est pas passée comme prévue, veuillez ressayer ultérieurement
ERROR.SEARCH.OFFLINE                = Le moteur de recherche est actuellement hors ligne. Veuillez réessayer plus tard !
ERROR.SEARCH.EXECUTION              = Syntaxe de recherche incorrecte. Veuillez modifier les paramètres de votre recherche
ERROR.FORM.PASSWORD_MISMATCH        = Vos mots de passe ne correspondent pas
ERROR.FORM.EMAIL_MISMATCH           = Vos adresses électroniques ne correspondent pas
ERROR.FORM.EMAIL_BLACKLISTED        = Le nom de domaine de ce courrier électronique a été interdit
ERROR.FORM.EMAIL_ALREADY_EXIST      = Cette adresse électronique est déjà utilisée
ERROR.FORM.PASSWORD_WEAK            = Votre mot de passe est trop faible. Celui-ci doit contenir au moins 6 chiffres, lettres et caractères spéciaux (tels que & et %)
ERROR.FORM.NOT.UUID                 = Ceci n''est pas un uid valide
ERROR.FORM.INVALID.FILE.TYPE        = Ce fichier n’est pas un {0} ({1}) valide
ERROR.FORM.FILE                     = Une erreur est survenue avec ce fichier


# Authentication
AUTHENTICATION.TITLE                       = Ouverture de session
AUTHENTICATION.FORM.LABEL.USERNAME         = Nom d’utilisateur
AUTHENTICATION.FORM.LABEL.PASSWORD         = Mot de passe
AUTHENTICATION.FORM.ERROR.CREDENTIAL       = Mauvais nom d’utilisateur ou le mot de passe est incorrect
AUTHENTICATION.BUTTON.LOGIN                = Se connecter
AUTHENTICATION.LINK.RESET_PASSWORD         = Vous avez oublié votre mot de passe ?
AUTHENTICATION.FLASH.DISCONNECTED          = À bientôt sur MyBee Student
AUTHENTICATION.FLASH.ACCOUNT_NOT_VALIDATED = Ce compte utilisateur n’est pas validé
AUTHENTICATION.FLASH.ACCOUNT_FROZEN        = Ce compte utilisateur est désactivé


# Google ReCAPTCHA
RECAPTCHA.ERROR.MISSING_INPUT_SECRET   = Le paramètre secret est requis
RECAPTCHA.ERROR.INVALID_INPUT_SECRET   = Le paramètre secret est invalide ou malformé
RECAPTCHA.ERROR.MISSING_INPUT_RESPONSE = Le captcha doit être renseigné
RECAPTCHA.ERROR.INVALID_INPUT_RESPONSE = Le captcha doit être renseigné


# Lost Password
LOSTPASSWORD.TITLE                 = Modifier votre mot de passe
LOSTPASSWORD.FORM.LABEL.EMAIL      = Quelle est l’adresse électronique de votre compte ?
LOSTPASSWORD.BUTTON.RESET_PASSWORD = Continuer
LOSTPASSWORD.TEXT.SUCCESS          = Si l’adresse saisie est correcte, vous allez recevoir prochainement un courrier électronique contenant les instructions pour réinitialiser votre mot de passe
LOSTPASSWORD.TEXT.EMAIL_SENT       = Courrier envoyé.
LOSTPASSWORD.LINK.NOT_RECEIVED     = Pas reçu ?
LOSTPASSWORD.EMAIL.SUBJECT         = Réinitialisation de votre mot de passe
LOSTPASSWORD.EMAIL.TITLE           = Votre nouveau mot de passe MyBee Student vous attend
LOSTPASSWORD.EMAIL.CONTENT_1       = Vous avez récemment demandé à réinitialiser votre mot de passe. Veuillez cliquer sur le bouton ci-dessous afin de commencer le processus de réinitialisation.
LOSTPASSWORD.EMAIL.CONTENT_2       = Si vous n’avez pas demandé un changement de mot de passe, vous pouvez ignorer ce message et votre mot de passe restera inchangé.
LOSTPASSWORD.EMAIL.BUTTON          = Changer de mot de passe


# Reset Password
RESETPASSWORD.TITLE                       = Choisissez votre nouveau mot de passe
RESETPASSWORD.FORM.LABEL.PASSWORD         = Nouveau mot de passe
RESETPASSWORD.FORM.LABEL.PASSWORD_CONFIRM = Confirmation du nouveau mot de passe
RESETPASSWORD.FORM.LABEL.VALIDATE         = Valider
RESETPASSWORD.SUCCESS                     = Votre mot de passe a été changé avec succès.


# Registration
REGISTRATION.TITLE                         = Inscription
REGISTRATION.BUTTON.VALIDATE               = Inscription
REGISTRATION.FORM.LABEL.FIRST_NAME         = Prénom
REGISTRATION.FORM.LABEL.LAST_NAME          = Nom de famille
REGISTRATION.FORM.LABEL.EMAIL              = Adresse électronique
REGISTRATION.FORM.LABEL.EMAIL_CONFIRM      = Confirmation de l’adresse électronique
REGISTRATION.FORM.LABEL.PASSWORD           = Mot de passe
REGISTRATION.FORM.LABEL.PASSWORD_CONFIRM   = Confirmation du mot de passe
REGISTRATION.FORM.LABEL.TOS                = En cliquant sur Inscription, vous acceptez nos <a href="/policies">Conditions</a> et indiquez que vous avez lu notre <a href="">Politique d’utilisation des données</a>, y compris notre utilisation des <a href="">cookies</a>.
REGISTRATION.FORM.TOOLTIP.FIRSTNAME        = Quel est votre prénom ?
REGISTRATION.FORM.TOOLTIP.LASTNAME         = Quel est votre nom de famille ?
REGISTRATION.FORM.TOOLTIP.EMAIL            = Vous en aurez besoin pour vous connectez ou si vous devez réinitialiser votre mot de passe
REGISTRATION.FORM.TOOLTIP.EMAIL_CONFIRM    = Veuillez ressaisir votre adresse électronique
REGISTRATION.FORM.TOOLTIP.PASSWORD         = Saisissez un mot de passe contenant au moins six chiffres, lettres et caractères spéciaux (tels que & et %)
REGISTRATION.FORM.TOOLTIP.PASSWORD_CONFIRM = Veuillez ressaisir votre mot de passe
REGISTRATION.FLASH.SUCCESS                 = Votre compte a été crée. Vous allez recevoir un courrier électronique de confirmation. Vous devez valider votre adresse électronique avant de pouvoir vous connecter.
REGISTRATION.EMAIL.SUBJECT                 = Validez votre compte
REGISTRATION.EMAIL.TITLE                   = Validation du compte
REGISTRATION.EMAIL.CONTENT                 = Vous êtes à un pas d’accéder à MyBee Student. Il ne vous reste qu’à valider votre compte en cliquant sur le bouton ci-dessous.
REGISTRATION.EMAIL.BUTTON                  = Valider mon compte
REGISTRATION.VALIDATION.FLASH.INVALID      = Ce lien de validation a expiré.
REGISTRATION.VALIDATION.FLASH.SUCCESS      = Votre compte est maintenant validé. Vous pouvez vous connecter.


# Realm
REALM.ERROR.NOT_ENROLLED = Votre compte utilisateur n’est pas autorisé sur ce réseau


# Realm Selector
REALM.SELECTOR.INVITE.TITLE         = Rejoindre un domaine
REALM.SELECTOR.INVITE.CONTENT       = Vous avez reçu un email contenant un code d'invitation pour rejoindre le domaine
REALM.SELECTOR.INVITE.TOKEN.LABEL   = Code d'invitation
REALM.SELECTOR.INVITE.SUCCESS       = Vous avez été ajouté à ''{0}''
REALM.SELECTOR.INVITE.FAIL          = Ce code d'invitation n'existe pas ou n'est plus valide


# Realm Administration Menu
REALM.ADMIN.MENU.TITLE.NETWORK            = Réseau
REALM.ADMIN.MENU.NETWORK.CONFIGURATION    = Configuration
REALM.ADMIN.MENU.NETWORK.ROLES            = Rôles
REALM.ADMIN.MENU.TITLE.USERS              = Utilisateurs
REALM.ADMIN.MENU.USERS.INVITE_USERS       = Inviter utilisateurs
REALM.ADMIN.MENU.USERS.REMOVE_USERS       = Supprimer utilisateurs
REALM.ADMIN.MENU.USERS.ACCOUNTS_ACTIVITY  = Activité des comptes
REALM.ADMIN.MENU.USERS.BLOCK_USERS        = Bloquer utilisateurs
REALM.ADMIN.MENU.USERS.BULK_UPDATE_USERS  = Mise à jour de masse des utilisateurs
REALM.ADMIN.MENU.TITLE.SOCIAL             = Social
REALM.ADMIN.MENU.SOCIAL.MANAGE_GROUPS     = Gestion de groupes
REALM.ADMIN.MENU.SOCIAL.MANAGE_PAGES      = Gestion de pages
REALM.ADMIN.MENU.TITLE.CONTENT            = Contenu
REALM.ADMIN.MENU.CONTENT.MONITOR_KEYWORDS = Monitorer les mots-clefs
REALM.ADMIN.MENU.CONTENT.EXPORT_DATA      = Exporter les données
REALM.ADMIN.MENU.TITLE.ANALYTICS          = Analytiques
REALM.ADMIN.MENU.ANALYTICS.DASHBOARD      = Vue d’enssemble


# Realm Network Administration
REALM.ADMIN.BUTTON                                         = Administration
REALM.ADMIN.NETWORK.FORM.ERROR.NAME.EXIST                  = Ce nom est déjà utilisé
REALM.ADMIN.NETWORK.FORM.ERROR.SLUG.EXIST                  = Cette URL est déjà utilisée
REALM.ADMIN.NETWORK.FORM.ERROR.SLUG.INVALID                = Cette URL contient des caractères invalides
REALM.ADMIN.NETWORK.FLASH.SUCCESS                          = Les informations ont bien été sauvegardées
REALM.ADMIN.NETWORK.SLUG.DESCRIPTION                       = Seuls les caractères alphanumériques et les symboles - et _ sont autorisés pour l’URL
REALM.ADMIN.NETWORK.CONFIGURATION                          = Configuration
REALM.ADMIN.NETWORK.CONFIGURATION.BASIC                    = Basique
REALM.ADMIN.NETWORK.FORM.LABEL.NAME                        = Nom du Réseau
REALM.ADMIN.NETWORK.FORM.PLACEHOLDER.NAME                  = Le nom de votre réseau
REALM.ADMIN.NETWORK.FORM.LABEL.SLUG                        = URL
REALM.ADMIN.NETWORK.FORM.PLACEHOLDER.SLUG                  = Comment accèder à votre réseau
REALM.ADMIN.NETWORK.CONFIGURATION.ABOUT                    = À propos de votre réseau
REALM.ADMIN.NETWORK.FORM.LABEL.LOGO                        = Logo
REALM.ADMIN.NETWORK.FORM.BUTTON.LOGO                       = Choisir un logo
REALM.ADMIN.NETWORK.LOGO.DESCRIPTION                       = Votre logo doit être un carré de 512px X 512px
REALM.ADMIN.NETWORK.FORM.LABEL.BANNER                      = Bannière
REALM.ADMIN.NETWORK.FORM.BUTTON.BANNER                     = Choisir une bannière
REALM.ADMIN.NETWORK.BANNER.DESCRIPTION                     = Votre bannière  doit être un rectangle de 1200px X 250px
REALM.ADMIN.NETWORK.CONFIGURATION.EMAIL_DOMAINS_WHITHELIST = Domaines en liste blanche
REALM.ADMIN.NETWORK.EMAIL_DOMAINS_WHITHELIS.INVALID        = Domaine de l’adresse électronique invalide
REALM.ADMIN.NETWORK.EMAIL_DOMAINS_WHITHELIST.DESCRIPTION   = Vous pouvez ici alimenter les domaines que vous voulez authoriser.
REALM.ADMIN.NETWORK.EMAIL_DOMAINS_WHITHELIST.SAMPLE        = Exemple: (@epitech.com) pour tous les élèves d’Epitech (premom.nom@epitech.com)
REALM.ADMIN.NETWORK.EMAIL_DOMAINS_WHITHELIST.BUTTON.ADD    = Ajouter un domaine
REALM.ADMIN.NETWORK.EMAIL_DOMAINS_WHITHELIST.INFO_DELETE   = Les domaines cochés (en rouge) seront supprimés en cliquant sur le bouton "Sauvegarder"


# Realm Role Administration
REALM.ADMIN.ROLES.TITLE                    = Gérer les rôles
REALM.ADMIN.ROLES-ON-REALM.TITLE           = Gérer les rôles sur {0}
REALM.ADMIN.ROLE.TITLE                     = Gérer le rôle {0}
REALM.ADMIN.USERS-ON-ROLE.TITLE            = Gérer les utilisateurs pour le rôle {0}
REALM.ADMIN.ROLE.FORM.PLACEHOLDER.USER     = Trouver un utilisateur par son prénom, nom ou email
REALM.ADMIN.PERMISSIONS-ON-ROLE.TITLE      = Gérer les permissions sur {0}
REALM.ADMIN.TAB.ROLE.PERMISSSIONS          = Permissions
REALM.ADMIN.TAB.ROLE.USERS                 = Utilisateurs
REALM.ADMIN.ROLE.USER-ROLE-EXIST           = Utilisateurs {0} possède déjà le rôle {1}
REALM.ADMIN.ROLE.PERMISSION.SCOPE.TITLE    = Permission
REALM.ADMIN.ROLE.PERMISSION.SCOPE.ADD      = Ajouter ?
REALM.ADMIN.PERMISSION.READONY.INFO        = Le rôle "{0}" est en lecture seul, vous ne pouvez modifier ses permissions.
REALM.ADMIN.ROLE.USER.ADD.FLASH.SUCCESS    = Ajout du rôle {0} sur l’utilisateur {1}
REALM.ADMIN.ROLE.USER.DELETE.FLASH.SUCCESS = Suppression du rôle {0} sur l’utilisateur {1}
REALM.ADMIN.ROLE.CREATE.BUTTON             = Créer un rôle
REALM.ADMIN.ROLE.CREATE.TITLE              = Créer un rôle
REALM.ADMIN.ROLE.FORM.LABEL.NAME           = Nom
REALM.ADMIN.ROLE.PLACEHOLDER.NAME          = Nom de votre rôle
REALM.ADMIN.ROLE.PERMISSIONS.FLASH.SUCCESS = Les permissions concernant le rôle {0} ont bien était sauvegardé
REALM.ADMIN.ROLE.ADD.FLASH.SUCCESS         = Le rôle est sauvegardé.
REALM.ADMIN.ROLE.FORM.ERROR.NAME.EXIST     = Ce nom de rôle existe déjà.
REALM.ADMIN.ROLE.INFO_DELETE               = Les rôles cochés (en rouge) seront supprimé á la sauvegarde.
REALM.ADMIN.ROLE.BUTTON.ADD                = Ajouter une ligne
REALM.ADMIN.ROLE.DELETE.CONFIRM            = Voouslez-vous vraiment supprimer les {0} rôle(s) ? Tous les utilisateurs associés perdront leurs privilèges.


# Realm invite users
REALM.ADMIN.INVITE.USERS.TITLE                          = Inviter utilisateur
REALM.ADMIN.INVITE.USER.EMAIL.PLACEHOLDER               = L''email de l''utilisateur à inviter
REALM.ADMIN.INVITE.USER.INVITE.BUTTON                   = Inviter utilisateur
REALM.ADMIN.INVITE.USERS.CSV.LABEL                      = Inviter des utilisateur par CSV
REALM.ADMIN.INVITE.USERS.SELECT.FILE                    = Importer un CSV pour inviter des utilisateurs
REALM.ADMIN.INVITE.USERS.CSV.SELECTED.FILE              = Fichier sélectionné
REALM.ADMIN.INVITE.USERS.CSV.BUTTON                     = Inviter utilisateurs
REALM.ADMIN.INVITE.WELCOME.MAIL.SUBJECT                 = Bienvenue dans ''{0}''
REALM.ADMIN.INVITE.KNW.WELCOME.MAIL.TITLE               = Bienvenue {0} dans le domaine ''{1}''
REALM.ADMIN.INVITE.KNW.WELCOME.MAIL.CONTENT             = Vous avez été ajouté à ''{0}'', vous pouvez déjà accéder à ce domaine depuis votre compte.
REALM.ADMIN.INVITE.KNW.WELCOME.MAIL.BUTTON              = Lien direct à ''{0}''
REALM.ADMIN.INVITE.UKNW.WELCOME.MAIL.TITLE              = Bienvenue dans le domaine ''{0}''
REALM.ADMIN.INVITE.UKNW.WELCOME.MAIL.CONTENT            = Vous avez été ajouté à ''{0}'', malheureusement vous n''avez pas encore de compte.
REALM.ADMIN.INVITE.UKNW.WELCOME.MAIL.NEW.ACCOUNT        = Créer un compte
REALM.ADMIN.INVITE.UKNW.WELCOME.MAIL.TOKEN.CONTENT      = Votre email à été pré-autorisé pour ce domaine, vous aurez besoin d''utiliser ce code.
REALM.ADMIN.INVITE.USER.FLASH.ADDED                     = L’utilisateur à été ajouter à <b>{0}</b>
REALM.ADMIN.INVITE.USER.FLASH.ALREADY.ENROLLED          = L’utilisateur fait déjà partie de <b>{0}</b>
REALM.ADMIN.INVITE.USER.FLASH.UNKNW.INVITED             = Utilisateur non inscrit, une invitation à été envoyée
REALM.ADMIN.INVITE.USER.FLASH.ALREADY.INVITED           = Utilisateur non inscrit, l’invitation envoyée est encore valide
REALM.ADMIN.INVITE.USERS.FLASH.COUNT.ENROLLED           = Utilisateur ajouté : {0}
REALM.ADMIN.INVITE.USERS.FLASH.COUNT.ALREADY.ENROLLED   = Utilisateur déjà ajouté : {0}
REALM.ADMIN.INVITE.USERS.FLASH.COUNT.INVITED            = Utilisateur invité : {0}
REALM.ADMIN.INVITE.USERS.FLASH.COUNT.ALREADY.INVITED    = Utilisateur déjà invité : {0}


# Realm revoke users
REALM.ADMIN.REVOKE.USERS.TITLE                              = Supprimer utilisateur
REALM.ADMIN.REVOKE.USER.EMAIL.PLACEHOLDER                   = L''email de l''utilisateur à supprimer
REALM.ADMIN.REVOKE.USER.REVOKE.BUTTON                       = Supprimer utilisateur
REALM.ADMIN.REVOKE.USERS.CSV.LABEL                          = Supprimer des utilisateur par CSV
REALM.ADMIN.REVOKE.USERS.SELECT.FILE                        = Importer un CSV pour supprimer des utilisateurs
REALM.ADMIN.REVOKE.USERS.SELECTED.FILE                      = Fichier sélectionné
REALM.ADMIN.REVOKE.USERS.CSV.BUTTON                         = Supprimer utilisateurs
REALM.ADMIN.REVOKE.USER.FLASH.REVOKED                       = L’utilisateur à été supprimé de <b>{0}</b>
REALM.ADMIN.REVOKE.USER.FLASH.NOT.ENROLLED                  = L’utilisateur ne fait pas partie de <b>{0}</b>
REALM.ADMIN.REVOKE.USER.FLASH.UNKNOWN.USER                  = Utilisateur non inscrit
REALM.ADMIN.REVOKE.USER.FLASH.REVOKED.INVITE                = L’invitation de l’utilisateur à été supprimée
REALM.ADMIN.REVOKE.USERS.FLASH.COUNT.REVOKED                = Utilisateur supprimé : {0}
REALM.ADMIN.REVOKE.USERS.FLASH.COUNT.REVOKED.INVITE         = Invitation supprimé : {0}
REALM.ADMIN.REVOKE.USERS.FLASH.COUNT.REVOKE.ERROR           = Erreur de suppression : {0}
REALM.ADMIN.REVOKE.USERS.FLASH.COUNT.REVOKE.NOT.INVOLVED    = Utilisateur non concerné : {0}


# Realm manage realm group
REALM.ADMIN.REALM.GROUP.FORM.SLUG.ERROR     = Cette URL de groupe contient des caractères invalides
REALM.ADMIN.REALM.GROUP.FORM.SLUG.EXIST     = Cette URL de groupes est déjà utilisée
REALM.ADMIN.REALM.GROUP.TITLE               = Gestion de groupe
REALM.ADMIN.REALM.GROUP.HEADER.LIST         = Mes groupes
REALM.ADMIN.REALM.GROUP.CREATE.NEW.GROUP    = Créer un groupe
REALM.ADMIN.REALM.GROUP.BASE.INFO.HEADER    = Information basique
REALM.ADMIN.REALM.GROUP.NAME.LABEL          = Nom du groupe
REALM.ADMIN.REALM.GROUP.SLUG.LABEL          = Url du groupe
REALM.ADMIN.REALM.GROUP.ABOUT.GROUP.HEADER  = Á propos du groupe
REALM.ADMIN.REALM.GROUP.LOGO.LABEL          = Logo
REALM.ADMIN.REALM.GROUP.BANNER.LABEL        = Bannière
REALM.ADMIN.REALM.GROUP.BTN.SELECT.IMG      = Choisir une image
REALM.ADMIN.REALM.GROUP.WARNING.LOGO        = Votre logo doit être un carré de 512px X 512px
REALM.ADMIN.REALM.GROUP.WARNING.BANNER      = Votre bannière  doit être un rectangle de 1200px X 250px
REALM.ADMIN.REALM.GROUP.MEMBERS.HEADER      = Membre du groupe
REALM.ADMIN.REALM.GROUP.ADMINS.HEADER       = Group administrator management
REALM.ADMIN.REALM.GROUP.ADD.MEMBER          = Ajouter un membre
REALM.ADMIN.REALM.GROUP.ADD.ADMIN           = Add an administrator
REALM.ADMIN.REALM.GROUP.SEARCH.USER         = Trouver un utilisateur par son prénom, nom ou email
REALM.ADMIN.REALM.GROUP.MEMBER.ALREADY.ADD  = ''{0}'' est déjà présent
REALM.ADMIN.REALM.GROUP.FLASH.SUCCESS       = Ce groupe à bien été créer
REALM.ADMIN.REALM.GROUP.ADMIN.NAME          = Administrateur


# Realm manage realm page
REALM.ADMIN.PAGE.FORM.SLUG.ERROR    = Cette URL de page contient des caractères invalides
REALM.ADMIN.PAGE.FORM.SLUG.EXIST    = Cette URL de page est déjà utilisée
REALM.ADMIN.PAGE.TITLE              = Gestion de page
REALM.ADMIN.PAGE.HEADER.LIST        = Mes pages
REALM.ADMIN.PAGE.CREATE.NEW.PAGE    = Créer une page
REALM.ADMIN.PAGE.BASE.INFO.HEADER   = Information basique
REALM.ADMIN.PAGE.NAME.LABEL         = Nom de la page
REALM.ADMIN.PAGE.SLUG.LABEL         = Url de la page
REALM.ADMIN.PAGE.ABOUT.PAGE.HEADER  = Á propos de la page
REALM.ADMIN.PAGE.LOGO.LABEL         = Logo
REALM.ADMIN.PAGE.BANNER.LABEL       = Bannière
REALM.ADMIN.PAGE.BTN.SELECT.IMG     = Choisir une image
REALM.ADMIN.PAGE.WARNING.LOGO       = Votre logo doit être un carré de 512px X 512px
REALM.ADMIN.PAGE.WARNING.BANNER     = Votre bannière  doit être un rectangle de 1200px X 250px
REALM.ADMIN.PAGE.FLASH.SUCCESS      = Cette page à bien été créer
REALM.ADMIN.PAGE.ROLE.ADMIN.NAME    = Administrator
REALM.ADMIN.PAGE.ROLE.VENDOR.NAME   = Vendor

# Realm Group posts page
REALM.GROUP.POSTS.FORM.SUCCESS              = Post have been published
REALM.GROUP.POSTS.FORM.PICTURE.DESCRIPTION  = Picture must be a square of 512x512px
REALM.GROUP.POSTS.FORM.PICTURE.DELETE       = Remove picture
REALM.GROUP.POSTS.FORM.EMPTY.POST.ERROR     = Post should contain text or picture
REALM.GROUP.POSTS.BOARD.ELAPSED.DAYS        = {0} days ago
REALM.GROUP.POSTS.BOARD.ELAPSED.DAY         = one day ago
REALM.GROUP.POSTS.BOARD.ELAPSED.HOURS       = {0} hours ago
REALM.GROUP.POSTS.BOARD.ELAPSED.HOUR        = one hour ago
REALM.GROUP.POSTS.BOARD.ELAPSED.MINUTES     = {0} minutes ago
REALM.GROUP.POSTS.BOARD.ELAPSED.MINUTE      = one minute ago
REALM.GROUP.POSTS.BOARD.ELAPSED.LESSMINUTE  = less than minute
REALM.GROUP.POSTS.BOARD.DELETE.POST         = Delete post
REALM.GROUP.POSTS.BOARD.DELETE.POST.CONFIRM = Do you really want to delete post?
REALM.GROUP.POSTS.BOARD.DELETE.POST.SUCCESS = Post deleted
REALM.GROUP.POSTS.BOARD.DELETE.POST.ERROR   = Could not delete post, see log for details...

# Permissions
PERMS.analytics.dashboard = Accéder aux données analytics
PERMS.content.export      = Exporter les données
PERMS.content.monitor     = Monitorer les mots-clefs
PERMS.social.group        = Gestion des groupes
PERMS.social.page         = Gestion des pages
PERMS.network.setting     = Modifier les informations de bases du réseau
PERMS.network.role        = Gérer les rôles et assigner des rôles à des membres du réseau
PERMS.user.invite         = Inviter des membres à rejoindre le réseau
PERMS.user.revoke         = Retirer des membres du réseau
PERMS.user.activity       = Surveiller l''activité des membres du réseau
PERMS.user.block          = Bannir des membres
PERMS.user.bulk           = Exécuter des opération de masse

#Page permissions
PERMS.page.grant.roles              = Grant roles
PERMS.page.request.bank.transfert   = Request IBAN bank transfert


# Account settings
ACCOUNT_SETTINGS.TITLE                             = Paramètres de mon compte
ACCOUNT_SETTINGS.MENU.PROFILE                      = Profil
ACCOUNT_SETTINGS.MENU.PREFERENCES                  = Préférences
ACCOUNT_SETTINGS.MENU.ADDRESSES                    = Adresses
ACCOUNT_SETTINGS.MENU.CONTACT                      = Contact
ACCOUNT_SETTINGS.MENU.NOTIFICATION                 = Notification
ACCOUNT_SETTINGS.MENU.BILLING                      = Paiement

ACCOUNT_SETTINGS.BUTTON.ADD                        = Ajouter

ACCOUNT_SETTINGS.TOOLTIP.PRIMARY                   = Favori
ACCOUNT_SETTINGS.TOOLTIP.SET.PRIMARY               = Marquer comme favori
ACCOUNT_SETTINGS.TOOLTIP.DELETE                    = Supprimer
ACCOUNT_SETTINGS.TOOLTIP.NOT.VALIDATED             = Non confirmée

ACCOUNT_SETTINGS.PROFILE.TITLE                     = Mon profil
ACCOUNT_SETTINGS.PROFILE.FORM.BASE_INFO            = Information générale
ACCOUNT_SETTINGS.PROFILE.FORM.BASE_INFO.LAST_NAME  = Nom
ACCOUNT_SETTINGS.PROFILE.FORM.BASE_INFO.FIRST_NAME = Prénom
ACCOUNT_SETTINGS.PROFILE.FORM.FB.SYNC.BUTTON       = sync FB account
ACCOUNT_SETTINGS.PROFILE.FORM.FB.SYNC.WAIT.MESSAGE = Synchronizing with Facebook account...
ACCOUNT_SETTINGS.PROFILE.FORM.FB.SYNC.FAIL.MESSAGE = Facebook synchronization failed! Try later, please...

ACCOUNT_SETTINGS.PREFERENCES.TITLE                   = Mes préférences
ACCOUNT_SETTINGS.PREFERENCES.FORM.REGION_INFO        = Option régionale
ACCOUNT_SETTINGS.PREFERENCES.FORM.BASE_INFO.TIMEZONE = Timezone
ACCOUNT_SETTINGS.PREFERENCES.FORM.BASE_INFO.LANGUAGE = Langue

ACCOUNT_SETTINGS.ADDRESSES.TITLE                   = Mes adresses
ACCOUNT_SETTINGS.ADDRESSES.SAVED_ADDRESSES         = Mes adresses enregistrées
ACCOUNT_SETTINGS.ADDRESSES.ADD_ADDRESS             = Ajouter une adresses
ACCOUNT_SETTINGS.ADDRESSES.ADD.DEST_NAME           = Nom du destinataire
ACCOUNT_SETTINGS.ADDRESSES.ADD.STREET1             = Adresse 1
ACCOUNT_SETTINGS.ADDRESSES.ADD.STREET2             = Adresse 2
ACCOUNT_SETTINGS.ADDRESSES.ADD.ZIPCODE             = Code postal
ACCOUNT_SETTINGS.ADDRESSES.ADD.CITY                = Ville
ACCOUNT_SETTINGS.ADDRESSES.ADD.COUNTRY             = Pays

ACCOUNT_SETTINGS.CONTACT.TITLE                     = Mes coordonnées de contact
ACCOUNT_SETTINGS.CONTACT.EMAIL.TITLE               = Mes emails
ACCOUNT_SETTINGS.CONTACT.PHONE.TITLE               = Mes numéros de téléphone
ACCOUNT_SETTINGS.CONTACT.LABEL.EMAIL               = Ajouter un email
ACCOUNT_SETTINGS.CONTACT.PLACEHOLDER.EMAIL         = Votre email
ACCOUNT_SETTINGS.CONTACT.LABEL.PHONE               = Ajouter un numéro
ACCOUNT_SETTINGS.CONTACT.FLASH.MARK.FAVORITE.KO    = Non confirmée, impossible de marquer comme favori
ACCOUNT_SETTINGS.CONTACT.FLASH.VALIDATE.OK         = Confirmation réussie
ACCOUNT_SETTINGS.CONTACT.FLASH.VALIDATE.KO         = Confirmation échouée
ACCOUNT_SETTINGS.CONTACT.CONFIRM.MAIL.SUBJECT      = Confirmation de votre email
ACCOUNT_SETTINGS.CONTACT.CONFIRM.MAIL.TITLE        = Confirmation de votre email
ACCOUNT_SETTINGS.CONTACT.CONFIRM.MAIL.CONTENT      = Vous avez ajouter une nouvelle adresse email a votre compte. Pour garantir la securité de votre compte, nous vous avons demandé un code de confirmation, vous-pouvez le trouver ci-dessous.
ACCOUNT_SETTINGS.CONTACT.CONFIRM.MAIL.TOKEN        = Votre code de confirmation :
ACCOUNT_SETTINGS.CONTACT.CONFIRM.SMS.MESSAGE       = Code de confirmation MyBee Student : {0}
ACCOUNT_SETTINGS.CONTACT.VALIDATE.MODAL.TITLE      = Confirmation contact
ACCOUNT_SETTINGS.CONTACT.VALIDATE.MODAL.CONTENT    = Nous vous avons envoyé un code de confirmation à <span class="uk-text-bold">''{0}''</span>, pour garantir la sécurité de votre complte.
ACCOUNT_SETTINGS.CONTACT.VALIDATE.INPUT.LABEL      = Code de confirmation
ACCOUNT_SETTINGS.CONTACT.VALIDATE.INPUT.PLACEHOLDER= Votre code de confirmation


ACCOUNT_SETTINGS.NOTIFICATION.TITLE                = Mes paramètres de notification

ACCOUNT_SETTINGS.BILLING.TITLE                     = Mes paramètres de facturation


# Policies
POLICIES.HEADER.TITLE       = Conditions et règlements de<br> MyBee Student
POLICIES.LEFTMENU.TERMS     = Déclaration des droits et responsabilités
POLICIES.LEFTMENU.DATA      = Politique d’utilisation des données
POLICIES.LEFTMENU.COMMUNITY = Standards de la communauté


# TopBar
TOPBAR.PLACEHOLDER.SEARCH = Chercher des personnes, des pages ou d’autres choses
TOPBAR.LINK.SETTINGS      = Paramètres
TOPBAR.LINK.MY_WALLET     = Mon portefeuille
TOPBAR.LINK.MY_CARD       = Ma carte
TOPBAR.LINK.DISCONNECT    = Se déconnecter
TOPBAR.LINK.LOGIN         = Connexion


# SideBar
SIDEBAR.USER.FAVOURITES            = Favoris
SIDEBAR.USER.FAVOURITES.MY_PROFILE = Mon profil
SIDEBAR.USER.FAVOURITES.NEWS_FEED  = Fil d’actualité
SIDEBAR.USER.GROUPS                = Groupes
SIDEBAR.USER.PAGES                 = Pages


# LeftBar
LEFTBAR.USER.PEOPLE_YOU_MAY_KNOW = Connaissez-vous...
LEFTBAR.USER.RECENTLY_ADDED      = Nouveautés
LEFTBAR.USER.RECOMMENDATIONS     = Recommandations
LEFTBAR.BUTTON.ADD_FRIEND        = Ajouter
LEFTBAR.REALM.NO.BEST.ARTICLES   = No selling articles info available
LEFTBAR.REALM.NO.LAST.ARTICLES   = No last articles info available


# Wall
WALL.PLACEHOLDER.WHATS_ON_YOUR_MIND = Exprimez-vous
WALL.PLACEHOLDER.ADD_COMMENT        = Want to add something?
WALL.BUTTON.PUBLISH                 = Publier
WALL.TOOLTIP.ADD_LINK               = Ajouter un lien
WALL.TOOLTIP.ADD_PICTURE            = Ajouter une image
WALL.TOOLTIP.ADD_VIDEO              = Ajouter une vidéo
WALL.TOOLTIP.UP_VOTE                = Évaluer positivement
WALL.TOOLTIP.DOWN_VOTE              = Évaluer négativement
WALL.TOOLTIP.COMMENTS               = Voir et rédiger des commentaires


# Footer
FOOTER.LINK.PRIVACY = Confidentialité
FOOTER.LINK.TERMS   = Conditions d’utilisation
FOOTER.LINK.COOKIES = Cookies
FOOTER.LINK.STATUS  = Statut


# Uncategorized translation
BASEPAGE.LINK.READ_MORE = En savoir plus


SEARCH.RESULT.TYPE.all = Tous
SEARCH.RESULT.TYPE.article = Articles
SEARCH.RESULT.TYPE.page = Vendor pages
SEARCH.RESULT.TYPE.group = Groups
SEARCH.RESULT.TYPE.user = Utilisateurs

SEARCH.RESULT.NO.RESULTS = No results found

ARTICLE.MANAGE.PAGE.CREATE.TITLE            = New Article
ARTICLE.MANAGE.PAGE.EDIT.TITLE              = Edit Article
ARTICLE.MANAGE.PAGE.ABOUT.TITLE             = About article
ARTICLE.MANAGE.PAGE.NAME.TITLE              = Article name
ARTICLE.MANAGE.PAGE.DESCRIPTION.TITLE       = Description
ARTICLE.MANAGE.PAGE.SALE.ON.TITLE          = Sales on
ARTICLE.MANAGE.PAGE.SALE.ON.WEB.TITLE       = Platform
ARTICLE.MANAGE.PAGE.SALE.ON.MOBILE.TITLE    = Mobile
ARTICLE.MANAGE.PAGE.SALE.ON.TICKETING.TITLE = Ticketing
ARTICLE.MANAGE.PAGE.STATUS.TITLE            = Status
ARTICLE.MANAGE.PAGE.STATUS.SALES_OPEN       = Sales open
ARTICLE.MANAGE.PAGE.STATUS.SALES_CLOSED     = Sales closed
ARTICLE.MANAGE.PAGE.STATUS.DISABLED         = Disabled
ARTICLE.MANAGE.PAGE.PICTURE.TITLE           = Picture
ARTICLE.MANAGE.PAGE.OPTIONS.TITLE           = Questions
ARTICLE.MANAGE.PAGE.OPTIONS.NAME.TITLE      = Fieldname
ARTICLE.MANAGE.PAGE.OPTIONS.TYPE.TITLE      = Fieldtype
ARTICLE.MANAGE.PAGE.OPTIONS.ARGUMENT.TITLE  = Argument (optional)
ARTICLE.MANAGE.PAGE.OPTIONS.MANDATORY.TITLE = Mandatory?
ARTICLE.MANAGE.PAGE.OPTIONS.DELETE.TITLE    = Delete?
ARTICLE.MANAGE.PAGE.OPTION.TYPE.CHECKBOX    = Checkbox
ARTICLE.MANAGE.PAGE.OPTION.TYPE.SELECT      = Selectbox
ARTICLE.MANAGE.PAGE.OPTION.TYPE.MULTISELECT = Multiple choices
ARTICLE.MANAGE.PAGE.OPTION.TYPE.TEXT        = Text
ARTICLE.MANAGE.PAGE.OPTION.ADD              = Add question
ARTICLE.MANAGE.PAGE.PRICING.TITLE           = Pricing
ARTICLE.MANAGE.PAGE.PRICING.PRICE.TITLE     = Default price
ARTICLE.MANAGE.PAGE.PRICING.RULE.TITLE      = Rule
ARTICLE.MANAGE.PAGE.PRICING.ARGUMENT.TITLE  = Argument
ARTICLE.MANAGE.PAGE.PRICING.AMOUNT.TITLE    = Amount
ARTICLE.MANAGE.PAGE.PRICING.DELETE.TITLE    = Delete?
ARTICLE.MANAGE.PAGE.RULE.TYPE.OWN_AN_ARTICLE    = Own an article
ARTICLE.MANAGE.PAGE.RULE.TYPE.MEMBER_OF_GROUP   = Member of group
ARTICLE.MANAGE.PAGE.RULE.ARTICLE.ERROR      = You should specify article
ARTICLE.MANAGE.PAGE.RULE.GROUP.ERROR        = You should specify group
ARTICLE.MANAGE.PAGE.LIMITATION.TITLE        =  Limitation
ARTICLE.MANAGE.PAGE.LIMITATION.QUANTITY     =  Quantity in stock
ARTICLE.MANAGE.PAGE.LIMITATION.PURCHASE     =  Purchase limit per user
ARTICLE.MANAGE.PAGE.CREATE.SUCCESS          = Article created
ARTICLE.MANAGE.PAGE.UPDATE.SUCCESS          = Article saved

ARTICLE.DETAIL.PAGE.DESCRIPTION     = Description
ARTICLE.DETAIL.PAGE.ABOUT.VENDOR    = About vendor
ARTICLE.DETAIL.PAGE.OPTIONS.TITLE   = Additional customer information
ARTICLE.DETAIL.PAGE.FRIENDS.ALL     = and {0} more friends
ARTICLE.DETAIL.PAGE.FRIENDS.BOUGHT  = already bought this article
ARTICLE.DETAIL.PAGE.BUY.BUTTON      = Buy

VENDOR.PAGE.HEADER.ADMIN.BUTTON     = Page Administration
VENDOR.PAGE.ADMIN.DASHBOARD.BUTTON  = Dashboard
VENDOR.PAGE.ADMIN.ARTICLES.BUTTON   = Articles
VENDOR.PAGE.ADMIN.HISTORY.BUTTON    = History
VENDOR.PAGE.ADMIN.VENDOR.BUTTON     = Vendor information
VENDOR.PAGE.ADMIN.ARTICLES.TITLE    = Articles
VENDOR.PAGE.ADMIN.ARTICLES.TURNOVER = Quantity sold: {0}
VENDOR.PAGE.ADMIN.ARTICLES.EDIT     = Edit article
VENDOR.PAGE.ADMIN.ARTICLES.DETAILS  = Details
VENDOR.PAGE.ADMIN.ARTICLES.ON.SALE  = On sale
VENDOR.PAGE.ADMIN.ARTICLES.CLOSED   = Sales closed
VENDOR.PAGE.ADMIN.ARTICLES.DISABLED = Disabled
VENDOR.PAGE.ADMIN.ARTICLES.NEW      = Add an article

VENDOR.PAGE.ADMIN.INFO.TITLE        = Vendor information
VENDOR.PAGE.ADMIN.INFO.NAME         = Vendor name
VENDOR.PAGE.ADMIN.INFO.DESCRIPTION  = Description
VENDOR.PAGE.ADMIN.INFO.LOGO         = Image
VENDOR.PAGE.ADMIN.INFO.CHOOSE.LOGO  = Choose image
VENDOR.PAGE.ADMIN.INFO.LOGO.INFO    = Image must be a square of 512 x 512px
VENDOR.PAGE.ADMIN.INFO.EMAIL        = Email
VENDOR.PAGE.ADMIN.INFO.PHONE        = Phone
VENDOR.PAGE.ADMIN.INFO.ADDRESS      = Address
VENDOR.PAGE.ADMIN.INFO.CITY         = City
VENDOR.PAGE.ADMIN.INFO.ZIPCODE      = Zipcode
VENDOR.PAGE.ADMIN.INFO.IBAN         = IBAN/BIC Number
VENDOR.PAGE.ADMIN.INFO.IBAN.PDF     = IBAN/BIC Document
VENDOR.PAGE.ADMIN.INFO.IBAN.DROP    = Remove IBAN/BIC Document
VENDOR.PAGE.ADMIN.INFO.IBAN.DROPPED = IBAN/BIC Document Removed
VENDOR.PAGE.ADMIN.INFO.IBAN.DROPERR = Could not remove IBAN/BIC Document
VENDOR.PAGE.ADMIN.INFO.IBAN.FILE    = Document file
VENDOR.PAGE.ADMIN.INFO.EMAIL.ERROR  = Invalid email format
VENDOR.PAGE.ADMIN.INFO.UPDATED      = Vendor information saved

VENDOR.PAGE.BLOCK.ARTICLES.HEADER = Articles
VENDOR.PAGE.BLOCK.POSTS.HEADER = News

REALM.COMMENTS.SHOW_MORE=Show more

CARD.PAGE.TITLE                     = Mes cartes
CARD.PAGE.LIST_TITLE                = Cartes
CARD.PAGE.ADD_CARD                  = Ajouter nouvelle carte
CARD.PAGE.CARD_NUMBER               = numéro de carte
CARD.PAGE.CARD_TYPE                 = Type de carte
CARD.PAGE.BUTTON.ADD                = Ajouter
CARD.PAGE.SAVED_CARDS               = Mes cartes
CARD.ERROR.FORM.CARD_NUMBER_EXISTS  = Ce numéro existe déjà
CARD.ERROR.FORM.INVALID_CARD_TYPE   = Impossible de gérer le type de carte
CARD.ERROR.FORM.INVALID_PATTERN     = Le numéro de carte ne correspond pas au type
CARD.PAGE.BUTTON.TOOLTIP.DELETE     = Supprimer la carte